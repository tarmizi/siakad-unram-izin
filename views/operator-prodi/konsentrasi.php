<?php

namespace SIAKAD\Views\Operator\PRODI;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Konsentrasi;
use SIAKAD\Controller\Bagian;
use SIAKAD\Controller\ProgramStudi;
use SIAKAD\Controller\Sessions;

Headers::get_instance()
    ->set_page_title( 'Konsentrasi' )
    ->set_page_name( 'Konsentrasi' )
    ->set_page_sub_name( 'Konsentrasi');

$tingkat1 = Routes::get_instance()->get_tingkat( 1 );
$tingkat2 = Routes::get_instance()->get_tingkat( 2 );
$tingkat3 = Routes::get_instance()->get_tingkat( 3 );
$tingkat4 = Routes::get_instance()->get_tingkat( 4 );
$tingkat5 = Routes::get_instance()->get_tingkat( 5 );
$tingkat6 = Routes::get_instance()->get_tingkat( 6 );
$is_perbaiki = Routes::get_instance()->is_tingkat( 3, 'perbaiki') && Routes::get_instance()->has_tingkat( 4 );
$is_simpan = Routes::get_instance()->is_tingkat( 3, 'simpan' );
$is_hapus = Routes::get_instance()->is_tingkat( 3, 'hapus') && Routes::get_instance()->has_tingkat( 4 );
$is_terhapus = Routes::get_instance()->is_tingkat( 3, 'hapus');
$is_perbaiki_bagian = Routes::get_instance()->is_tingkat( 5, 'perbaiki') && Routes::get_instance()->has_tingkat( 6 );
$is_simpan_bagian = Routes::get_instance()->is_tingkat( 4, 'simpan' );
$is_hapus_bagian = Routes::get_instance()->is_tingkat( 5, 'hapus') && Routes::get_instance()->has_tingkat( 6 );
$is_terhapus_bagian = Routes::get_instance()->is_tingkat( 4, 'hapus');

$is_bagian = Routes::get_instance()->is_tingkat( 3, 'bagian' );
$is_bagian_konsentrasi = Routes::get_instance()->is_tingkat( 3, 'bagian' ) && Routes::get_instance()->has_tingkat( 4 ) && ( !Routes::get_instance()->is_tingkat( 4, 'simpan' ) && !Routes::get_instance()->is_tingkat( 4, 'hapus' ));

!$is_bagian || Headers::get_instance()->set_page_sub_name( 'Bagian' );

Contents::get_instance()->get_header();
$obj_prodi = ProgramStudi::get_instance()->_get(
    Sessions::get_instance()->_retrieve()->getObjLevelAkses()->getKodeObject(), 'kode_prodi_unram'
);
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">
                Kurikulum
                <small><?php if( $is_bagian ) : ?>Bagian<?php else : ?>Daftar<?php endif; ?></small>
            </h1>
            <?php if( $is_bagian ) : ?>
                <?php if($is_simpan_bagian) :
                    $kodePS = $obj_prodi->getKode();
                    $b = Bagian::get_instance()->_get($kodePS.$_POST['nomor']);
                    $b->setId($_POST['konsentrasi'].$_POST['nomor']);
                    $b->setKodePS($kodePS);
                    $b->setIdKonsentrasi($_POST['konsentrasi']);
                    $b->setNomor($_POST['nomor']);
                    $b->setNama($_POST['nama']);
                    if(isset($_POST['new'])) :
                        Bagian::get_instance()->insert($b) or die("Query salah : " . mysql_error());
                    elseif(isset($_POST['save'])) :    
                        Bagian::get_instance()->update($b) or die("Query salah : " . mysql_error());
                    endif;
                elseif($is_terhapus_bagian) :
                    $kodePS = $obj_prodi->getKode();
                    if(isset($_POST['hapus'])) :
                        Bagian::get_instance()->delete($_POST['konsentrasi'].$_POST['nomor']) or die("Query salah : " . mysql_error());
                    endif;
                endif; ?>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-md-8">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Yang dicentang</label>
                                        <div class="col-sm-4">
                                            <select class="form-control">
                                                <option>--pilih aksi</option>
                                                <option>Hapus masal</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <button class="btn btn-primary"><i class="fa fa-legal"></i> OK</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4">
                                <form>
                                    <div class="input-group">
                                        <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                    </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <br/>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><input type="checkbox"></th>
                                <th>Konsentrasi</th>
                                <th>Nomor</th>
                                <th>ID</th>
                                <th>Nama</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                            if($is_bagian_konsentrasi){
                                $bag = Bagian::get_instance()->_gets(array('kode_PS'=>$obj_prodi->getKode(),'id_konsentrasi'=>$tingkat4,'orderby'=>'nomor','order'=>'ASC','number'=>-1));
                            } else $bag = Bagian::get_instance()->_gets(array('kode_PS'=>$obj_prodi->getKode(),'orderby'=>'id_konsentrasi','order'=>'ASC','number'=>-1));
                            foreach( $bag as $bagian ) : ?>
                                <tr>
                                    <td><input type="checkbox"></td>
                                    <td><?php echo $bagian->getIdKonsentrasi(); ?></td>
                                    <td><?php echo $bagian->getNomor(); ?></td>
                                    <td><?php echo $bagian->getId(); ?></td>
                                    <td><?php echo $bagian->getNama(); ?></td>
                                    <td>
                                        <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3 . DS; if($is_bagian_konsentrasi) echo $tingkat4; else echo $bagian->getIdKonsentrasi(); ?>/perbaiki/<?php echo $bagian->getId() ?>" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                                        <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3 . DS; if($is_bagian_konsentrasi) echo $tingkat4; else echo $bagian->getIdKonsentrasi(); ?>/hapus/<?php echo $bagian->getID() ?>" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <ul class="pagination">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">&raquo;</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <div class="panel panel-<?php if($is_perbaiki_bagian) echo 'info';elseif($is_hapus_bagian) echo 'danger';else echo 'primary'; ?>">
                            <div class="panel-heading warning">
                                <?php if($is_perbaiki_bagian || $is_hapus_bagian) : 
                                    $b = Bagian::get_instance()->_get($tingkat6);
                                    if($is_perbaiki_bagian) :?>
                                        <strong><i class="glyphicon glyphicon-pencil"></i> Edit Bagian</strong>
                                    <?php elseif($is_hapus_bagian):?>
                                        <strong><i class="glyphicon glyphicon-remove"></i> Hapus Bagian</strong>
                                    <?php endif;
                                else : ?>
                                <strong><i class="glyphicon glyphicon-plus"></i> Bagian Baru</strong>
                                <?php endif; ?>
                            </div>
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" action="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3; if($is_hapus_bagian)  echo '/hapus';else echo '/simpan'; ?>" method="post">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Konsentrasi</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="konsentrasi">
                                                <?php if($is_perbaiki_bagian || $is_hapus_bagian) :
                                                    $kon = Konsentrasi::get_instance()->_gets(array('kode_PS'=>$obj_prodi->getKode(),'orderby'=>'nomor','order'=>'ASC'));
                                                    foreach($kon as $konsentrasi) : ?>
                                                ?>
                                                <option value="<?php echo $konsentrasi->getId(); ?>" <?php if($tingkat4==$konsentrasi->getId()) echo 'selected' ?>><?php echo $konsentrasi->getNama(); ?></option>
                                                <?php endforeach; else :
                                                    $kon = Konsentrasi::get_instance()->_gets(array('kode_PS'=>$obj_prodi->getKode(),'orderby'=>'nomor','order'=>'ASC'));
                                                    foreach($kon as $konsentrasi) : ?>
                                                <option value="<?php echo $konsentrasi->getId(); ?>"><?php echo $konsentrasi->getNama(); ?></option>
                                                <?php endforeach; endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Nomor</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="nomor">
                                                <?php if($is_perbaiki_bagian || $is_hapus_bagian) :
                                                    foreach(range(1,15) as $i) : ?>
                                                ?>
                                                <option value="<?php echo $i; ?>" <?php if($b->getNomor()==$i) echo 'selected' ?>><?php echo $i; ?></option>
                                                <?php endforeach; else : 
                                                    foreach(range(1,15) as $i) : ?>
                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                <?php endforeach; endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Nama</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="nama" <?php if($is_perbaiki_bagian || $is_hapus_bagian) echo 'value="'.$b->getNama().'"'?> placeholder="Nama Konsentrasi">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <span class="col-sm-4"></span>
                                        <div class="col-sm-8">
                                            <?php if($is_perbaiki_bagian) : ?>
                                            <button type="submit" class="btn btn-primary" name="save"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                                            <?php elseif($is_hapus_bagian) : ?>
                                            <button type="submit" class="btn btn-primary" name="hapus"><i class="glyphicon glyphicon-remove"></i> Hapus</button>
                                            <?php else : ?>
                                            <button type="submit" class="btn btn-primary" name="new"><i class="glyphicon glyphicon-floppy-disk"></i> Tambah</button>
                                            <?php endif; ?>
                                            
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <?php else : ?>
                <?php if($is_simpan) :
                    $kodePS = $obj_prodi->getKode();
                    $k = Konsentrasi::get_instance()->_get($kodePS.$_POST['nomor']);
                    $k->setId($kodePS.$_POST['nomor']);
                    $k->setKodePS($kodePS);
                    $k->setNomor($_POST['nomor']);
                    $k->setNama($_POST['nama']);
                    if(isset($_POST['new'])) :
                        Konsentrasi::get_instance()->insert($k) or die("Query salah : " . mysql_error());
                    elseif(isset($_POST['save'])) :    
                        Konsentrasi::get_instance()->update($k) or die("Query salah : " . mysql_error());
                    endif;
                elseif($is_terhapus) :
                    $kodePS = '112011';
                    if(isset($_POST['hapus'])) :
                        Konsentrasi::get_instance()->delete($kodePS.$_POST['nomor']) or die("Query salah : " . mysql_error());
                    endif;
                endif; ?>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-md-8">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Yang dicentang</label>
                                        <div class="col-sm-4">
                                            <select class="form-control">
                                                <option>--pilih aksi</option>
                                                <option>Hapus masal</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <button class="btn btn-primary"><i class="fa fa-legal"></i> OK</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4">
                                <form>
                                    <div class="input-group">
                                        <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                    </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <br/>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><input type="checkbox"></th>
                                <th>Nomor</th>
                                <th>ID</th>
                                <th>Nama</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $kon = Konsentrasi::get_instance()->_gets(array('kode_PS'=>$obj_prodi->getKode(),'orderby'=>'nomor','order'=>'ASC'));
                            foreach( $kon as $konsentrasi ) : 
                                $bag = Bagian::get_instance()->_gets(array('id_konsentrasi'=>$konsentrasi->getId()));
                                ?>
                                <tr>
                                    <td><input type="checkbox"></td>
                                    <td><?php echo $konsentrasi->getNomor(); ?></td>
                                    <td><?php echo $konsentrasi->getId(); ?></td>
                                    <td><?php echo $konsentrasi->getNama(); ?></td>
                                    <td>
                                        <?php if(!empty($bag)) : ?>
                                        <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 ?>/bagian/<?php echo $konsentrasi->getID() ?>" title="Lihat Bagian"><i class="glyphicon glyphicon-search"></i></a>
                                        <?php else : ?>
                                        <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 ?>/bagian/<?php echo $konsentrasi->getID() ?>" title="Tambah Bagian"><i class="glyphicon glyphicon-plus"></i></a>
                                        <?php endif; ?>
                                        <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 ?>/perbaiki/<?php echo $konsentrasi->getId() ?>" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                                        <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 ?>/hapus/<?php echo $konsentrasi->getID() ?>" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <ul class="pagination">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">&raquo;</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <div class="panel panel-<?php if($is_perbaiki) echo 'info';elseif($is_hapus) echo 'danger';else echo 'primary'; ?>">
                            <div class="panel-heading warning">
                                <?php if($is_perbaiki || $is_hapus) : 
                                    $k = Konsentrasi::get_instance()->_get($tingkat4);
                                    if($is_perbaiki) :?>
                                        <strong><i class="glyphicon glyphicon-pencil"></i> Edit Konsentrasi</strong>
                                    <?php elseif($is_hapus):?>
                                        <strong><i class="glyphicon glyphicon-remove"></i> Hapus Konsentrasi</strong>
                                    <?php endif;
                                else : ?>
                                <strong><i class="glyphicon glyphicon-plus"></i> Konsentrasi Baru</strong>
                                <?php endif; ?>
                            </div>
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" action="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2; if($is_hapus)  echo '/hapus';else echo '/simpan'; ?>" method="post">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Nomor</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="nomor">
                                                <?php if($is_perbaiki || $is_hapus) :
                                                    foreach(range(1,15) as $i) : ?>
                                                ?>
                                                <option value="<?php echo $i; ?>" <?php if($k->getNomor()==$i) echo 'selected' ?>><?php echo $i; ?></option>
                                                <?php endforeach; else : 
                                                    foreach(range(1,15) as $i) : ?>
                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                <?php endforeach; endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Nama</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="nama" <?php if($is_perbaiki || $is_hapus) echo 'value="'.$k->getNama().'"'?> placeholder="Nama Konsentrasi">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <span class="col-sm-4"></span>
                                        <div class="col-sm-8">
                                            <?php if($is_perbaiki) : ?>
                                            <button type="submit" class="btn btn-primary" name="save"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                                            <?php elseif($is_hapus) : ?>
                                            <button type="submit" class="btn btn-primary" name="hapus"><i class="glyphicon glyphicon-remove"></i> Hapus</button>
                                            <?php else : ?>
                                            <button type="submit" class="btn btn-primary" name="new"><i class="glyphicon glyphicon-floppy-disk"></i> Tambah</button>
                                            <?php endif; ?>
                                            
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();