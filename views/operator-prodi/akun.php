<?php

namespace SIAKAD\Views\Operator\PRODI;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Helpers;
use SIAKAD\Controller\Routes;

global $status, $tingkat1, $tingkat2, $tingkat3, $is_tambah, $is_perbaiki, $is_hapus, $is_simpan;

$status = isset( $_REQUEST[ Helpers::status_param] ) ? $_REQUEST[ Helpers::status_param ] : '';

$tingkat1 = Routes::get_instance()->get_tingkat( 1 );
$tingkat2 = Routes::get_instance()->get_tingkat( 2 );
$tingkat3 = Routes::get_instance()->get_tingkat( 3 );

$is_tambah = Routes::get_instance()->has_tingkat( 3 )
    && Routes::get_instance()->is_tingkat( 4, Helpers::aksi_tambah );

$is_perbaiki = Routes::get_instance()->has_tingkat( 3 )
    && Routes::get_instance()->is_tingkat( 4, Helpers::aksi_perbaiki )
    && Routes::get_instance()->has_tingkat( 5 );

$is_hapus = Routes::get_instance()->has_tingkat( 3 )
    && Routes::get_instance()->is_tingkat( 4, Helpers::aksi_hapus )
    && Routes::get_instance()->has_tingkat( 5 );

$is_simpan = Routes::get_instance()->has_tingkat( 3 )
    && Routes::get_instance()->is_tingkat( 4, Helpers::aksi_simpan );

Headers::get_instance()
    ->set_page_title( 'Daftar Akun' )
    ->set_page_name( 'Daftar Akun' );

Contents::get_instance()->get_header();

ob_start();

if( Routes::get_instance()->is_tingkat( 3, 'mahasiswa' ) ) {
    Headers::get_instance()->set_page_sub_name( 'Mahasiswa' )->set_page_title( 'Mahasiswa &mdash; Daftar Akun' );
    Contents::get_instance()->get_template( 'akun-mahasiswa' );
} else {
    Headers::get_instance()->set_page_sub_name( 'Dosen' )->set_page_title( 'Dosen &mdash; Daftar Akun' );
    Contents::get_instance()->get_template( 'akun-dosen' );
}

$contents = ob_get_clean();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <?php echo $contents; ?>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();