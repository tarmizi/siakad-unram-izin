<?php

namespace SIAKAD\Controller;

class Footers {

    var $script = '';

    /** @var Footers $instance */
    private static $instance;

    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * contoh :
     * add_script( 'jquery.min.js' )
     *
     * keluaran :
     * <script type="text/javascript" src="http://root/asset/js/jquery.min.js" />
     *
     * @param $file
     * @param $position
     * @return $this
     */
    function add_script( $file, $position = Headers::add_position_after ) {

        /**
         * pertama lakukan pengecekan script di module terlebih dahulu
         */
        $src = is_readable( Contents::get_instance()->get_module_script_path() . DS . $file ) ?

            /**
             * jika memang ada, return URI pathnya
             */
            Contents::get_instance()->get_module_script_path( Contents::path_type_uri ) . DS . $file :

            /**
             * berarti kemungkinan ada di sistem utama siakad,
             * cek dulu keberadaan file
             */
            ( is_readable( SIAKAD_SCRIPT_ABS_PATH . DS . $file ) ?

                /**
                 * jika ada return URI pathnya, dan sebaliknya
                 * jika tidak ditemukan set kosong
                 */
                SIAKAD_SCRIPT_URI_PATH . DS . $file : '' )
        ;

        /**
         * jika tidak kosong, set head script utama
         */
        if( !empty( $src ) ) {
            switch( $position ) {
                case Headers::add_position_after : $this->script .= '<script type="text/javascript" src="' . $src . '"></script>' . PHP_EOL; break;
                case Headers::add_position_before : $this->script = '<script type="text/javascript" src="' . $src . '"></script>' . PHP_EOL . $this->script; break;
            }
        }

        return $this;

    }

    function get_script() {
        return $this->script;
    }

}