<?php

namespace SIAKAD\Views\Publik;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;

Headers::get_instance()
    ->set_page_title( 'Halaman tidak ditemukan' );

Contents::get_instance()->get_header();

?>

    <div class="jumbotron">
        <div class="container">
            <h1>Aduh :(</h1>
            <p>Halaman yang side coba akses tidak ada atau sudah dihapus.</p>
            <p><a href="<?php echo SIAKAD_URI_PATH; ?>" class="btn btn-primary"><i class="glyphicon glyphicon-home"></i> Beranda</a></p>
        </div>
    </div>

<?php Contents::get_instance()->get_footer();