<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Controller;

use SIAKAD\Model\Login as ModelLogin;

class Login extends Databases {

    static $status = array( 0 => 'Tidak Aktif', 1 => 'Aktif' );

    private $field_id;
    private $class_name;
    private $table_name;
    private $table_user_level;
    private $table_level_akses;
    private $count_query;

    /** @var ModelLogin $obj_login */
    private $obj_login = null;

    /** @var Login $instance */
    private static $instance;

    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function __construct() {
        parent::_connect();
        $this->field_id = 'username';
        $this->class_name = '\SIAKAD\Model\Login';
        $this->table_name = 'login';
        $this->table_user_level = 'user_level';
        $this->table_level_akses = 'level_akses';
        $this->count_query = 'SELECT * FROM ' . $this->table_name;
    }

    function _init( ModelLogin $login ) {
        $this->obj_login = $login;
        return $this;
    }

    /**
     * @param string $username
     * @return object|ModelLogin
     */
    function _get( $username = '' ) {
        $username = empty( $username ) ? $this->obj_login->getUsername() : $username;
        $query = 'SELECT ' . $this->table_name . '.*, ' . $this->table_level_akses . '.* FROM ' . $this->table_name;
        $query .= ' LEFT JOIN ' . $this->table_user_level . ' ON ' . $this->table_user_level . '.username = ' . $this->table_name . '.username';
        $query .= ' LEFT JOIN ' . $this->table_level_akses . ' ON ' . $this->table_level_akses . '.kode_akses = ' . $this->table_user_level . '.level_akses';
        $query .= ' WHERE ' . $this->table_name . '.username = "' . $username . '"';
        if( !is_null( $this->obj_login ) ) {
            $query .= ' AND ' . $this->table_name . '.password = "' . $this->obj_login->getPassword() . '"';
            $query .= ' AND ' . $this->table_name . '.status = "' . $this->obj_login->getStatus() . '"';
        }
        return ( $return = mysql_fetch_object(
            mysql_query( $query ), $this->class_name
        ) ) ? $return : new $this->class_name;
    }

    function _gets( $args = array() ) {

        $return = array();

        $default_args = array(
            'level_akses'               => -1,
            'username'                  => '',
            'status'                    => -1,
            'exclude'                   => array(),
            'conditions'                => '',
            'orderby'                   => $this->field_id,
            'order'                     => 'DESC',
            'number'                    => 10,
            'offset'                    => 0
        );

        $list_args = sync_default_params( $default_args, $args );
        $query = 'SELECT ' . $this->table_name . '.*, ' . $this->table_level_akses . '.* FROM ' . $this->table_name;
        $query .= ' LEFT JOIN ' . $this->table_user_level . ' ON ' . $this->table_user_level . '.username = ' . $this->table_name . '.username';
        $query .= ' LEFT JOIN ' . $this->table_level_akses . ' ON ' . $this->table_level_akses . '.kode_akses = ' . $this->table_user_level . '.level_akses';
        $query .= ' WHERE 1';

        /**
         * level akses
         */
        if( $list_args[ 'level_akses' ] >= 0 )
            $query .= ' AND level_akses LIKE "' . $list_args[ 'level_akses' ] . '%"';

        /**
         * username
         */
        if( !empty( $list_args[ 'username' ] ) ) {
            $query .= ' AND ';
            $name_arr = explode( ' ', $list_args[ 'username' ] );
            foreach( $name_arr as $key => $name )
                $query .= ( $key > 0 ? ' OR' : '' ) . $this->table_name . '.`username` LIKE "%' . $name . '%"';
        }

        /**
         * status
         */
        if( $list_args[ 'status' ] >= 0 )
            $query .= ' AND status = "' . $list_args[ 'status' ] . '"';

        /**
         * exclude
         */
        if( !empty( $list_args[ 'exclude' ] ) ) {

            foreach( $list_args[ 'exclude' ] as $ex )
                $query .= ' AND `' . $this->field_id . '` <> ' . $ex;

        }

        /**
         * untuk custom query pada conditions
         */
        if( !empty( $list_args[ 'conditions' ] ) ) {
            foreach( $list_args[ 'conditions' ] as $conditions )
                $query .= ' AND ' . $conditions[ 'field' ] . ' ' . $conditions[ 'operator' ] . ' ' . $conditions[ 'comparison' ];
        }

        $this->count_query = $query;

        /**
         * orderby dan jenis order
         */
        $query .= ' ORDER BY `' . $list_args[ 'orderby' ] . '` ' . $list_args[ 'order' ];

        /**
         * limit
         */
        if( $list_args[ 'number' ] >= 0 )
            $query .= ' LIMIT ' . $list_args[ 'offset' ] . ', ' . $list_args[ 'number' ];

        $resource = mysql_query( $query );

        //echo $query . ' : ' . mysql_error();

        while( $row = mysql_fetch_object( $resource, $this->class_name ) )
            $return[] = $row;

        return $return;

    }

    function insert( ModelLogin $a ) {
        return parent::insert(
            $this->table_name,
            array(
                'username',
                'password',
                'status'
            ),
            array(
                array(
                    $a->getUsername(),
                    $a->getPassword(),
                    $a->getStatus()
                )
            )
        );
    }

    function update( ModelLogin $a ) {
        return parent::update(
            $this->table_name,
            array(
                'username',
                'password',
                'status'
            ),
            array(
                $a->getUsername(),
                $a->getPassword(),
                $a->getStatus()
            ),
            array(
                'username', $a->getUsername()
            )
        );
    }

    function delete( $username ) {
        return parent::delete(
            $this->table_name,
            'username',
            $username
        );
    }

    function _count() {
        return mysql_num_rows(
            mysql_query( $this->count_query )
        );
    }

    function _valid_username() {
        return mysql_num_rows(
            mysql_query(
                'SELECT * FROM ' . $this->table_name . ' WHERE status = "' . $this->obj_login->getStatus() . '" AND username = "' . $this->obj_login->getUsername() . '"'
            )
        );
    }

    function _valid_password() {
        return mysql_num_rows(
            mysql_query(
                'SELECT * FROM ' . $this->table_name . ' WHERE status = "' . $this->obj_login->getStatus() . '" AND username = "' . $this->obj_login->getUsername() . '" AND password = "' . $this->obj_login->getPassword() . '"'
            )
        );
    }

} 