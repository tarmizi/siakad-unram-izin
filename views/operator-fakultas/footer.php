<?php

namespace SIAKAD\Views\Operator\Fakultas;

?>
    <hr>
    <div class="container">
        <footer>
            <p>&copy; Hak Cipta <?php echo date( 'Y' ); ?> <?php echo SIAKAD_APP_AUTHOR; ?></p>
        </footer>
    </div>
</body>
</html>