<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Model;


class Fakultas {

    private $kode;
    private $nama;
    private $nama_ingg;
    private $alias;
    private $keterangan;
    private $kode_univ;

    function _init( $request ) {
        !isset( $request[ 'kode' ] ) || $this->kode = $request[ 'kode' ];
        !isset( $request[ 'nama' ] ) || $this->nama = $request[ 'nama' ];
        !isset( $request[ 'nama_ingg' ] ) || $this->nama_ingg = $request[ 'nama_ingg' ];
        !isset( $request[ 'alias' ] ) || $this->alias = $request[ 'alias' ];
        !isset( $request[ 'keterangan' ] ) || $this->keterangan = $request[ 'keterangan' ];
        !isset( $request[ 'kode_univ' ] ) || $this->kode_univ = $request[ 'kode_univ' ];
    }

    /**
     * @param mixed $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    }

    /**
     * @return mixed
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param mixed $keterangan
     */
    public function setKeterangan($keterangan)
    {
        $this->keterangan = $keterangan;
    }

    /**
     * @return mixed
     */
    public function getKeterangan()
    {
        return $this->keterangan;
    }

    /**
     * @param mixed $kode
     */
    public function setKode($kode)
    {
        $this->kode = $kode;
    }

    /**
     * @return mixed
     */
    public function getKode()
    {
        return $this->kode;
    }

    /**
     * @param mixed $kode_univ
     */
    public function setKodeUniv($kode_univ)
    {
        $this->kode_univ = $kode_univ;
    }

    /**
     * @return mixed
     */
    public function getKodeUniv()
    {
        return $this->kode_univ;
    }

    /**
     * @param mixed $nama
     */
    public function setNama($nama)
    {
        $this->nama = $nama;
    }

    /**
     * @return mixed
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * @param mixed $nama_ingg
     */
    public function setNamaIngg($nama_ingg)
    {
        $this->nama_ingg = $nama_ingg;
    }

    /**
     * @return mixed
     */
    public function getNamaIngg()
    {
        return $this->nama_ingg;
    }

    function has_nama_ingg() {
        return '' != $this->nama_ingg;
    }

} 