<?php

namespace SIAKAD\Views\Mahasiswa;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Matakuliah;
use SIAKAD\Controller\SyaratMK;
use SIAKAD\Controller\Nilai;
use SIAKAD\Controller\Mahasiswa;
use SIAKAD\Controller\Sessions;
use SIAKAD\Controller\KrsTemp;
use SIAKAD\Controller\DosenPa;
use SIAKAD\Controller\Dosen;
use SIAKAD\Controller\Fakultas;
use SIAKAD\Controller\ProgramStudi;

Headers::get_instance()
    ->set_page_title( 'cetak-krs' )
    ->set_page_name( 'cetak-krs' );
Contents::get_instance()->get_header('cetak');
$me = Mahasiswa::get_instance()->_get(Sessions::get_instance()->_retrieve()->getObjLogin()->getUsername());
$myDosenPa = Dosen::get_instance()->_get(DosenPa::get_instance()->_get($me->getNim())->getKodeDosen());
$myProdi = ProgramStudi::get_instance()->_get($me->getKodeProdi());
$myFakultas = Fakultas::get_instance()->_get($myProdi->getKodeFakultas());
$krstemp = KrsTemp::get_instance()->_get($me->getNim());
if($krstemp->getNim()==null){?>
        <div class="alert alert-danger">
            <p><i class="glyphicon glyphicon-warning-sign"></i> Anda Belum Mengisi KRS</p>
        </div>
<?php }
else{ 
    if($krstemp->getStatus()==2){ ?>
        <div class="alert alert-danger">
            <p><i class="glyphicon glyphicon-warning-sign"></i> KRS Anda Ditolak</p>
        </div>
    <?php }
    elseif($krstemp->getStatus()==0){ ?>
    <div class="alert alert-info">
        <p><i class="glyphicon glyphicon-warning-sign"></i> KRS Anda Belum Disetujui</p>
    </div>
    <?php }
    else{ 

$nilai_all = Nilai::get_instance()->_gets(array('nim'=>$me->getNIM(),'nilai_min'=>1));
$jml_sks = 0;
$semester = jumlah_semester($me->getThnMasuk(), 20142);
if(!empty($nilai_all)){
    foreach ($nilai_all as $n){
        $mk = Matakuliah::get_instance()->_get($n->getIdMk());
        $jml_sks += $mk->getSks();
    }}
?>

<div class="container-fluid">
    <div class="row">
        <div id="header">
            <div id="logofakultas">
                <img>
            </div>
            <div id="isi" align="center">
                <b>
                KEMENTERIAN PENDIDIKAN DAN KEBUDAYAAN<br>
                UNIVERSITAS MATARAM<br>
                <?php echo strtoupper($myFakultas->getNama()); ?><br>
                </b>
                <?php echo $myProdi->getAlamatProdi(); ?> NTB 83125<br>
            </div>
        </div>
        <br><br>
        <div id="info">
            <h3><?php echo $me->getNIM(); ?></h3>
            <table style="width: 100%">
                <thead>
                <tr>
                    <th>Tahun Ajaran</th>
                    <td>2014</td>
                </tr><tr>
                    <th>Semester</th>
                    <td><?php echo $semester; ?> / Genap</td>
                </tr><tr>
                    <th>SKS yang telah diambil</th>
                    <td><?php echo $jml_sks; ?></td>
                </tr><tr>
                    <th>SKS yang boleh diambil</th>
                    <td>24</td>
                </tr>
                </thead>
            </table>
        </div>
        <br><br>
        <div id="krs">
            <?php $krstemp = KrsTemp::get_instance()->_get($me->getNim());?>
            <table class="table-krs">
                <thead>
                <tr>
                    <th>Kode</th>
                    <th>Nama</th>
                    <th>SKS</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                $daftar_matakuliah = explode('-', $krstemp->getIdMk());
                foreach ( range(0, count($daftar_matakuliah)-2) as $i ) :
                    $mk = Matakuliah::get_instance()->_get($daftar_matakuliah[$i]);
                    ?>
                <tr>
                    <td><?php echo $mk->getKodeMk(); ?></td>
                    <td><?php echo $mk->getNamaMk(); ?></td>
                    <td><?php echo $mk->getSks(); ?></td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <br><br>
        <div id="ttd" style="padding-left: 11.5cm">
            <p>Mengetahui</p>
            <p>Dosen Pembimbing,</p>
            <br><br><br>
            <p><?php echo $myDosenPa->getNama(); ?></p>
            <p><?php echo $myDosenPa->getNip(); ?></p>
        </div>
    </div>
</div>


    <?php }
}
Contents::get_instance()->get_footer('cetak');