<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Model;

class UserLevel {

    private $username;
    private $level_akses;

    private $password;
    private $status;
    private $nama_level;
    private $deskripsi;

    function _init( $request ) {
        !isset( $request[ 'username' ] ) || $this->username = $request[ 'username' ];
        !isset( $request[ 'level_akses' ] ) || $this->level_akses = $request[ 'level_akses' ];
        return $this;
    }

    /**
     * @param mixed $deskripsi
     */
    public function setDeskripsi($deskripsi)
    {
        $this->deskripsi = $deskripsi;
    }

    /**
     * @return mixed
     */
    public function getDeskripsi()
    {
        return $this->deskripsi;
    }

    /**
     * @param mixed $level_akses
     */
    public function setLevelAkses($level_akses)
    {
        $this->level_akses = $level_akses;
    }

    /**
     * @return mixed
     */
    public function getLevelAkses()
    {
        return $this->level_akses;
    }

    /**
     * @param mixed $nama_level
     */
    public function setNamaLevel($nama_level)
    {
        $this->nama_level = $nama_level;
    }

    /**
     * @return mixed
     */
    public function getNamaLevel()
    {
        return $this->nama_level;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }


} 