<?php

namespace SIAKAD\Views\Operator\Fakultas;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Sessions;
use SIAKAD\Controller\Fakultas;

Headers::get_instance()
    ->set_page_title( 'Operator Fakultas' )
    ->set_page_name( 'Operator Fakultas' );

Contents::get_instance()->get_header();

$obj_level_akses = Sessions::get_instance()->_retrieve()->getObjLevelAkses();
$obj_fakultas = Fakultas::get_instance()->_get( $obj_level_akses->getKodeObject() );

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header"><?php echo $obj_fakultas->getNama(); ?></h1>
            <form class="form-horizontal">
                <div class="form-group">
                    <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Kode</label>
                    <div class="col-xs-8 col-sm-5 col-lg-3">
                        <input type="text" class="form-control" placeholder="Kode fakultas" name="kode" value="<?php echo $obj_fakultas->getKode(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Nama</label>
                    <div class="col-xs-8 col-sm-5 col-lg-3">
                        <input type="text" class="form-control" placeholder="Nama fakultas" name="nama" value="<?php echo $obj_fakultas->getNama(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Nama (Ingg)</label>
                    <div class="col-xs-8 col-sm-5 col-lg-3">
                        <input type="text" class="form-control" placeholder="Nama fakultas dalam inggris" name="nama_ingg" value="<?php echo $obj_fakultas->getNamaIngg(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Alias</label>
                    <div class="col-xs-8 col-sm-5 col-lg-3">
                        <input type="text" class="form-control" placeholder="Alias fakultas" name="alias" value="<?php echo $obj_fakultas->getAlias(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Keterangan</label>
                    <div class="col-xs-8 col-sm-5 col-lg-3">
                        <textarea class="form-control" placeholder="Keterangan fakultas" name="keterangan"><?php echo $obj_fakultas->getKeterangan(); ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-8 col-xs-offset-4 col-sm-5 col-sm-offset-3 col-lg-3 col-lg-offset-2">
                        <input type="hidden" name="kode_univ" value="<?php echo SIAKAD_UNIVERSIY_ID; ?>">
                        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();