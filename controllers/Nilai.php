<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Controller;

use SIAKAD\Model\Nilai as ModelNilai;

class Nilai extends Databases {

    private $field_id;
    private $class_name;
    private $table_name;
    private $count_query;

    /** @var Nilai $instance */
    private static $instance;

    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function __construct() {
        parent::_connect();
        $this->field_id = 'id';
        $this->class_name = '\SIAKAD\Model\Nilai';
        $this->table_name = 'nilai';
        $this->count_query = 'SELECT * FROM ' . $this->table_name;
    }

    /**
     * @param $kode
     * @param string $by
     * @return ModelNilai
     */
    function _get( $kode, $by = '' ) {
        $query = 'SELECT ' . $this->table_name . '.* FROM ' . $this->table_name;
        $query .= ' WHERE ' . $this->table_name . '.`' . ( empty( $by ) ? $this->field_id : $by ) . '` = "' . $kode . '"';
        return ( $return = mysql_fetch_object(
            mysql_query( $query ), $this->class_name
        ) ) ? $return : new $this->class_name;
    }

    function _gets( $args = array() ) {

        $return = array();

        $default_args = array(
            'nim'                       => '',
            'id_mk'                     => '',
            'nilai_min'                 => -1,
            'tahun_akademik'            => -1,
            'exclude'                   => array(),
            'conditions'                => '',
            'orderby'                   => $this->field_id,
            'order'                     => 'DESC',
            'number'                    => 10,
            'offset'                    => 0
        );

        $list_args = sync_default_params( $default_args, $args );
        $query = 'SELECT ' . $this->table_name . '.* FROM ' . $this->table_name . ' WHERE 1';
        
        /**
         * nim
         */
        if( !empty( $list_args[ 'nim' ] ) )
            $query .= ' AND ' . $this->table_name . '.nim = \'' . $list_args[ 'nim' ].'\'';
        
        /**
         * id_mk
         */
        if( !empty( $list_args[ 'id_mk' ] ) )
            $query .= ' AND ' . $this->table_name . '.id_mk = \'' . $list_args[ 'id_mk' ].'\'';
        
        /**
         * id_mk
         */
        if($list_args['nilai_min']>=0)
            $query .= ' AND ' . $this->table_name . '.indeks_nilai >= ' . $list_args[ 'nilai_min' ];
        
        /**
         * tahun_akademik
         */
        if($list_args['tahun_akademik']>=0)
            $query .= ' AND ' . $this->table_name . '.tahun_akademik >= ' . $list_args[ 'tahun_akademik' ];
        
        /**
         * exclude
         */
        if( !empty( $list_args[ 'exclude' ] ) ) {

            foreach( $list_args[ 'exclude' ] as $ex )
                $query .= ' AND `kode` <> ' . $ex;

        }

        /**
         * untuk custom query pada conditions
         */
        if( !empty( $list_args[ 'conditions' ] ) ) {
            foreach( $list_args[ 'conditions' ] as $conditions )
                $query .= ' AND ' . $list_args . '.' . $conditions[ 'field' ] . ' ' . $conditions[ 'operator' ] . ' ' . $conditions[ 'comparison' ];
        }

        $this->count_query = $query;

        /**
         * orderby dan jenis order
         */
        $query .= ' ORDER BY `' . $list_args[ 'orderby' ] . '` ' . $list_args[ 'order' ];

        /**
         * limit
         */
        if( $list_args[ 'number' ] >= 0 )
            $query .= ' LIMIT ' . $list_args[ 'offset' ] . ', ' . $list_args[ 'number' ];

        $resource = mysql_query( $query );

        //echo $query . ' : ' . mysql_error();

        while( $row = mysql_fetch_object( $resource, $this->class_name ) )
            $return[] = $row;

        return $return;

    }

    function insert( ModelNilai $a ) {
        return parent::insert(
            $this->table_name,
            $a->column,
            array(
                array(
                    $a->getId(),
                    $a->getIdMk(),
                    $a->getTahunAkademik(),
                    $a->getNim(),
                    $a->getSemester(),
                    $a->getKeterangan(),
                    $a->getIndeksNilai()
                )
            )
        );
    }

    function update( ModelNilai $a ) {
        return parent::update(
            $this->table_name,
            $a->column,
            array(
                $a->getId(),
                $a->getIdMk(),
                $a->getTahunAkademik(),
                $a->getNim(),
                $a->getSemester(),
                $a->getKeterangan(),
                $a->getIndeksNilai()
            ),
            array(
                'id', $a->getId()
            )
        );
    }

    function delete( $kode ) {
        return parent::delete(
            $this->table_name,
            'id',
            $kode
        );
    }
} 