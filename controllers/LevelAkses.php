<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Controller;

use SIAKAD\Model\LevelAkses as ModelLevelAkses;

class LevelAkses extends Databases {

    const delimiter = '-';

    const kode_view_admin = 'A';
    const kode_view_akademik = 'AK';
    const kode_view_operator_siakad = 'OS';
    const kode_view_operator_fakultas = 'OF';
    const kode_view_operator_prodi = 'OP';
    const kode_view_dosen = 'D';
    const kode_view_mahasiswa = 'M';

    static $kode_views = array(
        self::kode_view_admin => Routes::view_admin,
        self::kode_view_akademik => Routes::view_akademik,
        self::kode_view_operator_siakad => Routes::view_operator_siakad,
        self::kode_view_operator_fakultas => Routes::view_operator_fakultas,
        self::kode_view_operator_prodi => Routes::view_operator_prodi,
        self::kode_view_dosen => Routes::view_dosen,
        self::kode_view_mahasiswa => Routes::view_mahasiswa
    );

    private $field_id;
    private $class_name;
    private $table_name;
    private $count_query;

    /** @var LevelAkses $instance */
    private static $instance;

    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function __construct() {
        parent::_connect();
        $this->field_id = 'kode_akses';
        $this->class_name = '\SIAKAD\Model\LevelAkses';
        $this->table_name = 'level_akses';
        $this->count_query = 'SELECT * FROM ' . $this->table_name;
    }

    /**
     * @param $kode
     * @param string $by
     * @return ModelLevelAkses
     */
    function _get( $kode, $by = '' ) {
        $query = 'SELECT ' . $this->table_name . '.* FROM';
        $query .= ' WHERE ' . $this->table_name . '.`' . ( empty( $by ) ? $this->field_id : $kode ) . '` = "' . $kode . '"';
        return ( $return = mysql_fetch_object(
            mysql_query( $query ), $this->class_name
        ) ) ? $return : new $this->class_name;
    }

    function _gets( $args = array() ) {

        $return = array();

        $default_args = array(
            'kode_akses'                => -1,
            'exclude'                   => array(),
            'conditions'                => '',
            'orderby'                   => $this->field_id,
            'order'                     => 'DESC',
            'number'                    => 10,
            'offset'                    => 0
        );

        $list_args = sync_default_params( $default_args, $args );
        $query = 'SELECT ' . $this->table_name . '.* FROM ' . $this->table_name . ' WHERE 1';

        /**
         * level akses
         */
        if( $list_args[ 'kode_akses' ] >= 0 )
            $query .= ' AND kode_akses LIKE "' . $list_args[ 'kode_akses' ] . '%"';

        /**
         * exclude
         */
        if( !empty( $list_args[ 'exclude' ] ) ) {

            foreach( $list_args[ 'exclude' ] as $ex )
                $query .= ' AND `kode` <> ' . $ex;

        }

        /**
         * untuk custom query pada conditions
         */
        if( !empty( $list_args[ 'conditions' ] ) ) {
            foreach( $list_args[ 'conditions' ] as $conditions )
                $query .= ' AND ' . $list_args . '.' . $conditions[ 'field' ] . ' ' . $conditions[ 'operator' ] . ' ' . $conditions[ 'comparison' ];
        }

        $this->count_query = $query;

        /**
         * orderby dan jenis order
         */
        $query .= ' ORDER BY `' . $list_args[ 'orderby' ] . '` ' . $list_args[ 'order' ];

        /**
         * limit
         */
        if( $list_args[ 'number' ] >= 0 )
            $query .= ' LIMIT ' . $list_args[ 'offset' ] . ', ' . $list_args[ 'number' ];

        $resource = mysql_query( $query );

        //echo $query . ' : ' . mysql_error();

        while( $row = mysql_fetch_object( $resource, $this->class_name ) )
            $return[] = $row;

        return $return;

    }

    function insert( ModelLevelAkses $a ) {
        return parent::insert(
            $this->table_name,
            array(
                'kode_akses',
                'nama_level',
                'deskripsi'
            ),
            array(
                array(
                    $a->getKodeAkses(),
                    $a->getNamaLevel(),
                    $a->getDeskripsi()
                )
            )
        );
    }

    function update( ModelLevelAkses $a ) {
        return parent::update(
            $this->table_name,
            array(
                'kode_akses',
                'nama_level',
                'deskripsi'
            ),
            array(
                $a->getKodeAkses(),
                $a->getNamaLevel(),
                $a->getDeskripsi()
            ),
            array(
                'kode_akses', $a->getKodeAkses()
            )
        );
    }

    function delete( $kode_akses ) {
        return parent::delete(
            $this->table_name,
            'kode_akses',
            $kode_akses
        );
    }
} 