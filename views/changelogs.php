<?php
/**
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Views\Publik;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;

$project_url = 'https://bitbucket.org/siakadunram/siakad-unram/overview';
$feed_url = 'https://bitbucket.org/siakadunram/siakad-unram/rss?token=7b92c0483672de59edeb200ce4eece22';
$commit_url = 'https://bitbucket.org/siakadunram/siakad-unram/commits/branch/versi-1.0.0';

Headers::get_instance()
    ->set_page_title( 'Aktifitas Perubahan Terkini' );

Contents::get_instance()->get_header();

?>

    <div class="container info">
        <div class="row">
            <div class="col-sm-8 main">
                <div class="item">
                    <a href="<?php echo $project_url; ?>" title="Langsung dari Bitbucket">
                        <h2 class="title">Aktifitas perubahan terkini</h2>
                    </a>
                    <div class="content">
                        <?php if( $lrs_engine_xml = simplexml_load_file( $feed_url ) ) : ?>
                            <?php foreach( $lrs_engine_xml->channel->item as $item ) : ?>
                                <small>
                                    &raquo;&nbsp;
                                    <a href="<?php echo $item->link; ?>" title="<?php echo $item->author; ?>">
                                        <?php echo $item->title; ?>
                                    </a>
                                    &nbsp;&mdash;&nbsp;
                                    <?php echo date( 'M j, Y', strtotime( $item->pubDate ) ); ?>, <i><?php echo $item->author; ?></i>
                                    <br/>
                                </small>
                            <?php endforeach; ?>
                            <br/><br/>
                            <p>Selengkapnya <a href="<?php echo $commit_url; ?>"><?php echo $commit_url; ?></a></p>
                        <?php else : ?>
                            <strong>Gagal mengambil aktifitas.</strong>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 sidebar">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong>Link terkait</strong>
                    </div>
                    <div class="panel-body">
                        <ul>
                            <li><a href="#">Universitas Mataram</a></li>
                            <li><a href="#">Fakultas Teknik</a></li>
                            <li><a href="#">Teknik Informatika</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php Contents::get_instance()->get_footer();