<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Controller;

use SIAKAD\Model\DosenPa as ModelDosenPa;

class DosenPa extends Databases {

    private $field_id;
    private $class_name;
    private $table_name;
    private $table_mahasiswa;
    private $table_dosen;
    private $count_query;

    /** @var Jenjang $instance */
    private static $instance;

    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function __construct() {
        parent::_connect();
        $this->field_id = 'nim_mahasiswa';
        $this->class_name = '\SIAKAD\Model\DosenPa';
        $this->table_name = 'dosen_pa';
        $this->table_mahasiswa = 'mahasiswa';
        $this->table_dosen = 'dosen';
        $this->count_query = 'SELECT * FROM ' . $this->table_name;
    }

    /**
     * @param $nim_mahasiswa
     * @return object|ModelDosenPa
     */
    function _get( $nim_mahasiswa ) {
        $query = 'SELECT ' . $this->table_name . '.* FROM ' . $this->table_name;
        $query .= ' WHERE ' . $this->table_name . '.`nim_mahasiswa` = "' . $nim_mahasiswa . '"';
        return ( $return = mysql_fetch_object(
            mysql_query( $query ), $this->class_name
        ) ) ? $return : new $this->class_name;
    }

    function _gets( $args = array() ) {

        $return = array();

        $default_args = array(
            'kode_dosen'                => '',
            'orderby'                   => $this->field_id,
            'order'                     => 'DESC',
            'number'                    => 10,
            'offset'                    => 0
        );

        $list_args = sync_default_params( $default_args, $args );
        $query = 'SELECT *';
        $query .= ', ' . $this->table_mahasiswa . '.nama AS nama_mahasiswa';
        $query .= ', ' . $this->table_dosen . '.nama AS nama_dosen';
        $query .= ' FROM ' . $this->table_name;
        $query .= ' LEFT JOIN ' . $this->table_mahasiswa . ' ON ' . $this->table_mahasiswa . '.NIM = ' . $this->table_name . '.nim_mahasiswa';
        $query .= ' LEFT JOIN ' . $this->table_dosen . ' ON ' . $this->table_dosen . '.kode = ' . $this->table_name . '.kode_dosen';
        $query .= ' WHERE 1';

        /**
         * kode_dosen
         */
        if( !empty( $list_args[ 'kode_dosen' ] ) ) {
            $query .= ' AND `kode_dosen` = "' . $list_args[ 'kode_dosen' ] .'"';
        }

        $this->count_query = $query;

        /**
         * orderby dan jenis order
         */
        $query .= ' ORDER BY `' . $list_args[ 'orderby' ] . '` ' . $list_args[ 'order' ];

        /**
         * limit
         */
        if( $list_args[ 'number' ] >= 0 )
            $query .= ' LIMIT ' . $list_args[ 'offset' ] . ', ' . $list_args[ 'number' ];

        $resource = mysql_query( $query );

        //echo $query . ' : ' . mysql_error();

        while( $row = mysql_fetch_object( $resource, $this->class_name ) )
            $return[] = $row;

        return $return;

    }

    function insert( ModelDosenPa $a ) {
        return parent::insert(
            $this->table_name,
            array(
                'nim_mahasiswa',
                'kode_dosen'
            ),
            array(
                array(
                    $a->getNimMahasiswa(),
                    $a->getKodeDosen()
                )
            )
        );
    }

    function delete( $nim_mahasiswa ) {
        return parent::delete( $this->table_name, $this->field_id, $nim_mahasiswa );
    }

    function _count() {
        return mysql_num_rows( mysql_query( $this->count_query ) );
    }

} 