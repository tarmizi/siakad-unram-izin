-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 26, 2014 at 08:57 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `siakad_unram`
--
CREATE DATABASE IF NOT EXISTS `siakad_unram` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `siakad_unram`;

-- --------------------------------------------------------

--
-- Table structure for table `krs_temp`
--

CREATE TABLE IF NOT EXISTS `krs_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nim` varchar(10) NOT NULL,
  `id_mk` varchar(500) NOT NULL,
  `tanggal_ajukan` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE IF NOT EXISTS `nilai` (
  `id` int(11) NOT NULL,
  `id_mk` varchar(20) DEFAULT NULL,
  `tahun_akademik` int(5) DEFAULT NULL COMMENT '4 digit pertama adalah tahun,',
  `nim` varchar(10) DEFAULT NULL,
  `semester` int(11) DEFAULT NULL COMMENT 'Semester dimana seorang mahasiswa mengambil suatu mata kuliah.',
  `keterangan` text,
  `kelas` varchar(2) DEFAULT NULL,
  `kelas_manual` varchar(2) DEFAULT NULL,
  `kode_dosen` varchar(25) DEFAULT NULL COMMENT 'Kode Dosen yang mengajar mata kuliah',
  `indeks_nilai` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_nilai_2_idx` (`id_mk`),
  KEY `fk_nilai_idx` (`indeks_nilai`),
  KEY `fk_nim_idx` (`nim`),
  KEY `fk_dosen_idx` (`kode_dosen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`id`, `id_mk`, `tahun_akademik`, `nim`, `semester`, `keterangan`, `kelas`, `kelas_manual`, `kode_dosen`, `indeks_nilai`) VALUES
(1, '11201120051MK201', 20141, 'MHS0001', 1, NULL, 'A', NULL, 'DS01', 21),
(2, '11201120051MK201', 20141, 'MHS0003', 1, NULL, 'A', NULL, 'DS01', 20),
(3, '11201120051MK027', 20141, 'MHS0003', 1, NULL, 'A', NULL, 'DS02', 10);

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE IF NOT EXISTS `pesan` (
  `id` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `jenis_pesan` smallint(2) NOT NULL,
  `tanggal_kirim` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_pengirim` varchar(25) NOT NULL,
  `id_penerima` varchar(25) NOT NULL,
  `pesan` text,
  `lampiran` text,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`id`, `jenis_pesan`, `tanggal_kirim`, `id_pengirim`, `id_penerima`, `pesan`, `lampiran`) VALUES
('2014-07-03 17:19:37', 1, '2014-07-03 17:19:37', 'DS01', 'MHS0001', 'Mata Kuliah xxx diubah saja menjadi matakuliah yyy', NULL),
('2014-07-03 17:20:34', 1, '2014-07-03 17:20:34', 'MHS0001', 'DS01', 'oke pak, terima kasih', NULL),
('2014-07-03 17:24:01', 1, '2014-07-03 17:24:01', 'MHS0002', 'DS01', 'maaf pak, kapan krs saya disetujui?', NULL),
('2014-07-03 18:08:26', 1, '2014-07-03 18:08:26', 'MHS0001', 'DS01', 'sudah saya ubah pak, apakah sudah bisa diterima?', NULL),
('2014-07-06 21:30:04', 1, '2014-07-06 21:30:04', 'MHS0001', 'DS01', 'Pak KRS sya diterima dong', ''),
('2014-07-06 22:25:44', 1, '2014-07-06 22:25:44', 'DS01', 'MHS0001', 'saya sibuk', ''),
('2014-07-06 22:26:18', 1, '2014-07-06 22:26:18', 'MHS0001', 'DS01', 'kok bapak gitu sih?', ''),
('2014-07-06 22:26:27', 1, '2014-07-06 22:26:27', 'DS01', 'MHS0001', 'masbulloh', ''),
('2014-07-11 15:10:58', 1, '2014-07-11 15:10:58', 'DS01', 'MHS0002', 'ketemu saya dulu', ''),
('2014-07-15 21:46:54', 1, '2014-07-15 21:46:54', 'MHS0001', 'DS01', 'Pak, kapan jadinya nih?', ''),
('2014-07-16 04:52:01', 1, '2014-07-16 04:52:01', 'DS01', 'MHS0002', 'Kamu kok belum ketemu sama saya?', '');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `nilai`
--
ALTER TABLE `nilai`
  ADD CONSTRAINT `fk_dosen_n` FOREIGN KEY (`kode_dosen`) REFERENCES `dosen` (`kode`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_makul_n` FOREIGN KEY (`id_mk`) REFERENCES `matakuliah` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_nilai_n` FOREIGN KEY (`indeks_nilai`) REFERENCES `indeks_nilai` (`kode`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_nim_n` FOREIGN KEY (`nim`) REFERENCES `mahasiswa_inti` (`nim`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
