<?php
/**
 * daftarkan master autoload untuk controllers dan models
 * demi mempermudah instance
 */

spl_autoload_register( function( $class ){

    /**
     * sejak menggunakan namespace, ini menjadi perlu
     * karena format parameter $class akan berbeda.
     * maka perlu untuk di parsing / filter terlebih dahulu
     *
     * strukturnya :
     *
     * {SIAKAD|Module}\{Controller|Model}\{class_name}
     */

    $class_to_include = '';

    /**
     * pisahkan masing-masing bagian
     */
    $parts = explode( '\\', $class );
    list( $root, $type ) = $parts;

    /**
     * cek apakah root nya SIAKAD
     */
    $is_SIAKAD = $root == 'SIAKAD';

    /**
     * jika bukan SIAKAD, filter string root
     * sehingga yang diperbolehkan hanya huruf kecil dan dash saja
     */
    $is_SIAKAD || $root = preg_replace( '/[^a-z0-9]+/', '-', strtolower( $root ) );

    /**
     * nama class nya adalah bagian paling akhir
     * dari parts, tinggal di pop
     */
    $class_name = array_pop( $parts );

    /** cek apakah class adalah controller */
    if( 'Controller' == $type )
        $class_to_include = is_readable(
            $x = ( $is_SIAKAD ? SIAKAD_CONTROLLER_ABS_PATH : SIAKAD_MODULE_ABS_PATH . DS . $root . DS . 'controllers' ) .
                DS . $class_name . '.php' ) ? $x : '';

    /** cek juga apakah class adalah model */
    else if ( 'Model' == $type )
        $class_to_include = is_readable(
            $x = ( $is_SIAKAD ? SIAKAD_MODEL_ABS_PATH : SIAKAD_MODULE_ABS_PATH . DS . $root . DS . 'models' ) .
                DS . $class_name . '.php' ) ? $x : '';

    /** jika kosong, gak usah di proses */
    if( !empty( $class_to_include ) ) require_once( $class_to_include );

} );