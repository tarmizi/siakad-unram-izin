<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Controller;

use SIAKAD\Model\Fakultas as ModelFakultas;


class Fakultas extends Databases {

    private $field_id;
    private $class_name;
    private $count_query;

    /** @var Fakultas $instance */
    private static $instance;

    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function __construct() {
        parent::_connect();
        $this->field_id = 'kode';
        $this->class_name = '\SIAKAD\Model\Fakultas';
        $this->table_name = 'fakultas';
        $this->count_query = 'SELECT * FROM ' . $this->table_name;
    }

    /**
     * @param $kode
     * @param string $by
     * @return \SIAKAD\Model\Fakultas
     */
    function _get( $kode, $by = '' ) {
        $query = 'SELECT * FROM ' . $this->table_name . ' WHERE `' . ( empty( $by ) ? $this->field_id : $kode ) . '` = "' . $kode . '"';
        return ( $return = mysql_fetch_object(
            mysql_query( $query ), $this->class_name
        ) ) ? $return : new $this->class_name;
    }

    function _gets( $args = array() ) {

        $return = array();

        $default_args = array(
            'nama'                      => '',
            'nama_ingg'                 => '',
            'exclude'                   => array(),
            'conditions'                => '',
            'orderby'                   => $this->field_id,
            'order'                     => 'DESC',
            'number'                    => 10,
            'offset'                    => 0
        );

        $list_args = sync_default_params( $default_args, $args );
        $query = 'SELECT * FROM ' . $this->table_name . ' WHERE 1';

        if( !empty( $list_args[ 'nama' ] ) || !empty( $list_args[ 'nama_ingg' ] ) ) {

            $query .= ' AND ( ';
            $post_title_arr = explode( ' ', $list_args[ 'nama' ] );

            foreach( $post_title_arr as $key => $title )
                $query .= ( $key > 0 ? ' OR' : '' ) .  '`nama` LIKE "%' . $title . '%"';

            $query .= ' OR ';
            $post_content_arr = explode( ' ', $list_args[ 'nama_ingg' ] );

            foreach( $post_content_arr as $key => $content )
                $query .= ( $key > 0 ? ' OR' : '' ) . '`nama_ingg` LIKE "%' . $content . '%"';

            $query .= ' )';

        }

        /**
         * exclude
         */
        if( !empty( $list_args[ 'exclude' ] ) ) {

            foreach( $list_args[ 'exclude' ] as $ex )
                $query .= ' AND `kode` <> ' . $ex;

        }

        /**
         * untuk custom query pada conditions
         */
        if( !empty( $list_args[ 'conditions' ] ) ) {
            foreach( $list_args[ 'conditions' ] as $conditions )
                $query .= ' AND ' . $list_args . '.' . $conditions[ 'field' ] . ' ' . $conditions[ 'operator' ] . ' ' . $conditions[ 'comparison' ];
        }

        $this->count_query = $query;

        /**
         * orderby dan jenis order
         */
        $query .= ' ORDER BY `' . $list_args[ 'orderby' ] . '` ' . $list_args[ 'order' ];

        /**
         * limit
         */
        if( $list_args[ 'number' ] >= 0 )
            $query .= ' LIMIT ' . $list_args[ 'offset' ] . ', ' . $list_args[ 'number' ];

        $resource = mysql_query( $query );

        //echo $query . ' : ' . mysql_error();

        while( $row = mysql_fetch_object( $resource, $this->class_name ) )
            $return[] = $row;

        return $return;

    }

    function insert( ModelFakultas $fakultas ) {
        return parent::insert(
            $this->table_name,
            array(
                'kode',
                'nama',
                'nama_ingg',
                'alias',
                'keterangan',
                'kode_univ'
            ),
            array(
                array(
                    $fakultas->getKode(),
                    $fakultas->getNama(),
                    $fakultas->getNamaIngg(),
                    $fakultas->getAlias(),
                    $fakultas->getKeterangan(),
                    $fakultas->getKodeUniv()
                )
            )
        );
    }

    function update( ModelFakultas $fakultas ) {
        return parent::update(
            $this->table_name,
            array(
                'kode',
                'nama',
                'nama_ingg',
                'alias',
                'keterangan',
                'kode_univ'
            ),
            array(
                $fakultas->getKode(),
                $fakultas->getNama(),
                $fakultas->getNamaIngg(),
                $fakultas->getAlias(),
                $fakultas->getKeterangan(),
                $fakultas->getKodeUniv()
            ),
            array(
                'kode', $fakultas->getKode()
            )
        );
    }

    function delete( $kode ) {
        return parent::delete(
            $this->table_name,
            'kode',
            $kode
        );
    }

} 