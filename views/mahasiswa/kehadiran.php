<?php

namespace SIAKAD\Views\Mahasiswa;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;

Headers::get_instance()
    ->set_page_title( 'Kehadiran' )
    ->set_page_name( 'Kehadiran' );

$tingkat2 = Routes::get_instance()->get_tingkat( 2 );
$tingkat3 = Routes::get_instance()->get_tingkat( 3 );
$tingkat4 = Routes::get_instance()->get_tingkat( 4 );

$is_kelas = Routes::get_instance()->is_tingkat( 3, 'kelas' );
!$is_kelas || Headers::get_instance()->set_page_sub_name( 'Kelas' );
$is_peserta = $is_kelas && Routes::get_instance()->has_tingkat( 4 ) && Routes::get_instance()->is_tingkat( 5, 'peserta' );

$is_tambah = Routes::get_instance()->is_tingkat( 3, 'tambah' );
$is_perbaiki = Routes::get_instance()->is_tingkat( 3, 'perbaiki' ) && Routes::get_instance()->has_tingkat( 4 );
$is_simpan = Routes::get_instance()->is_tingkat( 3, 'simpan' );
$is_hapus = Routes::get_instance()->is_tingkat( 3, 'hapus' );


Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">
                <?php echo Headers::get_instance()->get_page_name(); ?>
                <small>Daftar</small>
            </h1>
            <div class="row">
                <div class="col-md-9">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-3">
                                <select class="form-control">
                                    <option>--tahun ajaran</option>
                                    <option>2010</option>
                                    <option selected>2011</option>
                                    <option>2012</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control">
                                    <option>--mata kuliah</option>
                                    <option selected>Bahasa Sasak</option>
                                    <option>Bahasa Jerman</option>
                                </select>
                            </div>
                            <div class="col-sm-1">
                                <button class="btn btn-primary"><i class="fa fa-filter"></i> OK</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-3">
                    <form>
                        <div class="input-group">
                            <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                    </span>
                        </div>
                    </form>
                </div>
            </div>
            <br/>
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Kelas</th>
                    <th>Hadir?</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>12 Des 2010</td>
                    <td>A</td>
                    <td><i class="glyphicon glyphicon-ok"></i> Ya</td>
                </tr><tr>
                    <td>17 Des 2010</td>
                    <td>A</td>
                    <td><i class="glyphicon glyphicon-remove"></i> Tidak</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();