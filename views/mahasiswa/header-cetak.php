<?php

namespace SIAKAD\Views\Mahasiswa;

use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Helpers;
use SIAKAD\Controller\Sessions;

/**
 * jika sesi tidak valid,
 * buang ke halaman utama
 */
Sessions::get_instance()->_validate( SIAKAD_URI_PATH );

$headers = Headers::get_instance()
    ->add_head_meta( array(
        array( 'charset' => 'utf-8' ),
        array( 'http-equiv' => 'X-UA-Compatible', 'content' => 'IE=edge' ),
        array( 'name' => 'viewport', 'content' => 'width=21cm, initial-scale=1' ),
        array( 'name' => 'description', 'content' => SIAKAD_APP_DESCRIPTION ),
        array( 'name' => 'author', 'content' => SIAKAD_APP_AUTHOR )
    ) )
    ->add_style( 'bootstrap.min.css' )
    ->add_style( 'font-awesome.min.css' )
    ->add_style( 'style.css' )
    ->add_style( 'mahasiswa.css' )
    ->add_style( 'print.css' )
    ->add_favicon();

?>

<!DOCTYPE html>
<html lang="id">
<head>
    <?php echo $headers->get_head_meta(); ?>
    <?php echo $headers->get_head_title(); ?>
    <?php echo $headers->get_head_link(); ?>
    <?php echo $headers->get_head_script(); ?>
</head>

<body class="view mahasiswa halaman">