<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Model;


class IndeksNilai {

    private $huruf;
    private $angka;
    private $tahun_akademik;

    /**
     * @param mixed $huruf
     */
    public function setHuruf($huruf)
    {
        $this->huruf = $huruf;
    }

    /**
     * @return mixed
     */
    public function getHuruf()
    {
        return $this->huruf;
    }
    
    /**
     * @param mixed $angka
     */
    public function setAngka($angka)
    {
        $this->angka = $angka;
    }

    /**
     * @return mixed
     */
    public function getAngka()
    {
        return $this->angka;
    }
    
    /**
     * @param mixed $tahun_akademik
     */
    public function setTahunAkademik($tahun_akademik)
    {
        $this->tahun_akademik = $tahun_akademik;
    }

    /**
     * @return mixed
     */
    public function getTahunAkademik()
    {
        return $this->tahun_akademik;
    }

} 