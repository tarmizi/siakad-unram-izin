<?php

namespace SIAKAD\Views\Publik;

use SIAKAD\Controller\Footers;

Footers::get_instance()
    ->add_script( 'jquery.min.js' )
    ->add_script( 'jquery.ui.min.js' )
    ->add_script( 'bootstrap.min.js' )
    ->add_script( 'soal.js' );

?>

    <hr>
    <div class="container">
        <footer>
            <p>&copy; Hak Cipta <?php echo date( 'Y' ); ?> <?php echo SIAKAD_APP_AUTHOR; ?></p>
        </footer>
    </div>

    <?php echo Footers::get_instance()->get_script(); ?>

</body>
</html>