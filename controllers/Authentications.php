<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Controller;

use SIAKAD\Model\Login as ModelLogin;
use SIAKAD\Model\LevelAkses as ModelLevelAkses;
use SIAKAD\Model\Session;

class Authentications extends Databases {

    const success = 1,
        err_field = 2,
        err_user = 3,
        err_pass = 4;

    /**
     * validasi masukan :
     * - huruf a-z besar atau kecil
     * - angka 0-9
     * - underscode (_)
     * - gak boleh spasi
     * - gak boleh karakter selain itu
     *
     * @var string
     */
    private $regex = '/^[a-zA-Z0-9_]+$/';

    /** @var ModelLogin $obj_login */
    private $obj_login;

    /** @var ModelLevelAkses $obj_level_akses */
    private $obj_level_akses;

    static $error_messages = array(
        self::success => 'Berhasil masuk',
        self::err_field => 'Masukan tidak valid',
        self::err_user => 'Username tidak valid',
        self::err_pass => 'Username dan Password tidak valid',
    );

    /** @var Authentications $instance */
    private static $instance;

    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function __construct() {
        $this->obj_login = new ModelLogin();
        $this->obj_level_akses = new ModelLevelAkses();
    }

    /**
     * amankan inputan username dan password
     * yang disubmit user
     *
     * @param $username
     * @param $password
     * @param $status
     * @return $this
     */
    function _init( $username, $password, $status = 1 ){
        Login::get_instance()->_init(
            $this->obj_login
                ->setUsername( $username )
                ->setPassword( $password )
                ->setStatus( $status )
        );
        return $this;
    }

    function _valid_input() {
        return preg_match(
            $this->regex, $this->obj_login->getUsername()
        );
    }

    /**
     * @param string $redirect_while_success
     * @return int
     */
    function _auth( $redirect_while_success = '' ) {

        /**
         * secara default, return success dengan asumsi
         * tidak ada validasi berikut (switch)
         * yang bermasalah
         */
        $return = self::success;

        switch( false ) {
            case $this->_valid_input()      :
                $return = self::err_field; break;
            case Login::get_instance()->_valid_username()   :
                $return = self::err_user; break;
            case Login::get_instance()->_valid_password()   :
                $return = self::err_pass;
        }

        /**
         * jika tidak ada masalah dengan validasi,
         * generate session
         */
        if( $return == self::success ) {

            $this->obj_login = Login::get_instance()->_get();
            $this->obj_level_akses->setKodeAkses( $this->obj_login->getKodeAkses() )->fetchKodeAkses();

            Sessions::get_instance()->_generate(
                Session::get_instance()
                    ->_init( $this->obj_login, $this->obj_level_akses )
                    ->setView(
                        LevelAkses::$kode_views[ $this->obj_level_akses->getKodeView() ]
                    )
            );

            /**
             * jika target redirect di-set
             */
            if( !empty( $redirect_while_success ) ) siakad_redirect( $redirect_while_success . '?' . Helpers::status_param . '=' . Helpers::status_masuk );
        }

        /**
         * sisanya kembalikan status return-nya
         */
        return $return;

    }

} 