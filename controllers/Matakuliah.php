<?php
/**  
 * Canvas Production
 *
 * Author : Dani
 */

namespace SIAKAD\Controller;

use SIAKAD\Model\Matakuliah as ModelMatakuliah;

class Matakuliah extends Databases {

    static $semester = array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 );
    static $jenis_semester = array( 0 => 'Semua', 1 => 'Ganjil', 2 => 'Genap' );
    static $status = array( 0 => 'Tidak Aktif', 1 => 'Aktif' );
    static $jenis_pembagian_kelas = array(
        1 => 'Pilih Sendiri',
        2 => 'Ganjil Genap',
        3 => 'Interval NIM',
        4 => 'Random'
    );

    private $field_id;
    private $class_name;
    private $table_name;
    private $table_kurikulum;
    private $table_konsentrasi;
    private $table_bagian;
    private $table_sifat_mata_kuliah;
    private $table_dosen;
    private $table_dosen_pengajar;
    private $table_prodi;
    private $count_query;

    /** @var Matakuliah $instance */
    private static $instance;

    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function __construct() {
        parent::_connect();
        $this->field_id = 'id';
        $this->class_name = '\SIAKAD\Model\Matakuliah';
        $this->table_name = 'matakuliah';
        $this->table_kurikulum = 'kurikulum';
        $this->table_konsentrasi = 'konsentrasi';
        $this->table_bagian = 'bagian';
        $this->table_sifat_mata_kuliah = 'sifat_mata_kuliah';
        $this->table_dosen = 'dosen';
        $this->table_dosen_pengajar = 'dosen_pengajar';
        $this->table_prodi = 'prodi';
        $this->count_query = 'SELECT * FROM ' . $this->table_name;
    }

    /**
     * @param $id
     * @param string $by
     * @return \SIAKAD\Model\Matakuliah
     */
    function _get( $id, $by = '' ) {
        $query = 'SELECT * FROM ' . $this->table_name . ' WHERE `' . ( empty( $by ) ? $this->field_id : $by ) . '` = "' . $id . '"';
        return ( $return = mysql_fetch_object(
            mysql_query( $query ), $this->class_name
        ) ) ? $return : new $this->class_name;
    }

    function _gets( $args = array() ) {

        $return = array();

        $default_args = array(
            'kode_PS'                   => '',
            'id_kurikulum'              => -1,
            'id_konsentrasi'            => -1,
            'id_bagian'                 => -1,
            'semester'                  => -1,
            'jenis_semester'            => -1,
            'status'                    => -1,
            'kode_dosen'                => '',
            'nama_mk'                   => '',
            'exclude'                   => array(),
            'conditions'                => '',
            'orderby'                   => $this->field_id,
            'order'                     => 'DESC',
            'number'                    => 10,
            'offset'                    => 0,
            'custom'                    =>''
        );

        $list_args = sync_default_params( $default_args, $args );
        $query = 'SELECT ' . $this->table_name . '.*';
        $query .= ', ' . $this->table_kurikulum . '.tahun_akademik AS tahun_akademik_kurikulum';
        $query .= ', ' . $this->table_konsentrasi . '.nama AS nama_konsentrasi';
        $query .= ', ' . $this->table_bagian . '.nama AS nama_bagian';
        $query .= ', ' . $this->table_sifat_mata_kuliah . '.nama AS nama_sifat_mata_kuliah';
        $query .= ', ' . $this->table_dosen . '.nama AS nama_dosen';
        $query .= ', ' . $this->table_prodi . '.nama_nasional AS nama_nasional_prodi';
        $query .= ' FROM ' . $this->table_name;
        $query .= ' LEFT JOIN ' . $this->table_kurikulum . ' ON ' . $this->table_kurikulum . '.id = ' . $this->table_name . '.id_kurikulum';
        $query .= ' LEFT JOIN ' . $this->table_konsentrasi . ' ON ' . $this->table_konsentrasi . '.id = ' . $this->table_name . '.id_konsentrasi';
        $query .= ' LEFT JOIN ' . $this->table_prodi . ' ON ' . $this->table_prodi . '.kode = ' . $this->table_kurikulum . '.kode_PS';
        $query .= ' LEFT JOIN ' . $this->table_bagian . ' ON ' . $this->table_bagian . '.id = ' . $this->table_name . '.id_bagian';
        $query .= ' LEFT JOIN ' . $this->table_sifat_mata_kuliah . ' ON ' . $this->table_sifat_mata_kuliah . '.kode = ' . $this->table_name . '.kode_sifat';
        $query .= ' LEFT JOIN ' . $this->table_dosen_pengajar . ' ON ' . $this->table_dosen_pengajar . '.id_matakuliah = ' . $this->table_name . '.id';
        $query .= ' LEFT JOIN ' . $this->table_dosen . ' ON ' . $this->table_dosen . '.kode = ' . $this->table_dosen_pengajar . '.kode_dosen';
        $query .= ' WHERE 1';

        /**
         * id_kurikulum
         */
        if( !empty( $list_args[ 'kode_PS' ] ) )
            $query .= ' AND ' . $this->table_prodi . '.kode = ' . $list_args[ 'kode_PS' ];

        /**
         * id_kurikulum
         */
        if( $list_args[ 'id_kurikulum' ] >= 0 )
            $query .= ' AND ' . $this->table_name . '.id_kurikulum = ' . $list_args[ 'id_kurikulum' ];

        /**
         * id_konsentrasi
         */
        if( $list_args[ 'id_konsentrasi' ] >= 0 )
            $query .= ' AND ' . $this->table_name . '.id_konsentrasi = ' . $list_args[ 'id_konsentrasi' ];

        /**
         * id_bagian
         */
        if( $list_args[ 'id_bagian' ] >= 0 )
            $query .= ' AND ' . $this->table_name . '.id_bagian = ' . $list_args[ 'id_bagian' ];

        /**
         * nama_mk
         */
        if( !empty( $list_args[ 'nama_mk' ] ) ) {
            $query .= ' AND ';
            $name_arr = explode( ' ', $list_args[ 'nama_mk' ] );
            foreach( $name_arr as $key => $name )
                $query .= ( $key > 0 ? ' OR' : '' ) . $this->table_name . '.`nama_mk` LIKE "%' . $name . '%"';
        }

        /**
         * semester
         */
        if( $list_args[ 'semester' ] >= 0 )
            $query .= ' AND ' . $this->table_name . '.semester = ' . $list_args[ 'semester' ];

        /**
         * jenis_semester
         */
        if( $list_args[ 'jenis_semester' ] >= 0 )
            $query .= ' AND ' . $this->table_name . '.jenis_semester = ' . $list_args[ 'jenis_semester' ];

        /**
         * status
         */
        if( $list_args[ 'status' ] >= 0 )
            $query .= ' AND ' . $this->table_name . '.status = ' . $list_args[ 'status' ];

        /**
         * kode_dosen
         */
        if( !empty( $list_args[ 'kode_dosen' ] ) )
            $query .= ' AND ' . $this->table_name . '.kode_dosen = ' . $list_args[ 'kode_dosen' ];

        /**
         * exclude
         */
        if( !empty( $list_args[ 'exclude' ] ) ) {

            foreach( $list_args[ 'exclude' ] as $ex )
                $query .= ' AND `id` <> ' . $ex;

        }

        /**
         * untuk custom query pada conditions
         */
        if( !empty( $list_args[ 'conditions' ] ) ) {
            foreach( $list_args[ 'conditions' ] as $conditions )
                $query .= ' AND ' . $conditions['claue'] . '.' . $conditions[ 'field' ] . ' ' . $conditions[ 'operator' ] . ' ' . $conditions[ 'comparison' ];
        }
        
        
        $this->count_query = $query;

        /**
         * orderby dan jenis order
         */
        $query .= ' ORDER BY `' . $list_args[ 'orderby' ] . '` ' . $list_args[ 'order' ];

        /**
         * limit
         */
        if( $list_args[ 'number' ] >= 0 )
            $query .= ' LIMIT ' . $list_args[ 'offset' ] . ', ' . $list_args[ 'number' ];
        
        if( !empty( $list_args[ 'custom' ] ) ) {
            $query = $list_args['custom'];
        }
       
        $resource = mysql_query( $query );

        //echo $query . ' : ' . mysql_error();

        while( $row = mysql_fetch_object( $resource, $this->class_name ) )
            $return[] = $row;

        return $return;

    }
    
    function insert( ModelMatakuliah $a ) {
        return parent::insert(
            $this->table_name,
            array(
                'id',
                'id_kurikulum',
                'kode_mk',
                'nama_mk',
                'nama_mk_ingg',
                'sks',
                'kode_sifat',
                'semester',
                'jenis_semester',
                'status',
                'id_konsentrasi',
                'id_bagian',
                'kode_dosen_koordinator',
                'silabus',
                'sap',
                'bahan_ajar',
                'sks_teori',
                'sks_praktikum',
                'jenis_pembagian_kelas'
            ),
            array(
                array(
                    $a->getId(),
                    $a->getIdKurikulum(),
                    $a->getKodeMk(),
                    $a->getNamaMk(),
                    $a->getNamaMkIngg(),
                    $a->getSks(),
                    $a->getKodeSifat(),
                    $a->getSemester(),
                    $a->getJenisSemester(),
                    $a->getStatus(),
                    $a->getIdKonsentrasi(),
                    $a->getIdBagian(),
                    $a->getKodeDosenKoordinator(),
                    $a->getSilabus(),
                    $a->getSap(),
                    $a->getBahanAjar(),
                    $a->getSksTeori(),
                    $a->getSksPraktikum(),
                    $a->getJenisPembagianKelas()
                    
                )
            )
        );
    }

    function update( ModelMatakuliah $a ) {
        return parent::update(
            $this->table_name,
            array(
                'id',
                'id_kurikulum',
                'kode_mk',
                'nama_mk',
                'nama_mk_ingg',
                'sks',
                'kode_sifat',
                'semester',
                'jenis_semester',
                'status',
                'id_konsentrasi',
                'id_bagian',
                'kode_dosen_koordinator',
                'silabus',
                'sap',
                'bahan_ajar',
                'sks_teori',
                'sks_praktikum',
                'jenis_pembagian_kelas'
            ),
            array(
                $a->getId(),
                $a->getIdKurikulum(),
                $a->getKodeMk(),
                $a->getNamaMk(),
                $a->getNamaMkIngg(),
                $a->getSks(),
                $a->getKodeSifat(),
                $a->getSemester(),
                $a->getJenisSemester(),
                $a->getStatus(),
                $a->getIdKonsentrasi(),
                $a->getIdBagian(),
                $a->getKodeDosenKoordinator(),
                $a->getSilabus(),
                $a->getSap(),
                $a->getBahanAjar(),
                $a->getSksTeori(),
                $a->getSksPraktikum(),
                $a->getJenisPembagianKelas()
            ),
            array(
                'kode_mk', $a->getKodeMk()
            )
        );
    }

    function delete( $kode_mk ) {
        return parent::delete(
            $this->table_name,
            'kode_mk',
            $kode_mk
        );
    }

    function _count() {
        return mysql_num_rows( mysql_query( $this->count_query ) );
    }

}