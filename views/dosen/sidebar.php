<?php

namespace SIAKAD\Views\Dosen;

use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Contents;

$base_link = SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view(); ?>
<ul class="nav nav-sidebar">
    <li <?php echo 'Dosen' == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>"><i class="fa fa-user"></i>&nbsp;&nbsp;Ahmad Zafrullah, S.T.</a></li>
    <li <?php echo ( $page = 'Absensi' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/absensi"><i class="fa fa-calendar"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
    <li <?php echo ( $page = 'Nilai' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/nilai"><i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
    <li <?php echo ( $page = 'Mahasiswa' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/mahasiswa"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
    <li <?php echo ( $page = 'Pengumuman' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/pengumuman"><i class="fa fa-paste"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
</ul>