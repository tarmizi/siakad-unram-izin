<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace Ujian_Online;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;

Headers::get_instance()
    ->set_page_title( 'Ujian Online | Login' )
    ->set_page_name( 'Ujian Online | Login' );

Contents::get_instance()->get_header( 'login' );

?>

<body>

<div class="container">

    <form class="form-signin" role="form" action="<?php echo Contents::get_instance()->get_module_path( Contents::path_type_uri ); ?>">
        <h2 class="form-signin-heading">Silakan Login</h2>
        <input type="hidden" name="loggedin" value="loggedin">
        <input type="text" class="form-control" placeholder="NIM" required autofocus>
        <input type="password" class="form-control" placeholder="Password" required>
        <div class="checkbox">
            <label>
                <input type="checkbox" value="remember-me"> Ingatkan saya
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Masuk</button>
    </form>

</div> <!-- /container -->

</body>


<?php Contents::get_instance()->get_footer( 'login' );