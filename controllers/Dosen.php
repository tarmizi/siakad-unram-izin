<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Controller;

use SIAKAD\Model\Dosen as ModelDosen;

class Dosen extends Databases {

    static $jenjang = array( 'A', 'B', 'C', 'D', 'E', 'F' );
    static $jenis_kelamin = array( 'L', 'P' );
    static $status_ikatan_kerja = array( 'A', 'B', 'C', 'D' );

    private $field_id;
    private $class_name;
    private $count_query;
    private $table_name;
    private $table_prodi;
    private $table_golongan;
    private $table_jabatan;
    private $table_jenjang;
    private $table_status_dosen;
    private $table_pangkat;
    private $table_agama;
    private $table_konsentrasi;


    /** @var Dosen $instance */
    private static $instance;

    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function __construct() {
        parent::_connect();
        $this->field_id = 'kode';
        $this->class_name = '\SIAKAD\Model\Dosen';
        $this->table_name = 'dosen';
        $this->table_prodi = 'prodi';
        $this->table_golongan = 'golongan';
        $this->table_jabatan = 'jabatan';
        $this->table_jenjang = 'jenjang';
        $this->table_status_dosen = 'status_dosen';
        $this->table_pangkat = 'pangkat';
        $this->table_agama = 'agama';
        $this->table_konsentrasi = 'konsentrasi';
        $this->count_query = 'SELECT * FROM ' . $this->table_name;
    }

    /**
     * @param $id
     * @param string $by
     * @return \SIAKAD\Model\Dosen
     */
    function _get( $id, $by = '' ) {
        $query = 'SELECT ' . $this->table_name . '.*';
        $query .= ', ' . $this->table_prodi . '.nama_PS';
        $query .= ', ' . $this->table_golongan . '.nama AS nama_golongan';
        $query .= ', ' . $this->table_jabatan . '.nama AS nama_jabatan';
        $query .= ', ' . $this->table_jenjang . '.nama AS nama_jenjang';
        $query .= ', ' . $this->table_pangkat . '.nama AS nama_pangkat';
        $query .= ', ' . $this->table_agama . '.nama AS nama_agama';
        $query .= ', ' . $this->table_status_dosen . '.nama AS nama_status_dosen';
        $query .= ', ' . $this->table_konsentrasi . '.nama AS nama_konsentrasi';
        $query .= ' FROM ' . $this->table_name;
        $query .= ' LEFT JOIN ' . $this->table_prodi . ' ON ' . $this->table_prodi . '.kode = ' . $this->table_name . '.kode_PS';
        $query .= ' LEFT JOIN ' . $this->table_golongan . ' ON ' . $this->table_golongan . '.kode = ' . $this->table_name . '.kode_golongan';
        $query .= ' LEFT JOIN ' . $this->table_jabatan . ' ON ' . $this->table_jabatan . '.kode = ' . $this->table_name . '.kode_jabatan';
        $query .= ' LEFT JOIN ' . $this->table_jenjang . ' ON ' . $this->table_jenjang . '.kode = ' . $this->table_name . '.kode_pendidikan_tertinggi';
        $query .= ' LEFT JOIN ' . $this->table_pangkat . ' ON ' . $this->table_pangkat . '.kode = ' . $this->table_name . '.kode_pangkat';
        $query .= ' LEFT JOIN ' . $this->table_agama . ' ON ' . $this->table_agama . '.kode = ' . $this->table_name . '.kode_agama';
        $query .= ' LEFT JOIN ' . $this->table_status_dosen . ' ON ' . $this->table_status_dosen . '.kode = ' . $this->table_name . '.kode_status_dosen';
        $query .= ' LEFT JOIN ' . $this->table_konsentrasi . ' ON ' . $this->table_konsentrasi . '.id = ' . $this->table_name . '.id_konsentrasi';
        $query .= ' WHERE ' . $this->table_name . '.`' . ( empty( $by ) ? $this->field_id : $id ) . '` = "' . $id . '"';
        return ( $return = mysql_fetch_object(
            mysql_query( $query ), $this->class_name
        ) ) ? $return : new $this->class_name;
    }

    function _gets( $args = array() ) {

        $return = array();

        $default_args = array(
            'kode_PS'                   => '',
            'nama'                      => '',
            'exclude'                   => array(),
            'conditions'                => '',
            'orderby'                   => $this->field_id,
            'order'                     => 'DESC',
            'number'                    => 10,
            'offset'                    => 0
        );

        $list_args = sync_default_params( $default_args, $args );
        $query = 'SELECT ' . $this->table_name . '.*';
        $query .= ', ' . $this->table_golongan . '.nama AS nama_golongan';
        $query .= ', ' . $this->table_jabatan . '.nama AS nama_jabatan';
        $query .= ', ' . $this->table_jenjang . '.nama AS nama_jenjang';
        $query .= ', ' . $this->table_pangkat . '.nama AS nama_pangkat';
        $query .= ', ' . $this->table_agama . '.nama AS nama_agama';
        $query .= ', ' . $this->table_status_dosen . '.nama AS nama_status_dosen';
        $query .= ', ' . $this->table_konsentrasi . '.nama AS nama_konsentrasi';
        $query .= ' FROM ' . $this->table_name;
        $query .= ' LEFT JOIN ' . $this->table_golongan . ' ON ' . $this->table_golongan . '.kode = ' . $this->table_name . '.kode_golongan';
        $query .= ' LEFT JOIN ' . $this->table_jabatan . ' ON ' . $this->table_jabatan . '.kode = ' . $this->table_name . '.kode_jabatan';
        $query .= ' LEFT JOIN ' . $this->table_jenjang . ' ON ' . $this->table_jenjang . '.kode = ' . $this->table_name . '.kode_pendidikan_tertinggi';
        $query .= ' LEFT JOIN ' . $this->table_pangkat . ' ON ' . $this->table_pangkat . '.kode = ' . $this->table_name . '.kode_pangkat';
        $query .= ' LEFT JOIN ' . $this->table_agama . ' ON ' . $this->table_agama . '.kode = ' . $this->table_name . '.kode_agama';
        $query .= ' LEFT JOIN ' . $this->table_status_dosen . ' ON ' . $this->table_status_dosen . '.kode = ' . $this->table_name . '.kode_status_dosen';
        $query .= ' LEFT JOIN ' . $this->table_konsentrasi . ' ON ' . $this->table_konsentrasi . '.id = ' . $this->table_name . '.id_konsentrasi';
        $query .= ' WHERE 1';

        if( !empty( $list_args[ 'kode_PS' ] ) )
            $query .= ' AND ' . $this->table_name . '.kode_PS = "' . $list_args[ 'kode_PS' ] . '"';

        if( !empty( $list_args[ 'nama' ] ) ) {
            $query .= ' AND ';
            $name_arr = explode( ' ', $list_args[ 'nama' ] );
            foreach( $name_arr as $key => $name )
                $query .= ( $key > 0 ? ' OR' : '' ) . $this->table_name . '.`nama` LIKE "%' . $name . '%"';
        }

        /**
         * exclude
         */
        if( !empty( $list_args[ 'exclude' ] ) ) {

            foreach( $list_args[ 'exclude' ] as $ex )
                $query .= ' AND `NIM` <> ' . $ex;

        }

        /**
         * untuk custom query pada conditions
         */
        if( !empty( $list_args[ 'conditions' ] ) ) {
            foreach( $list_args[ 'conditions' ] as $conditions )
                $query .= ' AND ' . $list_args . '.' . $conditions[ 'field' ] . ' ' . $conditions[ 'operator' ] . ' ' . $conditions[ 'comparison' ];
        }

        $this->count_query = $query;

        /**
         * orderby dan jenis order
         */
        $query .= ' ORDER BY `' . $list_args[ 'orderby' ] . '` ' . $list_args[ 'order' ];

        /**
         * limit
         */
        if( $list_args[ 'number' ] >= 0 )
            $query .= ' LIMIT ' . $list_args[ 'offset' ] . ', ' . $list_args[ 'number' ];

        $resource = mysql_query( $query );

        //echo $query . ' : ' . mysql_error();

        while( $row = mysql_fetch_object( $resource, $this->class_name ) )
            $return[] = $row;

        return $return;

    }

    function insert( ModelDosen $dosen ) {
        return parent::insert(
            $this->table_name,
            array(
                'kode',
                'kode_PS',
                'jenjang',
                'KTP',
                'nama',
                'gelar_tertinggi',
                'NIP',
                'NIDN',
                'tempat_lahir',
                'tgl_lahir',
                'jenis_kelamin',
                'kode_golongan',
                'kode_jabatan',
                'kode_pendidikan_tertinggi',
                'status_ikatan_kerja',
                'akta_sertifikat_mengajar',
                'surat_ijin_mengajar',
                'kode_pangkat',
                'kode_agama',
                'tlp_rumah',
                'tlp_hp',
                'id_konsentrasi',
                'kode_status_dosen'
            ),
            array(
                array(
                    $dosen->getKode(),
                    $dosen->getKodePS(),
                    $dosen->getJenjang(),
                    $dosen->getKTP(),
                    $dosen->getNama(),
                    $dosen->getGelarTertinggi(),
                    $dosen->getNIP(),
                    $dosen->getNIDN(),
                    $dosen->getTempatLahir(),
                    $dosen->getTglLahir(),
                    $dosen->getJenisKelamin(),
                    $dosen->getKodeGolongan(),
                    $dosen->getKodeJabatan(),
                    $dosen->getKodePendidikanTertinggi(),
                    $dosen->getStatusIkatanKerja(),
                    $dosen->getAktaSertifikatMengajar(),
                    $dosen->getSuratIjinMengajar(),
                    $dosen->getKodePangkat(),
                    $dosen->getKodeAgama(),
                    $dosen->getTlpRumah(),
                    $dosen->getTlpHp(),
                    $dosen->getIdKonsentrasi(),
                    $dosen->getKodeStatusDosen()
                )
            )
        );
    }

    function update( ModelDosen $dosen ) {
        return parent::update(
            $this->table_name,
            array(
                'kode',
                'kode_PS',
                'jenjang',
                'KTP',
                'nama',
                'gelar_tertinggi',
                'NIP',
                'NIDN',
                'tempat_lahir',
                'tgl_lahir',
                'jenis_kelamin',
                'kode_golongan',
                'kode_jabatan',
                'kode_pendidikan_tertinggi',
                'status_ikatan_kerja',
                'akta_sertifikat_mengajar',
                'surat_ijin_mengajar',
                'kode_pangkat',
                'kode_agama',
                'tlp_rumah',
                'tlp_hp',
                'id_konsentrasi',
                'kode_status_dosen'
            ),
            array(
                $dosen->getKode(),
                $dosen->getKodePS(),
                $dosen->getJenjang(),
                $dosen->getKTP(),
                $dosen->getNama(),
                $dosen->getGelarTertinggi(),
                $dosen->getNIP(),
                $dosen->getNIDN(),
                $dosen->getTempatLahir(),
                $dosen->getTglLahir(),
                $dosen->getJenisKelamin(),
                $dosen->getKodeGolongan(),
                $dosen->getKodeJabatan(),
                $dosen->getKodePendidikanTertinggi(),
                $dosen->getStatusIkatanKerja(),
                $dosen->getAktaSertifikatMengajar(),
                $dosen->getSuratIjinMengajar(),
                $dosen->getKodePangkat(),
                $dosen->getKodeAgama(),
                $dosen->getTlpRumah(),
                $dosen->getTlpHp(),
                $dosen->getIdKonsentrasi(),
                $dosen->getKodeStatusDosen()
            ),
            array(
                'kode', $dosen->getKode()
            )
        );
    }

    function delete( $kode ) {
        return parent::delete(
            $this->table_name, 'kode', $kode
        );
    }

    function _count() {
        return mysql_num_rows( mysql_query( $this->count_query ) );
    }

}