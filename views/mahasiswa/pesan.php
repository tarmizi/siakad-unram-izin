<?php

namespace SIAKAD\Views\Operator\Fakultas;

use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Mahasiswa;
use SIAKAD\Controller\Pesan;
use SIAKAD\Controller\Sessions;
use SIAKAD\Controller\DosenPa;

Headers::get_instance()
    ->set_page_title( 'Pesan' )
    ->set_page_name( 'Pesan' );

$tingkat2 = Routes::get_instance()->get_tingkat( 2 );
$is_tambah = Routes::get_instance()->is_tingkat( 3, 'tambah' );
$is_perbaiki = Routes::get_instance()->is_tingkat( 3, 'perbaiki' );
$is_simpan = Routes::get_instance()->is_tingkat( 3, 'simpan' );
$is_hapus = Routes::get_instance()->is_tingkat( 3, 'hapus' );

Contents::get_instance()->get_header();
$me = Mahasiswa::get_instance()->_get(Sessions::get_instance()->_retrieve()->getObjLogin()->getUsername());
$myDosenPa = DosenPa::get_instance()->_get($me->getNIM(),'nim_mahasiswa');
$conditions=array(array('logika'=>'AND','tabel'=>'( pesan','field'=>'id_pengirim','operator'=>'=','comparison'=>'\''.$me->getNIM().'\''),
                  array('logika'=>'AND','tabel'=>'pesan','field'=>'id_penerima','operator'=>'=','comparison'=>'\''.$myDosenPa->getKodeDosen().'\')'),
                  array('logika'=>'OR','tabel'=>'( pesan','field'=>'id_pengirim','operator'=>'=','comparison'=>'\''.$myDosenPa->getKodeDosen().'\''),
                  array('logika'=>'AND','tabel'=>'pesan','field'=>'id_penerima','operator'=>'=','comparison'=>'\''.$me->getNIM().'\')'));
?>

<div class="container-fluid pengumuman">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">Pesan<small>Dosen Pembimbing Akademik</small>
            </h1>
            <div class="row">
                <div class="col-sm-12">
                    <?php
                        if(isset($_POST['send'])){
                            $p = new \SIAKAD\Model\Pesan();
                            $p->setIdPenerima($myDosenPa->getKodeDosen());
                            $p->setIdPengirim($me->getNIM());
                            $p->setIsiPesan($_POST['pesan']);
                            Pesan::get_instance()->insert($p) or die("Query salah : " . mysql_error());
                        }
                    ?>
                    <form class="form-horizontal" method="post" action="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2; ?>">
                        <textarea  name="pesan" class="form-control" placeholder="Ketik Pesan Anda"></textarea><button name="send" class="form-control" type="submit">Kirim <i class="glyphicon glyphicon-send"></i></button>
                    </form>
                    <table class="table">
                        <tbody>
                        <?php /** @var $mahasiswa \SIAKAD\Model\Pesan */
                        $list_pesan = Pesan::get_instance()->_gets(array('conditions'=>$conditions));
                        foreach( $list_pesan as $pesan ) : ?>
                        <tr>
                            <td><?php echo $pesan->getTanggalKirim(); ?></td><td><?php echo $pesan->getIsiPesan(); ?></td><td><?php echo '<i class="glyphicon glyphicon-'; if($pesan->getIdPengirim()==$me->getNIM()) echo 'forward'; else echo 'inbox'; echo '"></i>'; ?></td>
                        </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <ul class="pagination">
                        <li><a href="#">&laquo;</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">&raquo;</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();