<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Model;


class ProgramStudi {

    private $kode;
    private $kode_nasional;
    private $kode_fakultas;
    private $kode_prodi_unram;
    private $nama_nasional;
    private $nama_nasional_ingg;
    private $jenjang;
    private $nama_PS;
    private $keterangan;
    private $kurikulum_pakai;
    private $alamat_prodi;
    private $sk_dikti;
    private $tgl_sk_dikti;
    private $tgl_akhir_sk_dikti;
    private $jumlah_sks_kelulusan;
    private $status;
    private $tahun_semester_mulai;
    private $email;
    private $tgl_awal_pendirian;
    private $sk_ban_pt;
    private $tgl_sk_ban_pt;
    private $tgl_akhir_sk_akreditasi;
    private $kode_akreditasi;

    /**
     * variabel pembantu sementara saja
     */
    private $jenis;
    private $nama_fakultas;

    function _init( $request ) {

        !isset( $request[ 'jenis' ] ) || $this->jenis = $request[ 'jenis' ];
        !isset( $request[ 'kode_nasional' ] ) || $this->kode_nasional = $request[ 'kode_nasional' ];

        /**
         * kode adalah gabungan antara kode nasional dengan jenis
         * 1 reguler pagi
         * 2 ekstensi
         * dst
         */
        $this->kode = $this->kode_nasional . $this->jenis;

        !isset( $request[ 'kode_fakultas' ] ) || $this->kode_fakultas = $request[ 'kode_fakultas' ];
        !isset( $request[ 'kode_prodi_unram' ] ) || $this->kode_prodi_unram = $request[ 'kode_prodi_unram' ];
        !isset( $request[ 'nama_nasional' ] ) || $this->nama_nasional = $request[ 'nama_nasional' ];
        !isset( $request[ 'nama_nasional_ingg' ] ) || $this->nama_nasional_ingg = $request[ 'nama_nasional_ingg' ];
        !isset( $request[ 'jenjang' ] ) || $this->jenjang = $request[ 'jenjang' ];
        !isset( $request[ 'nama_PS' ] ) || $this->nama_PS = $request[ 'nama_PS' ];
        !isset( $request[ 'keterangan' ] ) || $this->keterangan = $request[ 'keterangan' ];
        !isset( $request[ 'kurikulum_pakai' ] ) || $this->kurikulum_pakai = $request[ 'kurikulum_pakai' ];
        !isset( $request[ 'alamat_prodi' ] ) || $this->alamat_prodi = $request[ 'alamat_prodi' ];
        !isset( $request[ 'sk_dikti' ] ) || $this->sk_dikti = $request[ 'sk_dikti' ];
        !isset( $request[ 'tgl_sk_dikti' ] ) || $this->tgl_sk_dikti = $request[ 'tgl_sk_dikti' ];
        !isset( $request[ 'tgl_akhir_sk_dikti' ] ) || $this->tgl_akhir_sk_dikti = $request[ 'tgl_akhir_sk_dikti' ];
        !isset( $request[ 'jumlah_sks_kelulusan' ] ) || $this->jumlah_sks_kelulusan = $request[ 'jumlah_sks_kelulusan' ];
        !isset( $request[ 'status' ] ) || $this->status = $request[ 'status' ];
        !isset( $request[ 'tahun_semester_mulai' ] ) || $this->tahun_semester_mulai = $request[ 'tahun_semester_mulai' ];
        !isset( $request[ 'email' ] ) || $this->email = $request[ 'email' ];
        !isset( $request[ 'tgl_awal_pendirian' ] ) || $this->tgl_awal_pendirian = $request[ 'tgl_awal_pendirian' ];
        !isset( $request[ 'sk_ban_pt' ] ) || $this->sk_ban_pt = $request[ 'sk_ban_pt' ];
        !isset( $request[ 'tgl_sk_ban_pt' ] ) || $this->tgl_sk_ban_pt = $request[ 'tgl_sk_ban_pt' ];
        !isset( $request[ 'tgl_akhir_sk_akreditasi' ] ) || $this->tgl_akhir_sk_akreditasi = $request[ 'tgl_akhir_sk_akreditasi' ];
        !isset( $request[ 'kode_akreditasi' ] ) || $this->kode_akreditasi = $request[ 'kode_akreditasi' ];
    }

    /**
     * @param mixed $alamat_prodi
     */
    public function setAlamatProdi($alamat_prodi)
    {
        $this->alamat_prodi = $alamat_prodi;
    }

    /**
     * @return mixed
     */
    public function getAlamatProdi()
    {
        return $this->alamat_prodi;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $jenjang
     */
    public function setJenjang($jenjang)
    {
        $this->jenjang = $jenjang;
    }

    /**
     * @return mixed
     */
    public function getJenjang()
    {
        return $this->jenjang;
    }

    /**
     * @param mixed $jumlah_sks_kelulusan
     */
    public function setJumlahSksKelulusan($jumlah_sks_kelulusan)
    {
        $this->jumlah_sks_kelulusan = $jumlah_sks_kelulusan;
    }

    /**
     * @return mixed
     */
    public function getJumlahSksKelulusan()
    {
        return $this->jumlah_sks_kelulusan;
    }

    /**
     * @param mixed $keterangan
     */
    public function setKeterangan($keterangan)
    {
        $this->keterangan = $keterangan;
    }

    /**
     * @return mixed
     */
    public function getKeterangan()
    {
        return $this->keterangan;
    }

    /**
     * @param mixed $kode
     */
    public function setKode($kode)
    {
        $this->kode = $kode;
    }

    /**
     * @return mixed
     */
    public function getKode()
    {
        return $this->kode;
    }

    /**
     * @param mixed $kode_akreditasi
     */
    public function setKodeAkreditasi($kode_akreditasi)
    {
        $this->kode_akreditasi = $kode_akreditasi;
    }

    /**
     * @return mixed
     */
    public function getKodeAkreditasi()
    {
        return $this->kode_akreditasi;
    }

    /**
     * @param mixed $kode_fakultas
     */
    public function setKodeFakultas($kode_fakultas)
    {
        $this->kode_fakultas = $kode_fakultas;
    }

    /**
     * @return mixed
     */
    public function getKodeFakultas()
    {
        return $this->kode_fakultas;
    }

    /**
     * @param mixed $kode_nasional
     */
    public function setKodeNasional($kode_nasional)
    {
        $this->kode_nasional = $kode_nasional;
    }

    /**
     * @return mixed
     */
    public function getKodeNasional()
    {
        return $this->kode_nasional;
    }

    /**
     * @param mixed $kode_prodi_unram
     */
    public function setKodeProdiUnram($kode_prodi_unram)
    {
        $this->kode_prodi_unram = $kode_prodi_unram;
    }

    /**
     * @return mixed
     */
    public function getKodeProdiUnram()
    {
        return $this->kode_prodi_unram;
    }

    /**
     * @param mixed $kurikulum_pakai
     */
    public function setKurikulumPakai($kurikulum_pakai)
    {
        $this->kurikulum_pakai = $kurikulum_pakai;
    }

    /**
     * @return mixed
     */
    public function getKurikulumPakai()
    {
        return $this->kurikulum_pakai;
    }

    /**
     * @param mixed $nama_PS
     */
    public function setNamaPS($nama_PS)
    {
        $this->nama_PS = $nama_PS;
    }

    /**
     * @return mixed
     */
    public function getNamaPS()
    {
        return $this->nama_PS;
    }

    /**
     * @param mixed $nama_nasional
     */
    public function setNamaNasional($nama_nasional)
    {
        $this->nama_nasional = $nama_nasional;
    }

    /**
     * @return mixed
     */
    public function getNamaNasional()
    {
        return $this->nama_nasional;
    }

    /**
     * @param mixed $nama_nasional_ingg
     */
    public function setNamaNasionalIngg($nama_nasional_ingg)
    {
        $this->nama_nasional_ingg = $nama_nasional_ingg;
    }

    /**
     * @return mixed
     */
    public function getNamaNasionalIngg()
    {
        return $this->nama_nasional_ingg;
    }

    /**
     * @param mixed $sk_ban_pt
     */
    public function setSkBanPt($sk_ban_pt)
    {
        $this->sk_ban_pt = $sk_ban_pt;
    }

    /**
     * @return mixed
     */
    public function getSkBanPt()
    {
        return $this->sk_ban_pt;
    }

    /**
     * @param mixed $sk_dikti
     */
    public function setSkDikti($sk_dikti)
    {
        $this->sk_dikti = $sk_dikti;
    }

    /**
     * @return mixed
     */
    public function getSkDikti()
    {
        return $this->sk_dikti;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $tahun_semester_mulai
     */
    public function setTahunSemesterMulai($tahun_semester_mulai)
    {
        $this->tahun_semester_mulai = $tahun_semester_mulai;
    }

    /**
     * @return mixed
     */
    public function getTahunSemesterMulai()
    {
        return $this->tahun_semester_mulai;
    }

    /**
     * @param mixed $tgl_akhir_sk_akreditasi
     */
    public function setTglAkhirSkAkreditasi($tgl_akhir_sk_akreditasi)
    {
        $this->tgl_akhir_sk_akreditasi = $tgl_akhir_sk_akreditasi;
    }

    /**
     * @return mixed
     */
    public function getTglAkhirSkAkreditasi()
    {
        return $this->tgl_akhir_sk_akreditasi;
    }

    /**
     * @param mixed $tgl_akhir_sk_dikti
     */
    public function setTglAkhirSkDikti($tgl_akhir_sk_dikti)
    {
        $this->tgl_akhir_sk_dikti = $tgl_akhir_sk_dikti;
    }

    /**
     * @return mixed
     */
    public function getTglAkhirSkDikti()
    {
        return $this->tgl_akhir_sk_dikti;
    }

    /**
     * @param mixed $tgl_awal_pendirian
     */
    public function setTglAwalPendirian($tgl_awal_pendirian)
    {
        $this->tgl_awal_pendirian = $tgl_awal_pendirian;
    }

    /**
     * @return mixed
     */
    public function getTglAwalPendirian()
    {
        return $this->tgl_awal_pendirian;
    }

    /**
     * @param mixed $tgl_sk_ban_pt
     */
    public function setTglSkBanPt($tgl_sk_ban_pt)
    {
        $this->tgl_sk_ban_pt = $tgl_sk_ban_pt;
    }

    /**
     * @return mixed
     */
    public function getTglSkBanPt()
    {
        return $this->tgl_sk_ban_pt;
    }

    /**
     * @param mixed $tgl_sk_dikti
     */
    public function setTglSkDikti($tgl_sk_dikti)
    {
        $this->tgl_sk_dikti = $tgl_sk_dikti;
    }

    /**
     * @return mixed
     */
    public function getTglSkDikti()
    {
        return $this->tgl_sk_dikti;
    }

    /**
     * @param mixed $jenis
     */
    public function setJenis($jenis)
    {
        $this->jenis = $jenis;
    }

    /**
     * @return mixed
     */
    public function getJenis()
    {
        return substr( $this->kode, -1 );
    }

    /**
     * @param mixed $nama_fakultas
     */
    public function setNamaFakultas($nama_fakultas)
    {
        $this->nama_fakultas = $nama_fakultas;
    }

    /**
     * @return mixed
     */
    public function getNamaFakultas()
    {
        return $this->nama_fakultas;
    }

}