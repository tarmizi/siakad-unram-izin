<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Model;

use SIAKAD\Controller\LevelAkses as ControllerLevelAkses;
use SIAKAD\Controller\Routes;

/**
 * - admin <kode_view>
 * - akademik <kode_view>
 * - operator siakad <kode_view>
 * - operator fakultas <kode_view><kode_fakultas>
 * - operator prodi <kode_view><kode_prodi>
 * - dosen <kode_view> : kode dosen sudah menajdi username di tabel login
 * - mahasiswa <kode_view> : nim mahasiswa sudah menjadi username di tabel login
 *
 * Class LevelAkses
 * @package SIAKAD\Model
 */

class LevelAkses {

    private $kode_akses;
    private $nama_level;
    private $deskripsi;

    private $kode_view;
    private $kode_object;

    function is_view_admin() { return ControllerLevelAkses::$kode_views[ $this->kode_view ] == Routes::view_admin; }
    function is_view_operator_siakad() { return ControllerLevelAkses::$kode_views[ $this->kode_view ] == Routes::view_operator_siakad; }
    function is_view_operator_fakultas() { return ControllerLevelAkses::$kode_views[ $this->kode_view ] == Routes::view_operator_fakultas; }
    function is_view_operator_prodi() { return ControllerLevelAkses::$kode_views[ $this->kode_view ] == Routes::view_operator_prodi; }
    function is_view_dosen() { return ControllerLevelAkses::$kode_views[ $this->kode_view ] == Routes::view_dosen; }
    function is_view_mahasiswa() { return ControllerLevelAkses::$kode_views[ $this->kode_view ] == Routes::view_mahasiswa; }

    public function initKodeAkses()
    {
        $this->kode_akses = $this->kode_view;

        /**
         * yang menggunakan 2 bagian cuma operator fakultas
         * dan operator prodi, karena bagian kedua mengandung kode
         * masing-masing view
         */
        if ( $this->is_view_operator_fakultas() || $this->is_view_operator_prodi() )
            $this->kode_akses .= ControllerLevelAkses::delimiter . $this->kode_object;

        return $this;
    }

    public function fetchKodeAkses()
    {
        list( $view, $object ) = explode( ControllerLevelAkses::delimiter, $this->kode_akses );
        $this->setKodeView( $view )->setKodeObject( $object );
        return $this;
    }

    /**
     * @param $kode_object
     * @return $this
     */
    public function setKodeObject($kode_object)
    {
        $this->kode_object = $kode_object;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKodeObject()
    {
        return $this->kode_object;
    }

    /**
     * @param $kode_view
     * @return $this
     */
    public function setKodeView($kode_view)
    {
        $this->kode_view = $kode_view;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKodeView()
    {
        return $this->kode_view;
    }


    /**
     * @param $deskripsi
     * @return $this
     */
    public function setDeskripsi($deskripsi)
    {
        $this->deskripsi = $deskripsi;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeskripsi()
    {
        return $this->deskripsi;
    }

    /**
     * @param $kode_akses
     * @return $this
     */
    public function setKodeAkses($kode_akses)
    {
        $this->kode_akses = $kode_akses;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKodeAkses()
    {
        return $this->kode_akses;
    }

    /**
     * @param $nama_level
     * @return $this
     */
    public function setNamaLevel($nama_level)
    {
        $this->nama_level = $nama_level;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNamaLevel()
    {
        return $this->nama_level;
    }



} 