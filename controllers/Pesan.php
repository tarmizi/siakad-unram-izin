<?php
/**  
 * Canvas Production
 *
 * Author : Dani
 */

namespace SIAKAD\Controller;
use SIAKAD\Model\Pesan as ModelPesan;

class Pesan extends Databases {

    private $field_id;
    private $class_name;
    private $count_query;

    /** @var Pesan $instance */
    private static $instance;

    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function __construct() {
        parent::_connect();
        $this->field_id = 'tanggal_kirim';
        $this->class_name = '\SIAKAD\Model\Pesan';
        $this->table_name = 'pesan';
        $this->count_query = 'SELECT * FROM ' . $this->table_name;
    }

    /**
     * @param $id
     * @param string $by
     * @return \SIAKAD\Model\Pesan
     */
    function _get( $id, $by = '' ) {
        $query = 'SELECT * FROM ' . $this->table_name . ' WHERE `' . ( empty( $by ) ? $this->field_id : $id ) . '` = "' . $id . '"';
        return ( $return = mysql_fetch_object(
            mysql_query( $query ), $this->class_name
        ) ) ? $return : new $this->class_name;
    }

    function _gets( $args = array() ) {

        $return = array();

        $default_args = array(
            'id_dosen'                  => '',
            'nim_mahasiswa'             => '',
            'exclude'                   => array(),
            'conditions'                => '',
            'orderby'                   => $this->field_id,
            'order'                     => 'DESC',
            'number'                    => 10,
            'offset'                    => 0
        );

        $list_args = sync_default_params( $default_args, $args );
        $query = 'SELECT * FROM ' . $this->table_name . ' WHERE 1';

        /**
         * id_pengirim
         */
        if( !empty( $list_args[ 'id_dosen' ] ) )
            $query .= ' AND ' . $this->table_name . '.id_dosen = ' . $list_args[ 'id_dosen' ];

        /**
         * id_penerima
         */
        if( !empty( $list_args[ 'nim_mahasiswa' ] ) )
            $query .= ' AND ' . $this->table_name . '.nim_mahasiswa = ' . $list_args[ 'nim_mahasiswa' ];
        
        /**
         * exclude
         */
        if( !empty( $list_args[ 'exclude' ] ) ) {

            foreach( $list_args[ 'exclude' ] as $ex )
                $query .= ' AND `id` <> ' . $ex;

        }

        /**
         * untuk custom query pada conditions
         */
        if( !empty( $list_args[ 'conditions' ] ) ) {
            foreach( $list_args[ 'conditions' ] as $conditions )
                $query .= ' '.$conditions['logika'].' ' . $conditions['tabel'] . '.' . $conditions[ 'field' ] . ' ' . $conditions[ 'operator' ] . ' ' . $conditions[ 'comparison' ];
        }

        $this->count_query = $query;

        /**
         * orderby dan jenis order
         */
        $query .= ' ORDER BY `' . $list_args[ 'orderby' ] . '` ' . $list_args[ 'order' ];

        /**
         * limit
         */
        if( $list_args[ 'number' ] >= 0 )
            $query .= ' LIMIT ' . $list_args[ 'offset' ] . ', ' . $list_args[ 'number' ];

        $resource = mysql_query( $query );

        //echo $query . ' : ' . mysql_error();

        while( $row = mysql_fetch_object( $resource, $this->class_name ) )
            $return[] = $row;

        return $return;

    }
    
    function insert( ModelPesan $pesan ) {
        return parent::insert(
            $this->table_name,
            array(
                'id_pengirim',
                'id_penerima',
                'judul',
                'isi_pesan'
            ),
            array(
                array(
                    $pesan->getIdPengirim(),
                    $pesan->getIdPenerima(),
                    $pesan->getJudul(),
                    $pesan->getIsiPesan()
                )
            )
        );
    }

    function update( ModelPesan $pesan ) {
        return parent::update(
            $this->table_name,
            array(
                'id_pengirim',
                'id_penerima',
                'judul',
                'isi_pesan'
            ),
            array(
                $pesan->getIdPengirim(),
                $pesan->getIdPenerima(),
                $pesan->getJudul(),
                $pesan->getIsiPesan()
            ),
            array(
                'tanggal_kirim', $pesan->getTanggalKirim()
            )
        );
    }

    function delete( $kode ) {
        return parent::delete(
            $this->table_name,
            'tanggal_kirim',
            $kode
        );
    }
}