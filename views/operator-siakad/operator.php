<?php

namespace SIAKAD\Views\Operator\SIAKAD;

use SIAKAD\Controller\Helpers;
use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Login;
use SIAKAD\Controller\LevelAkses;
use SIAKAD\Controller\UserLevel;

use SIAKAD\Model\Login as ModelLogin;
use SIAKAD\Model\UserLevel as ModelUserLevel;
use SIAKAD\Model\LevelAkses as ModelLevelAkses;

Headers::get_instance()
    ->set_page_title( 'Operator Fakultas' )
    ->set_page_name( 'Operator' )
    ->set_page_sub_name( 'Operator Fakultas');

Contents::get_instance()->get_header();

$tingkat1 = Routes::get_instance()->get_tingkat( 1 );
$tingkat2 = Routes::get_instance()->get_tingkat( 2 );
$tingkat3 = Routes::get_instance()->get_tingkat( 3 );

$status = isset( $_REQUEST[ Helpers::status_param] ) ? $_REQUEST[ Helpers::status_param ] : '';

$is_perbaiki = Routes::get_instance()->is_tingkat( 4, 'perbaiki' );
$is_simpan = Routes::get_instance()->is_tingkat( 4, 'simpan' );
$is_hapus = Routes::get_instance()->is_tingkat( 4, 'hapus' );

$obj_login = $is_perbaiki ? Login::get_instance()->_get( Routes::get_instance()->get_tingkat( 5 ) ) : new ModelLogin();
$obj_user_level = new ModelUserLevel();

if( $is_simpan ) {
    $aksi = isset( $_REQUEST[ Helpers::aksi_param ] ) ? $_REQUEST[ Helpers::aksi_param ] : '';
    $aksi_perbarui = $aksi == Helpers::aksi_perbarui;
    $aksi_tambah = $aksi == Helpers::aksi_tambah;

    $obj_user_level->_init( $_REQUEST );

    if( $aksi_tambah ) {
        $obj_login->_init( $_REQUEST );
        siakad_redirect( SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . (
            Login::get_instance()->insert( $obj_login ) && UserLevel::get_instance()->insert( $obj_user_level ) ?
                DS . Helpers::aksi_perbaiki . DS . $obj_login->getUsername() . '?status=' . Helpers::status_tambah : '?status=' . Helpers::status_gagal
            )
        );
    }

    elseif( $aksi_perbarui ) {

        /**
         * ambil data awal dari database,
         * ini untuk menjaga password tidak dilihat di "view source"
         */
        $obj_login = Login::get_instance()->_get( $_REQUEST[ 'username' ] );
        $obj_login->_init( $_REQUEST );

        /** /
        echo '<pre>'; print_r( $obj_login ); echo '</pre>';
        echo '<pre>'; print_r( $obj_user_level ); echo '</pre>';
        echo '<pre>'; print_r( $_REQUEST ); echo '</pre>';
        /**/

        /***/
        siakad_redirect( SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . (
            Login::get_instance()->update( $obj_login ) && UserLevel::get_instance()->update( $obj_user_level ) ?
                DS . Helpers::aksi_perbaiki . DS . $obj_login->getUsername() . '?status=' . Helpers::status_perbarui : '?status=' . Helpers::status_gagal
            )
        );
        /**/
    }

} elseif( $is_hapus ) {
    siakad_redirect( SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . '?status=' . (
        Login::get_instance()->delete( Routes::get_instance()->get_tingkat( 5 ) )
        && UserLevel::get_instance()->delete( Routes::get_instance()->get_tingkat( 5 ) ) ?
            Helpers::status_hapus : Helpers::status_gagal
        )
    );
}

$operator_lists = Login::get_instance()->_gets( array(
    'level_akses' => LevelAkses::kode_view_operator_fakultas,
    'number' => -1
) );
$level_akses_lists = LevelAkses::get_instance()->_gets( array(
    'kode_akses' => LevelAkses::kode_view_operator_fakultas,
    'number' => -1
) );

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">

            <h1 class="page-header">Operator Fakultas</h1>

            <?php if( $status ) Helpers::get_instance()->set_message_object( 'Operator' )->display_message( $status ); ?>

            <div class="row">

                <div class="col-sm-8">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Kode Akses</th>
                            <th>Username</th>
                            <th>Level</th>
                            <th>Status</th>
                            <th>#</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php /** @var $operator ModelLogin */
                        foreach( $operator_lists as $operator ) : ?>
                            <tr>
                                <td><?php echo $operator->getKodeAkses(); ?></td>
                                <td><?php echo $operator->getUsername(); ?></td>
                                <td><?php echo $operator->getNamaLevel(); ?></td>
                                <td><?php echo Login::$status[ $operator->getStatus() ]; ?></td>
                                <td>
                                    <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3 . DS . Helpers::aksi_perbaiki . DS . $operator->getUsername(); ?>" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                                    <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3 . DS . Helpers::aksi_hapus . DS . $operator->getUsername(); ?>" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-<?php if( $is_perbaiki ) : ?>info<?php else : ?>primary<?php endif; ?>">
                        <div class="panel-heading">
                            <strong>
                                <?php if( $is_perbaiki ) : ?>
                                    <i class="glyphicon glyphicon-pencil"></i>
                                    &nbsp;&nbsp;Perbaiki
                                <?php else : ?>
                                    <i class="glyphicon glyphicon-plus"></i>
                                    &nbsp;&nbsp;Tambah
                                <?php endif; ?>
                                Operator
                            </strong>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal form-operator" role="form" action="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3 . DS . Helpers::aksi_simpan; ?>" method="post">
                                <div class="form-group">
                                    <label for="username" class="col-sm-4 control-label">Username</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="<?php echo $obj_login->getUsername(); ?>" <?php if( $is_perbaiki ) : ?>readonly<?php endif; ?> >
                                        <?php if( $is_perbaiki ) : ?><small><i>username tidak dapat dirubah, silakan menghapus kemudian membuat akun baru</i></small><?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-sm-4 control-label">Password</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                        <?php if( $is_perbaiki ) : ?><small><i>biarkan kosong jika tidak ingin mengganti password</i></small><?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="level_akses" class="col-sm-4 control-label">Level</label>
                                    <div class="col-sm-8">
                                        <select id="level_akses" name="level_akses" class="form-control">
                                            <option value>--pilih--</option>
                                            <?php /** @var $level_akses ModelLevelAkses */
                                            foreach( $level_akses_lists as $level_akses ) : ?>
                                                <option value="<?php echo $level_akses->getKodeAkses(); ?>" <?php if( $obj_login->getKodeAkses() == $level_akses->getKodeAkses() ) : ?>selected<?php endif; ?> ><?php echo $level_akses->getNamaLevel(); ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="status" class="col-sm-4 control-label">Status</label>
                                    <div class="col-sm-8">
                                        <select id="status" name="status" class="form-control">
                                            <option value>--pilih--</option>
                                            <?php foreach( Login::$status as $kode => $status ) : ?>
                                                <option value="<?php echo $kode; ?>" <?php if( $obj_login->getStatus() == $kode ) : ?>selected<?php endif; ?> ><?php echo $status; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-4">
                                        <input type="hidden" name="<?php echo Helpers::aksi_param; ?>" value="<?php echo $is_perbaiki ? Helpers::aksi_perbarui : Helpers::aksi_tambah; ?>">
                                        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                                        <?php if( $is_perbaiki ) : ?>
                                            <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . DS . Helpers::aksi_tambah; ?>" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Baru</a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </form>

                            <script>
                                $(document).ready(function() {
                                    $('.form-operator').bootstrapValidator({
                                        message: 'Data salah',
                                        feedbackIcons: {
                                            valid: 'glyphicon glyphicon-ok',
                                            invalid: 'glyphicon glyphicon-remove',
                                            validating: 'glyphicon glyphicon-refresh'
                                        },
                                        fields: {
                                            username: {
                                                validators: {
                                                    notEmpty: {
                                                        message: 'Username tidak boleh kosong'
                                                    },
                                                    regexp: {
                                                        regexp: /^[a-zA-Z0-9]+$/,
                                                        message: 'Hanya alphanumerik tanpa spasi'
                                                    }
                                                }
                                            },
                                            /** / jika perbaiki, memungkinkan untuk field password kosong
                                            password: {
                                                validators: {
                                                    notEmpty: {
                                                        message: 'Password tidak boleh kosong'
                                                    }
                                                }
                                            },
                                            /**/
                                            level_akses: {
                                                validators: {
                                                    notEmpty: {
                                                        message: 'Pilih salah satu level'
                                                    }
                                                }
                                            },
                                            status: {
                                                validators: {
                                                    notEmpty: {
                                                        message: 'Pilih salah satu status'
                                                    }
                                                }
                                            }

                                        }
                                    });
                                });
                            </script>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();