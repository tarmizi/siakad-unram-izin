<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Model;


class SyaratMK {

    private $id;
    private $id_kurikulum;
    private $urutan;
    private $kondisi;
    private $prioritas;
    private $id_mk;
    private $id_mk_prasyarat;
    private $sudah_ambil;
    private $id_indeks_nilai_prasyarat;
    private $syarat_sks_lulus;
    private $syarat_sks_berjalan;

    /**
     * @param mixed $input
     */
    public function setId($input)
    {
        $this->id = $input;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @param mixed $input
     */
    public function setIdKurikulum($input)
    {
        $this->id_kurikulum = $input;
    }

    /**
     * @return mixed
     */
    public function getIdKurikulum()
    {
        return $this->id_kurikulum;
    }
    
    /**
     * @param mixed $input
     */
    public function setUrutan($input)
    {
        $this->urutan = $input;
    }

    /**
     * @return mixed
     */
    public function getUrutan()
    {
        return $this->urutan;
    }
    
    /**
     * @param mixed $input
     */
    public function setKondisi($input)
    {
        $this->kondisi = $input;
    }

    /**
     * @return mixed
     */
    public function getKondisi()
    {
        return $this->kondisi;
    }
    
    /**
     * @param mixed $input
     */
    public function setPrioritas($input)
    {
        $this->prioritas = $input;
    }

    /**
     * @return mixed
     */
    public function getPrioritas()
    {
        return $this->prioritas;
    }
    
    /**
     * @param mixed $input
     */
    public function setIdMk($input)
    {
        $this->id_mk = $input;
    }

    /**
     * @return mixed
     */
    public function getIdMk()
    {
        return $this->id_mk;
    }
    
    /**
     * @param mixed $input
     */
    public function setIdMkPrasyarat($input)
    {
        $this->id_mk_prasyarat = $input;
    }

    /**
     * @return mixed
     */
    public function getIdMkPrasyarat()
    {
        return $this->id_mk_prasyarat;
    }
    
    /**
     * @param mixed $input
     */
    public function setSudahAmbil($input)
    {
        $this->sudah_ambil = $input;
    }

    /**
     * @return mixed
     */
    public function getSudahAmbil()
    {
        return $this->sudah_ambil;
    }
    
    /**
     * @param mixed $input
     */
    public function setIdIndeksNilaiPrasyarat($input)
    {
        $this->id_indeks_nilai_prasyarat = $input;
    }

    /**
     * @return mixed
     */
    public function getIdIndeksNilaiPrasyarat()
    {
        return $this->id_indeks_nilai_prasyarat;
    }
    
    /**
     * @param mixed $input
     */
    public function setSyaratSksLulus($input)
    {
        $this->syarat_sks_lulus = $input;
    }

    /**
     * @return mixed
     */
    public function getSyaratSksLulus()
    {
        return $this->syarat_sks_lulus;
    }
    
    /**
     * @param mixed $input
     */
    public function setSyaratSksBerjalan($input)
    {
        $this->syarat_sks_berjalan = $input;
    }

    /**
     * @return mixed
     */
    public function getSyaratSksBerjalan()
    {
        return $this->syarat_sks_berjalan;
    }
} 