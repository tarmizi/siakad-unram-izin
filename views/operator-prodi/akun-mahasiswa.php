<?php

namespace SIAKAD\Views\Operator\PRODI;

use SIAKAD\Controller\Helpers;
use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Mahasiswa;
use SIAKAD\Controller\ProgramStudi;
use SIAKAD\Controller\Agama;
use SIAKAD\Controller\Provinsi;
use SIAKAD\Controller\Konsentrasi;
use SIAKAD\Controller\PekerjaanWali;
use SIAKAD\Controller\Jenjang;
use SIAKAD\Controller\Sessions;

use SIAKAD\Model\Mahasiswa as ModelMahasiswa;
use SIAKAD\Model\ProgramStudi as ModelProgramStudi;
use SIAKAD\Model\Agama as ModelAgama;
use SIAKAD\Model\Provinsi as ModelProvinsi;
use SIAKAD\Model\Konsentrasi as ModelKonsentrasi;
use SIAKAD\Model\PekerjaanWali as ModelPekerjaanWali;
use SIAKAD\Model\Jenjang as ModelJenjang;

global $status, $tingkat1, $tingkat2, $tingkat3, $is_tambah, $is_perbaiki, $is_hapus, $is_simpan;
$obj_prodi = ProgramStudi::get_instance()->_get(
    Sessions::get_instance()->_retrieve()->getObjLevelAkses()->getKodeObject(), 'kode_prodi_unram'
);
if( $is_simpan ) {
    $aksi = isset( $_REQUEST[ Helpers::aksi_param ] ) ? $_REQUEST[ Helpers::aksi_param ] : '';
    $aksi_perbarui = $aksi == Helpers::aksi_perbarui;
    $aksi_tambah = $aksi == Helpers::aksi_tambah;
    $obj_mahasiswa = new ModelMahasiswa();
    $obj_mahasiswa->_init( $_REQUEST );

    if( $aksi_tambah )
        siakad_redirect( SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . (
            Mahasiswa::get_instance()->insert( $obj_mahasiswa ) ?
                DS . Helpers::aksi_perbaiki . DS . $obj_mahasiswa->getNIM() . '?status=' . Helpers::status_tambah : '?status=' . Helpers::status_gagal
            )
        );

    elseif( $aksi_perbarui )
        siakad_redirect( SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . (
            Mahasiswa::get_instance()->update( $obj_mahasiswa ) ?
                DS . Helpers::aksi_perbaiki . DS . $obj_mahasiswa->getNIM() . '?status=' . Helpers::status_perbarui : '?status=' . Helpers::status_gagal
            )
        );

} elseif( $is_hapus ) {
    siakad_redirect( SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . '?status=' . (
        Mahasiswa::get_instance()->delete(
            Routes::get_instance()->get_tingkat( 5 ) ) ? Helpers::status_hapus : Helpers::status_gagal
        )
    );
}

$default_params = array(
    'kode_PS'       => $obj_prodi->getKode(),
    'nama'          => '',
    'number'        => 20,
    'page'          => 1,
);
$list_params = sync_default_params( $default_params, $_GET );

/** ambil daftar prodi */
$daftar_prodi = ProgramStudi::get_instance()->_gets( array(
    'number'        => -1,
    'orderby'       => 'nama',
    'order'         => 'ASC'
) );

$daftar_prodi_by_fakultas = array();
/** @var $prodi ModelProgramStudi */
foreach( $daftar_prodi as $prodi )
    in_array( $prodi->getNamaFakultas(), $daftar_prodi_by_fakultas ) || $daftar_prodi_by_fakultas[ $prodi->getNamaFakultas() ][] = $prodi;

if( $is_tambah || $is_perbaiki ) :
    $obj_mahasiswa = Mahasiswa::get_instance()->_get( Routes::get_instance()->get_tingkat( 5 ) );
    $daftar_jenjang = Jenjang::get_instance()->_gets( array( 'number' => -1, 'order' => 'ASC' ) );
    $daftar_agama = Agama::get_instance()->_gets( array( 'number' => -1, 'order' => 'ASC' ) );
    $daftar_provinsi = Provinsi::get_instance()->_gets( array( 'number' => -1, 'order' => 'ASC' ) );
    $daftar_konsentrasi = Konsentrasi::get_instance()->_gets( array( 'kode_PS' => $obj_mahasiswa->getKodeProdi(), 'number' => -1 ) );
    $daftar_pekerjaan_wali = PekerjaanWali::get_instance()->_gets( array( 'number' => -1, 'order' => 'ASC' ) ); ?>

    <h1 class="page-header">
        Mahasiswa
        <small><?php if( $is_perbaiki ) : ?>Perbaiki<?php else : ?>Tambah<?php endif; ?></small>
    </h1>

    <?php if( $status ) Helpers::get_instance()->set_message_object( 'Mahasiswa' )->display_message( $status ); ?>

    <form action="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . DS . Helpers::aksi_simpan; ?>" class="form-mahasiswa form-horizontal" method="post">
    <!-- sebelah kiri -->
    <div class="col-md-6">
        <div class="form-group">
            <label for="NIM" class="col-xs-4 control-label">NIM</label>
            <div class="col-xs-8">
                <input id="NIM" name="NIM" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getNIM(); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="kode_prodi" class="col-xs-4 control-label">PRODI</label>
            <div class="col-xs-8">
                <select id="kode_prodi" name="kode_prodi" class="form-control">
                    <option value>--pilih--</option>
                    <?php foreach( $daftar_prodi_by_fakultas as $fakultas => $prodis ) : ?>
                        <optgroup label="<?php echo $fakultas; ?>">
                            <?php /** @var $prodi ModelProgramStudi **/
                            foreach( $prodis as $prodi ) : ?>
                                <option value="<?php echo $prodi->getKode(); ?>" <?php if( $prodi->getKode() == $obj_mahasiswa->getKodeProdi() ) : ?>selected<?php endif; ?> ><?php echo $prodi->getNamaPS(); ?></option>
                            <?php endforeach; ?>
                        </optgroup>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="nama" class="col-xs-4 control-label">Nama</label>
            <div class="col-xs-8">
                <input id="nama" name="nama" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getNama(); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="jenjang" class="col-xs-4 control-label">Jenjang</label>
            <div class="col-xs-8">
                <select id="jenjang" name="jenjang" class="form-control">
                    <option value>--pilih--</option>
                    <?php /** @var $jenjang ModelJenjang */
                    foreach( $daftar_jenjang as $jenjang ) : ?>
                        <option value="<?php echo $jenjang->getKode(); ?>" <?php if( $jenjang->getKode() == $obj_mahasiswa->getJenjang() ) : ?>selected<?php endif; ?> ><?php echo $jenjang->getNama(); ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="shift" class="col-xs-4 control-label">Shift</label>
            <div class="col-xs-8">
                <select id="shift" name="shift" class="form-control">
                    <option value>--pilih--</option>
                    <?php foreach( Mahasiswa::$shift as $initial => $means ) : ?>
                        <option value="<?php echo $initial; ?>" <?php if( $initial == $obj_mahasiswa->getShift() ) : ?>selected<?php endif; ?> ><?php echo $means; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="tempat_lahir" class="col-xs-4 control-label">Tempat Lahir</label>
            <div class="col-xs-8">
                <textarea id="tempat_lahir" name="tempat_lahir" class="form-control"><?php echo $obj_mahasiswa->getTempatLahir(); ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="tgl_lahir" class="col-xs-4 control-label">Tanggal Lahir</label>
            <div class="col-xs-8">
                <div class="input-group">
                    <input id="tgl_lahir" type="text" class="form-control" name="tgl_lahir" value="<?php echo $obj_mahasiswa->getTglLahir(); ?>"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="jns_kelamin" class="col-xs-4 control-label">Jenis Kelamin</label>
            <div class="col-xs-8">
                <select id="jns_kelamin" name="jns_kelamin" class="form-control">
                    <option value>--pilih--</option>
                    <?php foreach( Mahasiswa::$jenis_kelamin as $initial => $means ) : ?>
                        <option value="<?php echo $initial; ?>" <?php if( $initial == $obj_mahasiswa->getJnsKelamin() ) : ?>selected<?php endif; ?> ><?php echo $means; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="kode_agama" class="col-xs-4 control-label">Agama</label>
            <div class="col-xs-8">
                <select id="kode_agama" name="kode_agama" class="form-control">
                    <option value>--pilih--</option>
                    <?php /** @var $agama ModelAgama */
                    foreach( $daftar_agama as $agama ) : ?>
                        <option value="<?php echo $agama->getKode(); ?>" <?php if( $agama->getKode() == $obj_mahasiswa->getKodeAgama() ) : ?>selected<?php endif; ?> ><?php echo $agama->getNama(); ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="alamat" class="col-xs-4 control-label">Alamat</label>
            <div class="col-xs-8">
                <textarea id="alamat" name="alamat" class="form-control"><?php echo $obj_mahasiswa->getAlamat(); ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="kode_provinsi" class="col-xs-4 control-label">Provinsi</label>
            <div class="col-xs-8">
                <select id="kode_provinsi" name="kode_provinsi" class="form-control">
                    <option value>--pilih--</option>
                    <?php /** @var $provinsi ModelProvinsi */
                    foreach( $daftar_provinsi as $provinsi ) : ?>
                        <option value="<?php echo $provinsi->getKode(); ?>" <?php if( $provinsi->getKode() == $obj_mahasiswa->getKodeProvinsi() ) : ?>selected<?php endif; ?> ><?php echo $provinsi->getNama(); ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="kode_provinsi_asal_sma" class="col-xs-4 control-label">Provinsi SMS asal</label>
            <div class="col-xs-8">
                <select id="kode_provinsi_asal_sma" name="kode_provinsi_asal_sma" class="form-control">
                    <option value>--pilih--</option>
                    <?php /** @var $provinsi ModelProvinsi */
                    foreach( $daftar_provinsi as $provinsi ) : ?>
                        <option value="<?php echo $provinsi->getKode(); ?>" <?php if( $provinsi->getKode() == $obj_mahasiswa->getKodeProvinsiAsalSma() ) : ?>selected<?php endif; ?> ><?php echo $provinsi->getNama(); ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="tgl_masuk" class="col-xs-4 control-label">Tanggal Masuk</label>
            <div class="col-xs-8">
                <div class="input-group">
                    <input id="tgl_masuk" type="text" class="form-control" name="tgl_masuk" value="<?php echo $obj_mahasiswa->getTglMasuk(); ?>"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="tgl_lulus" class="col-xs-4 control-label">Tanggal Lulus</label>
            <div class="col-xs-8">
                <div class="input-group">
                    <input id="tgl_lulus" type="text" class="form-control" name="tgl_lulus" value="<?php echo $obj_mahasiswa->getTglLulus(); ?>"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="thn_masuk" class="col-xs-4 control-label">Tahun Masuk</label>
            <div class="col-xs-8">
                <input id="thn_masuk" name="thn_masuk" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getThnMasuk(); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="semester_awal" class="col-xs-4 control-label">Semester Awal</label>
            <div class="col-xs-8">
                <input id="semester_awal" name="semester_awal" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getSemesterAwal(); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="batas_studi" class="col-xs-4 control-label">Batas Studi</label>
            <div class="col-xs-8">
                <input id="batas_studi" name="batas_studi" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getBatasStudi(); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="status_kuliah" class="col-xs-4 control-label">Status</label>
            <div class="col-xs-8">
                <select id="status_kuliah" name="status_kuliah" class="form-control">
                    <option value>--pilih--</option>
                    <?php foreach( Mahasiswa::$status_kuliah as $singkatan => $status ) : ?>
                        <option value="<?php echo $singkatan; ?>" <?php if( $singkatan == $obj_mahasiswa->getStatusKuliah() ) : ?>selected<?php endif; ?> ><?php echo $status; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
    </div>

    <!-- sebelah kanan -->
    <div class="col-md-6">
        <div class="form-group">
            <label for="id_konsentrasi" class="col-xs-4 control-label">Konsentrasi</label>
            <div class="col-xs-8">
                <select id="id_konsentrasi" name="id_konsentrasi" class="form-control">
                    <option value>--pilih--</option>
                    <?php /** @var $konsentrasi ModelKonsentrasi */
                    foreach( $daftar_konsentrasi as $konsentrasi ) : ?>
                        <option value="<?php echo $konsentrasi->getId(); ?>" <?php if( $konsentrasi->getId() == $obj_mahasiswa->getIdKonsentrasi() ) : ?>selected<?php endif; ?> ><?php echo $konsentrasi->getNama(); ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="sks_diakui" class="col-xs-4 control-label">SMS diakui</label>
            <div class="col-xs-8">
                <input id="sks_diakui" name="sks_diakui" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getSksDiakui(); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="nomor_ijazah" class="col-xs-4 control-label">Nmor Ijazah</label>
            <div class="col-xs-8">
                <input id="nomor_ijazah" name="nomor_ijazah" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getNomorIjazah(); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="nama_wali" class="col-xs-4 control-label">Nama Wali</label>
            <div class="col-xs-8">
                <input id="nama_wali" name="nama_wali" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getNamaWali(); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="alamat_wali" class="col-xs-4 control-label">Alamat Wali</label>
            <div class="col-xs-8">
                <textarea id="alamat_wali" name="alamat_wali" class="form-control"><?php echo $obj_mahasiswa->getAlamatWali(); ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="kode_pekerjaan_wali" class="col-xs-4 control-label">Pekerjaan Wali</label>
            <div class="col-xs-8">
                <select id="kode_pekerjaan_wali" name="kode_pekerjaan_wali" class="form-control">
                    <option value>--pilih--</option>
                    <?php /** @var $pw ModelPekerjaanWali */
                    foreach( $daftar_pekerjaan_wali as $pw ) : ?>
                        <option value="<?php echo $pw->getId() ?>" <?php if( $pw->getId() == $obj_mahasiswa->getKodePekerjaanWali() ) : ?>selected<?php endif; ?> ><?php echo $pw->getNama(); ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="penghasilan_wali" class="col-xs-4 control-label">Penghasilan Wali</label>
            <div class="col-xs-8">
                <input id="penghasilan_wali" name="penghasilan_wali" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getPenghasilanWali(); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="tlp_wali" class="col-xs-4 control-label">Telp. Wali</label>
            <div class="col-xs-8">
                <input id="tlp_wali" name="tlp_wali" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getTlpWali(); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="nomor_transkrip" class="col-xs-4 control-label">Nomor Transkrip</label>
            <div class="col-xs-8">
                <input id="nomor_transkrip" name="nomor_transkrip" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getNomorTranskrip(); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="status_bayar" class="col-xs-4 control-label">Status Bayar</label>
            <div class="col-xs-8">
                <select id="status_bayar" name="status_bayar" class="form-control">
                    <option value>--pilih--</option>
                    <option value="1" <?php if( $obj_mahasiswa->getStatusBayar() == '1' ) : ?>selected<?php endif; ?> >Sudah Bayar</option>
                    <option value="0" <?php if( $obj_mahasiswa->getStatusBayar() == '0' ) : ?>selected<?php endif; ?> >Belum Bayar</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="status_masuk" class="col-xs-4 control-label">Status Masuk</label>
            <div class="col-xs-8">
                <select id="status_masuk" name="status_masuk" class="form-control">
                    <option value>--pilih--</option>
                    <option value="1" <?php if( $obj_mahasiswa->getStatusMasuk() == '1' ) : ?>selected<?php endif; ?> >Baru</option>
                    <option value="0" <?php if( $obj_mahasiswa->getStatusMasuk() == '0' ) : ?>selected<?php endif; ?> >Pindahan</option>
                </select>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-8 col-xs-offset-4 col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
            <input type="hidden" name="<?php echo Helpers::aksi_param; ?>" value="<?php echo $is_perbaiki ? Helpers::aksi_perbarui : Helpers::aksi_tambah; ?>">
            <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3; ?>" class="btn btn-danger"><i class="glyphicon glyphicon-arrow-left"></i> Kembali</a>
            <button class="btn btn-success" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
            <?php if( $is_perbaiki ) : ?>
                <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . DS . Helpers::aksi_tambah; ?>" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Baru</a>
            <?php endif; ?>
        </div>
    </div>
    </form>

    <script>
        $(document).ready(function() {
            $('#tgl_lahir').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0"
            });
            $('#tgl_masuk').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0"
            });
            $('#tgl_lulus').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0"
            });
            $('.form-mahasiswa').bootstrapValidator({
                message: 'Data salah',
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    NIM: {
                        validators: {
                            notEmpty: {
                                message: 'NIM tidak boleh kosong'
                            },
                            regexp: {
                                regexp: /^[A-Z0-9]+$/,
                                message: 'Huruf besar dan alphanumerik'
                            }
                        }
                    },
                    kode_prodi: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih salah satu PRODI'
                            }
                        }
                    },
                    nama: {
                        validators: {
                            notEmpty: {
                                message: 'Nama tidak boleh kosong'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9\s]+$/,
                                message: 'Alphanumerik'
                            }
                        }
                    },
                    jenjang: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih salah satu jenjang'
                            }
                        }
                    },
                    shift: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih salah satu shift'
                            }
                        }
                    },
                    jns_kelamin: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih salah satu jenis kelamin'
                            }
                        }
                    },
                    thn_masuk: {
                        validators: {
                            notEmpty: {
                                message: 'Tahun masuk tidak boleh kosong'
                            }
                        }
                    },
                    batas_studi: {
                        validators: {
                            notEmpty: {
                                message: 'Batas studi tidak boleh kosong'
                            }
                        }
                    },
                    status_kuliah: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih salah satu status kuliah'
                            }
                        }
                    },
                    id_konsentrasi: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih salah satu konsentrasi'
                            }
                        }
                    },
                    sks_diakui: {
                        validators: {
                            notEmpty: {
                                message: 'SKS diakui tidak boleh kosong'
                            }
                        }
                    },
                    status_bayar: {
                        validators: {
                            notEmpty: {
                                message: 'Status bayar tidak boleh kosong'
                            }
                        }
                    },
                    status_masuk: {
                        validators: {
                            notEmpty: {
                                message: 'Status masuk tidak boleh kosong'
                            }
                        }
                    }
                }
            });
        });
    </script>

<?php else :
    $daftar_mahasiswa = Mahasiswa::get_instance()->_gets( array(
        'kode_prodi'        => $list_params[ 'kode_PS' ],
        'nama'              => $list_params[ 'nama' ],
        'number'            => $list_params[ 'number' ],
        'offset'            => ( $list_params[ 'page' ] - 1 ) * $list_params[ 'number' ]
    ) );
    $daftar_mahasiswa_count = Mahasiswa::get_instance()->_count(); ?>

    <h1 class="page-header">
        Mahasiswa
        <small>Daftar</small>
    </h1>

    <?php if( $status ) Helpers::get_instance()->set_message_object( 'Mahasiswa' )->display_message( $status ); ?>

    <div class="row">
        <form>
            <div class="form-group">
                <div class="col-sm-2 col-lg-1">
                    <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . DS . Helpers::aksi_tambah; ?>" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Baru</a>
                </div>
                <?php if( Sessions::get_instance()->_retrieve()->getObjLevelAkses()->is_view_admin() ) : ?>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <select id="kode_PS" name="kode_PS" class="form-control">
                            <option value="-1">--pilih--</option>
                            <?php foreach( $daftar_prodi_by_fakultas as $fakultas => $prodis ) : ?>
                                <optgroup label="<?php echo $fakultas; ?>">
                                    <?php /** @var $prodi ModelProgramStudi **/
                                    foreach( $prodis as $prodi ) : ?>
                                        <option value="<?php echo $prodi->getKode(); ?>" <?php if( $prodi->getKode() == $list_params[ 'kode_PS' ] ) : ?>selected<?php endif; ?> ><?php echo $prodi->getNamaPS(); ?></option>
                                    <?php endforeach; ?>
                                </optgroup>
                            <?php endforeach; ?>
                        </select>
                    </div>
                <?php endif; ?>
                <div class="col-sm-4 col-md-4 col-lg-3">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="glyphicon glyphicon-search"></i></div>
                        <input name="nama" class="form-control" type="text" value="<?php echo $list_params[ 'nama' ]; ?>" placeholder="Nama mahasiswa">
                    </div>
                </div>
                <div class="col-sm-2 col-lg-1">
                    <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-filter"></i> Filter</button>
                </div>
            </div>
        </form>
    </div>
    <br/>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>NIM</th>
            <th>Nama</th>
            <th>JK</th>
            <th>Angkatan</th>
            <th>Batas Studi</th>
            <th>Status</th>
            <th>#</th>
        </tr>
        </thead>
        <tbody>
        <?php /** @var $mahasiswa ModelMahasiswa */
        foreach( $daftar_mahasiswa as $mahasiswa ) : ?>
            <tr>
                <td><?php echo $mahasiswa->getNIM(); ?></td>
                <td><?php echo $mahasiswa->getNama(); ?></td>
                <td><?php echo Mahasiswa::$jenis_kelamin[ $mahasiswa->getJnsKelamin() ]; ?></td>
                <td><?php echo $mahasiswa->getThnMasuk(); ?></td>
                <td><?php echo $mahasiswa->getBatasStudi(); ?></td>
                <td><?php echo Mahasiswa::$status_kuliah[ $mahasiswa->getStatusKuliah() ]; ?></td>
                <td>
                    <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS .$tingkat3 . DS . Helpers::aksi_hapus . DS . $mahasiswa->getNIM(); ?>" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                    <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS .$tingkat3 . DS . Helpers::aksi_perbaiki . DS . $mahasiswa->getNIM(); ?>" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?php $base_link = SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . '?';
    $base_link .= '&kode_PS=' . $list_params[ 'kode_PS' ];
    $base_link .= '&nama=' . $list_params[ 'nama' ];
    siakad_paging_nav( $base_link, $daftar_mahasiswa_count, $list_params[ 'page' ], 20 );
endif; ?>