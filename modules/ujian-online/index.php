<?php
/**
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace Ujian_Online;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;

if( !isset( $_REQUEST[ 'loggedin' ] ) ) siakad_redirect( Contents::get_instance()->get_module_path( Contents::path_type_uri ) . DS . 'login' );

Headers::get_instance()
    ->set_page_title( 'Ujian Online' );

Contents::get_instance()->get_header();

?>

    <div class="container soal">
        <div class="alert alert-info">
            <strong><i class="fa fa-lightbulb-o"></i> Info :</strong> semua aktifitas side (jawaban sudah dijawab maupun belum, waktu mulai, waktu terakhir, dan sisa waktu) secara otomtais direkam dan disimpan di server.
        </div>
        <div class="row">
            <div class="col-sm-8 main">
                <div class="content">
                    <ul id="jenis_soal" class="nav nav-tabs nav-justified" role="tablist">
                        <li class="active"><a href="#pilihan"><h3>Pilihan <span class="badge">42</span></h3></a></li>
                        <li><a href="#isian"><h3>Isian <span class="badge">8</span></h3></a></li>
                    </ul>
                    <div class="tab-content tab-soal">
                        <div class="tab-pane active soal-pilihan" id="pilihan">
                            <div class="row soal-item">
                                <div class="col-xs-1 text-right">
                                    <strong>1</strong>
                                </div>
                                <div class="col-xs-10">
                                    <p class="soal-title">
                                        Berapa lama waktu yang diperlukan presiden Joko Widodo untuk membuat programmer stress?
                                    </p>
                                    <p class="soal-description">

                                    </p>
                                    <div class="soal-pilihan-jawaban">
                                        <label><input type="radio" name="soal-2" id="a">&nbsp;A&nbsp;&nbsp;&nbsp;2 Jam</label><br/>
                                        <label><input type="radio" name="soal-2" id="b">&nbsp;B&nbsp;&nbsp;&nbsp;2 Hari</label><br/>
                                        <label><input type="radio" name="soal-2" id="c">&nbsp;C&nbsp;&nbsp;&nbsp;2 Minggu</label><br/>
                                        <label><input type="radio" name="soal-2" id="d">&nbsp;D&nbsp;&nbsp;&nbsp;2 Bulan</label><br/>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row soal-item">
                                <div class="col-xs-1 text-right">
                                    <strong>2</strong>
                                </div>
                                <div class="col-xs-10">
                                    <p class="soal-title">
                                        Young Stars
                                    </p>
                                    <p class="soal-description">
                                        The finalists of "Akademi Fantasi Indosiar 1" (AFI) are wonderful young people.<br/>
                                        Mawar who was born on 26 February 1985 is a cute girl. She has straight, short hair. Her bright skin, chubby cheeks, and lovely smile make her look very marvellous. She is not very tall. However, her weight which is 40 kgs matches her body well and makes her look cute.<br/>
                                        Unlike Mawar, Ve looks tall. She is 1.69 meters tall. She looks quite slim. She weighs 45 kgs. Compared to Mawar, Ve looks darker. The 22 years-old girl has black, straight hair.<br/>
                                        Another finalist is Ismail who is better known as Smile. The young man who was born on 16 September 1983 looks much bigger and taller than his two female fiends. He is tall and muscular. His complexion is fair and his hair is short and straight.<br/>
                                        <br/>The text is about...
                                    </p>
                                    <div class="soal-pilihan-jawaban">
                                        <label><input type="radio" name="soal-2" id="a">&nbsp;A&nbsp;&nbsp;&nbsp;Mawar AFI</label><br/>
                                        <label><input type="radio" name="soal-2" id="b">&nbsp;B&nbsp;&nbsp;&nbsp;Ve AFI</label><br/>
                                        <label><input type="radio" name="soal-2" id="c">&nbsp;C&nbsp;&nbsp;&nbsp;Ismail AFI</label><br/>
                                        <label><input type="radio" name="soal-2" id="d">&nbsp;D&nbsp;&nbsp;&nbsp;The finalists of AFI</label><br/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane soal-isian" id="isian">
                            <div class="row soal-item">
                                <div class="col-xs-1 text-right">
                                    <strong>4</strong>
                                </div>
                                <div class="col-xs-10">
                                    <p class="soal-title">
                                        Jelaskan apa yang side ketahui tentang PHP (Pemberi Harapan Palsu)
                                    </p>
                                    <p class="soal-description">

                                    </p>
                                    <div class="soal-isian-jawaban">
                                        <textarea class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row soal-item">
                                <div class="col-xs-1 text-right">
                                    <strong>5</strong>
                                </div>
                                <div class="col-xs-10">
                                    <p class="soal-title">
                                        Bagaimana menurut side jika Indonesia memiliki 2 pemimpin aktif?
                                    </p>
                                    <p class="soal-description">

                                    </p>
                                    <div class="soal-isian-jawaban">
                                        <textarea class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 sidebar">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong>Ahmad Zafrullah</strong>
                    </div>
                    <div class="panel-body">
                        <div class="text-center">
                            <h4 class="text-primary"><strong>Dasar Pemrograman</strong></h4>
                            <br/>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="thumbnail">
                                    <img src="<?php echo SIAKAD_IMAGE_URI_PATH; ?>/uploads/linux.png" class="img-circle">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h4 class="text-success">F1B008004</h4>
                                <p><strong><i class="fa fa-bookmark-o"></i>&nbsp;&nbsp;Teknik Elektro</strong></p>
                                <p><strong><i class="fa fa-building-o"></i>&nbsp;&nbsp;Fakultas Teknik</strong></p>
                            </div>
                        </div>
                        <table class="table">
                            <tbody>
                            <tr>
                                <td><strong><i class="fa fa-file-o"></i>&nbsp;&nbsp;Jumlah Soal</strong></td>
                                <td>:</td>
                                <td>43/50</td>
                            </tr><tr>
                                <td><strong><i class="fa fa-clock-o"></i>&nbsp;&nbsp;Waktu</strong></td>
                                <td>:</td>
                                <td>1 jam</td>
                            </tr><tr>
                                <td><strong><i class="fa fa-play"></i>&nbsp;&nbsp;Mulai</strong></td>
                                <td>:</td>
                                <td><?php echo date( 'H:i:s' ); ?></td>
                            </tr><tr>
                                <td><strong><i class="fa fa-stop"></i>&nbsp;&nbsp;Selesai</strong></td>
                                <td>:</td>
                                <td><?php echo date( 'H:i:s' ); ?></td>
                            </tr><tr>
                                <td><strong><i class="fa fa-bomb"></i>&nbsp;&nbsp;Sisa</strong></td>
                                <td>:</td>
                                <td><?php echo date( 'H:i:s' ); ?></td>
                            </tr>
                            </tbody>
                        </table>
                        <button class="btn btn-lg btn-primary btn-block">Selesai</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php Contents::get_instance()->get_footer();