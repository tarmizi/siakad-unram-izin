<?php

namespace SIAKAD\Views\Operator\Fakultas;

use SIAKAD\Controller\ProgramStudi;
use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Helpers;
use SIAKAD\Controller\Fakultas;

use SIAKAD\Model\ProgramStudi as ModelProgramStudi;

$tingkat1 = Routes::get_instance()->get_tingkat( 1 );
$tingkat2 = Routes::get_instance()->get_tingkat( 2 );

$status = isset( $_REQUEST[ Helpers::status_param] ) ? $_REQUEST[ Helpers::status_param ] : '';

$is_tambah = Routes::get_instance()->is_tingkat( 3, 'tambah' );
$is_perbaiki = Routes::get_instance()->is_tingkat( 3, 'perbaiki' );
$is_simpan = Routes::get_instance()->is_tingkat( 3, 'simpan' );
$is_hapus = Routes::get_instance()->is_tingkat( 3, 'hapus' );

$obj_prodi = $is_perbaiki ? ProgramStudi::get_instance()->_get( Routes::get_instance()->get_tingkat( 4 ) ) : new ModelProgramStudi();
//echo '<pre>'; print_r( $obj_prodi ); echo '</pre>';

if( $is_simpan ) {
    $aksi = isset( $_REQUEST[ Helpers::aksi_param ] ) ? $_REQUEST[ Helpers::aksi_param ] : '';
    $aksi_perbarui = $aksi == Helpers::aksi_perbarui;
    $aksi_tambah = $aksi == Helpers::aksi_tambah;
    $obj_prodi->_init( $_REQUEST );

    if( $aksi_tambah )
    //{echo '<pre>'; print_r( $obj_prodi ); echo '</pre>';}
        /***/
        siakad_redirect( SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . (
            ProgramStudi::get_instance()->insert( $obj_prodi ) ?
                DS . Helpers::aksi_perbaiki . DS .$obj_prodi->getKode() . '?status=1' : '?status=4'
            )
        );
        /**/

    elseif( $aksi_perbarui )
    //{echo '<pre>'; print_r( $obj_prodi ); echo '</pre>';}
        /***/
        siakad_redirect( SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . (
            ProgramStudi::get_instance()->update( $obj_prodi ) ?
                DS . Helpers::aksi_perbaiki . DS .$obj_prodi->getKode() . '?status=2' : '?status=4'
            )
        );
        /**/

}

elseif( $is_hapus )
    siakad_redirect( SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . '?status=' . (
        ProgramStudi::get_instance()->delete(
            Routes::get_instance()->get_tingkat( 4 ) ) ? 3 : 4
        )
    );

/** ambil daftar fakultas */
$daftar_fakultas = Fakultas::get_instance()->_gets( array(
    'number'        => -1,
    'orderby'       => 'nama',
    'order'         => 'ASC'
) );

/** ambil daftar prodi */
$default_params = array(
    'kode_fakultas' => '',
    'nama_PS'       => '',
    'number'        => -1,
    'page'          => 1,
);

$list_params = sync_default_params( $default_params, $_GET );

$daftar_prodi = ProgramStudi::get_instance()->_gets( array(
    'kode_fakultas' => $list_params[ 'kode_fakultas' ],
    'nama_PS' => $list_params[ 'nama_PS' ],
    'number' => $list_params[ 'number' ],
    'offset' => ( $list_params[ 'page' ] - 1 ) * $list_params[ 'number' ]
) );

Headers::get_instance()
    ->set_page_title( 'Program Studi' )
    ->set_page_name( 'Daftar PRODI' );
Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <div class="row">
                <div class="col-xs-12">

                    <h1 class="page-header">
                        Program Studi
                        <small><?php if( $is_tambah ) : ?>Tambah<?php elseif( $is_perbaiki ) : ?>Perbaiki<?php else : ?>Daftar<?php endif; ?></small>
                    </h1>

                    <?php if( $status ) Helpers::get_instance()->set_message_object( 'Program Studi' )->display_message( $status ); ?>

                    <?php if( $is_tambah || $is_perbaiki ) : ?>
                        <form action="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . Helpers::aksi_simpan; ?>" class="form-prodi form-horizontal" method="post">

                            <!-- sebelah kiri -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="kode_nasional" class="col-xs-4 control-label">Kode Nasional</label>
                                    <div class="col-xs-8">
                                        <input id="kode_nasional" name="kode_nasional" type="text" class="form-control" placeholder="Kode nasional" value="<?php echo $obj_prodi->getKodeNasional(); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="jenis" class="col-xs-4 control-label">Jenis</label>
                                    <div class="col-xs-8">
                                        <select id="jenis" name="jenis" class="form-control">
                                            <option value>--pilih--</option>
                                            <option value="1" <?php if( $obj_prodi->getJenis() == 1 ) : ?>selected<?php endif; ?> >Reguler</option>
                                            <option value="0" <?php if( $obj_prodi->getJenis() == 0 ) : ?>selected<?php endif; ?> >Ekstensi</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="kode_fakultas" class="col-xs-4 control-label">Fakultas</label>
                                    <div class="col-xs-8">
                                        <select id="kode_fakultas" name="kode_fakultas" class="form-control">
                                            <option value>--pilih--</option>
                                            <?php /** @var $fakultas \SIAKAD\Model\Fakultas */
                                            foreach( $daftar_fakultas as $fakultas ) : ?>
                                                <option value="<?php echo $fakultas->getKode(); ?>" <?php if( $fakultas->getKode() == $obj_prodi->getKodeFakultas() ) : ?>selected<?php endif; ?> ><?php echo $fakultas->getNama(); ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="kode_prodi_unram" class="col-xs-4 control-label">Kode PRODI UNRAM</label>
                                    <div class="col-xs-8">
                                        <input id="kode_prodi_unram" name="kode_prodi_unram" type="text" class="form-control" placeholder="Kode dari UNRAM" value="<?php echo $obj_prodi->getKodeProdiUnram(); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama_nasional" class="col-xs-4 control-label">Nama Nasional</label>
                                    <div class="col-xs-8">
                                        <input id="nama_nasional" name="nama_nasional" type="text" class="form-control" placeholder="Nama dari nasional" value="<?php echo $obj_prodi->getNamaNasional(); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama_nasional_ingg" class="col-xs-4 control-label">Nama Nasional (Ingg)</label>
                                    <div class="col-xs-8">
                                        <input id="nama_nasional_ingg" name="nama_nasional_ingg" type="text" class="form-control" placeholder="Nama dari nasional Ingg" value="<?php echo $obj_prodi->getNamaNasionalIngg(); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="jenjang" class="col-xs-4 control-label">Jenjang</label>
                                    <div class="col-xs-8">
                                        <select id="jenjang" name="jenjang" class="form-control">
                                            <option value>--pilih--</option>
                                            <?php foreach( ProgramStudi::$jenjang as $jenjang ) : ?>
                                                <option value="<?php echo $jenjang; ?>" <?php if( $jenjang == $obj_prodi->getJenjang() ) : ?>selected<?php endif; ?> ><?php echo $jenjang; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama_PS" class="col-xs-4 control-label">Nama PRODI UNRAM</label>
                                    <div class="col-xs-8">
                                        <input id="nama_PS" name="nama_PS" type="text" class="form-control" placeholder="Nama dari UNRAM" value="<?php echo $obj_prodi->getNamaPS(); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="keterangan" class="col-xs-4 control-label">Keterangan</label>
                                    <div class="col-xs-8">
                                        <textarea id="keterangan" name="keterangan" class="form-control"><?php echo $obj_prodi->getKeterangan(); ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="kurikulum_pakai" class="col-xs-4 control-label">Kurikulum Pakai</label>
                                    <div class="col-xs-8">
                                        <select id="kurikulum_pakai" name="kurikulum_pakai" class="form-control">
                                            <option value>--pilih--</option>
                                            <option value="1" <?php if( $obj_prodi->getKurikulumPakai() == 1 ) : ?>selected<?php endif; ?> >Semua</option>
                                            <option value="0" <?php if( $obj_prodi->getKurikulumPakai() == 0 ) : ?>selected<?php endif; ?> >Sebagian</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="alamat_prodi" class="col-xs-4 control-label">Alamat</label>
                                    <div class="col-xs-8">
                                        <textarea id="alamat_prodi" name="alamat_prodi" class="form-control"><?php echo $obj_prodi->getAlamatProdi(); ?></textarea>
                                    </div>
                                </div>
                            </div>

                            <!-- sebelah kanan -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="sk_dikti" class="col-xs-4 control-label">SK DIKTI</label>
                                    <div class="col-xs-8">
                                        <input id="sk_dikti" name="sk_dikti" type="text" class="form-control" value="<?php echo $obj_prodi->getSkDikti(); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tgl_sk_dikti" class="col-xs-4 control-label">Tanggal SK DIKTI Mulai</label>
                                    <div class="col-xs-8">
                                        <div class="input-group date">
                                            <input id="tgl_sk_dikti" type="text" class="form-control" name="tgl_sk_dikti" value="<?php echo $obj_prodi->getTglSkDikti(); ?>"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tgl_akhir_sk_dikti" class="col-xs-4 control-label">Tanggal SK DIKTI Berakhir</label>
                                    <div class="col-xs-8">
                                        <div class="input-group date">
                                            <input id="tgl_akhir_sk_dikti" type="text" class="form-control" name="tgl_akhir_sk_dikti" value="<?php echo $obj_prodi->getTglAkhirSkDikti(); ?>"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="jumlah_sks_kelulusan" class="col-xs-4 control-label">Jumlah SKS Kelulusan</label>
                                    <div class="col-xs-8">
                                        <input id="jumlah_sks_kelulusan" name="jumlah_sks_kelulusan" type="text" class="form-control" value="<?php echo $obj_prodi->getJumlahSksKelulusan(); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="status" class="col-xs-4 control-label">Status</label>
                                    <div class="col-xs-8">
                                        <select id="status" name="status" class="form-control">
                                            <option value="A" <?php if( $obj_prodi->getStatus() == 'A' ) : ?>selected<?php endif; ?> >Aktif</option>
                                            <option value="T" <?php if( $obj_prodi->getStatus() == 'T' ) : ?>selected<?php endif; ?> >Tidak Aktif</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tahun_semester_mulai" class="col-xs-4 control-label">Tahun Semester Mulai</label>
                                    <div class="col-xs-8">
                                        <input id="tahun_semester_mulai" name="tahun_semester_mulai" type="text" class="form-control" value="<?php echo $obj_prodi->getTahunSemesterMulai(); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-xs-4 control-label">Email</label>
                                    <div class="col-xs-8">
                                        <input id="email" name="email" type="text" class="form-control" value="<?php echo $obj_prodi->getEmail(); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tgl_awal_pendirian" class="col-xs-4 control-label">Tanggal Awal Pendirian</label>
                                    <div class="col-xs-8">
                                        <div class="input-group date">
                                            <input id="tgl_awal_pendirian" type="text" class="form-control" name="tgl_awal_pendirian" value="<?php echo $obj_prodi->getTglAwalPendirian(); ?>"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="sk_ban_pt" class="col-xs-4 control-label">SK BAN PT</label>
                                    <div class="col-xs-8">
                                        <input id="sk_ban_pt" name="sk_ban_pt" type="text" class="form-control" value="<?php echo $obj_prodi->getSkBanPt(); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tgl_sk_ban_pt" class="col-xs-4 control-label">Tanggal SK BAN PT Mulai</label>
                                    <div class="col-xs-8">
                                        <div class="input-group date">
                                            <input id="tgl_sk_ban_pt" type="text" class="form-control" name="tgl_sk_ban_pt" value="<?php echo $obj_prodi->getTglSkBanPt(); ?>"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tgl_akhir_sk_akreditasi" class="col-xs-4 control-label">Tanggal SK Akreditasi Berakhir</label>
                                    <div class="col-xs-8">
                                        <div class="input-group date">
                                            <input id="tgl_akhir_sk_akreditasi" type="text" class="form-control" name="tgl_akhir_sk_akreditasi" value="<?php echo $obj_prodi->getTglAkhirSkAkreditasi(); ?>"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="kode_akreditasi" class="col-xs-4 control-label">Kode Akreditasi</label>
                                    <div class="col-xs-8">
                                        <select id="kode_akreditasi" name="kode_akreditasi" class="form-control">
                                            <option value>--pilih--</option>
                                            <?php foreach( ProgramStudi::$akreditasi as $akreditasi ) : ?>
                                                <option value="<?php echo $akreditasi; ?>" <?php if( $akreditasi == $obj_prodi->getKodeAkreditasi() ) : ?>selected<?php endif; ?> ><?php echo $akreditasi; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-xs-8 col-xs-offset-4 col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                    <input type="hidden" name="aksi" value="<?php echo $is_perbaiki ? Helpers::aksi_perbarui : Helpers::aksi_tambah; ?>">
                                    <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2; ?>" class="btn btn-danger"><i class="glyphicon glyphicon-arrow-left"></i> Kembali</a>
                                    <button class="btn btn-success" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                                    <?php if( $is_perbaiki ) : ?>
                                        <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . Helpers::aksi_tambah; ?>" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Baru</a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </form>
                    <?php else : ?>
                        <div class="row">
                            <form>
                                <div class="form-group">
                                    <div class="col-sm-2 col-lg-1">
                                        <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . Helpers::aksi_tambah; ?>" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Baru</a>
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-lg-3">
                                        <select name="kode_fakultas" class="form-control">
                                            <option value>--pilih--</option>
                                            <?php /** @var $fakultas \SIAKAD\Model\Fakultas */
                                            foreach( $daftar_fakultas as $fakultas ) : ?>
                                                <option value="<?php echo $fakultas->getKode(); ?>" <?php if( $fakultas->getKode() == $list_params[ 'kode_fakultas' ] ) : ?>selected<?php endif; ?> ><?php echo $fakultas->getNama(); ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-lg-3">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="glyphicon glyphicon-search"></i></div>
                                            <input name="nama_PS" class="form-control" type="text" value="<?php echo $list_params[ 'nama_PS' ]; ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-lg-1">
                                        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-filter"></i> Filter</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <br/>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Kode Prodi</th>
                                <th>Nama PRODI</th>
                                <th>Fakultas</th>
                                <th>Jenjang</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php /** @var $prodi \SIAKAD\Model\ProgramStudi */
                            foreach( $daftar_prodi as $prodi ) : ?>
                                <tr>
                                    <td><?php echo $prodi->getKodeProdiUnram(); ?></td>
                                    <td><?php echo $prodi->getNamaPS(); ?></td>
                                    <td><?php echo $prodi->getNamaFakultas(); ?></td>
                                    <td><?php echo $prodi->getJenjang(); ?></td>
                                    <td>
                                        <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . Helpers::aksi_hapus . DS . $prodi->getKode(); ?>" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                                        <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . Helpers::aksi_perbaiki . DS . $prodi->getKode(); ?>" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

    <script>
        $(document).ready(function() {
            $('#tgl_sk_dikti').datepicker();
            $('#tgl_akhir_sk_dikti').datepicker();
            $('#tgl_awal_pendirian').datepicker();
            $('#tgl_sk_ban_pt').datepicker();
            $('#tgl_akhir_sk_akreditasi').datepicker();
            $('.form-prodi').bootstrapValidator({
                message: 'Data salah',
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    kode_nasional: {
                        validators: {
                            notEmpty: {
                                message: 'Kode tidak boleh kosong'
                            },
                            stringLength: {
                                min: 5,
                                message: 'Kode minimal 5 karakter'
                            },
                            regexp: {
                                regexp: /^[0-9]+$/,
                                message: 'Hanya angka'
                            }
                        }
                    },
                    jenis: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih salah satu jenis'
                            }
                        }
                    },
                    kode_fakultas: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih salah satu fakultas'
                            }
                        }
                    },
                    kode_prodi_unram: {
                        validators: {
                            notEmpty: {
                                message: 'Kode tidak boleh kosong'
                            },
                            stringLength: {
                                min: 2,
                                max: 2,
                                message: 'Kode hanya 2 karakter'
                            },
                            regexp: {
                                regexp: /^[A-Z]+$/,
                                message: 'Hanya huruf besar'
                            }
                        }
                    },
                    nama_nasional: {
                        validators: {
                            notEmpty: {
                                message: 'Nama tidak boleh kosong'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z\s]+$/,
                                message: 'Hanya huruf (besar/kecil) dan spasi saja'
                            }
                        }
                    },
                    jenjang: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih salah satu jenjang'
                            }
                        }
                    },
                    nama_PS: {
                        validators: {
                            notEmpty: {
                                message: 'Nama tidak boleh kosong'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z\s]+$/,
                                message: 'Hanya huruf (besar/kecil) dan spasi saja'
                            }
                        }
                    },
                    kurikulum_pakai: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih salah satu kurikulum'
                            }
                        }
                    },
                    jumlah_sks_kelulusan: {
                        validators: {
                            notEmpty: {
                                message: 'Jumlah SKS tidak boleh kosong'
                            },
                            integer: {
                                message: 'Format data salah, data harus angka'
                            }
                        }
                    },
                    tahun_semester_mulai: {
                        validators: {
                            notEmpty: {
                                message: 'Tahun semester mulai tidak boleh kosong'
                            },
                            stringLength: {
                                min: 4,
                                max: 4,
                                message: 'Format tahun salah, format YYYY (misal 2014)'
                            },
                            integer: {
                                message: 'Isian harus angka'
                            }
                        }
                    },
                    email: {
                        validators: {
                            emailAddress: {
                                message: 'Format email salah, misal psti@ft.unram.ac.id'
                            }
                        }
                    },
                    kode_akreditasi: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih salah satu akreditasi'
                            }
                        }
                    }
                }
            });
        });
    </script>

<?php Contents::get_instance()->get_footer();