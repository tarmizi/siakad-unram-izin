<?php

namespace SIAKAD\Views\Operator\PRODI;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Mahasiswa;
use SIAKAD\Controller\Sessions;
use SIAKAD\Controller\Nilai;
use SIAKAD\Controller\Matakuliah;
use SIAKAD\Controller\IndeksNilai;

Headers::get_instance()
    ->set_page_title( 'Transkrip Nilai' )
    ->set_page_name( 'Transkrip Nilai' );

$tingkat1 = Routes::get_instance()->get_tingkat( 1 );
$tingkat2 = Routes::get_instance()->get_tingkat( 2 );
$tingkat3 = Routes::get_instance()->get_tingkat( 3 );

Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">
                Nilai
            </h1>
            <form class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-2">
                        <select class="form-control">
                            <option>--tahun ajaran</option>
                            <option>2010</option>
                            <option>2011</option>
                            <option>2012</option>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select class="form-control">
                            <option>--mata kuliah</option>
                            <option>Bahasa Sasak</option>
                        </select>
                    </div>
                    <div class="col-sm-1">
                        <button class="btn btn-primary"><i class="fa fa-legal"></i> OK</button>
                    </div>
                </div>
            </form>
            <br/>
            <?php $me = Mahasiswa::get_instance()->_get(Sessions::get_instance()->_retrieve()->getObjLogin()->getUsername());
                  $daftar_nilai = Nilai::get_instance()->_gets(array('nim'=>$me->getNIM(),'orderby'=>'tahun_akademik','order'=>'ASC'));
            ?>
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Tahun Akademik</th>
                    <th>Mata Kuliah</th>
                    <th>Sks</th>
                    <th>Nilai</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach( $daftar_nilai as $nilai ) : ?>
                    <tr>
                        <td><?php echo ($nilai->getTahunAkademik()/10); ?></td>
                        <td><?php echo Matakuliah::get_instance()->_get($nilai->getIdMk())->getNamaMk(); ?></td>
                        <td><?php echo Matakuliah::get_instance()->_get($nilai->getIdMk())->getSks(); ?></td>
                        <td><?php echo IndeksNilai::get_instance()->_get($nilai->getIndeksNilai())->getHuruf(); ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
                <button class="btn btn-primary"><i class="fa fa-print"></i> Cetak</button>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();