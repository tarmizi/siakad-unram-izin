<?php
/**  
 * Canvas Production
 *
 * Author : Dani
 */

namespace SIAKAD\Model;


class Pesan {

    private $tanggal_kirim;
    private $id_pengirim;
    private $id_penerima;
    private $judul;
    private $isi_pesan;

    /**
     * @param mixed $tanggal_kirim
     */
    public function setTanggalKirim($tanggal_kirim)
    {
        $this->tanggal_kirim = $tanggal_kirim;
    }

    /**
     * @return mixed
     */
    public function getTanggalKirim()
    {
        return $this->tanggal_kirim;
    }
    
    /**
     * @param mixed $id_pengirim
     */
    public function setIdPengirim($id_pengirim)
    {
        $this->id_pengirim = $id_pengirim;
    }

    /**
     * @return mixed
     */
    public function getIdPengirim()
    {
        return $this->id_pengirim;
    }

    /**
     * @param mixed $id_penerima
     */
    public function setIdPenerima($id_penerima)
    {
        $this->id_penerima= $id_penerima;
    }

    /**
     * @return mixed
     */
    public function getIdPenerima()
    {
        return $this->id_penerima;
    }

    /**
     * @param mixed $judul
     */
    public function setJudul($judul)
    {
        $this->judul = $judul;
    }

    /**
     * @return mixed
     */
    public function getJudul()
    {
        return $this->judul;
    }

    /**
     * @param mixed $isi_pesan
     */
    public function setIsiPesan($isi_pesan)
    {
        $this->isi_pesan = $isi_pesan;
    }

    /**
     * @return mixed
     */
    public function getIsiPesan()
    {
        return $this->isi_pesan;
    }
} 