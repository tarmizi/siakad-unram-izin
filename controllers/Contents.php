<?php

/**
 * Class ini berisi fungsi untuk keperluan
 * operasi komponen halaman
 */

namespace SIAKAD\Controller;

class Contents {

    const path_type_uri = 'uri';
    const path_type_abs = 'abs';
    const state_view = 'view';
    const state_module = 'module';

    /**
     * tempet nyimpen state {view|module}
     * defaultnya adalah view
     */
    private $state = self::state_view;

    /** tempet nyimpen view */
    private $view;

    /** tempet nyimpen module */
    private $module;

    /**
     * tempet nyimpen sementara instance dari class Contents
     *
     * @var $instance Contents
     */
    private static $instance;

    const type_header = 'header';
    const type_sidebar = 'sidebar';
    const type_footer = 'footer';

    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function set_state( $state ) {
        $this->state = $state;
        return $this;
    }
    function is_state_module() {
        return $this->state == self::state_module;
    }

    function set_view( $view ) {
        $this->view = $view;
        return $this;
    }
    function remove_view() {
        return $this->set_view( '' );
    }
    function get_view() {
        return $this->view;
    }
    function get_view_path() {
        return SIAKAD_VIEW_ABS_PATH . DS . $this->view;
    }

    function set_module( $module ) {
        $this->module = $module;
        return $this;
    }
    function get_module() {
        return $this->module;
    }
    function get_module_path( $type = self::path_type_abs ) {
        return self::path_type_uri == $type ? SIAKAD_URI_PATH . DS . $this->module : SIAKAD_MODULE_ABS_PATH . DS . $this->module;
    }

    /**
     * karena `module` nya bentuk dinamis,
     * maka untuk definisi pathnya tidak bisa menggunakan
     * fungsi define, solusinya adalah dengan dibuatkan method
     *
     * @param string $type
     * @return string
     */
    function get_module_asset_path( $type = self::path_type_abs ) {
        return ( self::path_type_uri == $type ? SIAKAD_MODULE_URI_PATH : SIAKAD_MODULE_ABS_PATH ) . DS . $this->module . DS . 'assets';
    }
    function get_module_style_path( $type = self::path_type_abs ) {
        return $this->get_module_asset_path( $type ) . DS . 'styles';
    }
    function get_module_script_path( $type = self::path_type_abs ) {
        return $this->get_module_asset_path( $type ) . DS . 'scripts';
    }
    function get_module_image_path( $type = self::path_type_abs ) {
        return $this->get_module_asset_path( $type ) . DS . 'images';
    }

    /**
     * @param $type
     * @param $name
     */
    function get( $type, $name = '' ) {
        $file_name = $type . ( '' == $name ? '' : '-' . $name  ) . Routes::script_ext;
        $file_path = ( $this->is_state_module() ? $this->get_module_path() : $this->get_view_path() ) . DS . $file_name ;
        if( file_exists( $file_path ) ) include( $file_path );
    }

    /**
     * @param string $name
     */
    function get_header( $name = '' ) {
        $this->get( self::type_header, $name );
    }

    /**
     * @param string $name
     */
    function get_sidebar( $name = '' ) {
        $this->get( self::type_sidebar, $name );
    }

    /**
     * @param string $name
     */
    function get_footer( $name = '' ) {
        $this->get( self::type_footer, $name );
    }

    /**
     * @param string $name
     */
    function get_template( $name = '' ) {
        $this->get( $name );
    }

}