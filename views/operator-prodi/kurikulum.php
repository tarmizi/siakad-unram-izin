<?php

namespace SIAKAD\Views\Operator\PRODI;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Kurikulum;
use SIAKAD\Controller\Sessions;
use SIAKAD\Controller\UserLevel;
use SIAKAD\Controller\ProgramStudi;

Headers::get_instance()
    ->set_page_title( 'Kurikulum' )
    ->set_page_name( 'Kurikulum' )
    ->set_page_sub_name( 'Kurikulum');

$tingkat1 = Routes::get_instance()->get_tingkat( 1 );
$tingkat2 = Routes::get_instance()->get_tingkat( 2 );
$tingkat3 = Routes::get_instance()->get_tingkat( 3 );
$is_perbaiki = Routes::get_instance()->is_tingkat( 3, 'perbaiki') && Routes::get_instance()->has_tingkat( 4 );
$is_simpan = Routes::get_instance()->is_tingkat( 3, 'simpan' );
$is_hapus = Routes::get_instance()->is_tingkat( 3, 'hapus') && Routes::get_instance()->has_tingkat( 4 );
$is_terhapus = Routes::get_instance()->is_tingkat( 3, 'hapus');

$is_konversi_kurikulum = Routes::get_instance()->is_tingkat( 3, 'konversi' );

!$is_konversi_kurikulum || Headers::get_instance()->set_page_sub_name( 'Konversi' );

$obj_prodi = ProgramStudi::get_instance()->_get(
    Sessions::get_instance()->_retrieve()->getObjLevelAkses()->getKodeObject(), 'kode_prodi_unram'
);
Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">
                Kurikulum
                <small><?php if( $is_konversi_kurikulum ) : ?>Konversi<?php else : ?>Daftar<?php endif; ?></small>
            </h1>
            <?php if( $is_konversi_kurikulum ) : ?>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-md-8">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <select class="form-control">
                                                <option>--kurikulum</option>
                                                <option>2011</option>
                                                <option>2012</option>
                                                <option>2013</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <button class="btn btn-primary"><i class="fa fa-legal"></i> OK</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4">
                                <form>
                                    <div class="input-group">
                                        <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                    </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <br/>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><input type="checkbox"></th>
                                <th>Kode Baru</th>
                                <th>Nama Baru</th>
                                <th>Asal</th>
                                <th>Semester</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach( range( 1, 3 ) as $i ) : ?>
                                <tr>
                                    <td><input type="checkbox"></td>
                                    <td>MK123</td>
                                    <td>Bahasa Sasak</td>
                                    <td>
                                        <ul>
                                            <li>MK111 (Bahasa Prancis)</li>
                                            <li>MK222 (Bahasa Jerman)</li>
                                            <li>MK333 (Bahasa Jepang)</li>
                                        </ul>
                                    </td>
                                    <td>Genap</td>
                                    <td>
                                        <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3; ?>/perbaiki/<?php echo $i; ?>" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                                        <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3; ?>/hapus/<?php echo $i; ?>" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <ul class="pagination">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">&raquo;</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <strong><i class="glyphicon glyphicon-plus"></i> Konversi Mata Kuliah</strong>
                            </div>
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" action="" method="post">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Kode Baru</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" placeholder="Kode MK baru">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Nama Baru</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" placeholder="Nama MK baru">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Asal</label>
                                        <div class="col-sm-8">
                                            <select class="form-control">
                                                <option>MK111 (Bahasa Prancis)</option>
                                                <option>MK222 (Bahasa Jerman)</option>
                                                <option>MK333 (Bahasa Jepang)</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Semester</label>
                                        <div class="col-sm-8">
                                            <select class="form-control">
                                                <option>Genam</option>
                                                <option selected>Ganjil</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Status</label>
                                        <div class="col-sm-8">
                                            <select class="form-control">
                                                <option>Aktif</option>
                                                <option selected>Tidak Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <?php else : ?>
                <?php if($is_simpan) :
                    $kodePS = $obj_prodi->getKode();
                    $thn_akademik = $_POST['tahun'].$_POST['semester'];
                    $k = Kurikulum::get_instance()->_get($kodePS.$thn_akademik);
                    $k->setId($kodePS.$thn_akademik);
                    $k->setKodePs($kodePS);
                    $k->setTahunAkademik($thn_akademik);
                    $k->setStatus($_POST['status']);
                    if(isset($_POST['new'])) :
                        Kurikulum::get_instance()->insert($k) or die("Query salah : " . mysql_error());
                    elseif(isset($_POST['save'])) :    
                        Kurikulum::get_instance()->update($k) or die("Query salah : " . mysql_error());
                    endif;
                elseif($is_terhapus) :
                    $kodePS = $obj_prodi->getKode();
                    $thn_akademik = $_POST['tahun'].$_POST['semester'];
                    if(isset($_POST['hapus'])) :
                        Kurikulum::get_instance()->delete($kodePS.$thn_akademik) or die("Query salah : " . mysql_error());
                    endif;
                endif; ?>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-md-8">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Yang dicentang</label>
                                        <div class="col-sm-4">
                                            <select class="form-control">
                                                <option>--pilih aksi</option>
                                                <option>Hapus masal</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <button class="btn btn-primary"><i class="fa fa-legal"></i> OK</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4">
                                <form>
                                    <div class="input-group">
                                        <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                    </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <br/>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><input type="checkbox"></th>
                                <th>Status</th>
                                <th>Tahun</th>
                                <th>Semester</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $kur = Kurikulum::get_instance()->_gets(array('kode_PS'=>$obj_prodi->getKode()));
                            foreach( $kur as $kurikulum ) : ?>
                                <tr>
                                    <td><input type="checkbox"></td>
                                    <td><?php echo $kurikulum->getStatus() ?></td>
                                    <?php $thn = round($kurikulum->getTahunAkademik()/10); 
                                          $smt = $kurikulum->getTahunAkademik()%2?>
                                    <td><?php echo $thn ?></td>
                                    <td><?php if($smt==0) echo "Genap"; else echo 'Ganjil'; ?></td>
                                    <td>
                                        <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 ?>/perbaiki/<?php echo $kurikulum->getId() ?>" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                                        <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 ?>/hapus/<?php echo $kurikulum->getID() ?>" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <ul class="pagination">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">&raquo;</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <div class="panel panel-<?php if($is_perbaiki) echo 'info';elseif($is_hapus) echo 'danger';else echo 'primary'; ?>">
                            <div class="panel-heading warning">
                                <?php if($is_perbaiki || $is_hapus) : 
                                    $k = Kurikulum::get_instance()->_get(Routes::get_instance()->get_tingkat( 4 ));
                                    $t = round($k->getTahunAkademik()/10); 
                                    $s = $k->getTahunAkademik()%2;
                                    if($is_perbaiki) :?>
                                        <strong><i class="glyphicon glyphicon-pencil"></i> Edit Kurikulum</strong>
                                    <?php elseif($is_hapus):?>
                                        <strong><i class="glyphicon glyphicon-remove"></i> Hapus Kurikulum</strong>
                                    <?php endif;
                                else : ?>
                                <strong><i class="glyphicon glyphicon-plus"></i> Kurikulum Baru</strong>
                                <?php endif; ?>
                            </div>
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" action="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2; if($is_hapus)  echo '/hapus';else echo '/simpan'; ?>" method="post">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Tahun</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="tahun">
                                                <?php $year = date('Y'); if($is_perbaiki || $is_hapus) :
                                                    foreach(range(0,50) as $i) : ?>
                                                ?>
                                                <option value="<?php echo $year-$i; ?>" <?php if($t==$year-$i) echo 'selected' ?>><?php echo $year-$i; ?></option>
                                                <?php endforeach; else : 
                                                    foreach(range(0,50) as $i) : ?>
                                                <option value="<?php echo $year-$i; ?>"><?php echo $year-$i; ?></option>
                                                <?php endforeach; endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Semester</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="semester">
                                                <?php if($is_perbaiki || $is_hapus) : ?>
                                                <option value="0" <?php if($s==0) echo 'selected' ?>>Genap</option>
                                                <option value="1" <?php if($s==1) echo 'selected' ?>>Ganjil</option>
                                                <?php else : ?>
                                                <option value="0">Genap</option>
                                                <option value="1" selected>Ganjil</option>
                                                <?php endif; ?>
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Status</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="status">
                                                <?php if($is_perbaiki || $is_hapus) : ?>
                                                <option value="1" <?php if($k->getStatus()==1) echo 'selected' ?>>Aktif</option>
                                                <option value="0" <?php if($k->getStatus()==0) echo 'selected' ?>>Tidak Aktif</option>
                                                <?php else : ?>
                                                <option value="1">Aktif</option>
                                                <option value="0" selected>Tidak Aktif</option>
                                                <?php endif; ?>
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <span class="col-sm-4"></span>
                                        <div class="col-sm-8">
                                            <?php if($is_perbaiki) : ?>
                                            <button type="submit" class="btn btn-primary" name="save"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                                            <?php elseif($is_hapus) : ?>
                                            <button type="submit" class="btn btn-primary" name="hapus"><i class="glyphicon glyphicon-remove"></i> Hapus</button>
                                            <?php else : ?>
                                            <button type="submit" class="btn btn-primary" name="new"><i class="glyphicon glyphicon-floppy-disk"></i> Tambah</button>
                                            <?php endif; ?>
                                            
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();