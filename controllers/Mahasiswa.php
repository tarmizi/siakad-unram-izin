<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Controller;
use SIAKAD\Model\Mahasiswa as ModelMahasiswa;

class Mahasiswa extends Databases {

    static $shift = array( 'R' => 'Reguler', 'N' => 'Non Reguler' );
    static $jenis_kelamin = array( 'L' => 'Laki-laki', 'P' => 'Perempuan' );
    static $status_kuliah = array( 'A' => 'Aktif', 'P' => 'Pindah' );

    private $field_id;
    private $class_name;
    private $table_prodi;
    private $table_fakultas;
    private $table_jenjang;
    private $table_agama;
    private $table_provinsi;
    private $table_status_mahasiswa;
    private $table_konsentrasi;
    private $table_pekerjaan_wali;
    private $count_query;

    /** @var Mahasiswa $instance */
    private static $instance;

    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function __construct() {
        parent::_connect();
        $this->field_id = 'NIM';
        $this->class_name = '\SIAKAD\Model\Mahasiswa';
        $this->table_name = 'mahasiswa';
        $this->table_prodi = 'prodi';
        $this->table_fakultas = 'fakultas';
        $this->table_jenjang = 'jenjang';
        $this->table_agama = 'agama';
        $this->table_provinsi = 'provinsi';
        $this->table_status_mahasiswa = 'status_mahasiswa';
        $this->table_konsentrasi = 'konsentrasi';
        $this->table_pekerjaan_wali = 'pekerjaan_wali';
        $this->count_query = 'SELECT * FROM ' . $this->table_name;
    }

    /**
     * @param $nim
     * @param string $by
     * @return object|ModelMahasiswa
     */
    function _get( $nim, $by = '' ) {
        $query = 'SELECT ' . $this->table_name . '.*';
        $query .= ', ' . $this->table_prodi . '.nama_nasional AS nama_nasional_prodi';
        $query .= ', ' . $this->table_fakultas . '.nama AS nama_fakultas';
        $query .= ', ' . $this->table_jenjang . '.nama AS nama_jenjang';
        $query .= ', ' . $this->table_agama . '.nama AS nama_agama';
        $query .= ', ' . $this->table_provinsi . '.nama AS nama_provinsi';
        $query .= ', ' . $this->table_status_mahasiswa . '.alias AS alias_status_mahasiswa';
        $query .= ', ' . $this->table_konsentrasi . '.nama AS nama_konsentrasi';
        $query .= ', ' . $this->table_pekerjaan_wali . '.nama AS nama_pekerjaan_wali';
        $query .= ' FROM ' . $this->table_name;
        $query .= ' LEFT JOIN ' . $this->table_prodi . ' ON ' . $this->table_prodi . '.kode = ' . $this->table_name . '.kode_prodi';
        $query .= ' LEFT JOIN ' . $this->table_fakultas . ' ON ' . $this->table_fakultas . '.kode = ' . $this->table_prodi . '.kode_fakultas';
        $query .= ' LEFT JOIN ' . $this->table_jenjang . ' ON ' . $this->table_jenjang . '.kode = ' . $this->table_name . '.jenjang';
        $query .= ' LEFT JOIN ' . $this->table_agama . ' ON ' . $this->table_agama . '.kode = ' . $this->table_name . '.kode_agama';
        $query .= ' LEFT JOIN ' . $this->table_provinsi . ' ON ' . $this->table_provinsi . '.kode = ' . $this->table_name . '.kode_provinsi';
        $query .= ' LEFT JOIN ' . $this->table_status_mahasiswa . ' ON ' . $this->table_status_mahasiswa . '.kode = ' . $this->table_name . '.status_kuliah';
        $query .= ' LEFT JOIN ' . $this->table_konsentrasi . ' ON ' . $this->table_konsentrasi . '.id = ' . $this->table_name . '.id_konsentrasi';
        $query .= ' LEFT JOIN ' . $this->table_pekerjaan_wali . ' ON ' . $this->table_pekerjaan_wali . '.id = ' . $this->table_name . '.kode_pekerjaan_wali';
        $query .= ' WHERE `' . ( empty( $by ) ? $this->field_id : $by ) . '` = "' . $nim . '"';
        return ( $return = mysql_fetch_object(
            mysql_query( $query ), $this->class_name
        ) ) ? $return : new $this->class_name;
    }

    function _gets( $args = array() ) {

        $return = array();

        $default_args = array(
            'kode_prodi'                => -1,
            'NIM'                       => '',
            'nama'                      => '',
            'tanggal_lahir_start_date'  => '',
            'tanggal_lahir_end_date'    => '',
            'jenis_kelamin'             => -1,
            'kode_agama'                => -1,
            'exclude'                   => array(),
            'conditions'                => '',
            'orderby'                   => $this->field_id,
            'order'                     => 'DESC',
            'number'                    => 10,
            'offset'                    => 0
        );

        $list_args = sync_default_params( $default_args, $args );
        $query = 'SELECT ' . $this->table_name . '.*';
        $query .= ', ' . $this->table_prodi . '.nama_nasional AS nama_nasional_prodi';
        $query .= ', ' . $this->table_fakultas . '.nama AS nama_fakultas';
        $query .= ', ' . $this->table_jenjang . '.nama AS nama_jenjang';
        $query .= ', ' . $this->table_agama . '.nama AS nama_agama';
        $query .= ', ' . $this->table_provinsi . '.nama AS nama_provinsi';
        $query .= ', ' . $this->table_status_mahasiswa . '.alias AS alias_status_mahasiswa';
        $query .= ', ' . $this->table_konsentrasi . '.nama AS nama_konsentrasi';
        $query .= ', ' . $this->table_pekerjaan_wali . '.nama AS nama_pekerjaan_wali';
        $query .= ' FROM ' . $this->table_name;
        $query .= ' LEFT JOIN ' . $this->table_prodi . ' ON ' . $this->table_prodi . '.kode = ' . $this->table_name . '.kode_prodi';
        $query .= ' LEFT JOIN ' . $this->table_fakultas . ' ON ' . $this->table_fakultas . '.kode = ' . $this->table_prodi . '.kode_fakultas';
        $query .= ' LEFT JOIN ' . $this->table_jenjang . ' ON ' . $this->table_jenjang . '.kode = ' . $this->table_name . '.jenjang';
        $query .= ' LEFT JOIN ' . $this->table_agama . ' ON ' . $this->table_agama . '.kode = ' . $this->table_name . '.kode_agama';
        $query .= ' LEFT JOIN ' . $this->table_provinsi . ' ON ' . $this->table_provinsi . '.kode = ' . $this->table_name . '.kode_provinsi';
        $query .= ' LEFT JOIN ' . $this->table_status_mahasiswa . ' ON ' . $this->table_status_mahasiswa . '.kode = ' . $this->table_name . '.status_kuliah';
        $query .= ' LEFT JOIN ' . $this->table_konsentrasi . ' ON ' . $this->table_konsentrasi . '.id = ' . $this->table_name . '.id_konsentrasi';
        $query .= ' LEFT JOIN ' . $this->table_pekerjaan_wali . ' ON ' . $this->table_pekerjaan_wali . '.id = ' . $this->table_name . '.kode_pekerjaan_wali';
        $query .= ' WHERE 1';

        /**
         * kode prodi
         */
        if( $list_args[ 'kode_prodi' ] >= 0 )
            $query .= ' AND ' . $this->table_name . '.kode_prodi = ' . $list_args[ 'kode_prodi' ];

        /**
         * search nim
         */
        if( !empty( $list_args[ 'NIM' ] ) )
            $query .= ' AND ' . $this->table_name . '.`NIM` LIKE "%' . $list_args[ 'NIM' ] . '%"';

        /**
         * search nama
         */
        if( !empty( $list_args[ 'nama' ] ) ) {
            $query .= ' AND ';
            $name_arr = explode( ' ', $list_args[ 'nama' ] );
            foreach( $name_arr as $key => $name )
                $query .= ( $key > 0 ? ' OR' : '' ) . $this->table_name . '.`nama` LIKE "%' . $name . '%"';
        }
        /**
         * Range tanggal lahir
         */
        if( !empty( $list_args[ 'tanggal_lahir_start_date' ] ) )
            $query .= ' AND ' . $this->table_name . '.tanggal_lahir_start_date <= "' . $list_args[ 'tanggal_lahir_start_date' ] . '"';

        if( !empty( $list_args[ 'tanggal_lahir_end_date' ] ) )
            $query .= ' AND ' . $this->table_name . '.tanggal_lahir_end_date >= "' . $list_args[ 'tanggal_lahir_end_date' ] . '"';

        /**
         * exclude
         */
        if( !empty( $list_args[ 'exclude' ] ) ) {

            foreach( $list_args[ 'exclude' ] as $ex )
                $query .= ' AND ' . $this->table_name . '.`NIM` <> "' . $ex . '"';

        }

        /**
         * untuk custom query pada conditions
         */
        if( !empty( $list_args[ 'conditions' ] ) ) {
            foreach( $list_args[ 'conditions' ] as $conditions )
                $query .= ' AND ' . $conditions[ 'field' ] . ' ' . $conditions[ 'operator' ] . ' ' . $conditions[ 'comparison' ];
        }

        $this->count_query = $query;

        /**
         * orderby dan jenis order
         */
        $query .= ' ORDER BY `' . $list_args[ 'orderby' ] . '` ' . $list_args[ 'order' ];

        /**
         * limit
         */
        if( $list_args[ 'number' ] >= 0 )
            $query .= ' LIMIT ' . $list_args[ 'offset' ] . ', ' . $list_args[ 'number' ];

        $resource = mysql_query( $query );

        //echo $query . ' : ' . mysql_error();

        while( $row = mysql_fetch_object( $resource, $this->class_name ) )
            $return[] = $row;

        return $return;

    }
    
    function insert( ModelMahasiswa $a ) {
        return parent::insert(
            $this->table_name,
            array(
                'NIM',
                'kode_prodi',
                'nama',
                'jenjang',
                'shift',
                'tempat_lahir',
                'tgl_lahir',
                'jns_kelamin',
                'kode_agama',
                'alamat',
                'kode_provinsi',
                'kode_provinsi_asal_sma',
                'tgl_masuk',
                'tgl_lulus',
                'thn_masuk',
                'semester_awal',
                'batas_studi',
                'status_kuliah',
                'id_konsentrasi',
                'sks_diakui',
                'nomor_ijazah',
                'nama_wali',
                'alamat_wali',
                'kode_pekerjaan_wali',
                'penghasilan_wali',
                'tlp_wali',
                'nomor_transkrip',
                'status_bayar',
                'status_masuk'
            ),
            array(
                array(
                    $a->getNIM(),
                    $a->getKodeProdi(),
                    $a->getNama(),
                    $a->getJenjang(),
                    $a->getShift(),
                    $a->getTempatLahir(),
                    $a->getTglLahir(),
                    $a->getJnsKelamin(),
                    $a->getKodeAgama(),
                    $a->getAlamat(),
                    $a->getKodeProvinsi(),
                    $a->getKodeProvinsiAsalSma(),
                    $a->getTglMasuk(),
                    $a->getTglLulus(),
                    $a->getThnMasuk(),
                    $a->getSemesterAwal(),
                    $a->getBatasStudi(),
                    $a->getStatusKuliah(),
                    $a->getIdKonsentrasi(),
                    $a->getSksDiakui(),
                    $a->getNomorIjazah(),
                    $a->getNamaWali(),
                    $a->getAlamatWali(),
                    $a->getKodePekerjaanWali(),
                    $a->getPenghasilanWali(),
                    $a->getTlpWali(),
                    $a->getNomorTranskrip(),
                    $a->getStatusBayar(),
                    $a->getStatusMasuk()
                )
            )
        );
    }

    function update( ModelMahasiswa $a ) {
        return parent::update(
            $this->table_name,
            array(
                'NIM',
                'kode_prodi',
                'nama',
                'jenjang',
                'shift',
                'tempat_lahir',
                'tgl_lahir',
                'jns_kelamin',
                'kode_agama',
                'alamat',
                'kode_provinsi',
                'kode_provinsi_asal_sma',
                'tgl_masuk',
                'tgl_lulus',
                'thn_masuk',
                'semester_awal',
                'batas_studi',
                'status_kuliah',
                'id_konsentrasi',
                'sks_diakui',
                'nomor_ijazah',
                'nama_wali',
                'alamat_wali',
                'kode_pekerjaan_wali',
                'penghasilan_wali',
                'tlp_wali',
                'nomor_transkrip',
                'status_bayar',
                'status_masuk'
            ),
            array(
                $a->getNIM(),
                $a->getKodeProdi(),
                $a->getNama(),
                $a->getJenjang(),
                $a->getShift(),
                $a->getTempatLahir(),
                $a->getTglLahir(),
                $a->getJnsKelamin(),
                $a->getKodeAgama(),
                $a->getAlamat(),
                $a->getKodeProvinsi(),
                $a->getKodeProvinsiAsalSma(),
                $a->getTglMasuk(),
                $a->getTglLulus(),
                $a->getThnMasuk(),
                $a->getSemesterAwal(),
                $a->getBatasStudi(),
                $a->getStatusKuliah(),
                $a->getIdKonsentrasi(),
                $a->getSksDiakui(),
                $a->getNomorIjazah(),
                $a->getNamaWali(),
                $a->getAlamatWali(),
                $a->getKodePekerjaanWali(),
                $a->getPenghasilanWali(),
                $a->getTlpWali(),
                $a->getNomorTranskrip(),
                $a->getStatusBayar(),
                $a->getStatusMasuk()
            ),
            array(
                'NIM', $a->getNIM()
            )
        );
    }

    function delete( $kode ) {
        return parent::delete(
            $this->table_name,
            'NIM',
            $kode
        );
    }

    function _count() {
        return mysql_num_rows( mysql_query( $this->count_query ) );
    }

}