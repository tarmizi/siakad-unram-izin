<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Controller;

use SIAKAD\Model\UserLevel as ModelUserLevel;

class UserLevel extends Databases {

    private $field_id;
    private $class_name;
    private $table_name;

    /** @var Login $instance */
    private static $instance;

    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function __construct() {
        parent::_connect();
        $this->field_id = 'username';
        $this->class_name = '\SIAKAD\Model\UserLevel';
        $this->table_name = 'user_level';
    }

    function insert( ModelUserLevel $a ) {
        return parent::insert(
            $this->table_name,
            array(
                'username',
                'level_akses'
            ),
            array(
                array(
                    $a->getUsername(),
                    $a->getLevelAkses()
                )
            )
        );
    }

    function update( ModelUserLevel $a ) {
        return parent::update(
            $this->table_name,
            array(
                'username',
                'level_akses'
            ),
            array(
                $a->getUsername(),
                $a->getLevelAkses()
            ),
            array(
                'username', $a->getUsername()
            )
        );
    }

    function delete( $username ) {
        return parent::delete(
            $this->table_name,
            'username',
            $username
        );
    }

} 