<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Controller;

use SIAKAD\Model\ProgramStudi as ModelProgramStudi;

class ProgramStudi extends Databases {

    static $jenjang = array( 'A', 'B', 'C', 'D', 'E', 'F' );
    static $akreditasi = array( 'A', 'B', 'C', 'D', 'E', 'F' );

    private $field_id;
    private $class_name;
    private $count_query;
    private $table_fakultas;

    /** @var ProgramStudi $instance */
    private static $instance;

    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function __construct() {
        parent::_connect();
        $this->field_id = 'kode';
        $this->class_name = '\SIAKAD\Model\ProgramStudi';
        $this->table_name = 'prodi';
        $this->table_fakultas = 'fakultas';
        $this->count_query = 'SELECT * FROM ' . $this->table_name;
    }

    /**
     * @param $kode
     * @param string $by
     * @return \SIAKAD\Model\ProgramStudi
     */
    function _get( $kode, $by = '' ) {
        $query = 'SELECT ' . $this->table_name . '.*';
        $query .= ', ' . $this->table_fakultas . '.nama AS nama_fakultas';
        $query .= ' FROM ' . $this->table_name;
        $query .= ' LEFT JOIN ' . $this->table_fakultas . ' ON ' . $this->table_fakultas . '.kode = ' . $this->table_name . '.kode_fakultas';
        $query .= ' WHERE ' . $this->table_name . '.`' . ( empty( $by ) ? $this->field_id : $by ) . '` = "' . $kode . '"';
        return ( $return = mysql_fetch_object(
            mysql_query( $query ), $this->class_name
        ) ) ? $return : new $this->class_name;
    }

    function _gets( $args = array() ) {

        $return = array();

        $default_args = array(
            'kode_fakultas'             => '',
            'nama_PS'                   => '',
            'exclude'                   => array(),
            'conditions'                => '',
            'orderby'                   => $this->field_id,
            'order'                     => 'DESC',
            'number'                    => 10,
            'offset'                    => 0
        );

        $list_args = sync_default_params( $default_args, $args );
        $query = 'SELECT ' . $this->table_name . '.*';
        $query .= ', ' . $this->table_fakultas . '.nama AS nama_fakultas';
        $query .= ' FROM ' . $this->table_name;
        $query .= ' LEFT JOIN ' . $this->table_fakultas . ' ON ' . $this->table_fakultas . '.kode = ' . $this->table_name . '.kode_fakultas';
        $query .= ' WHERE 1';

        if( !empty( $list_args[ 'kode_fakultas' ] ) )
            $query .= ' AND ' . $this->table_name . '.kode_fakultas = "' . $list_args[ 'kode_fakultas' ] . '"';

        if( !empty( $list_args[ 'nama_PS' ] ) ) {
            $query .= ' AND ';
            $name_arr = explode( ' ', $list_args[ 'nama_PS' ] );
            foreach( $name_arr as $key => $name )
                $query .= ( $key > 0 ? ' OR' : '' ) . '`nama_PS` LIKE "%' . $name . '%"';
        }

        /**
         * exclude
         */
        if( !empty( $list_args[ 'exclude' ] ) ) {

            foreach( $list_args[ 'exclude' ] as $ex )
                $query .= ' AND `kode` <> ' . $ex;

        }

        /**
         * untuk custom query pada conditions
         */
        if( !empty( $list_args[ 'conditions' ] ) ) {
            foreach( $list_args[ 'conditions' ] as $conditions )
                $query .= ' AND ' . $list_args . '.' . $conditions[ 'field' ] . ' ' . $conditions[ 'operator' ] . ' ' . $conditions[ 'comparison' ];
        }

        $this->count_query = $query;

        /**
         * orderby dan jenis order
         */
        $query .= ' ORDER BY `' . $list_args[ 'orderby' ] . '` ' . $list_args[ 'order' ];

        /**
         * limit
         */
        if( $list_args[ 'number' ] >= 0 )
            $query .= ' LIMIT ' . $list_args[ 'offset' ] . ', ' . $list_args[ 'number' ];

        $resource = mysql_query( $query );

        //echo $query . ' : ' . mysql_error();

        while( $row = mysql_fetch_object( $resource, $this->class_name ) )
            $return[] = $row;

        return $return;

    }

    function insert( ModelProgramStudi $prodi ) {
        return parent::insert(
            $this->table_name,
            array(
                'kode',
                'kode_nasional',
                'kode_fakultas',
                'kode_prodi_unram',
                'nama_nasional',
                'nama_nasional_ingg',
                'jenjang',
                'nama_PS',
                'keterangan',
                'kurikulum_pakai',
                'alamat_prodi',
                'sk_dikti',
                'tgl_sk_dikti',
                'tgl_akhir_sk_dikti',
                'jumlah_sks_kelulusan',
                'status',
                'tahun_semester_mulai',
                'email',
                'tgl_awal_pendirian',
                'sk_ban_pt',
                'tgl_sk_ban_pt',
                'tgl_akhir_sk_akreditasi',
                'kode_akreditasi'
            ),
            array(
                array(
                    $prodi->getKode(),
                    $prodi->getKodeNasional(),
                    $prodi->getKodeFakultas(),
                    $prodi->getKodeProdiUnram(),
                    $prodi->getNamaNasional(),
                    $prodi->getNamaNasionalIngg(),
                    $prodi->getJenjang(),
                    $prodi->getNamaPS(),
                    $prodi->getKeterangan(),
                    $prodi->getKurikulumPakai(),
                    $prodi->getAlamatProdi(),
                    $prodi->getSkDikti(),
                    $prodi->getTglSkDikti(),
                    $prodi->getTglAkhirSkDikti(),
                    $prodi->getJumlahSksKelulusan(),
                    $prodi->getStatus(),
                    $prodi->getTahunSemesterMulai(),
                    $prodi->getEmail(),
                    $prodi->getTglAwalPendirian(),
                    $prodi->getSkBanPt(),
                    $prodi->getTglSkBanPt(),
                    $prodi->getTglAkhirSkAkreditasi(),
                    $prodi->getKodeAkreditasi()
                )
            )
        );
    }

    function update( ModelProgramStudi $prodi ) {
        return parent::update(
            $this->table_name,
            array(
                'kode',
                'kode_nasional',
                'kode_fakultas',
                'kode_prodi_unram',
                'nama_nasional',
                'nama_nasional_ingg',
                'jenjang',
                'nama_PS',
                'keterangan',
                'kurikulum_pakai',
                'alamat_prodi',
                'sk_dikti',
                'tgl_sk_dikti',
                'tgl_akhir_sk_dikti',
                'jumlah_sks_kelulusan',
                'status',
                'tahun_semester_mulai',
                'email',
                'tgl_awal_pendirian',
                'sk_ban_pt',
                'tgl_sk_ban_pt',
                'tgl_akhir_sk_akreditasi',
                'kode_akreditasi'
            ),
            array(
                $prodi->getKode(),
                $prodi->getKodeNasional(),
                $prodi->getKodeFakultas(),
                $prodi->getKodeProdiUnram(),
                $prodi->getNamaNasional(),
                $prodi->getNamaNasionalIngg(),
                $prodi->getJenjang(),
                $prodi->getNamaPS(),
                $prodi->getKeterangan(),
                $prodi->getKurikulumPakai(),
                $prodi->getAlamatProdi(),
                $prodi->getSkDikti(),
                $prodi->getTglSkDikti(),
                $prodi->getTglAkhirSkDikti(),
                $prodi->getJumlahSksKelulusan(),
                $prodi->getStatus(),
                $prodi->getTahunSemesterMulai(),
                $prodi->getEmail(),
                $prodi->getTglAwalPendirian(),
                $prodi->getSkBanPt(),
                $prodi->getTglSkBanPt(),
                $prodi->getTglAkhirSkAkreditasi(),
                $prodi->getKodeAkreditasi()
            ),
            array(
                'kode', $prodi->getKode()
            )
        );
    }

    function delete( $kode ) {
        return parent::delete(
            $this->table_name,
            'kode',
            $kode
        );
    }
} 