<?php

/**
 * URI path adalah path relative di localhost
 * misal : http://sia.unram.ac.id
 */
$uri_path = 'http://127.0.0.1/projects/siakad-unram';


/** =================
 *   JANGAN DIGANTI
 * ================== */

/** pemisah direktory */
define( 'DS', DIRECTORY_SEPARATOR );
/** absolute path */
define( 'SIAKAD_ABS_PATH', dirname( dirname( __FILE__ ) ) );
/** uri path */
define( 'SIAKAD_URI_PATH', $uri_path );
/** module uri path */
define( 'SIAKAD_MODULE_URI_PATH', SIAKAD_URI_PATH . DS . 'modules' );
/** absolute path untuk include */
define( 'SIAKAD_INCLUDE_ABS_PATH', SIAKAD_ABS_PATH . DS . 'includes' );
/** absolute path untuk controller */
define( 'SIAKAD_CONTROLLER_ABS_PATH', SIAKAD_ABS_PATH . DS . 'controllers' );
/** absolute path untuk model */
define( 'SIAKAD_MODEL_ABS_PATH', SIAKAD_ABS_PATH . DS . 'models' );
/** absolute path untuk view */
define( 'SIAKAD_VIEW_ABS_PATH', SIAKAD_ABS_PATH . DS . 'views' );
/** absolute path untuk module */
define( 'SIAKAD_MODULE_ABS_PATH', SIAKAD_ABS_PATH . DS . 'modules' );
/** uri path untuk asset */
define( 'SIAKAD_ASSET_URI_PATH', SIAKAD_URI_PATH . DS . 'assets' );
/** uri path untuk style */
define( 'SIAKAD_STYLE_URI_PATH', SIAKAD_ASSET_URI_PATH . DS . 'styles' );
/** uri path untuk script */
define( 'SIAKAD_SCRIPT_URI_PATH', SIAKAD_ASSET_URI_PATH . DS . 'scripts' );
/** uri path untuk image */
define( 'SIAKAD_IMAGE_URI_PATH', SIAKAD_ASSET_URI_PATH . DS . 'images' );
/** uri path untuk asset */
define( 'SIAKAD_ASSET_ABS_PATH', SIAKAD_ABS_PATH . DS . 'assets' );
/** uri path untuk style */
define( 'SIAKAD_STYLE_ABS_PATH', SIAKAD_ASSET_ABS_PATH . DS . 'styles' );
/** uri path untuk script */
define( 'SIAKAD_SCRIPT_ABS_PATH', SIAKAD_ASSET_ABS_PATH . DS . 'scripts' );
/** uri path untuk image */
define( 'SIAKAD_IMAGE_ABS_PATH', SIAKAD_ASSET_ABS_PATH . DS . 'images' );
/** uri path untuk API */
define( 'SIAKAD_URI_API_PATH', SIAKAD_URI_PATH . DS . 'api' );