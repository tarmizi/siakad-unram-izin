<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Model;


class DosenPa {

    private $nim_mahasiswa;
    private $kode_dosen;

    private $nama_mahasiswa;
    private $nama_dosen;
    private $jns_kelamin;
    private $thn_masuk;

    /**
     * @return mixed
     */
    public function getJnsKelamin()
    {
        return $this->jns_kelamin;
    }

    /**
     * @return mixed
     */
    public function getThnMasuk()
    {
        return $this->thn_masuk;
    }

    /**
     * @return mixed
     */
    public function getNamaDosen()
    {
        return $this->nama_dosen;
    }

    /**
     * @return mixed
     */
    public function getNamaMahasiswa()
    {
        return $this->nama_mahasiswa;
    }

    /**
     * @param $nim_mahasiswa
     * @return $this
     */
    public function setNimMahasiswa($nim_mahasiswa)
    {
        $this->nim_mahasiswa = $nim_mahasiswa;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNimMahasiswa()
    {
        return $this->nim_mahasiswa;
    }

    /**
     * @param $kode_dosen
     * @return $this
     */
    public function setKodeDosen($kode_dosen)
    {
        $this->kode_dosen = $kode_dosen;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKodeDosen()
    {
        return $this->kode_dosen;
    }

} 