<?php

namespace SIAKAD\Views\Dosen;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;

Headers::get_instance()
    ->set_page_title( 'Log Nilai' )
    ->set_page_name( 'Log Nilai' );

$tingkat1 = Routes::get_instance()->get_tingkat( 1 );
$tingkat2 = Routes::get_instance()->get_tingkat( 2 );

Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">
                Log
                <small>Aktifitas perubahan nilai</small>
            </h1>
            <form class="form-horizontal">
                <div class="form-group">
                    <label class="col-md-1 control-label">Dari</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control">
                    </div>
                    <label class="col-md-1 control-label">Sampai</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-3">
                        <button class="btn btn-primary"><i class="fa fa-legal"></i> OK</button>
                    </div>
                </div>
            </form>
            <br/>
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Waktu</th>
                    <th>Akun</th>
                    <th>Jenis</th>
                    <th>Keterangan</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach( range( 1, 3 ) as $i ) : ?>
                    <tr>
                        <td>Tue Jul 01 12:30:00 2014</td>
                        <td>Ahmad Zafrullah</td>
                        <td>Update</td>
                        <td>Mata kuliah Bahasa Sasak dari A ke B</td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();