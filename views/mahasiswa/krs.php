<?php

namespace SIAKAD\Views\Mahasiswa;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Matakuliah;
use SIAKAD\Controller\SyaratMK;
use SIAKAD\Controller\Nilai;
use SIAKAD\Controller\Mahasiswa;
use SIAKAD\Controller\Sessions;
use SIAKAD\Controller\KrsTemp;

Headers::get_instance()
    ->set_page_title( 'KRS' )
    ->set_page_name( 'KRS' );

$tingkat2 = Routes::get_instance()->get_tingkat( 2 );
$tingkat3 = Routes::get_instance()->get_tingkat( 3 );
$tingkat4 = Routes::get_instance()->get_tingkat( 4 );

$is_kelas = Routes::get_instance()->is_tingkat( 3, 'kelas' );
!$is_kelas || Headers::get_instance()->set_page_sub_name( 'Kelas' );
$is_peserta = $is_kelas && Routes::get_instance()->has_tingkat( 4 ) && Routes::get_instance()->is_tingkat( 5, 'peserta' );

$is_tambah = Routes::get_instance()->is_tingkat( 3, 'tambah' );
$is_perbaiki = Routes::get_instance()->is_tingkat( 3, 'perbaiki' ) && Routes::get_instance()->has_tingkat( 4 );
$is_simpan = Routes::get_instance()->is_tingkat( 3, 'simpan' );
$is_hapus = Routes::get_instance()->is_tingkat( 3, 'hapus' );


Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">
                <?php echo Headers::get_instance()->get_page_name(); ?>
                <small>Pengisian</small>
            </h1>
            <?php 
                $sudah_ajukan = false;
                $diterima = false;
                $me = Mahasiswa::get_instance()->_get(Sessions::get_instance()->_retrieve()->getObjLogin()->getUsername());
                if(isset($_POST['ajukan'])){
                      if(!empty($_POST['mk'])) :
                          $mk_all = '';
                          foreach ( $_POST['mk'] as $mk_krs ){
                              $mk_all .= $mk_krs.'-';
                          }
                          $krstemp = new \SIAKAD\Model\KrsTemp();
                          $krstemp->setNim($me->getNim());
                          $krstemp->setIdMk($mk_all);
                          $krstemp->setTanggalAjukan(date('Y-m-d H:i:s'));
                          if($_POST['ajukan']=='new')
                              KrsTemp::get_instance()->insert($krstemp);
                          elseif($_POST['ajukan']=='edit'){
                              $krstemp->setStatus(0);
                              KrsTemp::get_instance()->update($krstemp);
                          }
                          $sudah_ajukan = true;
                          ?>
                          <div class="alert alert-success">
                            <p><i class="glyphicon glyphicon-ok"></i> KRS Anda Berhasil Dikirim</p>
                          </div>
                      <?php else : ?>
                        <div class="alert alert-danger">
                            <p><i class="glyphicon glyphicon-remove"></i> Pilih minimal 1 Matakuliah</p>
                        </div>
                      <?php endif;
                  }
                  else {
                      $krstemp = KrsTemp::get_instance()->_get($me->getNim());
                      if($krstemp->getNim()!=null){$sudah_ajukan = true; 
                         if($krstemp->getStatus()==2){ ?>
                          <div class="alert alert-danger">
                            <p><i class="glyphicon glyphicon-warning-sign"></i> KRS Anda Ditolak</p>
                          </div>
                         <?php }
                         elseif($krstemp->getStatus()==1){$diterima = true; ?>
                         <div class="alert alert-success">
                             <p><i class="glyphicon glyphicon-ok-circle"></i> KRS Anda Sudah Diterima &nbsp;&nbsp;<a target="_blank" href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS ?>cetak-krs"><button class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-print"></i> Cetak</button></a></p>
                          </div>
                         <?php }
                         elseif($krstemp->getStatus()==0){ ?>
                         <div class="alert alert-info">
                            <p><i class="glyphicon glyphicon-warning-sign"></i> Anda Sudah Mengajukan KRS</p>
                          </div>
                         <?php }
                         }
                  }
                  
                  $semester = jumlah_semester($me->getThnMasuk(), 20142);
                  
                  $nilai_all = Nilai::get_instance()->_gets(array('nim'=>$me->getNIM(),'nilai_min'=>1));
                  $jml_sks_total = hitung_sks($nilai_all);
                  
                  $nilai_semester_sebelumnya = Nilai::get_instance()->_gets(array('nim'=>$me->getNIM(),'tahun_akademik'=>20141));
                  $nilai_lulus_semester_sebelumnya = Nilai::get_instance()->_gets(array('nim'=>$me->getNIM(),'tahun_akademik'=>20141,'nilai_min'=>1));
                  $jml_sks_sebelumnya = hitung_sks($nilai_semester_sebelumnya);
                  $jml_sks_lulus_sebelumnya = hitung_sks($nilai_lulus_semester_sebelumnya);
                  
                  $ip_semester_lalu = hitung_IP($nilai_semester_sebelumnya);
                  $ipk = hitung_IP($nilai_all);
                  
                  $sks_diambil = hitung_sks_diambil($jml_sks_sebelumnya,$jml_sks_lulus_sebelumnya,$ip_semester_lalu);
                  
            ?>
<!--            <div class="alert alert-warning">
                <strong>Peringatan :</strong> side sudah semester 8, tapi sks masih 123.
            </div>-->
            <h3><?php echo $me->getNIM(); ?></h3>
            <table style="width: 100%">
                <thead>
                <tr>
                    <th>Tahun Ajaran</th>
                    <td>2014</td>
                </tr><tr>
                    <th>Semester</th>
                    <td><?php echo $semester; ?> / <?php if($semester%2==1)echo 'Ganjil';else echo 'Genap'; ?></td>
                </tr><tr>
                    <th>SKS yang telah diambil</th>
                    <td><?php echo $jml_sks_total .'/'.$jml_sks_sebelumnya; ?></td>
                </tr><tr>
                    <th>IP Semester Lalu</th>
                    <td><?php echo number_format($ip_semester_lalu,2); ?></td>
                </tr><tr>
                    <th>IPK</th>
                    <td><?php echo number_format($ipk,2); ?></td>
                </tr><tr>
                    <th>SKS yang boleh diambil</th>
                    <td><?php echo $sks_diambil; ?></td>
                </tr>
                </thead>
            </table>
            <br/>
            <form method="post" action="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2; ?>/ajukan">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Kode</th>
                    <th>Nama</th>
                    <th>SKS</th>
                    <th>Semester</th>
                    <th>Prasyarat</th>
                    <th>#</th>
                </tr>
                </thead>
                <tbody>
                <?php $daftar_matakuliah = Matakuliah::get_instance()->_gets(array('kode_PS'=>$me->getKodeProdi(),'status'=>1,'number'=>-1,'orderby'=>'semester','order'=>'ASC'));
                foreach ( $daftar_matakuliah as $mk ) :?>
                <tr>
                    <td><?php echo $mk->getKodeMk(); ?></td>
                    <td><?php echo $mk->getNamaMk(); ?></td>
                    <td><?php echo $mk->getSks(); ?></td>
                    <td><?php echo $mk->getSemester(); ?></td>
                    <td>
                        <?php $daftar_prasyarat = SyaratMK::get_instance()->_gets(array('id_mk'=>$mk->getId()));
                        $lulus = true;
                        foreach ( $daftar_prasyarat as $prasyarat) :
                            if($prasyarat->getIdMkPrasyarat()!=null){
                                $matakuliah = Matakuliah::get_instance()->_get($prasyarat->getIdMkPrasyarat());
                                $nilai = Nilai::get_instance()->_gets(array('nim'=>$me->getNIM(),'id_mk'=>$prasyarat->getIdMkPrasyarat(),'nilai_min'=>1));
                                if(empty($nilai)) : $lulus=false;?>
                                    <span class="text-danger"><i class="glyphicon glyphicon-remove">
                                <?php else :?>
                                    <span class="text-success"><i class="glyphicon glyphicon-ok">
                                <?php endif;?>
                                </i> <?php echo $matakuliah->getNamaMk(); ?></span><br/>
                            <?php }else {
                                if($jml_sks_total<$prasyarat->getSyaratSksLulus()) : $lulus=false;?>
                                    <span class="text-danger"><i class="glyphicon glyphicon-remove"></i></span>
                                <?php else :?>
                                    <span class="text-success"><i class="glyphicon glyphicon-ok"></i></span>
                                <?php endif;?>
                                    </i> <?php echo 'SKS Lulus '.$prasyarat->getSyaratSksLulus(); ?></span><br/>
                            <?php } endforeach; ?>
                    </td>
                    <td><input value="<?php echo $mk->getId();?>" name="mk[]" type="checkbox" <?php if(!$lulus)echo 'disabled'; ?>></td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
                <?php if(!$diterima): ?>
                <button name="ajukan" type="submit" value="<?php if($sudah_ajukan)echo 'edit';else echo 'new';?>" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-send"></i> Ajukan <?php if($sudah_ajukan)echo 'Lagi';?></button>
                <?php endif; ?>
            </form>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();