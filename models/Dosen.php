<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Model;


class Dosen {

    private $kode;
    private $kode_PS;
    private $jenjang;
    private $KTP;
    private $nama;
    private $gelar_tertinggi;
    private $NIP;
    private $NIDN;
    private $tempat_lahir;
    private $tgl_lahir;
    private $jenis_kelamin;
    private $kode_golongan;
    private $kode_jabatan;
    private $kode_pendidikan_tertinggi;
    private $status_ikatan_kerja;
    private $akta_sertifikat_mengajar;
    private $surat_ijin_mengajar;
    private $kode_pangkat;
    private $kode_agama;
    private $tlp_rumah;
    private $tlp_hp;
    private $id_konsentrasi;
    private $kode_status_dosen;

    private $nama_PS;
    private $nama_golongan;
    private $nama_jabatan;
    private $nama_jenjang;
    private $nama_pangkat;
    private $nama_agama;
    private $nama_konsentrasi;
    private $nama_status_dosen;

    function _init( $request ) {
        !isset( $request[ 'kode' ] ) || $this->kode = $request[ 'kode' ];
        !isset( $request[ 'kode_PS' ] ) || $this->kode_PS = $request[ 'kode_PS' ];
        !isset( $request[ 'jenjang' ] ) || $this->jenjang = $request[ 'jenjang' ];
        !isset( $request[ 'KTP' ] ) || $this->KTP = $request[ 'KTP' ];
        !isset( $request[ 'nama' ] ) || $this->nama = $request[ 'nama' ];
        !isset( $request[ 'gelar_tertinggi' ] ) || $this->gelar_tertinggi = $request[ 'gelar_tertinggi' ];
        !isset( $request[ 'NIP' ] ) || $this->NIP = $request[ 'NIP' ];
        !isset( $request[ 'NIDN' ] ) || $this->NIDN = $request[ 'NIDN' ];
        !isset( $request[ 'tempat_lahir' ] ) || $this->tempat_lahir = $request[ 'tempat_lahir' ];
        !isset( $request[ 'tgl_lahir' ] ) || $this->tgl_lahir = $request[ 'tgl_lahir' ];
        !isset( $request[ 'jenis_kelamin' ] ) || $this->jenis_kelamin = $request[ 'jenis_kelamin' ];
        !isset( $request[ 'kode_golongan' ] ) || $this->kode_golongan = $request[ 'kode_golongan' ];
        !isset( $request[ 'kode_jabatan' ] ) || $this->kode_jabatan = $request[ 'kode_jabatan' ];
        !isset( $request[ 'kode_pendidikan_tertinggi' ] ) || $this->kode_pendidikan_tertinggi = $request[ 'kode_pendidikan_tertinggi' ];
        !isset( $request[ 'status_ikatan_kerja' ] ) || $this->status_ikatan_kerja = $request[ 'status_ikatan_kerja' ];
        !isset( $request[ 'akta_sertifikat_mengajar' ] ) || $this->akta_sertifikat_mengajar = $request[ 'akta_sertifikat_mengajar' ];
        !isset( $request[ 'surat_ijin_mengajar' ] ) || $this->surat_ijin_mengajar = $request[ 'surat_ijin_mengajar' ];
        !isset( $request[ 'kode_pangkat' ] ) || $this->kode_pangkat = $request[ 'kode_pangkat' ];
        !isset( $request[ 'kode_agama' ] ) || $this->kode_agama = $request[ 'kode_agama' ];
        !isset( $request[ 'tlp_rumah' ] ) || $this->tlp_rumah = $request[ 'tlp_rumah' ];
        !isset( $request[ 'tlp_hp' ] ) || $this->tlp_hp = $request[ 'tlp_hp' ];
        !isset( $request[ 'id_konsentrasi' ] ) || $this->id_konsentrasi = $request[ 'id_konsentrasi' ];
        !isset( $request[ 'kode_status_dosen' ] ) || $this->kode_status_dosen = $request[ 'kode_status_dosen' ];
    }

    /**
     * @param mixed $KTP
     */
    public function setKTP($KTP)
    {
        $this->KTP = $KTP;
    }

    /**
     * @return mixed
     */
    public function getKTP()
    {
        return $this->KTP;
    }

    /**
     * @param mixed $NIDN
     */
    public function setNIDN($NIDN)
    {
        $this->NIDN = $NIDN;
    }

    /**
     * @return mixed
     */
    public function getNIDN()
    {
        return $this->NIDN;
    }

    /**
     * @param mixed $NIP
     */
    public function setNIP($NIP)
    {
        $this->NIP = $NIP;
    }

    /**
     * @return mixed
     */
    public function getNIP()
    {
        return $this->NIP;
    }

    /**
     * @param mixed $akta_sertifikat_mengajar
     */
    public function setAktaSertifikatMengajar($akta_sertifikat_mengajar)
    {
        $this->akta_sertifikat_mengajar = $akta_sertifikat_mengajar;
    }

    /**
     * @return mixed
     */
    public function getAktaSertifikatMengajar()
    {
        return $this->akta_sertifikat_mengajar;
    }

    /**
     * @param mixed $gelar_tertinggi
     */
    public function setGelarTertinggi($gelar_tertinggi)
    {
        $this->gelar_tertinggi = $gelar_tertinggi;
    }

    /**
     * @return mixed
     */
    public function getGelarTertinggi()
    {
        return $this->gelar_tertinggi;
    }

    /**
     * @param mixed $id_konsentrasi
     */
    public function setIdKonsentrasi($id_konsentrasi)
    {
        $this->id_konsentrasi = $id_konsentrasi;
    }

    /**
     * @return mixed
     */
    public function getIdKonsentrasi()
    {
        return $this->id_konsentrasi;
    }

    /**
     * @param mixed $jenis_kelamin
     */
    public function setJenisKelamin($jenis_kelamin)
    {
        $this->jenis_kelamin = $jenis_kelamin;
    }

    /**
     * @return mixed
     */
    public function getJenisKelamin()
    {
        return $this->jenis_kelamin;
    }

    /**
     * @param mixed $jenjang
     */
    public function setJenjang($jenjang)
    {
        $this->jenjang = $jenjang;
    }

    /**
     * @return mixed
     */
    public function getJenjang()
    {
        return $this->jenjang;
    }

    /**
     * @param mixed $kode
     */
    public function setKode($kode)
    {
        $this->kode = $kode;
    }

    /**
     * @return mixed
     */
    public function getKode()
    {
        return $this->kode;
    }

    /**
     * @param mixed $kode_PS
     */
    public function setKodePS($kode_PS)
    {
        $this->kode_PS = $kode_PS;
    }

    /**
     * @return mixed
     */
    public function getKodePS()
    {
        return $this->kode_PS;
    }

    /**
     * @param mixed $kode_agama
     */
    public function setKodeAgama($kode_agama)
    {
        $this->kode_agama = $kode_agama;
    }

    /**
     * @return mixed
     */
    public function getKodeAgama()
    {
        return $this->kode_agama;
    }

    /**
     * @param mixed $kode_golongan
     */
    public function setKodeGolongan($kode_golongan)
    {
        $this->kode_golongan = $kode_golongan;
    }

    /**
     * @return mixed
     */
    public function getKodeGolongan()
    {
        return $this->kode_golongan;
    }

    /**
     * @param mixed $kode_jabatan
     */
    public function setKodeJabatan($kode_jabatan)
    {
        $this->kode_jabatan = $kode_jabatan;
    }

    /**
     * @return mixed
     */
    public function getKodeJabatan()
    {
        return $this->kode_jabatan;
    }

    /**
     * @param mixed $kode_pangkat
     */
    public function setKodePangkat($kode_pangkat)
    {
        $this->kode_pangkat = $kode_pangkat;
    }

    /**
     * @return mixed
     */
    public function getKodePangkat()
    {
        return $this->kode_pangkat;
    }

    /**
     * @param mixed $kode_pendidikan_tertinggi
     */
    public function setKodePendidikanTertinggi($kode_pendidikan_tertinggi)
    {
        $this->kode_pendidikan_tertinggi = $kode_pendidikan_tertinggi;
    }

    /**
     * @return mixed
     */
    public function getKodePendidikanTertinggi()
    {
        return $this->kode_pendidikan_tertinggi;
    }

    /**
     * @param mixed $kode_status_dosen
     */
    public function setKodeStatusDosen($kode_status_dosen)
    {
        $this->kode_status_dosen = $kode_status_dosen;
    }

    /**
     * @return mixed
     */
    public function getKodeStatusDosen()
    {
        return $this->kode_status_dosen;
    }

    /**
     * @param mixed $nama
     */
    public function setNama($nama)
    {
        $this->nama = $nama;
    }

    /**
     * @return mixed
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * @param mixed $status_ikatan_kerja
     */
    public function setStatusIkatanKerja($status_ikatan_kerja)
    {
        $this->status_ikatan_kerja = $status_ikatan_kerja;
    }

    /**
     * @return mixed
     */
    public function getStatusIkatanKerja()
    {
        return $this->status_ikatan_kerja;
    }

    /**
     * @param mixed $surat_ijin_mengajar
     */
    public function setSuratIjinMengajar($surat_ijin_mengajar)
    {
        $this->surat_ijin_mengajar = $surat_ijin_mengajar;
    }

    /**
     * @return mixed
     */
    public function getSuratIjinMengajar()
    {
        return $this->surat_ijin_mengajar;
    }

    /**
     * @param mixed $tempat_lahir
     */
    public function setTempatLahir($tempat_lahir)
    {
        $this->tempat_lahir = $tempat_lahir;
    }

    /**
     * @return mixed
     */
    public function getTempatLahir()
    {
        return $this->tempat_lahir;
    }

    /**
     * @param mixed $tgl_lahir
     */
    public function setTglLahir($tgl_lahir)
    {
        $this->tgl_lahir = $tgl_lahir;
    }

    /**
     * @return mixed
     */
    public function getTglLahir()
    {
        return $this->tgl_lahir;
    }

    /**
     * @param mixed $tlp_hp
     */
    public function setTlpHp($tlp_hp)
    {
        $this->tlp_hp = $tlp_hp;
    }

    /**
     * @return mixed
     */
    public function getTlpHp()
    {
        return $this->tlp_hp;
    }

    /**
     * @param mixed $tlp_rumah
     */
    public function setTlpRumah($tlp_rumah)
    {
        $this->tlp_rumah = $tlp_rumah;
    }

    /**
     * @return mixed
     */
    public function getTlpRumah()
    {
        return $this->tlp_rumah;
    }

    /**
     * @param mixed $nama_agama
     */
    public function setNamaAgama($nama_agama)
    {
        $this->nama_agama = $nama_agama;
    }

    /**
     * @return mixed
     */
    public function getNamaAgama()
    {
        return $this->nama_agama;
    }

    /**
     * @param mixed $nama_golongan
     */
    public function setNamaGolongan($nama_golongan)
    {
        $this->nama_golongan = $nama_golongan;
    }

    /**
     * @return mixed
     */
    public function getNamaGolongan()
    {
        return $this->nama_golongan;
    }

    /**
     * @param mixed $nama_jabatan
     */
    public function setNamaJabatan($nama_jabatan)
    {
        $this->nama_jabatan = $nama_jabatan;
    }

    /**
     * @return mixed
     */
    public function getNamaJabatan()
    {
        return $this->nama_jabatan;
    }

    /**
     * @param mixed $nama_jenjang
     */
    public function setNamaJenjang($nama_jenjang)
    {
        $this->nama_jenjang = $nama_jenjang;
    }

    /**
     * @return mixed
     */
    public function getNamaJenjang()
    {
        return $this->nama_jenjang;
    }

    /**
     * @param mixed $nama_pangkat
     */
    public function setNamaPangkat($nama_pangkat)
    {
        $this->nama_pangkat = $nama_pangkat;
    }

    /**
     * @return mixed
     */
    public function getNamaPangkat()
    {
        return $this->nama_pangkat;
    }

    /**
     * @param mixed $nama_status_dosen
     */
    public function setNamaStatusDosen($nama_status_dosen)
    {
        $this->nama_status_dosen = $nama_status_dosen;
    }

    /**
     * @return mixed
     */
    public function getNamaStatusDosen()
    {
        return $this->nama_status_dosen;
    }

    /**
     * @param mixed $nama_PS
     */
    public function setNamaPS($nama_PS)
    {
        $this->nama_PS = $nama_PS;
    }

    /**
     * @return mixed
     */
    public function getNamaPS()
    {
        return $this->nama_PS;
    }

    /**
     * @param mixed $nama_konsentrasi
     */
    public function setNamaKonsentrasi($nama_konsentrasi)
    {
        $this->nama_konsentrasi = $nama_konsentrasi;
    }

    /**
     * @return mixed
     */
    public function getNamaKonsentrasi()
    {
        return $this->nama_konsentrasi;
    }

} 