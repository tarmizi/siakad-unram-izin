<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Model;


class DosenPengajar {

    private $id_matakuliah;
    private $kode_dosen;
    private $tahun_akademik;
    private $nama_kelas;

    /**
     * @param mixed $id_matakuliah
     */
    public function setIdMatakuliah($id_matakuliah)
    {
        $this->id_matakuliah = $id_matakuliah;
    }

    /**
     * @return mixed
     */
    public function getIdMatakuliah()
    {
        return $this->id_matakuliah;
    }

    /**
     * @param mixed $kode_dosen
     */
    public function setKodeDosen($kode_dosen)
    {
        $this->kode_dosen = $kode_dosen;
    }

    /**
     * @return mixed
     */
    public function getKodeDosen()
    {
        return $this->kode_dosen;
    }

    /**
     * @param mixed $nama_kelas
     */
    public function setNamaKelas($nama_kelas)
    {
        $this->nama_kelas = $nama_kelas;
    }

    /**
     * @return mixed
     */
    public function getNamaKelas()
    {
        return $this->nama_kelas;
    }

    /**
     * @param mixed $tahun_akademik
     */
    public function setTahunAkademik($tahun_akademik)
    {
        $this->tahun_akademik = $tahun_akademik;
    }

    /**
     * @return mixed
     */
    public function getTahunAkademik()
    {
        return $this->tahun_akademik;
    }



} 