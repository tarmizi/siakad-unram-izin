<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Model;


class Jabatan {

    private $kode;
    private $nama;

    /**
     * @param mixed $kode
     */
    public function setKode($kode)
    {
        $this->kode = $kode;
    }

    /**
     * @return mixed
     */
    public function getKode()
    {
        return $this->kode;
    }

    /**
     * @param mixed $nama
     */
    public function setNama($nama)
    {
        $this->nama = $nama;
    }

    /**
     * @return mixed
     */
    public function getNama()
    {
        return $this->nama;
    }

} 