<?php

namespace SIAKAD\Views\Operator\Fakultas;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;

Headers::get_instance()
    ->set_page_title( 'Cetak' )
    ->set_page_name( 'Cetak' );

Contents::get_instance()->get_header();

$tingkat1 = Routes::get_instance()->get_tingkat( 1 );
$tingkat2 = Routes::get_instance()->get_tingkat( 2 );
$tingkat3 = Routes::get_instance()->get_tingkat( 3 );
$tingkat4 = Routes::get_instance()->get_tingkat( 4 );
$tingkat5 = Routes::get_instance()->get_tingkat( 5 );

$is_cetak_transkrip = Routes::get_instance()->is_tingkat( 3, 'transkrip' );
$is_cetak_ijazah = Routes::get_instance()->is_tingkat( 3, 'ijazah' );

!$is_cetak_transkrip || Headers::get_instance()->set_page_sub_name( 'Transkrip' );
!$is_cetak_ijazah || Headers::get_instance()->set_page_sub_name( 'Ijazah' );

$is_cetak_tinjau = Routes::get_instance()->is_tingkat( 4, 'lihat' ) && Routes::get_instance()->has_tingkat( 5 );

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">
                Cetak
                <small><?php if( $is_cetak_transkrip ) : ?>Transkrip<?php elseif( $is_cetak_ijazah ) : ?>Ijazah<?php endif; ?></small>
            </h1>
            <?php if( $is_cetak_tinjau ) : ?>
                <h3>F1B008004</h3>
                <table style="width: 100%">
                    <tr>
                        <th>Nama</th>
                        <td>Ahmad Zafrullah</td>
                    </tr><tr>
                        <th>Fakultas</th>
                        <td>Teknik</td>
                    </tr><tr>
                        <th>Jurusan</th>
                        <td>Elektro</td>
                    </tr><tr>
                        <th>Angkatan</th>
                        <td>2008</td>
                    </tr><tr>
                        <th>Semester</th>
                        <td>8</td>
                    </tr>
                </table>
                <table class="table table-hover table-condensed">
                    <thead>
                    <tr>
                        <th>Mata Kuliah</th>
                        <th>SKS</th>
                        <th>Index</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Bahasa Sasak</td>
                        <td>6</td>
                        <td>A</td>
                    </tr><tr>
                        <td>Seni Vokal</td>
                        <td>4</td>
                        <td>A</td>
                    </tr><tr>
                        <td>Seni Musik</td>
                        <td>8</td>
                        <td>A</td>
                    </tr><tr>
                        <td>Seniman?</td>
                        <td>-</td>
                        <td>false</td>
                    </tr>
                    </tbody>
                </table>
                <br/>
                <button class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-print"></i> Cetak</button>
            <?php else : ?>
                <div class="row">
                    <div class="col-sm-8">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <select class="form-control">
                                        <option>-- PRODI</option>
                                        <option>Teknik Informatika</option>
                                        <option selected>Teknik Elektro</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control">
                                        <option>-- Bidang Keahlian</option>
                                        <option>Elektronika Digital</option>
                                        <option selected>Informatika</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control">
                                        <option>-- Tahun</option>
                                        <option selected>2010</option>
                                        <option>2011</option>
                                        <option>2012</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-4">
                        <form>
                            <div class="input-group">
                                <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                    </span>
                            </div>
                        </form>
                    </div>
                </div>
                <br/>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>NIM</th>
                        <th>Nama</th>
                        <th>Tempat/Tgl. Lahir</th>
                        <th>Jenis Kelamin</th>
                        <?php if( $is_cetak_transkrip || $is_cetak_ijazah ) : ?>
                            <th>#</th>
                        <?php endif; ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach( range(1,10) as $i ) : ?>
                        <tr>
                            <td>F1B 008004</td>
                            <td>Ahmad Zafrullah</td>
                            <td>Mataram/17-08-1945</td>
                            <td>Laki-laki</td>
                            <?php if( $is_cetak_transkrip || $is_cetak_ijazah ) : ?>
                                <td>
                                    <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3; ?>/lihat/f1b008004" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-zoom-in"></i></a>
                                    <a class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-print"></i></a>
                                </td>
                            <?php endif; ?>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
                <ul class="pagination">
                    <li><a href="#">&laquo;</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">&raquo;</a></li>
                </ul>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();