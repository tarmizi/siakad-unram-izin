<?php

namespace SIAKAD\Views\Publik;

use SIAKAD\Controller\Footers;

Footers::get_instance()
    ->add_script( 'jquery.min.js' )
    ->add_script( 'jquery.ui.min.js' )
    ->add_script( 'bootstrap.min.js' );

?>

    <hr>
    <div class="container">
        <footer>
            <p>&copy; Hak Cipta <?php echo date( 'Y' ); ?> <?php echo SIAKAD_APP_AUTHOR; ?></p>
            <p>versi-1.0.0, <a href="<?php echo SIAKAD_URI_PATH; ?>/changelogs">aktifitas perubahan terkini</a></p>
        </footer>
    </div>

    <?php echo Footers::get_instance()->get_script(); ?>

</body>
</html>