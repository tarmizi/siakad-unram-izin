<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Model;


class KrsTemp {

    private $nim;
    private $id_mk;
    private $tanggal_ajukan;
    private $status;

    /**
     * @param mixed $nim
     */
    public function setNim($nim)
    {
        $this->nim = $nim;
    }

    /**
     * @return mixed
     */
    public function getNim()
    {
        return $this->nim;
    }
    
    /**
     * @param mixed $id_mk
     */
    public function setIdMk($id_mk)
    {
        $this->id_mk = $id_mk;
    }

    /**
     * @return mixed
     */
    public function getIdMk()
    {
        return $this->id_mk;
    }
    
    /**
     * @param mixed $tanggal_ajukan
     */
    public function setTanggalAjukan($tanggal_ajukan)
    {
        $this->tanggal_ajukan = $tanggal_ajukan;
    }

    /**
     * @return mixed
     */
    public function getTanggalAjukan()
    {
        return $this->tanggal_ajukan;
    }
    
    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

} 