<?php
/**
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Views\Publik;

use SIAKAD\Controller\Authentications;
use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Helpers;
use SIAKAD\Controller\Sessions;

Headers::get_instance()
    ->set_page_title( 'Autentikasi' );

$is_lupa_kata_sandi = Routes::get_instance()->is_tingkat( 2, Helpers::aksi_lupa_kata_sandi );
$is_masuk = Routes::get_instance()->is_tingkat( 2, Helpers::aksi_masuk );
$is_keluar = Routes::get_instance()->is_tingkat( 2, Helpers::aksi_keluar );

$auth_status = 2;
$is_masuk_success = false;

if( $is_masuk ) {

    $username = isset( $_REQUEST[ 'username' ] ) ? $_REQUEST[ 'username' ] : '';
    $password = isset( $_REQUEST[ 'password' ] ) ? $_REQUEST[ 'password' ] : '';
    $auth_status = Authentications::get_instance()->_init( $username, $password )->_auth();
    $is_masuk_success = $auth_status == Authentications::success;

    if( $is_masuk_success )
        Headers::get_instance()->add_head_meta( array(
            array( 'http-equiv' => 'refresh', 'content' => '2;url=' . SIAKAD_URI_PATH . DS . Sessions::get_instance()->_retrieve()->getView() )
        ) );
}

elseif( $is_keluar ) Sessions::get_instance()->_destroy();

Contents::get_instance()->get_header( Sessions::get_instance()->_retrieve() ? 'loggedin' : '' );

?>

    <div class="container autentikasi">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <?php if( $is_masuk ) :
                    if( $is_masuk_success ) : ?>
                        <div class="alert alert-success">
                            <p><i class="glyphicon glyphicon-ok"></i>&nbsp;&nbsp;<?php echo Authentications::$error_messages[ $auth_status ]; ?>, beberapa saat lagi akan diarahkan ke halaman panel.</p>
                        </div>
                    <?php else : ?>
                        <div class="alert alert-danger">
                            <p><i class="glyphicon glyphicon-remove"></i>&nbsp;&nbsp;<?php echo Authentications::$error_messages[ $auth_status ]; ?>.</p>
                        </div>
                    <?php endif; ?>

                <?php

                /**
                 * @todo
                 */

                elseif( $is_lupa_kata_sandi ) : ?>
                    <h1>Autentikasi</h1>
                    <p>Permintaan kata sandi baru, silakan lengkapi isian berikut:</p>
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">ID Pengguna</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" placeholder="ID Pengguna">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Nama Lengkap</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" placeholder="Nama Lengkap">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Alamat Email</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" placeholder="Email aktif">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-6">
                                <button type="submit" class="btn btn-primary btn-block">Kirim</button>
                            </div>
                        </div>
                    </form>

                <?php elseif( $is_keluar ) : ?>
                    <div class="alert alert-warning">
                        <p><i class="fa fa-warning"></i> Anda berhasil keluar, beberapa saat lagi anda akan diarahkan ke beranda.</p>
                    </div>
                    <a href="<?php echo SIAKAD_URI_PATH; ?>" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-home"></i> Beranda</a>
                <?php else : ?>

                    <div class="alert alert-warning">
                        <p><i class="glyphicon glyphicon-remove"></i> <strong>Ehmmm... </strong>halaman ini hanya bagi developer yang masih lajang /o/</p>
                    </div>

                <?php endif; ?>
            </div>
        </div>
    </div>

<?php Contents::get_instance()->get_footer();