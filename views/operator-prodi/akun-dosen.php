<?php

namespace SIAKAD\Views\Operator\PRODI;

use SIAKAD\Controller\Agama;
use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Helpers;
use SIAKAD\Controller\Jabatan;
use SIAKAD\Controller\Konsentrasi;
use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Dosen;
use SIAKAD\Controller\ProgramStudi;
use SIAKAD\Controller\Golongan;
use SIAKAD\Controller\Jenjang;
use SIAKAD\Controller\Pangkat;
use SIAKAD\Controller\Sessions;
use SIAKAD\Controller\StatusDosen;

use SIAKAD\Model\Dosen as ModelDosen;
use SIAKAD\Model\ProgramStudi as ModelProgramStudi;
use SIAKAD\Model\Golongan as ModelGolongan;
use SIAKAD\Model\Jabatan as ModelJabatan;
use SIAKAD\Model\Jenjang as ModelJenjang;
use SIAKAD\Model\Pangkat as ModelPangkat;
use SIAKAD\Model\Agama as ModelAgama;
use SIAKAD\Model\StatusDosen as ModelStatusDosen;
use SIAKAD\Model\Konsentrasi as ModelKonsentrasi;

global $status, $tingkat1, $tingkat2, $tingkat3, $is_tambah, $is_perbaiki, $is_hapus, $is_simpan;

$is_mahasiswa_bimbingan = Routes::get_instance()->has_tingkat( 4 ) && Routes::get_instance()->is_tingkat( 5, Helpers::aksi_mahasiswa_bimbingan );

$obj_prodi = ProgramStudi::get_instance()->_get(
    Sessions::get_instance()->_retrieve()->getObjLevelAkses()->getKodeObject(), 'kode_prodi_unram'
);

if( $is_simpan ) {
    $aksi = isset( $_REQUEST[ Helpers::aksi_param ] ) ? $_REQUEST[ Helpers::aksi_param ] : '';
    $aksi_perbarui = $aksi == Helpers::aksi_perbarui;
    $aksi_tambah = $aksi == Helpers::aksi_tambah;
    $obj_dosen = new ModelDosen();
    $obj_dosen->_init( $_REQUEST );

    if( $aksi_tambah )
        siakad_redirect( SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . (
            Dosen::get_instance()->insert( $obj_dosen ) ?
                DS . Helpers::aksi_perbaiki . DS .$obj_dosen->getKode() . '?status=1' : '?status=9999'
            )
        );

    elseif( $aksi_perbarui )
        siakad_redirect( SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . (
            Dosen::get_instance()->update( $obj_dosen ) ?
                DS . Helpers::aksi_perbaiki . DS .$obj_dosen->getKode() . '?status=2' : '?status=9999'
            )
        );

} elseif( $is_hapus ) {
    siakad_redirect( SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . '?status=' . (
        Dosen::get_instance()->delete(
            Routes::get_instance()->get_tingkat( 4 ) ) ? 3 : 9999
        )
    );
}

/** ambil daftar dosen */
$default_params = array(
    'kode_PS'       => $obj_prodi->getKode(),
    'nama'          => '',
    'number'        => 20,
    'page'          => 1,
);
$list_params = sync_default_params( $default_params, $_GET );

/** ambil daftar prodi */
$daftar_prodi = ProgramStudi::get_instance()->_gets( array(
    'number'        => -1,
    'orderby'       => 'nama',
    'order'         => 'ASC'
) );

$daftar_prodi_by_fakultas = array();
/** @var $prodi ModelProgramStudi */
foreach( $daftar_prodi as $prodi )
    in_array( $prodi->getNamaFakultas(), $daftar_prodi_by_fakultas ) || $daftar_prodi_by_fakultas[ $prodi->getNamaFakultas() ][] = $prodi;

if( $is_tambah || $is_perbaiki ) :
    $obj_dosen = $is_perbaiki ? Dosen::get_instance()->_get( Routes::get_instance()->get_tingkat( 5 ) ) : new ModelDosen();
    $daftar_golongan = Golongan::get_instance()->_gets( array( 'number' => -1 ) );
    $daftar_jabatan = Jabatan::get_instance()->_gets( array( 'number' => -1 ) );
    $daftar_jenjang = Jenjang::get_instance()->_gets( array( 'number' => -1 ) );
    $daftar_pangkat = Pangkat::get_instance()->_gets( array( 'number' => -1 ) );
    $daftar_agama = Agama::get_instance()->_gets( array( 'number' => -1 ) );
    $daftar_status_dosen = StatusDosen::get_instance()->_gets( array( 'number' => -1 ) );
    $daftar_konsentrasi = Konsentrasi::get_instance()->_gets( array( 'kode_PS' => $obj_prodi->getKode(), 'number' => -1 ) );
    ?>

    <h1 class="page-header">
        Dosen
        <small><?php if( $is_perbaiki ) : ?>Perbaiki<?php else : ?>Tambah<?php endif; ?></small>
    </h1>

    <?php if( $status ) Helpers::get_instance()->set_message_object( 'Dosen' )->display_message( $status ); ?>

    <form action="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . DS . Helpers::aksi_simpan; ?>" class="form-dosen form-horizontal" method="post">
    <!-- sebelah kiri -->
    <div class="col-md-6">
        <div class="form-group">
            <label for="kode" class="col-xs-4 control-label">Kode Dosen</label>
            <div class="col-xs-8">
                <input id="kode" name="kode" type="text" class="form-control" value="<?php echo $obj_dosen->getKode(); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="kode_PS" class="col-xs-4 control-label">PRODI</label>
            <div class="col-xs-8">
                <select id="kode_PS" name="kode_PS" class="form-control">
                    <option value>--pilih--</option>
                    <?php foreach( $daftar_prodi_by_fakultas as $fakultas => $prodis ) : ?>
                        <optgroup label="<?php echo $fakultas; ?>">
                            <?php /** @var $prodi ModelProgramStudi **/
                            foreach( $prodis as $prodi ) : ?>
                                <option value="<?php echo $prodi->getKode(); ?>" <?php if( $prodi->getKode() == $obj_dosen->getKodePS() ) : ?>selected<?php endif; ?> ><?php echo $prodi->getNamaPS(); ?></option>
                            <?php endforeach; ?>
                        </optgroup>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="jenjang" class="col-xs-4 control-label">Jenjang</label>
            <div class="col-xs-8">
                <select id="jenjang" name="jenjang" class="form-control">
                    <option value>--pilih--</option>
                    <?php foreach( Dosen::$jenjang as $jenjang ) : ?>
                        <option value="<?php echo $jenjang; ?>" <?php if( $jenjang == $obj_dosen->getJenjang() ) : ?>selected<?php endif; ?> ><?php echo $jenjang; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="KTP" class="col-xs-4 control-label">KTP</label>
            <div class="col-xs-8">
                <input id="KTP" name="KTP" type="text" class="form-control" value="<?php echo $obj_dosen->getKTP(); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="nama" class="col-xs-4 control-label">Nama</label>
            <div class="col-xs-8">
                <input id="nama" name="nama" type="text" class="form-control" value="<?php echo $obj_dosen->getNama(); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="gelar_tertinggi" class="col-xs-4 control-label">Gelar Tertinggi</label>
            <div class="col-xs-8">
                <input id="gelar_tertinggi" name="gelar_tertinggi" type="text" class="form-control" value="<?php echo $obj_dosen->getGelarTertinggi(); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="NIP" class="col-xs-4 control-label">NIP</label>
            <div class="col-xs-8">
                <input id="NIP" name="NIP" type="text" class="form-control" value="<?php echo $obj_dosen->getNIP(); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="NIDN" class="col-xs-4 control-label">NIDN</label>
            <div class="col-xs-8">
                <input id="NIDN" name="NIDN" type="text" class="form-control" value="<?php echo $obj_dosen->getNIDN(); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="tempat_lahir" class="col-xs-4 control-label">Tempat Lahir</label>
            <div class="col-xs-8">
                <textarea id="tempat_lahir" name="tempat_lahir" class="form-control"><?php echo $obj_dosen->getTempatLahir(); ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="tgl_lahir" class="col-xs-4 control-label">Tanggal Lahir</label>
            <div class="col-xs-8">
                <div class="input-group">
                    <input id="tgl_lahir" type="text" class="form-control" name="tgl_lahir" value="<?php echo $obj_dosen->getTglLahir(); ?>"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="jenis_kelamin" class="col-xs-4 control-label">Jenis Kelamin</label>
            <div class="col-xs-8">
                <select id="jenis_kelamin" name="jenis_kelamin" class="form-control">
                    <option value>--pilih--</option>
                    <?php foreach( Dosen::$jenis_kelamin as $sex ) : ?>
                        <option value="<?php echo $sex; ?>" <?php if( $sex == $obj_dosen->getJenisKelamin() ) : ?>selected<?php endif; ?> ><?php echo $sex; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="kode_golongan" class="col-xs-4 control-label">Golongan</label>
            <div class="col-xs-8">
                <select id="kode_golongan" name="kode_golongan" class="form-control">
                    <option value>--pilih--</option>
                    <?php /** @var $golongan ModelGolongan */
                    foreach( $daftar_golongan as $golongan ) : ?>
                        <option value="<?php echo $golongan->getKode(); ?>" <?php if( $golongan->getKode() == $obj_dosen->getKodeGolongan() ) : ?>selected<?php endif; ?> ><?php echo $golongan->getNama(); ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
    </div>

    <!-- sebelah kanan -->
    <div class="col-md-6">
        <div class="form-group">
            <label for="kode_jabatan" class="col-xs-4 control-label">Jabatan</label>
            <div class="col-xs-8">
                <select id="kode_jabatan" name="kode_jabatan" class="form-control">
                    <option value>--pilih--</option>
                    <?php /** @var $jabatan ModelJabatan */
                    foreach( $daftar_jabatan as $jabatan ) : ?>
                        <option value="<?php echo $jabatan->getKode(); ?>" <?php if( $jabatan->getKode() == $obj_dosen->getKodeJabatan() ) : ?>selected<?php endif; ?> ><?php echo $jabatan->getNama(); ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="kode_pendidikan_tertinggi" class="col-xs-4 control-label">Pendidikan Tertinggi</label>
            <div class="col-xs-8">
                <select id="kode_pendidikan_tertinggi" name="kode_pendidikan_tertinggi" class="form-control">
                    <option value>--pilih--</option>
                    <?php /** @var $jenjang ModelJenjang */
                    foreach( $daftar_jenjang as $jenjang ) : ?>
                        <option value="<?php echo $jenjang->getKode(); ?>" <?php if( $jenjang->getKode() == $obj_dosen->getKodePendidikanTertinggi() ) : ?>selected<?php endif; ?> ><?php echo $jenjang->getNama(); ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="status_ikatan_kerja" class="col-xs-4 control-label">Status Ikatan Kerja</label>
            <div class="col-xs-8">
                <select id="status_ikatan_kerja" name="status_ikatan_kerja" class="form-control">
                    <option value>--pilih--</option>
                    <?php foreach( Dosen::$status_ikatan_kerja as $sex ) : ?>
                        <option value="<?php echo $sex; ?>" <?php if( $sex == $obj_dosen->getStatusIkatanKerja() ) : ?>selected<?php endif; ?> ><?php echo $sex; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="akta_sertifikat_mengajar" class="col-xs-4 control-label">Akta Sertifikat Mengajar</label>
            <div class="col-xs-8">
                <select id="akta_sertifikat_mengajar" name="akta_sertifikat_mengajar" class="form-control">
                    <option value>--pilih--</option>
                    <option value="1" <?php if( "1" == $obj_dosen->getAktaSertifikatMengajar() ) : ?>selected<?php endif; ?> >Ya</option>
                    <option value="0" <?php if( "0" == $obj_dosen->getAktaSertifikatMengajar() ) : ?>selected<?php endif; ?> >Tidak</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="surat_ijin_mengajar" class="col-xs-4 control-label">Surat Izin Mengajar</label>
            <div class="col-xs-8">
                <select id="surat_ijin_mengajar" name="surat_ijin_mengajar" class="form-control">
                    <option value>--pilih--</option>
                    <option value="1" <?php if( "1" == $obj_dosen->getSuratIjinMengajar() ) : ?>selected<?php endif; ?> >Ya</option>
                    <option value="0" <?php if( "0" == $obj_dosen->getSuratIjinMengajar() ) : ?>selected<?php endif; ?> >Tidak</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="kode_pangkat" class="col-xs-4 control-label">Pangkat</label>
            <div class="col-xs-8">
                <select id="kode_pangkat" name="kode_pangkat" class="form-control">
                    <option value>--pilih--</option>
                    <?php /** @var $pangkat ModelPangkat */
                    foreach( $daftar_pangkat as $pangkat ) : ?>
                        <option value="<?php echo $pangkat->getKode(); ?>" <?php if( $pangkat->getKode() == $obj_dosen->getKodePangkat() ) : ?>selected<?php endif; ?> ><?php echo $pangkat->getNama(); ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="kode_agama" class="col-xs-4 control-label">Agama</label>
            <div class="col-xs-8">
                <select id="kode_agama" name="kode_agama" class="form-control">
                    <option value>--pilih--</option>
                    <?php /** @var $agama ModelAgama */
                    foreach( $daftar_agama as $agama ) : ?>
                        <option value="<?php echo $agama->getKode(); ?>" <?php if( $agama->getKode() == $obj_dosen->getKodeAgama() ) : ?>selected<?php endif; ?> ><?php echo $agama->getNama(); ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="tlp_rumah" class="col-xs-4 control-label">Telp. Rumah</label>
            <div class="col-xs-8">
                <input id="tlp_rumah" name="tlp_rumah" type="text" class="form-control" value="<?php echo $obj_dosen->getTlpRumah(); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="tlp_hp" class="col-xs-4 control-label">Telp. HP</label>
            <div class="col-xs-8">
                <input id="tlp_hp" name="tlp_hp" type="text" class="form-control" value="<?php echo $obj_dosen->getTlpHp(); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="id_konsentrasi" class="col-xs-4 control-label">Konsentrasi</label>
            <div class="col-xs-8">
                <select id="id_konsentrasi" name="id_konsentrasi" class="form-control">
                    <option value>--pilih--</option>
                    <?php /** @var $konsentrasi ModelKonsentrasi */
                    foreach( $daftar_konsentrasi as $konsentrasi ) : ?>
                        <option value="<?php echo $konsentrasi->getId(); ?>" <?php if( $konsentrasi->getId() == $obj_dosen->getIdKonsentrasi() ) : ?>selected<?php endif; ?> ><?php echo $konsentrasi->getNama(); ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="kode_status_dosen" class="col-xs-4 control-label">Status Dosen</label>
            <div class="col-xs-8">
                <select id="kode_status_dosen" name="kode_status_dosen" class="form-control">
                    <option value>--pilih--</option>
                    <?php /** @var $status ModelStatusDosen */
                    foreach( $daftar_status_dosen as $status ) : ?>
                        <option value="<?php echo $status->getKode(); ?>" <?php if( $status->getKode() == $obj_dosen->getKodeStatusDosen() ) : ?>selected<?php endif; ?> ><?php echo $status->getNama(); ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-8 col-xs-offset-4 col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
            <input type="hidden" name="<?php echo Helpers::aksi_param; ?>" value="<?php echo $is_perbaiki ? Helpers::aksi_perbarui : Helpers::aksi_tambah; ?>">
            <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3; ?>" class="btn btn-danger"><i class="glyphicon glyphicon-arrow-left"></i> Kembali</a>
            <button class="btn btn-success" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
            <?php if( $is_perbaiki ) : ?>
                <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . DS . Helpers::aksi_tambah; ?>" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Baru</a>
            <?php endif; ?>
        </div>
    </div>
    </form>

    <script>
        $(document).ready(function() {
            $('#tgl_lahir').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0"
            });
            $('.form-dosen').bootstrapValidator({
                message: 'Data salah',
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    kode: {
                        validators: {
                            notEmpty: {
                                message: 'Kode tidak boleh kosong'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9]+$/,
                                message: 'Hanya alphanumeric'
                            }
                        }
                    },
                    kode_PS: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih salah satu PRODI'
                            }
                        }
                    },
                    jenjang: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih salah satu jenjang'
                            }
                        }
                    },
                    KTP: {
                        validators: {
                            notEmpty: {
                                message: 'KTP tidak boleh kosong'
                            },
                            regexp: {
                                regexp: /^[0-9]+$/,
                                message: 'Hanya angka'
                            }
                        }
                    },
                    nama: {
                        validators: {
                            notEmpty: {
                                message: 'Nama tidak boleh kosong'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9\s]+$/,
                                message: 'Hanya alphabet dan spasi saja'
                            }
                        }
                    },
                    jenis_kelamin: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih salah satu jenis kelamin'
                            }
                        }
                    },
                    kode_golongan: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih salah satu golongan'
                            }
                        }
                    },
                    kode_jabatan: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih salah satu jabatan'
                            }
                        }
                    },
                    kode_pendidikan_tertinggi: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih salah satu pendidikan tertinggi'
                            }
                        }
                    },
                    id_konsentrasi: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih salah satu konsentrasi'
                            }
                        }
                    },
                    kode_status_dosen: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih salah satu status dosen'
                            }
                        }
                    }
                }
            });
        });
    </script>

<?php elseif( $is_mahasiswa_bimbingan ) : Contents::get_instance()->get_template( 'akun-dosen-mahasiswa-bimbingan' ); else :

    $daftar_dosen = Dosen::get_instance()->_gets( array(
        'kode_PS'           => $list_params[ 'kode_PS' ],
        'nama'              => $list_params[ 'nama' ],
        'number'            => $list_params[ 'number' ],
        'offset'            => ( $list_params[ 'page' ] - 1 ) * $list_params[ 'number' ]
    ) );
    $daftar_dosen_count = Dosen::get_instance()->_count(); ?>

    <h1 class="page-header">
        Dosen
        <small>Daftar</small>
    </h1>

    <?php if( $status ) Helpers::get_instance()->set_message_object( 'Dosen' )->display_message( $status ); ?>

    <div class="row">
        <form>
            <div class="form-group">
                <div class="col-sm-2 col-lg-1">
                    <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . DS . Helpers::aksi_tambah; ?>" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Baru</a>
                </div>
                <?php if( Sessions::get_instance()->_retrieve()->getObjLevelAkses()->is_view_admin() ) : ?>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <select id="kode_PS" name="kode_PS" class="form-control">
                            <option value>--pilih--</option>
                            <?php foreach( $daftar_prodi_by_fakultas as $fakultas => $prodis ) : ?>
                                <optgroup label="<?php echo $fakultas; ?>">
                                    <?php /** @var $prodi ModelProgramStudi **/
                                    foreach( $prodis as $prodi ) : ?>
                                        <option value="<?php echo $prodi->getKode(); ?>" <?php if( $prodi->getKode() == $list_params[ 'kode_PS' ] ) : ?>selected<?php endif; ?> ><?php echo $prodi->getNamaPS(); ?></option>
                                    <?php endforeach; ?>
                                </optgroup>
                            <?php endforeach; ?>
                        </select>
                    </div>
                <?php endif; ?>
                <div class="col-sm-4 col-md-4 col-lg-3">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="glyphicon glyphicon-search"></i></div>
                        <input name="nama" class="form-control" type="text" value="<?php echo $list_params[ 'nama' ]; ?>" placeholder="Nama dosen">
                    </div>
                </div>
                <div class="col-sm-2 col-lg-1">
                    <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-filter"></i> Filter</button>
                </div>
            </div>
        </form>
    </div>
    <br/>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>NIP</th>
            <th>Nama</th>
            <th>Gelar Tertinggi</th>
            <th>Jenjang</th>
            <th>Jabatan</th>
            <th>#</th>
        </tr>
        </thead>
        <tbody>
        <?php /** @var $dosen ModelDosen */
        foreach( $daftar_dosen as $dosen ) : ?>
            <tr>
                <td><?php echo $dosen->getNIP(); ?></td>
                <td><?php echo $dosen->getNama(); ?></td>
                <td><?php echo $dosen->getGelarTertinggi(); ?></td>
                <td><?php echo $dosen->getJenjang(); ?></td>
                <td><?php echo $dosen->getNamaJabatan(); ?></td>
                <td>
                    <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS .$tingkat3 . DS . Helpers::aksi_hapus . DS . $dosen->getKode(); ?>" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                    <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS .$tingkat3 . DS . Helpers::aksi_perbaiki . DS . $dosen->getKode(); ?>" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                    <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS .$tingkat3 . DS . $dosen->getKode() . DS . Helpers::aksi_mahasiswa_bimbingan; ?>" title="Mahasiswa bimbingan"><i class="fa fa-smile-o"></i></a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?php $base_link = SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . '?';
    $base_link .= '&kode_PS=' . $list_params[ 'kode_PS' ];
    $base_link .= '&nama=' . $list_params[ 'nama' ];
    siakad_paging_nav( $base_link, $daftar_dosen_count, $list_params[ 'page' ], 20 );
endif; ?>