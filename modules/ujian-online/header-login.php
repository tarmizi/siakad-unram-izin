<?php

namespace Ujian_Online;

use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Helpers;
use SIAKAD\Controller\Sessions;

/**
 * @todo
 *
 * jika sesi tidak valid,
 * buang ke halaman login
 *
 * perbaiki untuk support module
 */
//Sessions::get_instance()->_validate( SIAKAD_URI_PATH );

$headers = Headers::get_instance()
    ->add_head_meta( array(
        array( 'charset' => 'utf-8' ),
        array( 'http-equiv' => 'X-UA-Compatible', 'content' => 'IE=edge' ),
        array( 'name' => 'viewport', 'content' => 'width=device-width, initial-scale=1' ),
        array( 'name' => 'description', 'content' => SIAKAD_APP_DESCRIPTION ),
        array( 'name' => 'author', 'content' => SIAKAD_APP_AUTHOR )
    ) )
    ->add_style( 'jquery-ui.css' )
    ->add_style( 'bootstrap.min.css' )
    ->add_style( 'font-awesome.min.css' )
    ->add_style( 'login.css' )
    ->add_head_script( 'jquery.min.js' )
    ->add_head_script( 'jquery.ui.min.js' )
    ->add_head_script( 'bootstrap.min.js' )
    ->add_head_script( 'bootstrapValidator.min.js' )
    ->add_favicon();

?>

<!DOCTYPE html>
<html lang="id">
<head>
    <?php echo $headers->get_head_meta(); ?>
    <?php echo $headers->get_head_title(); ?>
    <?php echo $headers->get_head_link(); ?>
    <?php echo $headers->get_head_script(); ?>
</head>
