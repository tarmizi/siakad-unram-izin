<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Controller;

use SIAKAD\Model\DosenPengajar as ModelDosenPengajar;

class DosenPengajar extends Databases {

    private $field_id;
    private $class_name;
    private $table_name;
    private $table_dosen;
    private $table_matakuliah;
    private $count_query;

    /** @var DosenPengajar $instance */
    private static $instance;

    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function __construct() {
        parent::_connect();
        $this->field_id = 'id';
        $this->class_name = '\SIAKAD\Model\DosenPengajar';
        $this->table_name = 'dosen_pengajar';
        $this->count_query = 'SELECT * FROM ' . $this->table_name;
    }

    /**
     * @param $kode
     * @param string $by
     * @return DosenPengajar
     */
    function _get( $kode, $by = '' ) {
        $query = 'SELECT ' . $this->table_name . '.* FROM '. $this->table_name;
        $query .= ' WHERE ' . $this->table_name . '.`' . ( empty( $by ) ? $this->field_id : $kode ) . '` = "' . $kode . '"';
        return ( $return = mysql_fetch_object(
            mysql_query( $query ), $this->class_name
        ) ) ? $return : new $this->class_name;
    }

    function _gets( $args = array() ) {

        $return = array();

        $default_args = array(
            'kode_dosen'                => -1,
            'id_matakuliah'             => -1,
            'exclude'                   => array(),
            'conditions'                => '',
            'orderby'                   => $this->field_id,
            'order'                     => 'DESC',
            'number'                    => 10,
            'offset'                    => 0
        );

        $list_args = sync_default_params( $default_args, $args );
        $query = 'SELECT ' . $this->table_name . '.* FROM ' . $this->table_name . ' WHERE 1';

        if( $list_args[ 'kode_dosen' ] >= 0 )
            $query .= ' AND ' . $this->table_name . '.kode_dosen = "' . $list_args[ 'kode_dosen' ] . '"';

        if( $list_args[ 'id_matakuliah' ] >= 0 )
            $query .= ' AND ' . $this->table_name . '.id_matakuliah = "' . $list_args[ 'id_matakuliah' ] . '"';

        /**
         * exclude
         */
        if( !empty( $list_args[ 'exclude' ] ) ) {

            foreach( $list_args[ 'exclude' ] as $ex )
                $query .= ' AND `kode` <> ' . $ex;

        }

        /**
         * untuk custom query pada conditions
         */
        if( !empty( $list_args[ 'conditions' ] ) ) {
            foreach( $list_args[ 'conditions' ] as $conditions )
                $query .= ' AND ' . $list_args . '.' . $conditions[ 'field' ] . ' ' . $conditions[ 'operator' ] . ' ' . $conditions[ 'comparison' ];
        }

        $this->count_query = $query;

        /**
         * orderby dan jenis order
         */
        $query .= ' ORDER BY `' . $list_args[ 'orderby' ] . '` ' . $list_args[ 'order' ];

        /**
         * limit
         */
        if( $list_args[ 'number' ] >= 0 )
            $query .= ' LIMIT ' . $list_args[ 'offset' ] . ', ' . $list_args[ 'number' ];

        $resource = mysql_query( $query );

        //echo $query . ' : ' . mysql_error();

        while( $row = mysql_fetch_object( $resource, $this->class_name ) )
            $return[] = $row;

        return $return;

    }

    function insert( ModelDosenPengajar $a ) {
        return parent::insert(
            $this->table_name,
            array(
                'id_matakuliah',
                'kode_dosen',
                'tahun_akademik',
                'nama_kelas'
            ),
            array(
                array(
                    $a->getIdMatakuliah(),
                    $a->getKodeDosen(),
                    $a->getTahunAkademik(),
                    $a->getNamaKelas()
                )
            )
        );
    }

    /**
     * karena ini tabel relasi, maka butuh perlakuan khusus
     * secara default
     *
     * @param ModelDosenPengajar $a
     * @param string $field
     * @return bool
     */
    function update( ModelDosenPengajar $a, $field = 'kode_dosen' ) {
        return parent::update(
            $this->table_name,
            array(
                'id_matakuliah',
                'kode_dosen',
                'tahun_akademik',
                'nama_kelas'
            ),
            array(
                $a->getIdMatakuliah(),
                $a->getKodeDosen(),
                $a->getTahunAkademik(),
                $a->getNamaKelas()
            ),
            'id_matakuliah' == $field ?
                array( 'id_matakuliah', $a->getIdMatakuliah() ) :
                array( $field, $a->getKodeDosen() )
        );
    }

    function delete( $kode ) {
        return parent::delete(
            $this->table_name,
            'id',
            $kode
        );
    }
} 