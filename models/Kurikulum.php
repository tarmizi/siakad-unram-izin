<?php
/**  
 * Canvas Production
 *
 * Author : Dani
 */

namespace SIAKAD\Model;


class Kurikulum {

    private $id;
    private $kode_PS;
    private $tahun_akademik;
    private $status;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $idKurikulum
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $kodeMK
     */
    public function setKodePs($kode_PS)
    {
        $this->kode_PS = $kode_PS;
    }

    /**
     * @return mixed
     */
    public function getKodePs()
    {
        return $this->kode_PS;
    }

    /**
     * @param mixed $namaMK
     */
    public function setTahunAkademik($tahun_akademik)
    {
        $this->tahun_akademik = $tahun_akademik;
    }

    /**
     * @return mixed
     */
    public function getTahunAkademik()
    {
        return $this->tahun_akademik;
    }
} 