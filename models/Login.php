<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Model;

use SIAKAD\Controller\Securities;

class Login {

    private $username;
    private $password;
    private $status;

    private $kode_akses;
    private $nama_level;
    private $deskripsi;

    function _init( $request ) {
        !isset( $request[ 'username' ] ) || $this->username = $request[ 'username' ];
        !isset( $request[ 'password' ] ) || empty( $request[ 'password' ] ) || $this->setPassword( $request[ 'password' ] );
        !isset( $request[ 'status' ] ) || $this->status = $request[ 'status' ];
        return $this;
    }

    /**
     * @param $deskripsi
     * @return $this
     */
    public function setDeskripsi($deskripsi)
    {
        $this->deskripsi = $deskripsi;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeskripsi()
    {
        return $this->deskripsi;
    }

    /**
     * @param $kode_akses
     * @return $this
     */
    public function setKodeAkses($kode_akses)
    {
        $this->kode_akses = $kode_akses;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKodeAkses()
    {
        return $this->kode_akses;
    }

    /**
     * @param $nama_level
     * @return $this
     */
    public function setNamaLevel($nama_level)
    {
        $this->nama_level = $nama_level;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNamaLevel()
    {
        return $this->nama_level;
    }

    /**
     * @param $password
     * @param bool $encrypt
     * @return $this
     */
    public function setPassword( $password, $encrypt = true )
    {
        $this->password = $encrypt ? Securities::get_instance()->encrypt( $password) : $password;
        return $this;
    }

    /**
     * @param bool $encrypted
     * @return bool|string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }



} 