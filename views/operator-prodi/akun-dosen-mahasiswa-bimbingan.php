<?php

namespace SIAKAD\Views\Operator\PRODI;

use SIAKAD\Controller\Dosen;
use SIAKAD\Controller\DosenPa;
use SIAKAD\Controller\Helpers;
use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Mahasiswa;

use SIAKAD\Model\DosenPa as ModelDosenPa;

global $status, $tingkat1, $tingkat2, $tingkat3;

$tingkat4 = Routes::get_instance()->get_tingkat( 4 );
$tingkat5 = Routes::get_instance()->get_tingkat( 5 );

$is_simpan = Routes::get_instance()->is_tingkat( 6, Helpers::aksi_simpan )
    && isset( $_REQUEST[ 'NIM' ] );
$is_hapus = Routes::get_instance()->has_tingkat( 6 )
    && Routes::get_instance()->is_tingkat( 7, Helpers::aksi_hapus );

$obj_dosen = Dosen::get_instance()->_get( Routes::get_instance()->get_tingkat( 4 ) );

if( $is_simpan ) {
    $obj_dosen_pa = new ModelDosenPa();
    $obj_dosen_pa->setKodeDosen( $obj_dosen->getKode() )->setNimMahasiswa( $_REQUEST[ 'NIM' ] );

    siakad_redirect(
        SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . DS . $tingkat4 . DS . $tingkat5 . DS . '?status=' . (
            DosenPa::get_instance()->insert( $obj_dosen_pa ) ? 1 : 9999
        )
    );

} elseif( $is_hapus ) {
    siakad_redirect(
        SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . DS . $tingkat4 . DS . $tingkat5 . DS . '?status=' . (
            DosenPa::get_instance()->delete( Routes::get_instance()->get_tingkat( 6 ) ) ? 3 : 9999
        )
    );
}

/** ambil daftar mahasiswa bimbingan */
$default_params = array(
    'kode_dosen'    => $obj_dosen->getKode(),
    'number'        => 20,
    'page'          => 1,
);
$list_params = sync_default_params( $default_params, $_GET );

$mahasiswa_bimbingan_lists = DosenPa::get_instance()->_gets( array(
    'kode_dosen'        => $list_params[ 'kode_dosen' ],
    'number'            => $list_params[ 'number' ],
    'offset'            => ( $list_params[ 'page' ] - 1 ) * $list_params[ 'number' ]
) );
$mahasiswa_bimbingan_lists_count = DosenPa::get_instance()->_count();

/**
 * ini nanti digunakan di API
 * untuk exclude mahasiswa yang sudah masuk di daftar
 * ketika melakukan request daftar mahasiswa
 */
$mahasiswa_bimbingan_lists_nim = array();

?>

<h1 class="page-header">
    <?php echo $obj_dosen->getNama(); ?>
    <small>Mahasiswa Bimbingan</small>
</h1>

<?php if( $status ) Helpers::get_instance()->set_message_object( 'Mahasiswa' )->display_message( $status ); ?>

<div class="row">
    <div class="col-sm-8">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>NIM</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Tahun Masuk</th>
                <th>#</th>
            </tr>
            </thead>
            <tbody>
            <?php /** @var $dosen_pa ModelDosenPa */
            foreach( $mahasiswa_bimbingan_lists as $dosen_pa ) :
                $mahasiswa_bimbingan_lists_nim[] = $dosen_pa->getNimMahasiswa(); ?>
                <tr>
                    <td><?php echo $dosen_pa->getNimMahasiswa(); ?></td>
                    <td><?php echo $dosen_pa->getNamaMahasiswa(); ?></td>
                    <td><?php echo Mahasiswa::$jenis_kelamin[ $dosen_pa->getJnsKelamin() ]; ?></td>
                    <td><?php echo $dosen_pa->getThnMasuk(); ?></td>
                    <td><a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . DS . $tingkat4 . DS . $tingkat5 . DS . $dosen_pa->getNimMahasiswa() . DS . Helpers::aksi_hapus; ?>" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <?php $base_link = SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . DS . $tingkat4 . DS . $tingkat5 . '?';
        siakad_paging_nav( $base_link, $mahasiswa_bimbingan_lists_count, $list_params[ 'page' ], 20 ); ?>
    </div>
    <div class="col-sm-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <strong><i class="glyphicon glyphicon-plus"></i> Mahasiswa</strong>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" action="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . DS . $tingkat4 . DS . $tingkat5 . DS . Helpers::aksi_simpan; ?>" method="post">
                    <div class="form-group">
                        <div class="col-sm-8">
                            <label><input name="NIM" id="NIM" type="text" class="form-control" placeholder="NIM mahasiswa"></label>
                        </div>
                        <div class="col-sm-4">
                            <button id="submit" class="btn btn-sm btn-primary" disabled><i class="glyphicon glyphicon-floppy-disk"></i> Tambah</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <p class="text-muted"><i>Mulai dengan mengetikan <strong>NIM</strong> mahasiswa maka secara otomatis akan muncul daftar mahasiswa yang relevan selama progress pengetikan, jangan lupa untuk mencocokan dengan <strong>Nama</strong> mahasiswa.</i></p>
                        </div>
                    </div>
                </form>

                <script>
                    $(function() {

                        $( "#NIM" ).autocomplete({
                            source: function(request, response) {
                                //console.log(request);
                                $.ajax({
                                    url: "<?php echo SIAKAD_URI_API_PATH; ?>/Mahasiswa",
                                    dataType: "json",
                                    data: {
                                        NIM: request.term,
                                        exclude: <?php echo json_encode( $mahasiswa_bimbingan_lists_nim ); ?>,
                                        conditions: <?php echo json_encode( array( array( 'field' => 'mahasiswa.NIM', 'operator' => 'NOT IN', 'comparison' => '(SELECT nim_mahasiswa FROM dosen_pa)' ) ) ); ?>,
                                        number: -1,
                                        kode_prodi: <?php echo $obj_dosen->getKodePS(); ?>
                                    },
                                    success: function(mahasiswa_lists) {
                                        //console.log(mahasiswa_lists);
                                        response($.map(mahasiswa_lists, function(mahasiswa) {
                                            //console.log(mahasiswa);
                                            return {
                                                label: mahasiswa.nama + ' (' + mahasiswa.NIM + ')',
                                                value: mahasiswa.NIM
                                            }
                                        }));
                                    }
                                });
                            },
                            select: function(e, ui) {
                                var submitButton = $('#submit');
                                if(ui.item) $(submitButton).removeAttr('disabled');
                                else $(submitButton).attr('disabled');
                            }
                        });
                    });
                </script>

            </div>
        </div>
    </div>
</div>