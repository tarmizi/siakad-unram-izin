<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Model;


class Session {

    /** @var $obj_login Login **/
    private $obj_login;

    /** @var $obj_level_akses LevelAkses */
    private $obj_level_akses;

    private $view;
    private $user_agent;
    private $ip_address;

    public function __construct() {
        $this
            ->setUserAgent( $_SERVER[ 'HTTP_USER_AGENT' ] )
            ->setIpAddress( $_SERVER[ 'REMOTE_ADDR' ] );
    }

    /** @var Session $instance */
    private static $instance;

    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function _init( Login $obj_login, LevelAkses $obj_level_akses ) {
        $this->obj_login = $obj_login;
        $this->obj_level_akses = $obj_level_akses;
        return $this;
    }

    /**
     * @return LevelAkses
     */
    public function getObjLevelAkses()
    {
        return $this->obj_level_akses;
    }

    /**
     * @return Login
     */
    public function getObjLogin()
    {
        return $this->obj_login;
    }

    /**
     * @param $ip_address
     * @return $this
     */
    public function setIpAddress($ip_address)
    {
        $this->ip_address = $ip_address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIpAddress()
    {
        return $this->ip_address;
    }

    /**
     * @param $ip_address
     * @return bool
     */
    public function compareIpAddress( $ip_address )
    {
        return $this->ip_address == $ip_address;
    }

    /**
     * @param $user_agent
     * @return $this
     */
    public function setUserAgent($user_agent)
    {
        $this->user_agent = $user_agent;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserAgent()
    {
        return $this->user_agent;
    }

    /**
     * @param $user_agent
     * @return bool
     */
    public function compareUserAgent( $user_agent )
    {
        return $this->user_agent == $user_agent;
    }

    /**
     * @param $view
     * @return $this
     */
    public function setView($view)
    {
        $this->view = $view;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getView()
    {
        return $this->view;
    }



} 