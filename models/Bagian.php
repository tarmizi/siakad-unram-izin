<?php
/**  
 * Canvas Production
 *
 * Author : Dani
 */

namespace SIAKAD\Model;


class Bagian {

    private $id;
    private $kode_PS;
    private $nomor;
    private $nama;
    private $id_konsentrasi;
    

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $nama
     */
    public function setNama($nama)
    {
        $this->nama = $nama;
    }

    /**
     * @return mixed
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * @param $kode_PS
     */
    public function setKodePs($kode_PS)
    {
        $this->kode_PS = $kode_PS;
    }

    /**
     * @return mixed
     */
    public function getKodePs()
    {
        return $this->kode_PS;
    }

    /**
     * @param $nomor
     */
    public function setNomor($nomor)
    {
        $this->nomor = $nomor;
    }

    /**
     * @return mixed
     */
    public function getNomor()
    {
        return $this->nomor;
    }

    /**
     * @param $id_konsentrasi
     */
    public function setIdKonsentrasi($id_konsentrasi)
    {
        $this->id_konsentrasi = $id_konsentrasi;
    }

    /**
     * @return mixed
     */
    public function getIdKonsentrasi()
    {
        return $this->id_konsentrasi;
    }
} 