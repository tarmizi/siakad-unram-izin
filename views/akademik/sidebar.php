<?php

namespace SIAKAD\Views\Operator\Akademik;

use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Contents;

$base_link = SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view(); ?>
<ul class="nav nav-sidebar">
    <li <?php echo 'Operator SIAKAD' == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;Ahmad Zafrullah</a></li>
    <li <?php echo ( $page = 'Statistik' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/statistik"><i class="fa fa-bar-chart-o"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
</ul>