<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

if( $is_peserta ) : ?>

    <h1 class="page-header">
        Kelas A
        <small>Peserta</small>
    </h1>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><input type="checkbox"></th>
            <th>NIM</th>
            <th>Nama</th>
            <th>#</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach( range( 1, 5 ) as $i ) : ?>
            <tr>
                <td><input type="checkbox"></td>
                <td>1234-<?php echo $i; ?></td>
                <td>Ahmad Zafrullah</td>
                <td>
                    <div class="form-group">
                        <select class="form-control">
                            <option>--pindah</option>
                            <option>Kelas B</option>
                            <option>Kelas C</option>
                        </select>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <button class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
<?php else : ?>
    <h1 class="page-header">
        Mata Kuliah
        <small>Kelas</small>
    </h1>
    <div class="row">
        <div class="col-md-9">
            <form class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-2">
                        <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2; ?>/tambah" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control">
                            <option>--semester</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control">
                            <option>--bidang keahlian</option>
                            <option>Sistem Cerdas</option>
                        </select>
                    </div>
                    <div class="col-sm-1">
                        <button class="btn btn-primary"><i class="fa fa-filter"></i> OK</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-3">
            <form>
                <div class="input-group">
                    <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                    </span>
                </div>
            </form>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-sm-8">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Mata Kuliah</th>
                    <th>Kelas</th>
                    <th>Dosen</th>
                    <th>Peserta saat ini</th>
                    <th>#</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach( range( 1, 3 ) as $i ) : ?>
                    <tr>
                        <td>Bahasa Sasak</td>
                        <td>A-<?php echo $i; ?></td>
                        <td>Ahmad Zafrullah</td>
                        <td>32</td>
                        <td>
                            <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3; ?>/perbaiki/<?php echo $i; ?>" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                            <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3; ?>/hapus/<?php echo $i; ?>" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                            <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3 . DS . $i; ?>/peserta" title="Daftar Peserta"><i class="fa fa-users"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <strong><i class="glyphicon glyphicon-plus"></i> Tambah Kelas</strong>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" action="" method="post">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Nama Kelas</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Dosen</label>
                            <div class="col-sm-8">
                                <select class="form-control">
                                    <option>Ahmad Zafrullah</option>
                                    <option>Afiuw Zumala</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Mata Kuliah</label>
                            <div class="col-sm-8">
                                <select class="form-control">
                                    <option>Bahasa Sasak</option>
                                    <option>Bahasa Jerman</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-4">
                                <button class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?> <!-- peserta -->

<?php