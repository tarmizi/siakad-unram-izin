<?php

namespace SIAKAD\Views\Mahasiswa;

use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Sessions;
use SIAKAD\Controller\Mahasiswa;

$obj_login = Sessions::get_instance()->_retrieve()->getObjLogin();

/** username = NIM mahasiswa */
$obj_mahasiswa = Mahasiswa::get_instance()->_get( $obj_login->getUsername()  );
$base_link = SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view(); ?>

<ul class="nav nav-sidebar">
    <li <?php echo $obj_login->getNamaLevel() == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>"><i class="fa fa-user"></i>&nbsp;&nbsp;<?php echo $obj_mahasiswa->getNama(); ?></a></li>
    <li <?php echo ( $page = 'Pesan' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/pesan"><i class="fa fa-envelope"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
    <li <?php echo ( $page = 'Mata Kuliah' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/mata-kuliah"><i class="fa fa-book"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
    <li <?php echo ( $page = 'Kehadiran' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/kehadiran"><i class="fa fa-calendar"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
    <li <?php echo ( $page = 'KRS' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/krs"><i class="fa fa-sitemap"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
    <li <?php echo ( $page = 'Transkrip Nilai' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/nilai"><i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
    <li <?php echo ( $page = 'Pengumuman' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/pengumuman"><i class="fa fa-paste"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
</ul>