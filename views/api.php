<?php
/**
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 *
 * untuk keperluan akses ke beberapa data,
 * biasanya dalam metode ajax
 */

namespace SIAKAD\Views\Publik;

use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Mahasiswa;

use SIAKAD\Model\Mahasiswa as ModelMahasiswa;

header('Content-Type: application/json');

if( Routes::get_instance()->is_tingkat( 2, 'Mahasiswa' ) ) {
    $mahasiswa_lists_array = array();
    $mahasiswa_lists = Mahasiswa::get_instance()->_gets( $_REQUEST );
    /** @var $mahasiswa ModelMahasiswa */
    foreach( $mahasiswa_lists as $mahasiswa ) $mahasiswa_lists_array[] = $mahasiswa->toArray();
    echo json_encode( $mahasiswa_lists_array );
}

else echo json_encode( array( false ) );