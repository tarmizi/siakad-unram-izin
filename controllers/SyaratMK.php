<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Controller;

use SIAKAD\Model\SyaratMK as SyaratMatakuliah;

class SyaratMK extends Databases {

    private $field_id;
    private $class_name;
    private $table_name;
    private $count_query;
    private $table_matakuliah;
    private $table_indeks_nilai;
    private $table_kurikulum;

    /** @var SyaratMatakuliah $instance */
    private static $instance;

    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function __construct() {
        parent::_connect();
        $this->field_id = 'id';
        $this->class_name = '\SIAKAD\Model\SyaratMK';
        $this->table_name = 'syaratmatakuliah';
        $this->table_kurikulum = 'kurikulum';
        $this->table_matakuliah = 'matakuliah';
        $this->table_indeks_nilai = 'indeks_nilai';
        $this->count_query = 'SELECT * FROM ' . $this->table_name;
    }

    /**
     * @param $kode
     * @param string $by
     * @return SyaratMatakuliah
     */
    function _get( $kode, $by = '' ) {
        $query = 'SELECT ' . $this->table_name . '.* FROM';
        $query .= ' WHERE ' . $this->table_name . '.`' . ( empty( $by ) ? $this->field_id : $kode ) . '` = "' . $kode . '"';
        return ( $return = mysql_fetch_object(
            mysql_query( $query ), $this->class_name
        ) ) ? $return : new $this->class_name;
    }

    function _gets( $args = array() ) {

        $return = array();

        $default_args = array(
            'id_mk'                     => '',
            'id_mk_prasyarat'           => '',
            'exclude'                   => array(),
            'conditions'                => '',
            'orderby'                   => $this->field_id,
            'order'                     => 'DESC',
            'number'                    => 10,
            'offset'                    => 0
        );

        $list_args = sync_default_params( $default_args, $args );
        $query = 'SELECT ' . $this->table_name . '.*';
        $query .= ', ' . $this->table_kurikulum . '.tahun_akademik AS tahun_akademik_kurikulum';
        $query .= ', ' . $this->table_matakuliah . '.nama_mk AS nama_matakuliah';
        $query .= ', ' . $this->table_indeks_nilai . '.huruf AS indeks_nilai';
        $query .= ' FROM ' . $this->table_name;
        $query .= ' LEFT JOIN ' . $this->table_kurikulum . ' ON ' . $this->table_kurikulum . '.id = ' . $this->table_name . '.id_kurikulum';
        $query .= ' LEFT JOIN ' . $this->table_matakuliah . ' ON ' . $this->table_matakuliah . '.id = ' . $this->table_name . '.id_mk';
        $query .= ' LEFT JOIN ' . $this->table_indeks_nilai . ' ON ' . $this->table_indeks_nilai . '.angka = ' . $this->table_name . '.nilai_prasyarat';
        $query .= ' WHERE 1';

        /**
         * kode_mk
         */
        if( !empty( $list_args[ 'id_mk' ] ) )
            $query .= ' AND ' . $this->table_matakuliah . '.id = \'' . $list_args[ 'id_mk' ].'\'';

        /**
         * kode_mk_prasyarat
         */
        if( !empty( $list_args[ 'kode_mk_prasyarat' ] ) )
            $query .= ' AND ' . $this->table_matakuliah . '.kode_mk = ' . $this->table_name . '.id_mk_prasyarat'. ' AND ' . $this->table_matakuliah . '.kode_mk = ' . $list_args[ 'kode_mk' ];

        
        /**
         * exclude
         */
        if( !empty( $list_args[ 'exclude' ] ) ) {

            foreach( $list_args[ 'exclude' ] as $ex )
                $query .= ' AND `kode` <> ' . $ex;

        }

        /**
         * untuk custom query pada conditions
         */
        if( !empty( $list_args[ 'conditions' ] ) ) {
            foreach( $list_args[ 'conditions' ] as $conditions )
                $query .= ' AND ' . $list_args . '.' . $conditions[ 'field' ] . ' ' . $conditions[ 'operator' ] . ' ' . $conditions[ 'comparison' ];
        }

        $this->count_query = $query;

        /**
         * orderby dan jenis order
         */
        $query .= ' ORDER BY `' . $list_args[ 'orderby' ] . '` ' . $list_args[ 'order' ];

        /**
         * limit
         */
        if( $list_args[ 'number' ] >= 0 )
            $query .= ' LIMIT ' . $list_args[ 'offset' ] . ', ' . $list_args[ 'number' ];

        $resource = mysql_query( $query );

        //echo $query . ' : ' . mysql_error();

        while( $row = mysql_fetch_object( $resource, $this->class_name ) )
            $return[] = $row;

        return $return;

    }

    function insert( SyaratMatakuliah $a ) {
        return parent::insert(
            $this->table_name,
            array(
                'id',
                'id_kurikulum',
                'urutan',
                'kondisi',
                'prioritas',
                'id_mk',
                'id_mk_prasyarat',
                'sudah_ambil',
                'id_indeks_nilai_prasyarat',
                'syarat_sks_lulus',
                'syarat_sks_berjalan'
            ),
            array(
                array(
                    $a->getId(),
                    $a->getIdKurikulum(),
                    $a->getUrutan(),
                    $a->getKondisi(),
                    $a->getPrioritas(),
                    $a->getIdMk(),
                    $a->getIdMkPrasyarat(),
                    $a->getSudahAmbil(),
                    $a->getIdIndeksNilaiPrasyarat(),
                    $a->getSyaratSksLulus(),
                    $a->getSyaratSksBerjalan()
                )
            )
        );
    }

    function update( SyaratMatakuliah $a ) {
        return parent::update(
            $this->table_name,
            array(
                'id',
                'id_kurikulum',
                'urutan',
                'kondisi',
                'prioritas',
                'id_mk',
                'id_mk_prasyarat',
                'sudah_ambil',
                'id_indeks_nilai_prasyarat',
                'syarat_sks_lulus',
                'syarat_sks_berjalan'
            ),
            array(
                $a->getId(),
                $a->getIdKurikulum(),
                $a->getUrutan(),
                $a->getKondisi(),
                $a->getPrioritas(),
                $a->getIdMk(),
                $a->getIdMkPrasyarat(),
                $a->getSudahAmbil(),
                $a->getIdIndeksNilaiPrasyarat(),
                $a->getSyaratSksLulus(),
                $a->getSyaratSksBerjalan()
            ),
            array(
                'id', $a->getId()
            )
        );
    }

    function delete( $kode ) {
        return parent::delete(
            $this->table_name,
            'id',
            $kode
        );
    }
} 