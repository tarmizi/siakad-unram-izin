<?php

namespace SIAKAD\Views\Operator\PRODI;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Helpers;
use SIAKAD\Controller\ProgramStudi;
use SIAKAD\Controller\Sessions;

use SIAKAD\Model\ProgramStudi as ModelProgramStudi;

$status = false;
$tingkat1 = Routes::get_instance()->get_tingkat( 1 );
$is_simpan = Routes::get_instance()->is_tingkat( 2, Helpers::aksi_simpan );

$obj_prodi = new ModelProgramStudi();

if( $is_simpan ) {
    $obj_prodi->_init( $_REQUEST );
    $status = ProgramStudi::get_instance()->update( $obj_prodi );
}

else
    $obj_prodi = ProgramStudi::get_instance()->_get(
        Sessions::get_instance()->_retrieve()->getObjLevelAkses()->getKodeObject(), 'kode_prodi_unram'
    );


/** /
echo '<pre>'; print_r( Sessions::get_instance()->_retrieve() ); echo '</pre>';
echo '<pre>'; print_r( $obj_prodi ); echo '</pre>';
/**/

Headers::get_instance()
    ->set_page_title( 'Operator Program Studi' )
    ->set_page_name( 'Operator PRODI' );

Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header"><?php echo $obj_prodi->getNamaPS(); ?></h1>

            <?php if( $is_simpan ) : if( $status ) : ?>
                <div class="alert alert-success">
                    <p><i class="glyphicon glyphicon-ok"></i>&nbsp;&nbsp;Data berhasil di perbarui.</p>
                </div>
            <?php else : ?>
                <div class="alert alert-danger">
                    <p><i class="glyphicon glyphicon-remove"></i>&nbsp;&nbsp;Terjadi kesalahan, harap menghubungi developer.</p>
                </div>
            <?php endif; endif; ?>

            <form action="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . Helpers::aksi_simpan; ?>" class="form-prodi form-horizontal" method="post">

            <!-- sebelah kiri -->
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kode_nasional" class="col-xs-4 control-label">Kode Nasional</label>
                    <div class="col-xs-8">
                        <input id="kode_nasional" name="kode_nasional" type="text" class="form-control" placeholder="Kode nasional" value="<?php echo $obj_prodi->getKodeNasional(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="jenis" class="col-xs-4 control-label">Jenis</label>
                    <div class="col-xs-8">
                        <select id="jenis" name="jenis" class="form-control">
                            <option value>--pilih--</option>
                            <option value="1" <?php if( $obj_prodi->getJenis() == 1 ) : ?>selected<?php endif; ?> >Reguler</option>
                            <option value="0" <?php if( $obj_prodi->getJenis() == 0 ) : ?>selected<?php endif; ?> >Ekstensi</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="kode_fakultas" class="col-xs-4 control-label">Fakultas</label>
                    <div class="col-xs-8">
                        <input type="hidden" name="kode_fakultas" value="<?php echo $obj_prodi->getKodeFakultas(); ?>">
                        <p class="form-control-static"><?php echo $obj_prodi->getNamaFakultas(); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="kode_prodi_unram" class="col-xs-4 control-label">Kode PRODI UNRAM</label>
                    <div class="col-xs-8">
                        <input id="kode_prodi_unram" name="kode_prodi_unram" type="text" class="form-control" placeholder="Kode dari UNRAM" value="<?php echo $obj_prodi->getKodeProdiUnram(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="nama_nasional" class="col-xs-4 control-label">Nama Nasional</label>
                    <div class="col-xs-8">
                        <input id="nama_nasional" name="nama_nasional" type="text" class="form-control" placeholder="Nama dari nasional" value="<?php echo $obj_prodi->getNamaNasional(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="nama_nasional_ingg" class="col-xs-4 control-label">Nama Nasional (Ingg)</label>
                    <div class="col-xs-8">
                        <input id="nama_nasional_ingg" name="nama_nasional_ingg" type="text" class="form-control" placeholder="Nama dari nasional Ingg" value="<?php echo $obj_prodi->getNamaNasionalIngg(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="jenjang" class="col-xs-4 control-label">Jenjang</label>
                    <div class="col-xs-8">
                        <select id="jenjang" name="jenjang" class="form-control">
                            <option value>--pilih--</option>
                            <?php foreach( ProgramStudi::$jenjang as $jenjang ) : ?>
                                <option value="<?php echo $jenjang; ?>" <?php if( $jenjang == $obj_prodi->getJenjang() ) : ?>selected<?php endif; ?> ><?php echo $jenjang; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="nama_PS" class="col-xs-4 control-label">Nama PRODI UNRAM</label>
                    <div class="col-xs-8">
                        <input id="nama_PS" name="nama_PS" type="text" class="form-control" placeholder="Nama dari UNRAM" value="<?php echo $obj_prodi->getNamaPS(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="keterangan" class="col-xs-4 control-label">Keterangan</label>
                    <div class="col-xs-8">
                        <textarea id="keterangan" name="keterangan" class="form-control"><?php echo $obj_prodi->getKeterangan(); ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="kurikulum_pakai" class="col-xs-4 control-label">Kurikulum Pakai</label>
                    <div class="col-xs-8">
                        <select id="kurikulum_pakai" name="kurikulum_pakai" class="form-control">
                            <option value>--pilih--</option>
                            <option value="1" <?php if( $obj_prodi->getKurikulumPakai() == 1 ) : ?>selected<?php endif; ?> >Semua</option>
                            <option value="0" <?php if( $obj_prodi->getKurikulumPakai() == 0 ) : ?>selected<?php endif; ?> >Sebagian</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="alamat_prodi" class="col-xs-4 control-label">Alamat</label>
                    <div class="col-xs-8">
                        <textarea id="alamat_prodi" name="alamat_prodi" class="form-control"><?php echo $obj_prodi->getAlamatProdi(); ?></textarea>
                    </div>
                </div>
            </div>

            <!-- sebelah kanan -->
            <div class="col-md-6">
                <div class="form-group">
                    <label for="sk_dikti" class="col-xs-4 control-label">SK DIKTI</label>
                    <div class="col-xs-8">
                        <input id="sk_dikti" name="sk_dikti" type="text" class="form-control" value="<?php echo $obj_prodi->getSkDikti(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="tgl_sk_dikti" class="col-xs-4 control-label">Tanggal SK DIKTI Mulai</label>
                    <div class="col-xs-8">
                        <div class="input-group date">
                            <input id="tgl_sk_dikti" type="text" class="form-control" name="tgl_sk_dikti" value="<?php echo $obj_prodi->getTglSkDikti(); ?>"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tgl_akhir_sk_dikti" class="col-xs-4 control-label">Tanggal SK DIKTI Berakhir</label>
                    <div class="col-xs-8">
                        <div class="input-group date">
                            <input id="tgl_akhir_sk_dikti" type="text" class="form-control" name="tgl_akhir_sk_dikti" value="<?php echo $obj_prodi->getTglAkhirSkDikti(); ?>"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="jumlah_sks_kelulusan" class="col-xs-4 control-label">Jumlah SKS Kelulusan</label>
                    <div class="col-xs-8">
                        <input id="jumlah_sks_kelulusan" name="jumlah_sks_kelulusan" type="text" class="form-control" value="<?php echo $obj_prodi->getJumlahSksKelulusan(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="status" class="col-xs-4 control-label">Status</label>
                    <div class="col-xs-8">
                        <select id="status" name="status" class="form-control">
                            <option value="A" <?php if( $obj_prodi->getStatus() == 'A' ) : ?>selected<?php endif; ?> >Aktif</option>
                            <option value="T" <?php if( $obj_prodi->getStatus() == 'T' ) : ?>selected<?php endif; ?> >Tidak Aktif</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tahun_semester_mulai" class="col-xs-4 control-label">Tahun Semester Mulai</label>
                    <div class="col-xs-8">
                        <input id="tahun_semester_mulai" name="tahun_semester_mulai" type="text" class="form-control" value="<?php echo $obj_prodi->getTahunSemesterMulai(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-xs-4 control-label">Email</label>
                    <div class="col-xs-8">
                        <input id="email" name="email" type="text" class="form-control" value="<?php echo $obj_prodi->getEmail(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="tgl_awal_pendirian" class="col-xs-4 control-label">Tanggal Awal Pendirian</label>
                    <div class="col-xs-8">
                        <div class="input-group date">
                            <input id="tgl_awal_pendirian" type="text" class="form-control" name="tgl_awal_pendirian" value="<?php echo $obj_prodi->getTglAwalPendirian(); ?>"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="sk_ban_pt" class="col-xs-4 control-label">SK BAN PT</label>
                    <div class="col-xs-8">
                        <input id="sk_ban_pt" name="sk_ban_pt" type="text" class="form-control" value="<?php echo $obj_prodi->getSkBanPt(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="tgl_sk_ban_pt" class="col-xs-4 control-label">Tanggal SK BAN PT Mulai</label>
                    <div class="col-xs-8">
                        <div class="input-group date">
                            <input id="tgl_sk_ban_pt" type="text" class="form-control" name="tgl_sk_ban_pt" value="<?php echo $obj_prodi->getTglSkBanPt(); ?>"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tgl_akhir_sk_akreditasi" class="col-xs-4 control-label">Tanggal SK Akreditasi Berakhir</label>
                    <div class="col-xs-8">
                        <div class="input-group date">
                            <input id="tgl_akhir_sk_akreditasi" type="text" class="form-control" name="tgl_akhir_sk_akreditasi" value="<?php echo $obj_prodi->getTglAkhirSkAkreditasi(); ?>"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="kode_akreditasi" class="col-xs-4 control-label">Kode Akreditasi</label>
                    <div class="col-xs-8">
                        <select id="kode_akreditasi" name="kode_akreditasi" class="form-control">
                            <option value>--pilih--</option>
                            <?php foreach( ProgramStudi::$akreditasi as $akreditasi ) : ?>
                                <option value="<?php echo $akreditasi; ?>" <?php if( $akreditasi == $obj_prodi->getKodeAkreditasi() ) : ?>selected<?php endif; ?> ><?php echo $akreditasi; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-8 col-xs-offset-4 col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                    <input type="hidden" name="aksi" value="<?php echo Helpers::aksi_perbarui; ?>">
                    <button class="btn btn-success" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                </div>
            </div>
            </form>

            <script>
                $(document).ready(function() {
                    $('#tgl_sk_dikti').datepicker();
                    $('#tgl_akhir_sk_dikti').datepicker();
                    $('#tgl_awal_pendirian').datepicker();
                    $('#tgl_sk_ban_pt').datepicker();
                    $('#tgl_akhir_sk_akreditasi').datepicker();
                    $('.form-prodi').bootstrapValidator({
                        message: 'Data salah',
                        feedbackIcons: {
                            valid: 'glyphicon glyphicon-ok',
                            invalid: 'glyphicon glyphicon-remove',
                            validating: 'glyphicon glyphicon-refresh'
                        },
                        fields: {
                            kode_nasional: {
                                validators: {
                                    notEmpty: {
                                        message: 'Kode tidak boleh kosong'
                                    },
                                    stringLength: {
                                        min: 5,
                                        message: 'Kode minimal 5 karakter'
                                    },
                                    regexp: {
                                        regexp: /^[0-9]+$/,
                                        message: 'Hanya angka'
                                    }
                                }
                            },
                            jenis: {
                                validators: {
                                    notEmpty: {
                                        message: 'Pilih salah satu jenis'
                                    }
                                }
                            },
                            kode_fakultas: {
                                validators: {
                                    notEmpty: {
                                        message: 'Pilih salah satu fakultas'
                                    }
                                }
                            },
                            kode_prodi_unram: {
                                validators: {
                                    notEmpty: {
                                        message: 'Kode tidak boleh kosong'
                                    },
                                    stringLength: {
                                        min: 2,
                                        max: 2,
                                        message: 'Kode hanya 2 karakter'
                                    },
                                    regexp: {
                                        regexp: /^[A-Z]+$/,
                                        message: 'Hanya huruf besar'
                                    }
                                }
                            },
                            nama_nasional: {
                                validators: {
                                    notEmpty: {
                                        message: 'Nama tidak boleh kosong'
                                    },
                                    regexp: {
                                        regexp: /^[a-zA-Z\s]+$/,
                                        message: 'Hanya huruf (besar/kecil) dan spasi saja'
                                    }
                                }
                            },
                            jenjang: {
                                validators: {
                                    notEmpty: {
                                        message: 'Pilih salah satu jenjang'
                                    }
                                }
                            },
                            nama_PS: {
                                validators: {
                                    notEmpty: {
                                        message: 'Nama tidak boleh kosong'
                                    },
                                    regexp: {
                                        regexp: /^[a-zA-Z\s]+$/,
                                        message: 'Hanya huruf (besar/kecil) dan spasi saja'
                                    }
                                }
                            },
                            kurikulum_pakai: {
                                validators: {
                                    notEmpty: {
                                        message: 'Pilih salah satu kurikulum'
                                    }
                                }
                            },
                            jumlah_sks_kelulusan: {
                                validators: {
                                    notEmpty: {
                                        message: 'Jumlah SKS tidak boleh kosong'
                                    },
                                    integer: {
                                        message: 'Format data salah, data harus angka'
                                    }
                                }
                            },
                            tahun_semester_mulai: {
                                validators: {
                                    notEmpty: {
                                        message: 'Tahun semester mulai tidak boleh kosong'
                                    },
                                    stringLength: {
                                        min: 4,
                                        max: 4,
                                        message: 'Format tahun salah, format YYYY (misal 2014)'
                                    },
                                    integer: {
                                        message: 'Isian harus angka'
                                    }
                                }
                            },
                            email: {
                                validators: {
                                    emailAddress: {
                                        message: 'Format email salah, misal psti@ft.unram.ac.id'
                                    }
                                }
                            },
                            kode_akreditasi: {
                                validators: {
                                    notEmpty: {
                                        message: 'Pilih salah satu akreditasi'
                                    }
                                }
                            }
                        }
                    });
                });
            </script>

        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();