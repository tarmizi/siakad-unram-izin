<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Controller;


class Helpers {

    const aksi_perbaiki = 'perbaiki';
    const aksi_perbarui = 'perbarui';
    const aksi_tambah = 'tambah';
    const aksi_hapus = 'hapus';
    const aksi_simpan = 'simpan';
    const aksi_cari = 'cari';
    const aksi_lupa_kata_sandi = 'lupa-kata-sandi';
    const aksi_daftar_baru = 'daftar-baru';
    const aksi_masuk = 'masuk';
    const aksi_keluar = 'keluar';
    const aksi_mahasiswa_bimbingan = 'mahasiswa-bimbingan';

    const status_gagal = 9999;
    const status_tambah = 1;
    const status_perbarui = 2;
    const status_hapus = 3;
    const status_masuk = 4;
    const status_masuk_gagal = 5;
    const status_keluar = 6;

    const cari_param = 'q';
    const aksi_param = 'aksi';
    const status_param = 'status';

    /** @var Helpers $instance */
    private static $instance;

    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private $message_object = 'Objek';

    function set_message_object( $object ) {
        $this->message_object = $object;
        return $this;
    }

    function display_message( $status, $message = '', $icon = '', $class = '' ) {
        switch( $status ) {
            case self::status_tambah  :
                $tmp_cls = 'alert-success'; $tmp_ico = 'ok';
                $tmp_msg = $this->message_object . ' berhasil di' . self::aksi_tambah;
                break;
            case self::status_perbarui  :
                $tmp_cls = 'alert-info'; $tmp_ico = 'ok';
                $tmp_msg = $this->message_object . ' berhasil di' . self::aksi_perbaiki;
                break;
            case self::status_hapus  :
                $tmp_cls = 'alert-warning'; $tmp_ico = 'ok';
                $tmp_msg = $this->message_object . ' berhasil di' . self::aksi_hapus;
                break;
            case self::status_masuk  :
                $tmp_cls = 'alert-success'; $tmp_ico = 'ok';
                $tmp_msg = 'Anda berhasil ' . self::aksi_masuk;
                break;
            case self::status_keluar  :
                $tmp_cls = 'alert-warning'; $tmp_ico = 'ok';
                $tmp_msg = 'Anda berhasil ' . self::aksi_keluar;
                break;
            default :
                $tmp_cls = 'alert-danger'; $tmp_ico = 'remove';
                $tmp_msg = 'Terjadi kesalahan, harap menghubungi developer';
        }
        empty( $message ) || $tmp_msg = $message;
        empty( $icon ) || $tmp_ico = $icon;
        empty( $class ) || $tmp_cls = $class;
        echo '<div class="alert ' . $tmp_cls . '"><p><i class="glyphicon glyphicon-' . $tmp_ico . '"></i> ' . $tmp_msg . '</p></div>';
    }


} 