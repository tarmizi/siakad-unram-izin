<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */


/**
 * mengakhiri eksekusi script/halaman dengan tambahan
 * opsi tampilan
 *
 * @param string $messages
 * @param string $type
 */
function siakad_exit( $messages = '', $type = 'info' ) {

    $out = '';

    if( !empty( $messages ) ) {

        ob_start(); ?>

        <html>
        <head>
            <title>SIAKAD Universitas Mataram</title>
            <style>
                .container {
                    margin: 20px auto;
                    text-align: center;
                }
                .message {
                    color: #fff;
                    display: inline;
                    padding: 10px;
                    font-size: 11px;
                    font-family: sans-serif;
                    -moz-border-radius: 3px;
                    border-radius: 3px;
                    background-color: #008cba;
                }
                .message.info {
                    background-color: #43ac6a;
                }
                .message.alert {
                    background-color: #f04124;
                }
            </style>
        </head>
        <body>
        <div class="container">
            <p class="message <?php echo $type; ?>"><?php echo $messages; ?></p>
        </div>
        </body>
        </html>

        <?php $out = ob_get_clean();

    }

    exit( $out );
}

/**
 * berfungsi untuk me-redirect ke halaman tertentu,
 * dengan mengeksekusi exit() di akhir untuk memastikan
 * tidak ada program yang berjalan selanjutnya
 *
 * @param string $target
 */
function siakad_redirect( $target = SIAKAD_URI_PATH ) {
    header( 'Location: ' . $target );
    siakad_exit();
}

/**
 * @param $default
 * @param $destination
 * @return array
 */
function sync_default_params( $default, $destination ) {
    $return = array();
    foreach( $default as $k => $d ) {
        if( isset( $destination[ $k ] ) ) $return[ $k ] = $destination[ $k ];
        else $return[ $k ] = $d;
    }
    return $return;
}

/**
 * @param $base_url
 * @param $total_data
 * @param $current_page
 * @param int $data_per_page
 * @param int $range_data
 */
function siakad_paging_nav( $base_url, $total_data, $current_page, $data_per_page = 10, $range_data = 3 ) {

    /** cek apakah memungkinkan untuk paging */
    if( $total_data > $data_per_page ) :
        $total_page = ceil( $total_data / $data_per_page );

        /** batas minimum */
        $ii = ( $ii = $current_page - $range_data ) < 1 ? 1 : $ii;

        /** batas maksimum */
        $iii = ( $iii = $current_page + $range_data ) > $total_page ? $total_page : $iii; ?>

        <div class="pull-right">
            <ul class="pagination">

                <?php /** tampilkan left arrow */
                if( $current_page == 1 ) : ?>
                    <li class="disabled">
                        <span>&laquo;</span>
                    </li>
                <?php else : ?>
                    <li>
                        <a href="<?php echo $base_url; ?>&page=1">
                            &laquo;
                        </a>
                    </li>
                <?php endif; ?>

                <?php /** jika tidak mepet dengan nilai minimum, tampilkan titik-titik */
                if( $ii != 1 ) : ?>
                    <li class="disabled">
                        <span>...</span>
                    </li>
                <?php endif; ?>

                <?php /** mulai iterasi sesuai range yang telah ditentukan */
                for( $i = $ii; $i <= $iii; $i++ ) : ?>
                    <li class="<?php echo $current_page == $i ? 'active' : ''; ?>">
                        <a href="<?php echo $base_url; ?>&page=<?php echo $i; ?>">
                            <?php echo $i; ?>
                        </a>
                    </li>
                <?php endfor; ?>

                <?php /** jika tidak mepet dengan nilai maksimum, tampilkan titik-titik */
                if( $iii != $total_page ) : ?>
                    <li class="disabled">
                        <span>...</span>
                    </li>
                <?php endif; ?>

                <?php /** tampilkan right arrow */
                if( $current_page == $total_page ) : ?>
                    <li class="disabled">
                        <span>&raquo;</span>
                    </li>
                <?php else : ?>
                    <li>
                        <a href="<?php echo $base_url; ?>&page=<?php echo $total_page; ?>">
                            &raquo;
                        </a>
                    </li>
                <?php endif; ?>

            </ul>
        </div>

    <?php endif;

    if( $total_data ) : ?>
        <p class="text-muted"><i>Total <?php echo $total_data; ?> data</i></p>
    <?php else : ?>
        <p class="text-danger"><i>Data kosong</i></p>
    <?php endif;

}

function is_module( $module_path ) {
    $found = false;
    if ( $handle = opendir( SIAKAD_MODULE_ABS_PATH ) ) {
        while ( false !== ( $file = readdir( $handle ) ) )
            !( $file != '.'
                && $file != '..'
                && $file == $module_path
                && is_dir( SIAKAD_MODULE_ABS_PATH . DS . $file ) )
            || $found = true;
        closedir($handle);
    }
    return $found;
}

function jumlah_semester($thn_awal, $thn_akhir){
    $semester = 1;
    if($thn_awal==$thn_akhir)
        return $semester;
    do{
        if($thn_awal%2==0){
            $thn_awal+=9;
        }
        else {
            $thn_awal++;
        }
        $semester++;
    }
    while($thn_awal!=$thn_akhir);
    return $semester;
}

function hitung_sks($daftar_nilai){
    $sks = 0;
    if(!empty($daftar_nilai)){foreach ($daftar_nilai as $n){
        $mk = SIAKAD\Controller\Matakuliah::get_instance()->_get($n->getIdMk());
        $sks += $mk->getSks();
    }}
    return $sks;
}
function hitung_IP($daftar_nilai){
    $sks = 0;
    $total_nilai = 0;
    if(!empty($daftar_nilai)){foreach ($daftar_nilai as $n){
        $mk = SIAKAD\Controller\Matakuliah::get_instance()->_get($n->getIdMk());
        $sks += $mk->getSks();
        $total_nilai += $n->getIndeksNilai()*$mk->getSks();
    }}
    if($sks==0)
        return 0;
    return $total_nilai/$sks;
}

function hitung_sks_diambil($sksA,$sksB,$ip){
    if($sksA>=19&&$sksA<=24){
        if($sksB>=19&&$sksB<=24){
            if($ip>=0&&$ip<1.5){
                return 16;
            }
            elseif($ip>=1.5&&$ip<2){
                return 18;
            }
            elseif($ip>=2&&$ip<2.5){
                return 20;
            }
            elseif($ip>=2.5&&$ip<3){
                return 22;
            }
            elseif($ip>=3&&$ip<=4){
                return 24;
            }
        }
        elseif($sksB>=13&&$sksB<=18){
            if($ip>=0&&$ip<1.5){
                return 15;
            }
            elseif($ip>=1.5&&$ip<2){
                return 17;
            }
            elseif($ip>=2&&$ip<2.5){
                return 19;
            }
            elseif($ip>=2.5&&$ip<3){
                return 21;
            }
            elseif($ip>=3&&$ip<=4){
                return 23;
            }
        }
        elseif($sksB>=7&&$sksB<=12){
            if($ip>=0&&$ip<1.5){
                return 13;
            }
            elseif($ip>=1.5&&$ip<2){
                return 15;
            }
            elseif($ip>=2&&$ip<2.5){
                return 17;
            }
            elseif($ip>=2.5&&$ip<3){
                return 19;
            }
            elseif($ip>=3&&$ip<=4){
                return 21;
            }
        }
        elseif($sksB>=0&&$sksB<=6){
            if($ip>=0&&$ip<1.5){
                return 10;
            }
            elseif($ip>=1.5&&$ip<2){
                return 12;
            }
            elseif($ip>=2&&$ip<2.5){
                return 14;
            }
            elseif($ip>=2.5&&$ip<3){
                return 16;
            }
            elseif($ip>=3&&$ip<=4){
                return 18;
            }
        }
    }
    elseif($sksA>=13&&$sksA<=18){
        if($sksB>=13&&$sksB<=18){
            if($ip>=0&&$ip<1.5){
                return 14;
            }
            elseif($ip>=1.5&&$ip<2){
                return 16;
            }
            elseif($ip>=2&&$ip<2.5){
                return 18;
            }
            elseif($ip>=2.5&&$ip<3){
                return 20;
            }
            elseif($ip>=3&&$ip<=4){
                return 22;
            }
        }
        elseif($sksB>=7&&$sksB<=12){
            if($ip>=0&&$ip<1.5){
                return 12;
            }
            elseif($ip>=1.5&&$ip<2){
                return 14;
            }
            elseif($ip>=2&&$ip<2.5){
                return 16;
            }
            elseif($ip>=2.5&&$ip<3){
                return 18;
            }
            elseif($ip>=3&&$ip<=4){
                return 20;
            }
        }
        elseif($sksB>=0&&$sksB<=6){
            if($ip>=0&&$ip<1.5){
                return 9;
            }
            elseif($ip>=1.5&&$ip<2){
                return 11;
            }
            elseif($ip>=2&&$ip<2.5){
                return 13;
            }
            elseif($ip>=2.5&&$ip<3){
                return 15;
            }
            elseif($ip>=3&&$ip<=4){
                return 17;
            }
        }
    }
    elseif($sksA>=7&&$sksA<=12){
        if($sksB>=7&&$sksB<=12){
            if($ip>=0&&$ip<1.5){
                return 11;
            }
            elseif($ip>=1.5&&$ip<2){
                return 13;
            }
            elseif($ip>=2&&$ip<2.5){
                return 15;
            }
            elseif($ip>=2.5&&$ip<3){
                return 17;
            }
            elseif($ip>=3&&$ip<=4){
                return 19;
            }
        }
        elseif($sksB>=0&&$sksB<=6){
            if($ip>=0&&$ip<1.5){
                return 8;
            }
            elseif($ip>=1.5&&$ip<2){
                return 10;
            }
            elseif($ip>=2&&$ip<2.5){
                return 12;
            }
            elseif($ip>=2.5&&$ip<3){
                return 14;
            }
            elseif($ip>=3&&$ip<=4){
                return 16;
            }
        }
    }
    elseif($sksA>=0&&$sksA<=6){
        if($sksB>=0&&$sksB<=6){
            if($ip>=0&&$ip<1.5){
                return 7;
            }
            elseif($ip>=1.5&&$ip<2){
                return 9;
            }
            elseif($ip>=2&&$ip<2.5){
                return 11;
            }
            elseif($ip>=2.5&&$ip<3){
                return 13;
            }
            elseif($ip>=3&&$ip<=4){
                return 15;
            }
        }
    }
    else return 0;
}
