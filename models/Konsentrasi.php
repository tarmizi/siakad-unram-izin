<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Model;


class Konsentrasi {

    private $id;
    private $kode_PS;
    private $nomor;
    private $nama;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $kode_PS
     */
    public function setKodePS($kode_PS)
    {
        $this->kode_PS = $kode_PS;
    }

    /**
     * @return mixed
     */
    public function getKodePS()
    {
        return $this->kode_PS;
    }

    /**
     * @param mixed $nama
     */
    public function setNama($nama)
    {
        $this->nama = $nama;
    }

    /**
     * @return mixed
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * @param mixed $nomor
     */
    public function setNomor($nomor)
    {
        $this->nomor = $nomor;
    }

    /**
     * @return mixed
     */
    public function getNomor()
    {
        return $this->nomor;
    }


} 