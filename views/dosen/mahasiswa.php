<?php

namespace SIAKAD\Views\Dosen;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Pesan;
use SIAKAD\Controller\Mahasiswa;
use SIAKAD\Controller\DosenPa;
use SIAKAD\Controller\Dosen;
use SIAKAD\Controller\Konsentrasi;
use SIAKAD\Controller\Nilai;
use SIAKAD\Controller\IndeksNilai;
use SIAKAD\Controller\Matakuliah;
use SIAKAD\Controller\KrsTemp;
use SIAKAD\Controller\Sessions;

Headers::get_instance()
    ->set_page_title( 'Mahasiswa Bimbingan' )
    ->set_page_name( 'Mahasiswa' );

$tingkat1 = Routes::get_instance()->get_tingkat( 1 );
$tingkat2 = Routes::get_instance()->get_tingkat( 2 );
$tingkat3 = Routes::get_instance()->get_tingkat( 3 );

$is_detail = Routes::get_instance()->has_tingkat( 3 ) && Routes::get_instance()->is_tingkat( 4, 'detil' );
$is_krs = Routes::get_instance()->has_tingkat( 3 ) && Routes::get_instance()->is_tingkat( 4, 'krs' );
$is_pesan = Routes::get_instance()->has_tingkat( 3 ) && Routes::get_instance()->is_tingkat( 4, 'pesan' );

Contents::get_instance()->get_header();
$me = Dosen::get_instance()->_get(Sessions::get_instance()->_retrieve()->getObjLogin()->getUsername());
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <?php if( $is_krs ) : 
                $mhs = Mahasiswa::get_instance()->_get($tingkat3);
                $nilai_all = Nilai::get_instance()->_gets(array('nim'=>$mhs->getNIM(),'nilai_min'=>90));
                $jml_sks = 0;
                $semester = jumlah_semester($mhs->getThnMasuk(), 20141);
                if(!empty($nilai_all)){foreach ($nilai_all as $n){
                    $mk = Matakuliah::get_instance()->_get($n->getIdMk());
                    $jml_sks += $mk->getSks();
                }}
                ?>
                <h1 class="page-header">
                    <?php echo $mhs->getNIM(); ?>
                    <small>Pengajuan KRS</small>
                </h1>
                <table style="width: 100%">
                    <thead>
                    <tr>
                        <th>Tahun Ajaran</th>
                        <td>2014</td>
                    </tr><tr>
                        <th>Semester</th>
                        <td><?php echo $semester; ?> / Ganjil</td>
                    </tr><tr>
                        <th>SKS yang telah diambil</th>
                        <td><?php echo $jml_sks; ?></td>
                    </tr><tr>
                        <th>SKS yang boleh diambil</th>
                        <td>24</td>
                    </tr>
                    </thead>
                </table>
                <br/>
                <?php 
                $krstemp = KrsTemp::get_instance()->_get($tingkat3);
                if($krstemp->getNim()==null){ ?>
                <div class="alert alert-info">
                    <p><i class="glyphicon glyphicon-warning-sign"></i> Mahasiswa Ini Belum Mengajukan KRS</p>
                </div>
                <?php die(); }?>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>SKS</th>
                        <th>Semester</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php 
                    $daftar_matakuliah = explode('-', $krstemp->getIdMk());
                    if(isset($_POST['terima'])){
                        $krstemp->setStatus(1);
                        KrsTemp::get_instance()->update($krstemp);
                        foreach ( range(0, count($daftar_matakuliah)-2) as $i ){
                            $nilai = new \SIAKAD\Model\Nilai();
                            $nilai->setId($daftar_matakuliah[$i].'20141'.$tingkat3);
                            $nilai->setIdMk($daftar_matakuliah[$i]);
                            $nilai->setTahunAkademik(20141);
                            $nilai->setNim($tingkat3);
                            $nilai->setSemester($semester);
                            Nilai::get_instance()->insert($nilai) or die("Query salah : " . mysql_error());
                        }
                    }
                    if(isset($_POST['tolak'])){
                        $krstemp->setStatus(2);
                        KrsTemp::get_instance()->update($krstemp);
                    }
                    foreach ( range(0, count($daftar_matakuliah)-2) as $i ) :
                        $mk = Matakuliah::get_instance()->_get($daftar_matakuliah[$i]);
                        ?>
                    <tr>
                        <td><?php echo $mk->getKodeMk(); ?></td>
                        <td><?php echo $mk->getNamaMk(); ?></td>
                        <td><?php echo $mk->getSks(); ?></td>
                        <td><?php echo $mk->getSemester(); ?></td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="clearfix"></div>
                <br/>
                <?php if($krstemp->getStatus()==1): ?>
                       <div class="alert alert-success">
                            <p><i class="glyphicon glyphicon-ok-circle"></i> KRS Ini Telah Anda Terima</p>
                          </div>
                <?php elseif($krstemp->getStatus()==2): ?>
                       <div class="alert alert-danger">
                            <p><i class="glyphicon glyphicon-warning-sign"></i> KRS Ini Telah Anda Tolak</p>
                          </div>
                <?php else : ?>
                <div class="col-sm-2"><form method="post" action="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3; ?>/krs">
                    <button type="submit" name="terima" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-ok"></i> Terima</button>
                    </form></div>
                <div class="col-sm-2"><form method="post" action="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3; ?>/krs">
                    <button type="submit" name="tolak" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i> Tolak</button>
                </form></div>
                <?php endif; ?>
            <?php elseif( $is_pesan ) :
                if(isset($_POST['send'])){
                    $p = new \SIAKAD\Model\Pesan();
                    $p->setIdPenerima($tingkat3);
                    $p->setIdPengirim($me->getKode());
                    $p->setIsiPesan($_POST['pesan']);
                    Pesan::get_instance()->insert($p) or die("Query salah : " . mysql_error());
                }
                $tingkat3 = Routes::get_instance()->get_tingkat( 3 );
                $tingkat4 = Routes::get_instance()->get_tingkat( 4 );
                $conditions=array(array('logika'=>'AND','tabel'=>'( pesan','field'=>'id_pengirim','operator'=>'=','comparison'=>'\''.$me->getKode().'\''),
                  array('logika'=>'AND','tabel'=>'pesan','field'=>'id_penerima','operator'=>'=','comparison'=>'\''.$tingkat3.'\')'),
                  array('logika'=>'OR','tabel'=>'( pesan','field'=>'id_pengirim','operator'=>'=','comparison'=>'\''.$tingkat3.'\''),
                  array('logika'=>'AND','tabel'=>'pesan','field'=>'id_penerima','operator'=>'=','comparison'=>'\''.$me->getKode().'\')'));  
            ?>
                <div class="col-xs-9 col-sm-10 main">
                    <h1 class="page-header">Pesan<small> Mahasiswa</small>
                    </h1>
                    <div class="row">
                        <div class="col-sm-12">
                            <form class="form-horizontal" method="post" action="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3 . DS . $tingkat4; ?>">
                                <textarea name="pesan" class="form-control" placeholder="Ketik Pesan Anda"></textarea><button name="send" class="form-control" type="submit">Kirim <i class="glyphicon glyphicon-send"></i></button>
                            </form>
                            <table class="table">
                                <tbody>
                                <?php /** @var $mahasiswa \SIAKAD\Model\Pesan */
                                $list_pesan = Pesan::get_instance()->_gets(array('conditions'=>$conditions));
                                foreach( $list_pesan as $pesan ) : ?>
                                <tr>
                                    <td><?php echo $pesan->getTanggalKirim(); ?></td><td><?php echo $pesan->getIsiPesan(); ?></td><td><?php echo '<i class="glyphicon glyphicon-'; if($pesan->getIdPengirim()==$me->getKode()) echo 'forward'; else echo 'inbox'; echo '"></i>'; ?></td>
                                </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <ul class="pagination">
                                <li><a href="#">&laquo;</a></li>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php elseif( $is_detail ) : 
                $mhs = Mahasiswa::get_instance()->_get($tingkat3);
                $daftar_nilai = Nilai::get_instance()->_gets(array('nim'=>$mhs->getNIM()));
                ?>
                <h1 class="page-header">
                    <?php echo $mhs->getNIM(); ?>
                    <small><?php echo $mhs->getNama(); ?></small>
                </h1>
                <form class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <select class="form-control">
                                <option>--semester</option>
                                <option>1</option>
                                <option>2</option>
                                <option selected>3</option>
                                <option>4</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <button class="btn btn-primary"><i class="fa fa-legal"></i> OK</button>
                        </div>
                    </div>
                </form>
                <br/>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Tahun Akademik</th>
                        <th>Mata Kuliah</th>
                        <th>Sks</th>
                        <th>Nilai</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach( $daftar_nilai as $nilai ) : ?>
                        <tr>
                            <td><?php echo ($nilai->getTahunAkademik()/10); ?></td>
                            <td><?php echo Matakuliah::get_instance()->_get($nilai->getIdMk())->getNamaMk(); ?></td>
                            <td><?php echo Matakuliah::get_instance()->_get($nilai->getIdMk())->getSks(); ?></td>
                            <td><?php echo IndeksNilai::get_instance()->_get($nilai->getIndeksNilai())->getHuruf(); ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php else : 
                $daftar_mahasiswa_pa = DosenPa::get_instance()->_gets(array('kode_dosen'=>$me->getKode()));
                ?>
                <h1 class="page-header">
                    Mahasiswa
                    <small>Bimbingan Akademik</small>
                </h1>
                <form class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <select class="form-control">
                                <option>--bidang keahlian</option>
                                <option>Sistem Cerdas</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <button class="btn btn-primary"><i class="fa fa-legal"></i> OK</button>
                        </div>
                    </div>
                </form>
                <br/>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>NIM</th>
                        <th>Nama</th>
                        <th>Bidang Keahlian</th>
                        <th>Semester</th>
                        <th>Keterangan</th>
                        <th>#</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach( $daftar_mahasiswa_pa as $m_pa ) : 
                        $mhs = Mahasiswa::get_instance()->_get($m_pa->getNimMahasiswa());
                        ?>
                        <tr>
                            <td><?php echo $mhs->getNIM(); ?></td>
                            <td><?php echo $mhs->getNama(); ?></td>
                            <td><?php echo Konsentrasi::get_instance()->_get($mhs->getIdKonsentrasi())->getNama(); ?></td>
                            <td><?php echo jumlah_semester($mhs->getThnMasuk(), 20142); ?></td>
                            <td></td>
                            <td>
                                <a class="btn btn-sm btn-primary" title="Detil Mahasiswa" href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $mhs->getNIM(); ?>/detil"><i class="fa fa-history"></i></a>
                                <a class="btn btn-sm btn-primary" title="Pengajuan KRS" href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $mhs->getNIM(); ?>/krs"><i class="fa fa-sitemap"></i></a>
                                <a class="btn btn-sm btn-primary" title="Pesan Mahasiswa" href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $mhs->getNIM(); ?>/pesan"><i class="fa fa-envelope"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();