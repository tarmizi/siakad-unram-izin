<?php
/**  
 * Canvas Production
 *
 * Author : Dani
 */

namespace SIAKAD\Model;


class Matakuliah {

    private $id;
    private $id_kurikulum;
    private $kode_mk;
    private $nama_mk;
    private $nama_mk_ingg;
    private $sks;
    private $kode_sifat;
    private $semester;
    private $jenis_semester;
    private $status;
    private $id_konsentrasi;
    private $id_bagian;
    private $kode_dosen_koordinator;
    private $silabus;
    private $sap;
    private $bahan_ajar;
    private $sks_teori;
    private $sks_praktikum;
    private $jenis_pembagian_kelas;

    private $tahun_akademik_kurikulum;
    private $nama_konsentrasi;
    private $nama_bagian;
    private $nama_sifat_mata_kuliah;
    private $nama_dosen;
    private $nama_nasional_prodi;

    public function _init( $request ) {
        !isset( $request[ 'id' ] ) || $this->id = $request[ 'id' ];
        !isset( $request[ 'id_kurikulum' ] ) || $this->id_kurikulum = $request[ 'id_kurikulum' ];
        !isset( $request[ 'kode_mk' ] ) || $this->kode_mk = $request[ 'kode_mk' ];
        !isset( $request[ 'nama_mk' ] ) || $this->nama_mk = $request[ 'nama_mk' ];
        !isset( $request[ 'nama_mk_ingg' ] ) || $this->nama_mk_ingg = $request[ 'nama_mk_ingg' ];
        !isset( $request[ 'sks' ] ) || $this->sks = $request[ 'sks' ];
        !isset( $request[ 'kode_sifat' ] ) || $this->kode_sifat = $request[ 'kode_sifat' ];
        !isset( $request[ 'semester' ] ) || $this->semester = $request[ 'semester' ];
        !isset( $request[ 'jenis_semester' ] ) || $this->jenis_semester = $request[ 'jenis_semester' ];
        !isset( $request[ 'status' ] ) || $this->status = $request[ 'status' ];
        !isset( $request[ 'id_konsentrasi' ] ) || $this->id_konsentrasi = $request[ 'id_konsentrasi' ];
        !isset( $request[ 'id_bagian' ] ) || $this->id_bagian = $request[ 'id_bagian' ];
        !isset( $request[ 'kode_dosen_koordinator' ] ) || $this->kode_dosen_koordinator = $request[ 'kode_dosen_koordinator' ];
        !isset( $request[ 'silabus' ] ) || $this->silabus = $request[ 'silabus' ];
        !isset( $request[ 'sap' ] ) || $this->sap = $request[ 'sap' ];
        !isset( $request[ 'bahan_ajar' ] ) || $this->bahan_ajar = $request[ 'bahan_ajar' ];
        !isset( $request[ 'sks_teori' ] ) || $this->sks_teori = $request[ 'sks_teori' ];
        !isset( $request[ 'sks_praktikum' ] ) || $this->sks_praktikum = $request[ 'sks_praktikum' ];
        !isset( $request[ 'jenis_pembagian_kelas' ] ) || $this->jenis_pembagian_kelas = $request[ 'jenis_pembagian_kelas' ];
    }

    /**
     * @param mixed $jenis_pembagian_kelas
     */
    public function setJenisPembagianKelas($jenis_pembagian_kelas)
    {
        $this->jenis_pembagian_kelas = $jenis_pembagian_kelas;
    }

    /**
     * @return mixed
     */
    public function getJenisPembagianKelas()
    {
        return $this->jenis_pembagian_kelas;
    }

    /**
     * @param mixed $nama_sifat_mata_kuliah
     */
    public function setNamaSifatMataKuliah($nama_sifat_mata_kuliah)
    {
        $this->nama_sifat_mata_kuliah = $nama_sifat_mata_kuliah;
    }

    /**
     * @return mixed
     */
    public function getNamaSifatMataKuliah()
    {
        return $this->nama_sifat_mata_kuliah;
    }

    /**
     * @param mixed $nama_bagian
     */
    public function setNamaBagian($nama_bagian)
    {
        $this->nama_bagian = $nama_bagian;
    }

    /**
     * @return mixed
     */
    public function getNamaBagian()
    {
        return $this->nama_bagian;
    }

    /**
     * @param mixed $nama_dosen
     */
    public function setNamaDosen($nama_dosen)
    {
        $this->nama_dosen = $nama_dosen;
    }

    /**
     * @return mixed
     */
    public function getNamaDosen()
    {
        return $this->nama_dosen;
    }

    /**
     * @param mixed $nama_konsentrasi
     */
    public function setNamaKonsentrasi($nama_konsentrasi)
    {
        $this->nama_konsentrasi = $nama_konsentrasi;
    }

    /**
     * @return mixed
     */
    public function getNamaKonsentrasi()
    {
        return $this->nama_konsentrasi;
    }

    /**
     * @param mixed $nama_mk_ingg
     */
    public function setNamaMkIngg($nama_mk_ingg)
    {
        $this->nama_mk_ingg = $nama_mk_ingg;
    }

    /**
     * @return mixed
     */
    public function getNamaMkIngg()
    {
        return $this->nama_mk_ingg;
    }

    /**
     * @param mixed $nama_nasional_prodi
     */
    public function setNamaNasionalProdi($nama_nasional_prodi)
    {
        $this->nama_nasional_prodi = $nama_nasional_prodi;
    }

    /**
     * @return mixed
     */
    public function getNamaNasionalProdi()
    {
        return $this->nama_nasional_prodi;
    }

    /**
     * @param mixed $tahun_akademik_kurikulum
     */
    public function setTahunAkademikKurikulum($tahun_akademik_kurikulum)
    {
        $this->tahun_akademik_kurikulum = $tahun_akademik_kurikulum;
    }

    /**
     * @return mixed
     */
    public function getTahunAkademikKurikulum()
    {
        return $this->tahun_akademik_kurikulum;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $idKurikulum
     */
    public function setIdKurikulum($idKurikulum)
    {
        $this->id_kurikulum = $idKurikulum;
    }

    /**
     * @return mixed
     */
    public function getIdKurikulum()
    {
        return $this->id_kurikulum;
    }

    /**
     * @param mixed $kodeMK
     */
    public function setKodeMk($kodeMK)
    {
        $this->kode_mk = $kodeMK;
    }

    /**
     * @return mixed
     */
    public function getKodeMk()
    {
        return $this->kode_mk;
    }

    /**
     * @param mixed $namaMK
     */
    public function setNamaMk($namaMK)
    {
        $this->nama_mk = $namaMK;
    }

    /**
     * @return mixed
     */
    public function getNamaMk()
    {
        return $this->nama_mk;
    }

    /**
     * @param mixed $id_konsentrasi
     */
    public function setIdKonsentrasi($id_konsentrasi)
    {
        $this->id_konsentrasi = $id_konsentrasi;
    }

    /**
     * @return mixed
     */
    public function getIdKonsentrasi()
    {
        return $this->id_konsentrasi;
    }

    /**
     * @param mixed $sks
     */
    public function setSks($sks)
    {
        $this->sks = $sks;
    }

    /**
     * @return mixed
     */
    public function getSks()
    {
        return $this->sks;
    }

    /**
     * @param $kode_sifat
     */
    public function setKodeSifat($kode_sifat)
    {
        $this->kode_sifat = $kode_sifat;
    }

    /**
     * @return mixed
     */
    public function getKodeSifat()
    {
        return $this->kode_sifat;
    }

    /**
     * @param mixed $semester
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;
    }

    /**
     * @return mixed
     */
    public function getSemester()
    {
        return $this->semester;
    }

    /**
     * @param mixed $jenis_semester
     */
    public function setJenisSemester($jenis_semester)
    {
        $this->jenis_semester = $jenis_semester;
    }

    /**
     * @return mixed
     */
    public function getJenisSemester()
    {
        return $this->jenis_semester;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $id_bagian
     */
    public function setIdBagian($id_bagian)
    {
        $this->id_bagian = $id_bagian;
    }

    /**
     * @return mixed
     */
    public function getIdBagian()
    {
        return $this->id_bagian;
    }

    /**
     * @param mixed $kode_dosen_koordinator
     */
    public function setKodeDosenKoordinator($kode_dosen_koordinator)
    {
        $this->kode_dosen_koordinator = $kode_dosen_koordinator;
    }

    /**
     * @return mixed
     */
    public function getKodeDosenKoordinator()
    {
        return $this->kode_dosen_koordinator;
    }

    /**
     * @param mixed $silabus
     */
    public function setSilabus($silabus)
    {
        $this->silabus = $silabus;
    }

    /**
     * @return mixed
     */
    public function getSilabus()
    {
        return $this->silabus;
    }

    /**
     * @param mixed $sap
     */
    public function setSap($sap)
    {
        $this->sap = $sap;
    }

    /**
     * @return mixed
     */
    public function getSap()
    {
        return $this->sap;
    }

    /**
     * @param mixed $bahan_ajar
     */
    public function setBahanAjar($bahan_ajar)
    {
        $this->bahan_ajar = $bahan_ajar;
    }

    /**
     * @return mixed
     */
    public function getBahanAjar()
    {
        return $this->bahan_ajar;
    }

    /**
     * @param mixed $sks_teori
     */
    public function setSksTeori($sks_teori)
    {
        $this->sks_teori = $sks_teori;
    }

    /**
     * @return mixed
     */
    public function getSksTeori()
    {
        return $this->sks_teori;
    }

    /**
     * @param mixed $sks_praktikum
     */
    public function setSksPraktikum($sks_praktikum)
    {
        $this->sks_praktikum = $sks_praktikum;
    }

    /**
     * @return mixed
     */
    public function getSksPraktikum()
    {
        return $this->sks_praktikum;
    }

} 