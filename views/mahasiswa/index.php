<?php

namespace SIAKAD\Views\Mahasiswa;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Mahasiswa;
use SIAKAD\Controller\Sessions;
use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Helpers;
use SIAKAD\Controller\Agama;
use SIAKAD\Controller\Provinsi;
use SIAKAD\Controller\PekerjaanWali;

use SIAKAD\Model\Agama as ModelAgama;
use SIAKAD\Model\Provinsi as ModelProvinsi;
use SIAKAD\Model\PekerjaanWali as ModelPekerjaanWali;

$tingkat1 = Routes::get_instance()->get_tingkat( 1 );

/**
 * @todo
 * lanjutkan aksi buat nyimpan data pribadi mahasiswa
 */

Headers::get_instance()
    ->set_page_title( 'Mahasiswa' )
    ->set_page_name( 'Mahasiswa' );

Contents::get_instance()->get_header();

$obj_login = Sessions::get_instance()->_retrieve()->getObjLogin();
$obj_mahasiswa = Mahasiswa::get_instance()->_get( $obj_login->getUsername() );

$daftar_agama = Agama::get_instance()->_gets( array( 'number' => -1, 'order' => 'ASC' ) );
$daftar_provinsi = Provinsi::get_instance()->_gets( array( 'number' => -1, 'order' => 'ASC' ) );
$daftar_pekerjaan_wali = PekerjaanWali::get_instance()->_gets( array( 'number' => -1, 'order' => 'ASC' ) );


?>

<div class="container-fluid">

    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">
                <?php echo $obj_mahasiswa->getNama(); ?>
                <small>Data pribadi</small>
            </h1>
            <div class="alert alert-warning">
                <p><strong><i class="glyphicon glyphicon-warning-sign"></i> Catatan : </strong>Beberapa isian tidak bisa diganti sendiri oleh mahasiswa, mohon menghubungi bagian prodi jika ada terdapat data yang salah atau tidak lengkap.</p>
            </div>
            <form action="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . Helpers::aksi_simpan; ?>" class="form-mahasiswa form-horizontal" method="post">
            <!-- sebelah kiri -->
            <div class="col-md-6">
                <div class="form-group">
                    <label for="NIM" class="col-xs-4 control-label">NIM</label>
                    <div class="col-xs-8">
                        <input id="NIM" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getNIM(); ?>" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label for="kode_prodi" class="col-xs-4 control-label">PRODI</label>
                    <div class="col-xs-8">
                        <input id="kode_prodi" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getNamaNasionalProdi(); ?>" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label for="nama" class="col-xs-4 control-label">Nama</label>
                    <div class="col-xs-8">
                        <input id="nama" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getNama(); ?>" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label for="jenjang" class="col-xs-4 control-label">Jenjang</label>
                    <div class="col-xs-8">
                        <input id="jenjang" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getNamaJenjang(); ?>" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label for="shift" class="col-xs-4 control-label">Shift</label>
                    <div class="col-xs-8">
                        <input id="shift" type="text" class="form-control" value="<?php echo Mahasiswa::$shift[ $obj_mahasiswa->getShift() ]; ?>" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tempat_lahir" class="col-xs-4 control-label">Tempat Lahir</label>
                    <div class="col-xs-8">
                        <textarea id="tempat_lahir" name="tempat_lahir" class="form-control"><?php echo $obj_mahasiswa->getTempatLahir(); ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tgl_lahir" class="col-xs-4 control-label">Tanggal Lahir</label>
                    <div class="col-xs-8">
                        <div class="input-group">
                            <input id="tgl_lahir" type="text" class="form-control" name="tgl_lahir" value="<?php echo $obj_mahasiswa->getTglLahir(); ?>"/>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="jns_kelamin" class="col-xs-4 control-label">Jenis Kelamin</label>
                    <div class="col-xs-8">
                        <input type="hidden" name="jns_kelamin" value="<?php echo $obj_mahasiswa->getJnsKelamin(); ?>">
                        <p class="form-control-static"><?php echo Mahasiswa::$jenis_kelamin[ $obj_mahasiswa->getJnsKelamin() ]; ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="kode_agama" class="col-xs-4 control-label">Agama</label>
                    <div class="col-xs-8">
                        <select id="kode_agama" name="kode_agama" class="form-control">
                            <option value>--pilih--</option>
                            <?php /** @var $agama ModelAgama */
                            foreach( $daftar_agama as $agama ) : ?>
                                <option value="<?php echo $agama->getKode(); ?>" <?php if( $agama->getKode() == $obj_mahasiswa->getKodeAgama() ) : ?>selected<?php endif; ?> ><?php echo $agama->getNama(); ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="alamat" class="col-xs-4 control-label">Alamat</label>
                    <div class="col-xs-8">
                        <textarea id="alamat" name="alamat" class="form-control"><?php echo $obj_mahasiswa->getAlamat(); ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="kode_provinsi" class="col-xs-4 control-label">Provinsi</label>
                    <div class="col-xs-8">
                        <select id="kode_provinsi" name="kode_provinsi" class="form-control">
                            <option value>--pilih--</option>
                            <?php /** @var $provinsi ModelProvinsi */
                            foreach( $daftar_provinsi as $provinsi ) : ?>
                                <option value="<?php echo $provinsi->getKode(); ?>" <?php if( $provinsi->getKode() == $obj_mahasiswa->getKodeProvinsi() ) : ?>selected<?php endif; ?> ><?php echo $provinsi->getNama(); ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="kode_provinsi_asal_sma" class="col-xs-4 control-label">Provinsi SMS asal</label>
                    <div class="col-xs-8">
                        <select id="kode_provinsi_asal_sma" name="kode_provinsi_asal_sma" class="form-control">
                            <option value>--pilih--</option>
                            <?php /** @var $provinsi ModelProvinsi */
                            foreach( $daftar_provinsi as $provinsi ) : ?>
                                <option value="<?php echo $provinsi->getKode(); ?>" <?php if( $provinsi->getKode() == $obj_mahasiswa->getKodeProvinsiAsalSma() ) : ?>selected<?php endif; ?> ><?php echo $provinsi->getNama(); ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tgl_masuk" class="col-xs-4 control-label">Tanggal Masuk</label>
                    <div class="col-xs-8">
                        <div class="input-group">
                            <input id="tgl_masuk" type="text" class="form-control" name="tgl_masuk" value="<?php echo $obj_mahasiswa->getTglMasuk(); ?>"/>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tgl_lulus" class="col-xs-4 control-label">Tanggal Lulus</label>
                    <div class="col-xs-8">
                        <div class="input-group">
                            <input id="tgl_lulus" type="text" class="form-control" name="tgl_lulus" value="<?php echo $obj_mahasiswa->getTglLulus(); ?>"/>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                </div>
            </div>

            <!-- sebelah kanan -->
            <div class="col-md-6">
                <div class="form-group">
                    <label for="thn_masuk" class="col-xs-4 control-label">Tahun Masuk</label>
                    <div class="col-xs-8">
                        <input id="thn_masuk" name="thn_masuk" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getThnMasuk(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="semester_awal" class="col-xs-4 control-label">Semester Awal</label>
                    <div class="col-xs-8">
                        <input id="semester_awal" name="semester_awal" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getSemesterAwal(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="batas_studi" class="col-xs-4 control-label">Batas Studi</label>
                    <div class="col-xs-8">
                        <input id="batas_studi" name="batas_studi" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getBatasStudi(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="status_kuliah" class="col-xs-4 control-label">Status</label>
                    <div class="col-xs-8">
                        <input type="hidden" name="status_kuliah" value="<?php echo $obj_mahasiswa->getStatusKuliah(); ?>">
                        <p class="form-control-static"><?php echo Mahasiswa::$status_kuliah[ $obj_mahasiswa->getStatusKuliah() ]; ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="id_konsentrasi" class="col-xs-4 control-label">Konsentrasi</label>
                    <div class="col-xs-8">
                        <input type="hidden" name="id_konsentrasi" value="<?php echo $obj_mahasiswa->getIdKonsentrasi(); ?>">
                        <p class="form-control-static"><?php echo $obj_mahasiswa->getNamaKonsentrasi(); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="sks_diakui" class="col-xs-4 control-label">SMS diakui</label>
                    <div class="col-xs-8">
                        <input id="sks_diakui" name="sks_diakui" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getSksDiakui(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="nomor_ijazah" class="col-xs-4 control-label">Nmor Ijazah</label>
                    <div class="col-xs-8">
                        <input id="nomor_ijazah" name="nomor_ijazah" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getNomorIjazah(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="nama_wali" class="col-xs-4 control-label">Nama Wali</label>
                    <div class="col-xs-8">
                        <input id="nama_wali" name="nama_wali" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getNamaWali(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="alamat_wali" class="col-xs-4 control-label">Alamat Wali</label>
                    <div class="col-xs-8">
                        <textarea id="alamat_wali" name="alamat_wali" class="form-control"><?php echo $obj_mahasiswa->getAlamatWali(); ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="kode_pekerjaan_wali" class="col-xs-4 control-label">Pekerjaan Wali</label>
                    <div class="col-xs-8">
                        <select id="kode_pekerjaan_wali" name="kode_pekerjaan_wali" class="form-control">
                            <option value>--pilih--</option>
                            <?php /** @var $pw ModelPekerjaanWali */
                            foreach( $daftar_pekerjaan_wali as $pw ) : ?>
                                <option value="<?php echo $pw->getId() ?>" <?php if( $pw->getId() == $obj_mahasiswa->getKodePekerjaanWali() ) : ?>selected<?php endif; ?> ><?php echo $pw->getNama(); ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="penghasilan_wali" class="col-xs-4 control-label">Penghasilan Wali</label>
                    <div class="col-xs-8">
                        <input id="penghasilan_wali" name="penghasilan_wali" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getPenghasilanWali(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="tlp_wali" class="col-xs-4 control-label">Telp. Wali</label>
                    <div class="col-xs-8">
                        <input id="tlp_wali" name="tlp_wali" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getTlpWali(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="nomor_transkrip" class="col-xs-4 control-label">Nomor Transkrip</label>
                    <div class="col-xs-8">
                        <input id="nomor_transkrip" name="nomor_transkrip" type="text" class="form-control" value="<?php echo $obj_mahasiswa->getNomorTranskrip(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="status_bayar" class="col-xs-4 control-label">Status Bayar</label>
                    <div class="col-xs-8">
                        <select id="status_bayar" name="status_bayar" class="form-control">
                            <option value>--pilih--</option>
                            <option value="1" <?php if( $obj_mahasiswa->getStatusBayar() == '1' ) : ?>selected<?php endif; ?> >Sudah Bayar</option>
                            <option value="0" <?php if( $obj_mahasiswa->getStatusBayar() == '0' ) : ?>selected<?php endif; ?> >Belum Bayar</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="status_masuk" class="col-xs-4 control-label">Status Masuk</label>
                    <div class="col-xs-8">
                        <select id="status_masuk" name="status_masuk" class="form-control">
                            <option value>--pilih--</option>
                            <option value="1" <?php if( $obj_mahasiswa->getStatusMasuk() == '1' ) : ?>selected<?php endif; ?> >Baru</option>
                            <option value="0" <?php if( $obj_mahasiswa->getStatusMasuk() == '0' ) : ?>selected<?php endif; ?> >Pindahan</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-8 col-xs-offset-4 col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                    <button class="btn btn-success" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                </div>
            </div>
            </form>

            <script>
                $(document).ready(function() {
                    $('#tgl_lahir').datepicker({
                        dateFormat: 'yy-mm-dd',
                        changeMonth: true,
                        changeYear: true,
                        yearRange: "-10000:+50"
                    });
                    $('#tgl_masuk').datepicker({
                        dateFormat: 'yy-mm-dd',
                        changeMonth: true,
                        changeYear: true,
                        yearRange: "-10000:+50"
                    });
                    $('#tgl_lulus').datepicker({
                        dateFormat: 'yy-mm-dd',
                        changeMonth: true,
                        changeYear: true,
                        yearRange: "-10000:+50"
                    });
                    $('.form-mahasiswa').bootstrapValidator({
                        message: 'Data salah',
                        feedbackIcons: {
                            valid: 'glyphicon glyphicon-ok',
                            invalid: 'glyphicon glyphicon-remove',
                            validating: 'glyphicon glyphicon-refresh'
                        },
                        fields: {
                            NIM: {
                                validators: {
                                    notEmpty: {
                                        message: 'NIM tidak boleh kosong'
                                    },
                                    regexp: {
                                        regexp: /^[A-Z0-9]+$/,
                                        message: 'Huruf besar dan alphanumerik'
                                    }
                                }
                            },
                            kode_prodi: {
                                validators: {
                                    notEmpty: {
                                        message: 'Pilih salah satu PRODI'
                                    }
                                }
                            },
                            nama: {
                                validators: {
                                    notEmpty: {
                                        message: 'Nama tidak boleh kosong'
                                    },
                                    regexp: {
                                        regexp: /^[a-zA-Z0-9\s]+$/,
                                        message: 'Alphanumerik'
                                    }
                                }
                            },
                            jenjang: {
                                validators: {
                                    notEmpty: {
                                        message: 'Pilih salah satu jenjang'
                                    }
                                }
                            },
                            shift: {
                                validators: {
                                    notEmpty: {
                                        message: 'Pilih salah satu shift'
                                    }
                                }
                            },
                            jns_kelamin: {
                                validators: {
                                    notEmpty: {
                                        message: 'Pilih salah satu jenis kelamin'
                                    }
                                }
                            },
                            thn_masuk: {
                                validators: {
                                    notEmpty: {
                                        message: 'Tahun masuk tidak boleh kosong'
                                    }
                                }
                            },
                            batas_studi: {
                                validators: {
                                    notEmpty: {
                                        message: 'Batas studi tidak boleh kosong'
                                    }
                                }
                            },
                            status_kuliah: {
                                validators: {
                                    notEmpty: {
                                        message: 'Pilih salah satu status kuliah'
                                    }
                                }
                            },
                            id_konsentrasi: {
                                validators: {
                                    notEmpty: {
                                        message: 'Pilih salah satu konsentrasi'
                                    }
                                }
                            },
                            sks_diakui: {
                                validators: {
                                    notEmpty: {
                                        message: 'SKS diakui tidak boleh kosong'
                                    }
                                }
                            },
                            status_bayar: {
                                validators: {
                                    notEmpty: {
                                        message: 'Status bayar tidak boleh kosong'
                                    }
                                }
                            },
                            status_masuk: {
                                validators: {
                                    notEmpty: {
                                        message: 'Status masuk tidak boleh kosong'
                                    }
                                }
                            }
                        }
                    });
                });
            </script>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();