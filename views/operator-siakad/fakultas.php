<?php

namespace SIAKAD\Views\Operator\SIAKAD;

use SIAKAD\Controller\Helpers;
use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Fakultas;
use SIAKAD\Model\Fakultas as ModelFakultas;

$tingkat1 = Routes::get_instance()->get_tingkat( 1 );
$tingkat2 = Routes::get_instance()->get_tingkat( 2 );

$q = isset( $_REQUEST[ Helpers::cari_param ] ) ? $_REQUEST[ Helpers::cari_param ] : '';
$status = isset( $_REQUEST[ Helpers::status_param] ) ? $_REQUEST[ Helpers::status_param ] : '';

$is_perbaiki = Routes::get_instance()->is_tingkat( 3, Helpers::aksi_perbaiki) && Routes::get_instance()->has_tingkat( 4 );
$is_simpan = Routes::get_instance()->is_tingkat( 3, Helpers::aksi_simpan );
$is_hapus = Routes::get_instance()->is_tingkat( 3, Helpers::aksi_hapus );
$is_cari = Routes::get_instance()->is_tingkat( 3, Helpers::aksi_cari ) && !empty( $q );

$obj_fakultas = $is_perbaiki ? Fakultas::get_instance()->_get( Routes::get_instance()->get_tingkat( 4 ) ) : new ModelFakultas();

if( $is_simpan ) {
    $aksi = isset( $_REQUEST[ Helpers::aksi_param ] ) ? $_REQUEST[ Helpers::aksi_param ] : '';
    $aksi_perbarui = $aksi == Helpers::aksi_perbarui;
    $aksi_tambah = $aksi == Helpers::aksi_tambah;
    $obj_fakultas->_init( $_REQUEST );

    if( $aksi_tambah )
        siakad_redirect( SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . (
            Fakultas::get_instance()->insert( $obj_fakultas ) ?
                DS . Helpers::aksi_perbaiki . DS .$obj_fakultas->getKode() . '?status=1' : '?status=4'
            )
        );

    elseif( $aksi_perbarui )
        siakad_redirect( SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . (
            Fakultas::get_instance()->update( $obj_fakultas ) ?
                DS . Helpers::aksi_perbaiki . DS .$obj_fakultas->getKode() . '?status=2' : '?status=4'
            )
        );

}

elseif( $is_hapus )
    siakad_redirect( SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . '?status=' . (
        Fakultas::get_instance()->delete(
            Routes::get_instance()->get_tingkat( 4 ) ) ? 3 : 4
        )
    );

$daftar_fakultas = Fakultas::get_instance()->_gets( array(
    'number'                => -1,
    'nama'                  => $q,
    'nama_ingg'             => $q,
    'order'                 => 'ASC'
) );

Headers::get_instance()
    ->set_page_title( 'Fakultas' )
    ->set_page_name( 'Daftar Fakultas' );

Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">Daftar Fakultas</h1>

            <?php if( $status ) Helpers::get_instance()->set_message_object( 'Fakultas' )->display_message( $status ); ?>

            <div class="row">
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-md-4">
                            <form action="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2; ?>/cari" method="get">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="q" value="<?php if( $is_cari ) echo $q; ?>">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <br/>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Nama Fakultas</th>
                            <th>Alias</th>
                            <th>Keterangan</th>
                            <th>#</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php /** @var $fakultas \SIAKAD\Model\Fakultas */
                        foreach( $daftar_fakultas as $fakultas ) : ?>
                            <tr <?php if( $is_perbaiki && Routes::get_instance()->is_tingkat( 4, $fakultas->getKode() ) ) : ?>class="info"<?php endif; ?> >
                                <td><?php echo $fakultas->getKode(); ?></td>
                                <td><?php echo $fakultas->getNama() . ( $fakultas->has_nama_ingg() ? ' (' . $fakultas->getNamaIngg() . ')' : '' ); ?></td>
                                <td><?php echo $fakultas->getAlias(); ?></td>
                                <td><?php echo $fakultas->getKeterangan(); ?></td>
                                <td>
                                    <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view(); ?>/fakultas/hapus/<?php echo $fakultas->getKode(); ?>" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                                    <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view(); ?>/fakultas/perbaiki/<?php echo $fakultas->getKode(); ?>" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <strong><i class="glyphicon glyphicon-<?php echo $is_perbaiki ? 'pencil' : 'plus'; ?>"></i>&nbsp;&nbsp;<?php if( $is_perbaiki ) : ?>Perbaiki<?php else : ?>Tambah<?php endif; ?> Fakultas</strong>
                        </div>
                        <div class="panel-body">
                            <form class="form-fakultas form-horizontal" action="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view(); ?>/fakultas/simpan" method="post">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Kode</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" placeholder="Kode fakultas" name="kode" value="<?php echo $obj_fakultas->getKode(); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Nama</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" placeholder="Nama fakultas" name="nama" value="<?php echo $obj_fakultas->getNama(); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Nama (Ingg)</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" placeholder="Nama fakultas dalam inggris" name="nama_ingg" value="<?php echo $obj_fakultas->getNamaIngg(); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Alias</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" placeholder="Alias fakultas" name="alias" value="<?php echo $obj_fakultas->getAlias(); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Keterangan</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" placeholder="Keterangan fakultas" name="keterangan"><?php echo $obj_fakultas->getKeterangan(); ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-4">
                                        <input type="hidden" name="kode_univ" value="<?php echo SIAKAD_UNIVERSIY_ID; ?>">
                                        <input type="hidden" name="aksi" value="<?php echo $is_perbaiki ? Helpers::aksi_perbarui : Helpers::aksi_tambah; ?>">
                                        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                                        <?php if( $is_perbaiki ) : ?>
                                            <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2; ?>" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Baru</a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.form-fakultas').bootstrapValidator({
            message: 'Data salah',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                kode: {
                    message: 'Kode fakultas salah',
                    validators: {
                        notEmpty: {
                            message: 'Kode fakultas tidak boleh kosong'
                        },
                        stringLength: {
                            min: 1,
                            max: 1,
                            message: 'Kode fakultas hanya boleh 1 huruf'
                        },
                        regexp: {
                            regexp: /^[A-Z]+$/,
                            message: 'Gunakan huruf besar'
                        }
                    }
                },
                nama: {
                    validators: {
                        notEmpty: {
                            message: 'Nama fakultas tidak boleh kosong'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z\s]+$/,
                            message: 'Hanya huruf (besar/kecil) dan spasi saja'
                        }
                    }
                },
                alias: {
                    validators: {
                        notEmpty: {
                            message: 'Alias fakultas tidak boleh kosong'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z]+$/,
                            message: 'Huruf dan tidak boleh ada spasi'
                        }
                    }
                }
            }
        });
    });
</script>

<?php Contents::get_instance()->get_footer();