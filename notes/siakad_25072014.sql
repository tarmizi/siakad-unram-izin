--
-- Ganti field `password` menjadi TEXT
--

ALTER TABLE `login` CHANGE `password` `password` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'Merupakan password ter-enkripsi dari setiap password login user.';

--
-- Dumping data for table `level_akses`
--

INSERT INTO `level_akses` (`kode_akses`, `nama_level`, `deskripsi`) VALUES
('A', 'Administrator', 'Admin utama untuk sistem informasi akademik'),
('AK', 'Akademik', 'Pejabat akademisi lingkungan universitas'),
('OF-F', 'Operator Fakultas', 'Operator fakultas untuk fakultas teknik'),
('OP-FB', 'Operator Program Studi Elektro', 'Operator untuk program studi elektro dari fakultas teknik'),
('OS', 'Operator Siakad', 'Operator utama sistem informasi akademik');

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`username`, `password`, `status`) VALUES
('admin', '$6$nuZMQ.8u$8VCOoneb9R6o7wUCUWIjpO/ToDU6DRQ5MEt2NMBoQzcYF3huGjPx01XLQ67tl89qlFBWTCSOU9cmKQsSg7Q231', 1),
('prodi', '$6$nuZMQ.8u$2dOUWiggeYsX0RAPiRhXQq9lFKEY5.c.t0IpCeZsMzLABU9RzYJanThYgxBROJV5hcuv3KunusOMPz5gcfybF.', 1);

--
-- Dumping data for table `user_level`
--

INSERT INTO `user_level` (`username`, `level_akses`) VALUES
('admin', 'A'),
('prodi', 'OP-FB');