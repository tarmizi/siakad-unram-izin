<?php

namespace SIAKAD\Views\Operator\PRODI;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Mahasiswa;
use SIAKAD\Controller\Konsentrasi;
use SIAKAD\Controller\Agama;
use SIAKAD\Controller\Provinsi;
use SIAKAD\Controller\StatusMahasiswa;
use SIAKAD\Controller\Sessions;

Headers::get_instance()
    ->set_page_title( 'Data Mahasiswa' )
    ->set_page_name( 'Mahasiswa' );

$tingkat2 = Routes::get_instance()->get_tingkat( 2 );

$is_edit = Routes::get_instance()->is_tingkat( 3, 'edit' ) && Routes::get_instance()->has_tingkat( 4 );
$is_simpan = Routes::get_instance()->is_tingkat( 3, 'simpan' );
$is_tambah = Routes::get_instance()->is_tingkat( 3, 'tambah' );

$obj_prodi = ProgramStudi::get_instance()->_get(
    Sessions::get_instance()->_retrieve()->getObjLevelAkses()->getKodeObject(), 'kode_prodi_unram'
);

Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <?php 
            if( $is_simpan )    {
                $m = Mahasiswa::get_instance()->_get($_POST['nim']);
                $m->setNIM($_POST['nim']);
                //$m->setKodeProdi($_POST['prodi']);
                $m->setNama($_POST['nama']);
                $m->setTempatLahir($_POST['tempat_lahir']);
                $m->setTglLahir($_POST['tgl_lahir']);
                $m->setJnsKelamin($_POST['kelamin']);
                $m->setKodeAgama($_POST['agama']);
                $m->setAlamat($_POST['alamat']);
                $m->setKodeProvinsi($_POST['provinsi']);
                $m->setThnMasuk($_POST['thn_masuk']);
                $m->setBatasStudi($_POST['batas_studi']);
                $m->setStatusKuliah($_POST['statuskuliah']);
                $m->setIdKonsentrasi($_POST['konsentrasi']);
                $m->setSksDiakui($_POST['sks']);
                $m->setNomorIjazah($_POST['no_ijazah']);
                $m->setNamaWali($_POST['nama_wali']);
                $m->setAlamatWali($_POST['alamat_wali']);
                $m->setKodePekerjaanWali($_POST['pekerjaan_wali']);
                $m->setPenghasilanWali($_POST['penghasilan_wali']);
                $m->setTlpWali($_POST['telepon_wali']);
                $m->setNomorTranskrip($_POST['no_transkrip']);
                $m->setStatusBayar($_POST['statusbayar']);
                $m->setStatusMasuk($_POST['statusmasuk']);
                if(isset($_POST['save'])){
                    Mahasiswa::get_instance()->update($m) or die("Query salah : " . mysql_error());
                    ?>
                        <div class="alert alert-success">
                            <p><i class="glyphicon glyphicon-ok"></i> Data berhasil disimpan.</p>
                        </div>
                    <?php
                }
                else if(isset($_POST['new'])){
                    Mahasiswa::get_instance()->insert($m) or die("Query salah : " . mysql_error());  
                    ?>
                        <div class="alert alert-success">
                            <p><i class="glyphicon glyphicon-ok"></i> Data berhasil ditambah.</p>
                        </div>
                    <?php
                }
            }
            if( $is_tambah ) : ?>
                <h1 class="page-header">Data Mahasiswa <small>Edit</small></h1>
                <form class="form-horizontal" method="post" action="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2; ?>/simpan">
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">NIM</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="nim" type="text" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Program Studi</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="prodi" type="text" class="form-control" value="Teknik Elektro"readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Konsentrasi</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <select name="konsentrasi" class="form-control">
                            <?php $ks = Konsentrasi::get_instance()->_gets(array('kode_PS'=>'112011'));
                                foreach ($ks as $k) :
                            ?>
                                <option value="<?php echo $k->getId(); ?>"><?php echo $k->getNama(); ?></option>
                            <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Nama</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="nama" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Tempat Lahir</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="tempat_lahir" type="text" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Tanggal Lahir</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="tgl_lahir" type="date" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Jenis Kelamin</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <select name="kelamin" class="form-control">
                                <option value="L">Laki-Laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Agama</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <select name="agama" class="form-control">
                                <?php $ag = Agama::get_instance()->_gets();
                                foreach ($ag as $a) :
                            ?>
                                <option value="<?php echo $a->getKode(); ?>"><?php echo $a->getNama(); ?></option>
                            <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Alamat</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="alamat" type="text" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Provinsi</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <select name="provinsi" class="form-control">
                                <?php $prov = Provinsi::get_instance()->_gets(array('number'=>-1));
                                foreach ($prov as $p) :
                            ?>
                                <option value="<?php echo $p->getKode(); ?>"><?php echo $p->getNama(); ?></option>
                            <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Tahun Masuk</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="thn_masuk" type="text" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Batas Studi</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="batas_studi" type="text" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Status Kuliah</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <select name="statuskuliah" class="form-control">
                                <?php $st = StatusMahasiswa::get_instance()->_gets(array('kode_PS'=>'112011'));
                                foreach ($st as $s) :
                            ?>
                                <option value="<?php echo $s->getKode(); ?>"><?php echo $s->getAlias(); ?></option>
                            <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">SKS Diakui</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="sks" type="text" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Nomor Ijazah</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="no_ijazah" type="text" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Nama Wali</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="nama_wali" type="text" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Alamat Wali</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="alamat_wali" type="text" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Pekerjaan Wali</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <select name="pekerjaan_wali" class="form-control">
                                <option value="0">0</option>
                                <option value="1">1</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Penghasilan Wali</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <select name="penghasilan_wali" class="form-control">
                                <option value="0">0</option>
                                <option value="1">1</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Telepon Wali</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="telepon_wali" type="text" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Nomor Transkrip</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="no_transkrip" type="text" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Status Bayar</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <select name="statusbayar" class="form-control">
                                <option value="0">Belum</option>
                                <option value="1">Sudah</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Status Masuk</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <select name="statusmasuk" class="form-control">
                                <option value="1">Baru</option>
                                <option value="0">Pindahan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-8 col-xs-offset-4 col-sm-5 col-sm-offset-3 col-lg-3 col-lg-offset-2">
                            <button class="btn btn-primary"  name="new"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                        </div>
                    </div>
                </form>
                
            <?php elseif( $is_edit ) : 
                $nim = $_POST['nim'];
                $mhs = Mahasiswa::get_instance()->_get($nim);
                ?>
                <h1 class="page-header">Data Mahasiswa <small>Edit</small></h1>
                <form class="form-horizontal" method="post" action="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2; ?>/simpan">
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">NIM</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="nim" type="text" class="form-control" value="<?php echo $mhs->getNIM(); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Program Studi</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="prodi" type="text" class="form-control" value="Teknik Elektro"readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Konsentrasi</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <select name="konsentrasi" class="form-control">
                            <?php $ks = Konsentrasi::get_instance()->_gets(array('kode_PS'=>'112011'));
                                foreach ($ks as $k) :
                            ?>
                                <option value="<?php echo $k->getId(); ?>"><?php echo $k->getNama(); ?></option>
                            <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Nama</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="nama" type="text" class="form-control" value="<?php echo $mhs->getNama(); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Tempat Lahir</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="tempat_lahir" type="text" class="form-control" value="<?php echo $mhs->getTempatLahir(); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Tanggal Lahir</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="tgl_lahir" type="date" class="form-control" value="<?php echo $mhs->getTglLahir(); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Jenis Kelamin</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <select name="kelamin" class="form-control">
                                <option value="L">Laki-Laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Agama</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <select name="agama" class="form-control">
                                <?php $ag = Agama::get_instance()->_gets();
                                foreach ($ag as $a) :
                            ?>
                                <option value="<?php echo $a->getKode(); ?>"><?php echo $a->getNama(); ?></option>
                            <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Alamat</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="alamat" type="text" class="form-control" value="<?php echo $mhs->getAlamat(); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Provinsi</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <select name="provinsi" class="form-control">
                                <?php $prov = Provinsi::get_instance()->_gets(array('number'=>-1));
                                foreach ($prov as $p) :
                            ?>
                                <option value="<?php echo $p->getKode(); ?>"><?php echo $p->getNama(); ?></option>
                            <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Tahun Masuk</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="thn_masuk" type="text" class="form-control" value="<?php echo $mhs->getThnMasuk(); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Batas Studi</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="batas_studi" type="text" class="form-control" value="<?php echo $mhs->getBatasStudi(); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Status Kuliah</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <select name="statuskuliah" class="form-control">
                                <?php $st = StatusMahasiswa::get_instance()->_gets(array('kode_PS'=>'112011'));
                                foreach ($st as $s) :
                            ?>
                                <option value="<?php echo $s->getKode(); ?>"><?php echo $s->getAlias(); ?></option>
                            <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">SKS Diakui</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="sks" type="text" class="form-control" value="<?php echo $mhs->getSksDiakui(); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Nomor Ijazah</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="no_ijazah" type="text" class="form-control" value="<?php echo $mhs->getNomorIjazah(); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Nama Wali</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="nama_wali" type="text" class="form-control" value="<?php echo $mhs->getNamaWali(); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Alamat Wali</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="alamat_wali" type="text" class="form-control" value="<?php echo $mhs->getAlamatWali(); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Pekerjaan Wali</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <select name="pekerjaan_wali" class="form-control">
                                <option value="0">0</option>
                                <option value="1">1</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Penghasilan Wali</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <select name="penghasilan_wali" class="form-control">
                                <option value="0">0</option>
                                <option value="1">1</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Telepon Wali</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="telepon_wali" type="text" class="form-control"  value="<?php echo $mhs->getTlpWali(); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Nomor Transkrip</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input name="no_transkrip" type="text" class="form-control" value="<?php echo $mhs->getNomorTranskrip(); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Status Bayar</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <select name="statusbayar" class="form-control">
                                <option value="0">Belum</option>
                                <option value="1">Sudah</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Status Masuk</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <select name="statusmasuk" class="form-control">
                                <option value="1">Baru</option>
                                <option value="0">Pindahan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-8 col-xs-offset-4 col-sm-5 col-sm-offset-3 col-lg-3 col-lg-offset-2">
                            <button class="btn btn-primary"  name="save"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                        </div>
                    </div>
                </form>
            <?php else :
                ?>
                <h1 class="page-header">Data Mahasiswa</h1>
                <form class="form-horizontal" method="post" action="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2; ?>">
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Cari</label>
                        <div class="col-sm-4">
                            <input name="nim" type="text" class="form-control" placeholder="Masukkan NIM">
                        </div>
                        <div class="col-sm-2">
                            <button name="cari" type="submit" class="form-control"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                        <div class="col-sm-2">
                            <a class="btn btn-sm btn-primary" href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2; ?>/tambah"><i class="glyphicon glyphicon-plus"></i> Tambah Baru</a>
                        </div>
                        <div class="col-sm-2">
                            <a class="btn btn-sm btn-primary" href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view(); ?>/daftar-mahasiswa"><i class="glyphicon glyphicon-user"></i> Lihat Semua</a>
                        </div>
                    </div>
                </form>
                <?php
                if(isset($_POST['cari'])) :
                    $nim = $_POST['nim'];
                $mahasiswa = Mahasiswa::get_instance()->_get($nim);//s(array('kode_prodi'=>112011,'number'=>-1));
                    if($mahasiswa->getNIM()!=null):
                ?>
                <table class="table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>NIM</th>
                        <th>Nama</th>
                        <th>Status Kuliah</th>
                        <th>Status Bayar</th>
                        <th>#</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var $mahasiswa \SIAKAD\Model\Mahasiswa */
                    $no=1; ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $mahasiswa->getNIM(); ?></td>
                            <td><?php echo $mahasiswa->getNama(); ?></td>
                            <td><?php echo $mahasiswa->getStatusKuliah(); ?></td>
                            <td><?php echo $mahasiswa->getStatusBayar(); ?></td>
                            <td><form method="post" action="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2; ?>/edit/<?php echo $mahasiswa->getNIM(); ?>"><input type="hidden" name="nim" value="<?php echo $mahasiswa->getNIM(); ?>"/><button type="submit" class="form-control"><i class="glyphicon glyphicon-pencil"></i></button></form></td>
                        </tr>
                    <?php ?>
                    </tbody>
                </table>
                <?php 
                else : ?>
                    <div class="alert alert-warning">
                        <p><i class="glyphicon glyphicon-remove"></i> Data mahasiswa tidak ditemukan.</p>
                    </div>
                <?php
                endif;
                endif; 

                endif; ?>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();