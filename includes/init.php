<?php

/** untuk keperluan debugging */
error_reporting( E_ALL & ~E_NOTICE & ~E_STRICT );

require( 'define.db.php' );
require( 'define.path.php' );
require( 'define.info.php' );
require( 'cls.autoload.php' );
require( 'fn.helper.php' );