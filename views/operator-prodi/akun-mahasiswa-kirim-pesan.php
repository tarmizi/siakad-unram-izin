<h1 class="page-header">Mahasiswa</h1>
<form class="form-horizontal">
    <div class="form-group">
        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Pesan</label>
        <div class="col-xs-8 col-sm-5 col-lg-3">
            <textarea class="form-control"></textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-8 col-xs-offset-4 col-sm-5 col-sm-offset-3 col-lg-3 col-lg-offset-2">
            <button class="btn btn-primary"><i class="glyphicon glyphicon-send"></i> Kirim</button>
        </div>
    </div>
</form>