<?php
/**  
 * Canvas Production
 *
 * Author : Dani
 */

namespace SIAKAD\Controller;
use SIAKAD\Model\Bagian as ModelBagian;


class Bagian extends Databases {

    private $field_id;
    private $class_name;
    private $count_query;

    /** @var Bagian $instance */
    private static $instance;

    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function __construct() {
        parent::_connect();
        $this->field_id = 'id';
        $this->class_name = '\SIAKAD\Model\Bagian';
        $this->table_name = 'bagian';
        $this->count_query = 'SELECT * FROM ' . $this->table_name;
    }

    /**
     * @param $id
     * @param string $by
     * @return \SIAKAD\Model\Bagian
     */
    function _get( $id, $by = '' ) {
        $query = 'SELECT * FROM ' . $this->table_name . ' WHERE `' . ( empty( $by ) ? $this->field_id : $id ) . '` = "' . $id . '"';
        return ( $return = mysql_fetch_object(
            mysql_query( $query ), $this->class_name
        ) ) ? $return : new $this->class_name;
    }

    function _gets( $args = array() ) {

        $return = array();

        $default_args = array(
            'kode_PS'                   => -1,
            'id_konsentrasi'            => -1,
            'nomor'                     => -1,
            'exclude'                   => array(),
            'conditions'                => '',
            'orderby'                   => $this->field_id,
            'order'                     => 'DESC',
            'number'                    => 10,
            'offset'                    => 0,
            'custom'                    =>''
        );

        $list_args = sync_default_params( $default_args, $args );
        $query = 'SELECT * FROM ' . $this->table_name . ' WHERE 1';

        /**
         * kode_PS
         */
        if( !empty( $list_args[ 'kode_PS' ] ) )
            $query .= ' AND ' . $this->table_name . '.kode_PS = ' . $list_args[ 'kode_PS' ];

        /**
         * id_konsentrasi
         */
        if( $list_args[ 'id_konsentrasi' ] >= 0 )
            $query .= ' AND ' . $this->table_name . '.id_konsentrasi = ' . $list_args[ 'id_konsentrasi' ];

        /**
         * nomor
         */
        if( $list_args[ 'nomor' ] >= 0 )
            $query .= ' AND ' . $this->table_name . '.nomor = ' . $list_args[ 'nomor' ];
        
        /**
         * exclude
         */
        if( !empty( $list_args[ 'exclude' ] ) ) {

            foreach( $list_args[ 'exclude' ] as $ex )
                $query .= ' AND `id` <> ' . $ex;

        }

        /**
         * untuk custom query pada conditions
         */
        if( !empty( $list_args[ 'conditions' ] ) ) {
            foreach( $list_args[ 'conditions' ] as $conditions )
                $query .= ' AND ' . $conditions['claue'] . '.' . $conditions[ 'field' ] . ' ' . $conditions[ 'operator' ] . ' ' . $conditions[ 'comparison' ];
        }
        
        
        $this->count_query = $query;

        /**
         * orderby dan jenis order
         */
        $query .= ' ORDER BY `' . $list_args[ 'orderby' ] . '` ' . $list_args[ 'order' ];

        /**
         * limit
         */
        if( $list_args[ 'number' ] >= 0 )
            $query .= ' LIMIT ' . $list_args[ 'offset' ] . ', ' . $list_args[ 'number' ];
        
        if( !empty( $list_args[ 'custom' ] ) ) {
            $query = $list_args['custom'];
        }
        
        $resource = mysql_query( $query );

        //echo $query . ' : ' . mysql_error();

        while( $row = mysql_fetch_object( $resource, $this->class_name ) )
            $return[] = $row;

        return $return;

    }
    
    function insert( ModelBagian $a ) {
        return parent::insert(
            $this->table_name,
            array(
                'id',
                'kode_PS',
                'id_konsentrasi',
                'nomor',
                'nama'
            ),
            array(
                array(
                    $a->getId(),
                    $a->getKodePs(),
                    $a->getIdKonsentrasi(),
                    $a->getNomor(),
                    $a->getNama()
                )
            )
        );
    }

    function update( ModelBagian $a ) {
        return parent::update(
            $this->table_name,
            array(
                'id',
                'kode_PS',
                'id_konsentrasi',
                'nomor',
                'nama'
            ),
            array(
                $a->getId(),
                $a->getKodePS(),
                $a->getIdKonsentrasi(),
                $a->getNomor(),
                $a->getNama()
            ),
            array(
                'id', $a->getId()
            )
        );
    }

    function delete( $kode ) {
        return parent::delete(
            $this->table_name,
            'id',
            $kode
        );
    }

}