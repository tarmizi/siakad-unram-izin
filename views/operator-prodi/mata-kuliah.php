<?php

namespace SIAKAD\Views\Operator\PRODI;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Helpers;
use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Matakuliah;
use SIAKAD\Controller\Konsentrasi;
use SIAKAD\Controller\Kurikulum;
use SIAKAD\Controller\Bagian;
use SIAKAD\Controller\Dosen;
use SIAKAD\Controller\ProgramStudi;
use SIAKAD\Controller\SifatMataKuliah;
use SIAKAD\Controller\Sessions;

use SIAKAD\Model\ProgramStudi as ModelProgramStudi;
use SIAKAD\Model\Konsentrasi as ModelKonsentrasi;
use SIAKAD\Model\Bagian as ModelBagian;
use SIAKAD\Model\Matakuliah as ModelMatakuliah;
use SIAKAD\Model\Kurikulum as ModelKurikulum;
use SIAKAD\Model\SifatMataKuliah as ModelSifatMataKuliah;
use SIAKAD\Model\Dosen as ModelDosen;

Headers::get_instance()
    ->set_page_title( 'Mata Kuliah' )
    ->set_page_name( 'Mata Kuliah' )
    ->set_page_sub_name( 'Mata Kuliah' );

$tingkat1 = Routes::get_instance()->get_tingkat( 1 );
$tingkat2 = Routes::get_instance()->get_tingkat( 2 );
$tingkat3 = Routes::get_instance()->get_tingkat( 3 );

$status = isset( $_REQUEST[ Helpers::status_param] ) ? $_REQUEST[ Helpers::status_param ] : '';
$is_tambah = Routes::get_instance()->is_tingkat( 4, Helpers::aksi_tambah );
$is_perbaiki = Routes::get_instance()->is_tingkat( 4, Helpers::aksi_perbaiki ) && Routes::get_instance()->has_tingkat( 5 );
$is_simpan = Routes::get_instance()->is_tingkat( 4, Helpers::aksi_simpan );
$is_hapus = Routes::get_instance()->is_tingkat( 4, Helpers::aksi_hapus ) && Routes::get_instance()->has_tingkat( 5 );
$is_prasyarat = Routes::get_instance()->is_tingkat( 4, 'prasyarat' ) && Routes::get_instance()->has_tingkat( 5 );

$obj_prodi = ProgramStudi::get_instance()->_get(
    Sessions::get_instance()->_retrieve()->getObjLevelAkses()->getKodeObject(), 'kode_prodi_unram'
);

if( $is_simpan ) {
    $aksi = isset( $_REQUEST[ Helpers::aksi_param ] ) ? $_REQUEST[ Helpers::aksi_param ] : '';
    $aksi_perbarui = $aksi == Helpers::aksi_perbarui;
    $aksi_tambah = $aksi == Helpers::aksi_tambah;
    $obj_matakuliah = new ModelMatakuliah();
    $obj_matakuliah->_init( $_REQUEST );

    if( $aksi_tambah )
        siakad_redirect( SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . (
            Matakuliah::get_instance()->insert( $obj_matakuliah ) ?
                DS . Helpers::aksi_perbaiki . DS .$obj_matakuliah->getKodeMk() . '?status=' . Helpers::status_tambah : '?status=' . Helpers::status_gagal
            )
        );

    elseif( $aksi_perbarui )
        siakad_redirect( SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . (
            Matakuliah::get_instance()->update( $obj_matakuliah ) ?
                DS . Helpers::aksi_perbaiki . DS .$obj_matakuliah->getKodeMk() . '?status=' . Helpers::status_perbarui : '?status=' . Helpers::status_gagal
            )
        );

} elseif( $is_hapus ) {
    siakad_redirect( SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . '?status=' . (
        Matakuliah::get_instance()->delete(
            Routes::get_instance()->get_tingkat( 5 ) ) ? Helpers::status_hapus : Helpers::status_gagal
        )
    );
}

$default_params = array(
    'kode_PS'           => $obj_prodi->getKode(),
    'id_konsentrasi'    => -1,
    'id_bagian'         => -1,
    'nama_mk'           => '',
    'number'            => 20,
    'page'              => 1,
);
$list_params = sync_default_params( $default_params, $_GET );

/** ambil daftar prodi */
$daftar_prodi = ProgramStudi::get_instance()->_gets( array(
    'number'        => -1,
    'orderby'       => 'nama',
    'order'         => 'ASC'
) );

$daftar_prodi_by_fakultas = array();

/** @var $prodi ModelProgramStudi */
foreach( $daftar_prodi as $prodi )
    in_array( $prodi->getNamaFakultas(), $daftar_prodi_by_fakultas ) || $daftar_prodi_by_fakultas[ $prodi->getNamaFakultas() ][] = $prodi;

$daftar_konsentrasi = Konsentrasi::get_instance()->_gets( array(
    'kode_PS'   => $list_params[ 'kode_PS' ],
    'number'    => -1
) );

$daftar_bagian = Bagian::get_instance()->_gets( array(
    'kode_PS'   => $list_params[ 'kode_PS' ],
    'number'    => -1
) );

$daftar_kurikulum = Kurikulum::get_instance()->_gets( array(
    'kode_PS'   => $list_params[ 'kode_PS' ],
    'number'    => -1
) );

$daftar_sifat_matakuliah = SifatMataKuliah::get_instance()->_gets( array(
    'number'    => -1
) );

$daftar_dosen = Dosen::get_instance()->_gets( array(
    'kode_prodi'    => $list_params[ 'kode_PS' ],
    'number'        => -1
) );

Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
        <?php if( $is_prasyarat ) : ?>
            
            
        <?php elseif( $is_tambah || $is_perbaiki ) :

            $obj_matakuliah = $is_perbaiki ? MataKuliah::get_instance()->_get( Routes::get_instance()->get_tingkat( 5 ), 'kode_mk' ) : new ModelMatakuliah(); ?>

            <h1 class="page-header">
                Mata Kuliah
                <small><?php if( $is_perbaiki ) : ?>Perbaiki<?php else : ?>Tambah<?php endif; ?></small>
            </h1>

            <?php if( $status ) Helpers::get_instance()->set_message_object( 'Mata Kuliah' )->display_message( $status ); ?>

            <form class="form-horizontal form-matakuliah" action="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . DS . Helpers::aksi_simpan; ?>" method="post">

                <!-- sebelah kiri -->
                <div class="col-md-6">

                    <div class="form-group">
                        <label for="id_kurikulum" class="col-xs-4 control-label">Kurikulum</label>
                        <div class="col-xs-8">
                            <select class="form-control" id="id_kurikulum" name="id_kurikulum">
                                <option value>--pilih--</option>
                                <?php /** @var $kurikulum ModelKurikulum */
                                foreach( $daftar_kurikulum as $kurikulum ) :?>
                                    <option value="<?php echo $kurikulum->getId(); ?>" <?php if( $kurikulum->getId() == $obj_matakuliah->getIdKurikulum() ) : ?>selected<?php endif; ?> ><?php echo $kurikulum->getId(); ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="kode_mk" class="col-xs-4 control-label">Kode Mata Kuliah</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" id="kode_mk" name="kode_mk" value="<?php echo $obj_matakuliah->getKodeMk(); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="nama_mk" class="col-xs-4 control-label">Nama Mata Kuliah</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" id="nama_mk" name="nama_mk" value="<?php echo $obj_matakuliah->getNamaMk(); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="nama_mk_ingg" class="col-xs-4 control-label">Nama Mata Kuliah Inggris</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" id="nama_mk_ingg" name="nama_mk_ingg" value="<?php echo $obj_matakuliah->getNamaMkIngg(); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="sks" class="col-xs-4 control-label">SKS</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" id="sks" name="sks" value="<?php echo $obj_matakuliah->getSks(); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="kode_sifat" class="col-xs-4 control-label">Sifat</label>
                        <div class="col-xs-8">
                            <select class="form-control" id="kode_sifat" name="kode_sifat">
                                <option value>--pilih--</option>
                                <?php /** @var $sifat ModelSifatMataKuliah */
                                foreach( $daftar_sifat_matakuliah as $sifat ) : ?>
                                    <option value="<?php echo $sifat->getKode(); ?>" <?php if( $sifat->getKode() == $obj_matakuliah->getKodeSifat() ) : ?>selected<?php endif; ?> ><?php echo $sifat->getNama(); ?> (<?php echo $sifat->getAlias(); ?>)</option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="semester" class="col-xs-4 control-label">Semester</label>
                        <div class="col-xs-8">
                            <select class="form-control" id="semester" name="semester">
                                <option value>--pilih--</option>
                                <?php foreach( MataKuliah::$semester as $semester ) : ?>
                                    <option value="<?php echo $semester; ?>" <?php if( $semester == $obj_matakuliah->getSemester() ) : ?>selected<?php endif; ?> ><?php echo $semester; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="jenis_semester" class="col-xs-4 control-label">Jenis Semester</label>
                        <div class="col-xs-8">
                            <select class="form-control" id="jenis_semester" name="jenis_semester">
                                <option value>--pilih--</option>
                                <?php foreach( MataKuliah::$jenis_semester as $id => $text ) : ?>
                                    <option value="<?php echo $id; ?>" <?php if( $id == $obj_matakuliah->getJenisSemester() ) : ?>selected<?php endif; ?> ><?php echo $text; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                </div>

                <!-- sebelah kanan -->
                <div class="col-md-6">

                    <div class="form-group">
                        <label for="status" class="col-xs-4 control-label">Status</label>
                        <div class="col-xs-8">
                            <select class="form-control" id="status" name="status">
                                <option value>--pilih--</option>
                                <?php foreach( MataKuliah::$status as $id => $text ) : ?>
                                    <option value="<?php echo $id; ?>" <?php if( $id == $obj_matakuliah->getStatus() ) : ?>selected<?php endif; ?> ><?php echo $text; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="id_konsentrasi" class="col-xs-4 control-label">Konsentrasi</label>
                        <div class="col-xs-8">
                            <select class="form-control" id="id_konsentrasi" name="id_konsentrasi">
                                <option value>--pilih--</option>
                                <?php /** @var $konsentrasi ModelKonsentrasi */
                                foreach( $daftar_konsentrasi as $konsentrasi ) :?>
                                    <option value="<?php echo $konsentrasi->getId(); ?>" <?php if( $konsentrasi->getId() == $obj_matakuliah->getIdKonsentrasi() ) : ?>selected<?php endif; ?> ><?php echo $konsentrasi->getNama(); ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="id_bagian" class="col-xs-4 control-label">Bagian</label>
                        <div class="col-xs-8">
                            <select class="form-control" id="id_bagian" name="id_bagian">
                                <option value>--pilih--</option>
                                <?php /** @var $bagian ModelBagian */
                                foreach( $daftar_bagian as $bagian ) :?>
                                    <option value="<?php echo $bagian->getId(); ?>" <?php if( $bagian->getId() == $obj_matakuliah->getIdBagian() ) : ?>selected<?php endif; ?> ><?php echo $bagian->getNama(); ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="kode_dosen_koordinator" class="col-xs-4 control-label">Dosen Koordinator</label>
                        <div class="col-xs-8">
                            <select class="form-control" id="kode_dosen_koordinator" name="kode_dosen_koordinator">
                                <option value>--pilih--</option>
                                <?php /** @var $dosen ModelDosen */
                                foreach( $daftar_dosen as $dosen ) :?>
                                    <option value="<?php echo $dosen->getKode(); ?>" <?php if( $dosen->getKode() == $obj_matakuliah->getKodeDosenKoordinator() ) : ?>selected<?php endif; ?> ><?php echo $dosen->getNama(); ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="silabus" class="col-xs-4 control-label">Silabus</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" id="silabus" name="silabus" value="<?php echo $obj_matakuliah->getSilabus(); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="sap" class="col-xs-4 control-label">SAP</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" id="sap" name="sap" value="<?php echo $obj_matakuliah->getSap(); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="bahan_ajar" class="col-xs-4 control-label">Bahan Ajar</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" id="bahan_ajar" name="bahan_ajar" value="<?php echo $obj_matakuliah->getBahanAjar(); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="sks_teori" class="col-xs-4 control-label">SKS Teori</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" id="sks_teori" name="sks_teori" value="<?php echo $obj_matakuliah->getSksTeori(); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="sks_praktikum" class="col-xs-4 control-label">SKS Praktikum</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" id="sks_praktikum" name="sks_praktikum" value="<?php echo $obj_matakuliah->getSksPraktikum(); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="jenis_pembagian_kelas" class="col-xs-4 control-label">Jenis Pembagian Kelas</label>
                        <div class="col-xs-8">
                            <select class="form-control" id="jenis_pembagian_kelas" name="jenis_pembagian_kelas">
                                <option value>--pilih--</option>
                                <?php foreach( MataKuliah::$jenis_pembagian_kelas as $id => $text ) : ?>
                                    <option value="<?php echo $id; ?>" <?php if( $id == $obj_matakuliah->getJenisPembagianKelas() ) : ?>selected<?php endif; ?> ><?php echo $text; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <div class="col-xs-8 col-xs-offset-4 col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                        <input type="hidden" name="<?php echo Helpers::aksi_param; ?>" value="<?php echo $is_perbaiki ? Helpers::aksi_perbarui : Helpers::aksi_tambah; ?>">
                        <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3; ?>" class="btn btn-danger"><i class="glyphicon glyphicon-arrow-left"></i> Kembali</a>
                        <button class="btn btn-success" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                        <?php if( $is_perbaiki ) : ?>
                            <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . DS . Helpers::aksi_tambah; ?>" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Baru</a>
                        <?php endif; ?>
                    </div>
                </div>

            </form>

            <script>
                $(document).ready(function() {
                    $('#tgl_lahir').datepicker();
                    $('.form-matakuliah').bootstrapValidator({
                        message: 'Data salah',
                        feedbackIcons: {
                            valid: 'glyphicon glyphicon-ok',
                            invalid: 'glyphicon glyphicon-remove',
                            validating: 'glyphicon glyphicon-refresh'
                        },
                        fields: {
                            id_kurikulum: {
                                validators: {
                                    notEmpty: {
                                        message: 'Pilih salah satu kurikulum'
                                    }
                                }
                            },
                            kode_mk: {
                                validators: {
                                    notEmpty: {
                                        message: 'Kode tidak boleh kosong'
                                    },
                                    regexp: {
                                        regexp: /^[a-zA-Z0-9]+$/,
                                        message: 'Hanya alphanumeric'
                                    }
                                }
                            },
                            nama_mk: {
                                validators: {
                                    notEmpty: {
                                        message: 'Nama tidak boleh kosong'
                                    },
                                    regexp: {
                                        regexp: /^[a-zA-Z0-9\s]+$/,
                                        message: 'Hanya alphanumeric dan spasi'
                                    }
                                }
                            },
                            nama_mk_ingg: {
                                validators: {
                                    regexp: {
                                        regexp: /^[a-zA-Z0-9\s]+$/,
                                        message: 'Hanya alphanumeric dan spasi'
                                    }
                                }
                            },
                            sks: {
                                validators: {
                                    notEmpty: {
                                        message: 'SKS tidak boleh kosong'
                                    }
                                }
                            },
                            kode_sifat: {
                                validators: {
                                    notEmpty: {
                                        message: 'Pilih salah satu sifat'
                                    }
                                }
                            },
                            semester: {
                                validators: {
                                    notEmpty: {
                                        message: 'Pilih salah satu semester'
                                    }
                                }
                            },
                            jenis_semester: {
                                validators: {
                                    notEmpty: {
                                        message: 'Pilih salah satu jenis semester'
                                    }
                                }
                            },
                            id_konsentrasi: {
                                validators: {
                                    notEmpty: {
                                        message: 'Pilih salah satu konsentrasi'
                                    }
                                }
                            }
                        }
                    });
                });
            </script>

        <?php else :

            $daftar_matakuliah = Matakuliah::get_instance()->_gets( array(
                'kode_PS'           => $list_params[ 'kode_PS' ],
                'id_konsentrasi'    => $list_params[ 'id_konsentrasi' ],
                'id_bagian'         => $list_params[ 'id_bagian' ],
                'nama_mk'           => $list_params[ 'nama_mk' ],
                'number'            => $list_params[ 'number' ],
                'offset'            => ( $list_params[ 'page' ] - 1 ) * $list_params[ 'number' ]
            ) );
            $daftar_matakuliah_count = Matakuliah::get_instance()->_count();

            ?>
            <h1 class="page-header">
                Mata Kuliah
                <small>Daftar</small>
            </h1>

            <?php if( $status ) Helpers::get_instance()->set_message_object( 'Mata Kuliah' )->display_message( $status ); ?>

            <div class="row">
                <form>
                    <div class="form-group">
                        <div class="col-sm-2">
                            <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . DS . Helpers::aksi_tambah; ?>" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Baru</a>
                        </div>
                        <div class="col-sm-2">
                            <select id="kode_PS" name="kode_PS" class="form-control">
                                <option value>--pilih--</option>
                                <?php foreach( $daftar_prodi_by_fakultas as $fakultas => $prodis ) : ?>
                                    <optgroup label="<?php echo $fakultas; ?>">
                                        <?php /** @var $prodi ModelProgramStudi **/
                                        foreach( $prodis as $prodi ) : ?>
                                            <option value="<?php echo $prodi->getKode(); ?>" <?php if( $prodi->getKode() == $list_params[ 'kode_PS' ] ) : ?>selected<?php endif; ?> ><?php echo $prodi->getNamaPS(); ?></option>
                                        <?php endforeach; ?>
                                    </optgroup>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <select id="id_konsentrasi" name="id_konsentrasi" class="form-control">
                                <option value="-1">--pilih--</option>
                                <?php /** @var $konsentrasi ModelKonsentrasi **/
                                foreach( $daftar_konsentrasi as $konsentrasi ) : ?>
                                    <option value="<?php echo $konsentrasi->getId(); ?>" <?php if( $konsentrasi->getId() == $list_params[ 'id_konsentrasi' ] ) : ?>selected<?php endif; ?> ><?php echo $konsentrasi->getNama(); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <select id="id_bagian" name="id_bagian" class="form-control">
                                <option value="-1">--pilih--</option>
                                <?php /** @var $bagian ModelBagian **/
                                foreach( $daftar_bagian as $bagian ) : ?>
                                    <option value="<?php echo $bagian->getId(); ?>" <?php if( $bagian->getId() == $list_params[ 'id_bagian' ] ) : ?>selected<?php endif; ?> ><?php echo $bagian->getNama(); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="glyphicon glyphicon-search"></i></div>
                                <input name="nama_mk" class="form-control" type="text" value="<?php echo $list_params[ 'nama_mk' ]; ?>" placeholder="Cari...">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-filter"></i> Filter</button>
                        </div>
                    </div>
                </form>
            </div>
            <br/>
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Kode</th>
                    <th>Nama</th>
                    <th>Konsentrasi</th>
                    <th>Dosen</th>
                    <th>SKS</th>
                    <th>Sifat</th>
                    <th>Semester</th>
                    <th>#</th>
                </tr>
                </thead>
                <tbody>
                <?php /** @var $matakuliah ModelMatakuliah */
                foreach( $daftar_matakuliah as $matakuliah ) : ?>
                    <tr>
                        <td><?php echo $matakuliah->getKodeMk(); ?></td>
                        <td><?php echo $matakuliah->getNamaMk(); ?></td>
                        <td><?php echo $matakuliah->getNamaKonsentrasi(); ?></td>
                        <td><?php echo $matakuliah->getNamaDosen(); ?></td>
                        <td><?php echo $matakuliah->getSks(); ?></td>
                        <td><?php echo $matakuliah->getNamaSifatMataKuliah(); ?></td>
                        <td><?php echo $matakuliah->getSemester(); ?></td>
                        <td>
                            <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS .$tingkat3 . DS . Helpers::aksi_hapus . DS . $matakuliah->getKodeMk(); ?>" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                            <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS .$tingkat3 . DS . Helpers::aksi_perbaiki . DS . $matakuliah->getKodeMk(); ?>" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                            <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS .$tingkat3 . DS . 'prasyarat' . DS . $matakuliah->getKodeMk(); ?>" title="Prasyarat"><i class="glyphicon glyphicon-align-left"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php $base_link = SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $tingkat3 . '?';
            $base_link .= 'kode_PS=' . $list_params[ 'kode_PS' ];
            $base_link .= '&id_konsentrasi=' . $list_params[ 'id_konsentrasi' ];
            $base_link .= '&id_bagian=' . $list_params[ 'id_bagian' ];
            $base_link .= '&nama_mk=' . $list_params[ 'nama_mk' ];
            siakad_paging_nav( $base_link, $daftar_matakuliah_count, $list_params[ 'page' ], 20 );
        endif; ?>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();