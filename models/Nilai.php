<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Model;


class Nilai {

    private $id;
    private $id_mk;
    private $tahun_akademik;
    private $nim;
    private $semester;
    private $keterangan;
    private $indeks_nilai;
    
    public $column = array('id','id_mk','tahun_akademik','nim','semester','keterangan','indeks_nilai');
    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id_mk
     */
    public function setIdMk($id_mk)
    {
        $this->id_mk = $id_mk;
    }

    /**
     * @return mixed
     */
    public function getIdMk()
    {
        return $this->id_mk;
    }

    /**
     * @param mixed $input
     */
    public function setTahunAkademik($input)
    {
        $this->tahun_akademik = $input;
    }

    /**
     * @return mixed
     */
    public function getTahunAkademik()
    {
        return $this->tahun_akademik;
    }
    
    /**
     * @param mixed $input
     */
    public function setNim($input)
    {
        $this->nim = $input;
    }

    /**
     * @return mixed
     */
    public function getNim()
    {
        return $this->nim;
    }
    
    /**
     * @param mixed $input
     */
    public function setSemester($input)
    {
        $this->semester = $input;
    }

    /**
     * @return mixed
     */
    public function getSemester()
    {
        return $this->semester;
    }
    
    /**
     * @param mixed $input
     */
    public function setKeterangan($input)
    {
        $this->keterangan = $input;
    }

    /**
     * @return mixed
     */
    public function getKeterangan()
    {
        return $this->keterangan;
    }

    
    /**
     * @param mixed $input
     */
    public function setIndeksNilai($input)
    {
        $this->indeks_nilai = $input;
    }

    /**
     * @return mixed
     */
    public function getIndeksNilai()
    {
        return $this->indeks_nilai;
    }
} 